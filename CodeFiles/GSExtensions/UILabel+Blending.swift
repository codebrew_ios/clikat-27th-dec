//
//  UILabelExtension.swift
//  Clikat Supplier
//
//  Created by Night Reaper on 08/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation


extension UILabel{
    
    func blendLabelWithImage(blendingImage : UIImage?, color : UIColor) -> UIImage?{
        guard let image = blendingImage else{return nil}
        let backgroundSize : CGSize = image.size
        UIGraphicsBeginImageContextWithOptions(backgroundSize, false, UIScreen.mainScreen().scale)
        let context : CGContext = UIGraphicsGetCurrentContext()!
        
        // translate/flip the graphics context (for transforming from CG* coords to UI* coords
        CGContextTranslateCTM(context, 0 , (backgroundSize.height - self.frame.origin.y)/2)
        CGContextScaleCTM(context, 1, -1)
        
        let rect : CGRect = CGRectMake(0, 0, image.size.width, image.size.height)
        
        CGContextClipToMask(context, rect, image.CGImage!)
        
        var r = CGFloat()
        var g = CGFloat()
        var b = CGFloat()
        var a = CGFloat()
        
        color.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        var rgbColorspace : CGColorSpaceRef
        let locations : [CGFloat] = [0.0, 1.0]
        let components  = [UIColor(red: r, green: g, blue: b, alpha: 1.0).CGColor, UIColor(red: r, green: g, blue: b, alpha: 1.0).CGColor]
        rgbColorspace = CGColorSpaceCreateDeviceRGB()
        
        let glossGradient : CGGradientRef = CGGradientCreateWithColors(rgbColorspace, components, locations)!
        let topCenter : CGPoint = CGPointMake(0,0)
        let bottomCenter : CGPoint = CGPointMake(0, self.frame.size.height)
        
        CGContextDrawLinearGradient(context, glossGradient, topCenter, bottomCenter, .DrawsBeforeStartLocation)
        CGContextSetRGBFillColor(context, r, g, b, a)
        CGContextFillRect(context, rect)
        
        guard let coloredImg: UIImage = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        coloredImg.drawInRect(rect, blendMode: CGBlendMode.Multiply, alpha: 0.5)
        
        UIGraphicsEndImageContext()
        
        return coloredImg
        
    }
    
    
    
}
