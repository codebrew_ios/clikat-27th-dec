//
//  UIButton+Kerning.swift
//  Clikat
//
//  Created by Night Reaper on 20/05/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation


extension UIButton {
    
    func kern(kerningValue:CGFloat) {
        let attributedText =  NSAttributedString(string: self.titleLabel!.text!, attributes: [NSKernAttributeName:kerningValue, NSFontAttributeName:self.titleLabel!.font, NSForegroundColorAttributeName:self.titleLabel!.textColor])
        self.setAttributedTitle(attributedText, forState: UIControlState.Normal)
    }
}


extension UILabel{
    func kern(kerningValue:CGFloat){
        let attributedString = NSMutableAttributedString(string: self.text!)
        attributedString.addAttribute(NSKernAttributeName, value: CGFloat(kerningValue), range: NSRange(location: 0, length: self.text!.characters.count))
        self.attributedText = attributedString
    }
}
