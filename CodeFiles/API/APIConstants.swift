//
//  APIConstants.swift
//  Clikat
//
//  Created by cbl73 on 4/22/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation
import Alamofire


struct APIConstants {
    
    static let BasePath = "https://appbean.clikat.com/"
   //static let BasePath = "http://54.200.224.97:3000/"
//   static let BasePath = "http://192.168.100.114:3000/"
    static let DataKey = "data"
    static let listKey = "list"
    static let Status = "status"
    static let message = "message"
    static let id = "id"
}



enum APIValidation : String {
    
    /**
     status : 4 -- success
     status : 1 -- parameter missing error
     status : 2 -- error of invalid access token
     status : 8 -- something went wrong error
     status : 5 -- user is not active
     */
    
    case None
    case Success = "200"
    case ParameterMissing = "400"
    case InvalidAccessToken = "401"
    case ApiError = "500"
    case Validation = "8"
    case Delivery = "501"
    
    
    var message : String?{
        
        switch self {
        case .ParameterMissing:
            return ""
        case .InvalidAccessToken :
            return ""
        case .ApiError :
            return ""
        case .Validation:
            return "Email Id or Password not correct."
        default:
            return nil
        }
    }
}

enum APIResponse {
    
    case Success(AnyObject?)
    case Failure(APIValidation)
}

typealias OptionalDictionary = [String : AnyObject]?


enum API {
    case Home(OptionalDictionary)
    case SupplierListing(OptionalDictionary)
    case SupplierInfo(OptionalDictionary)
    case MarkSupplierFav(OptionalDictionary)
    case UnFavoriteSupplier(OptionalDictionary)
    case SubCategoryListing(OptionalDictionary)
    case ProductListing(OptionalDictionary)
    case ProductDetail(OptionalDictionary)
    case MultiSearch(OptionalDictionary)
    case Register(OptionalDictionary)
    case Login(OptionalDictionary)
    case SendOTP(OptionalDictionary)
    case CheckOTP(OptionalDictionary)
    case RegisterLastStep(OptionalDictionary)
    case LocationList(OptionalDictionary,type : SelectedLocation)
    case ResendOTP(OptionalDictionary)
    case Addresses(OptionalDictionary)
    case AddNewAddress(OptionalDictionary)
    case EditAddress(OptionalDictionary)
    case AddToCart(OptionalDictionary)
    case UpdateCartInfo(OptionalDictionary)
    case PackageSupplierListing(OptionalDictionary)
    case PackageProductListing(OptionalDictionary)
    case LoyalityPointScreen(OptionalDictionary)
    case MyFavorites(OptionalDictionary)
    case OrderHistory(OptionalDictionary)
    case OrderUpcoming(OptionalDictionary)
    case OrderTrackingList(OptionalDictionary)
    case SupplierRating(OptionalDictionary)
    case BarCodeSearch(OptionalDictionary)
    case GenerateOrder(OptionalDictionary)
    case ScheduleOrder(OptionalDictionary)
    case RateOrderListing(OptionalDictionary)
    case RateMyOrder(OptionalDictionary)
    case PromotionProducts(OptionalDictionary)
    case DeleteAddress(OptionalDictionary)
    case OrderTrack(OptionalDictionary)

    //Notifications
    case AllNotifications(OptionalDictionary)
    case ClearAllNotifications(OptionalDictionary)
    case NotificationSwitch(OptionalDictionary)
    //Laundry 
    case LaundryServices (OptionalDictionary)
    case LaundryProductListing(OptionalDictionary)
    case LaundrySupplierList(OptionalDictionary)
    
    case LoyaltyPointsOrder(OptionalDictionary)
    case CancelOrder(OptionalDictionary)
    
    case LoginFacebook(OptionalDictionary)
    
    case ChangeProfile(OptionalDictionary)
    case ChangePassword(OptionalDictionary)
    case ForgotPassword(OptionalDictionary)
    case ViewAllOffers(OptionalDictionary)
    case SupplierImage(OptionalDictionary)
    case OrderDetails(OptionalDictionary)
    case CompareProducts(OptionalDictionary)
    case CompareProductResult(OptionalDictionary)
    
    case ScheduleNewOrder(OptionalDictionary)
    case ScheduledOrders(OptionalDictionary)
    
    case NotificationLanguage(OptionalDictionary)
    case TotalPendingSchedule(OptionalDictionary)
    case OrderDetail(OptionalDictionary)
    case ClearOneNotification(OptionalDictionary)
    case ConfirmScheduleOrder(OptionalDictionary)
}

protocol Router {
    var route : String { get }
    var parameters : OptionalDictionary { get }
}

extension API : Router {
    var route : String  {
        switch self {
        case .Home(_) : return "get_all_category"
        case .Register(_) : return "customer_register_step_first"
        case .Login(_) : return "login"
        case .SendOTP(_) : return "customer_register_step_second"
        case .CheckOTP(_) : return "check_otp"
        case .SupplierListing(_) : return "get_supplier_list"
        case .SupplierInfo(_) : return "supplier_details"
        case .MarkSupplierFav(_) : return "add_to_favourite"
        case .UnFavoriteSupplier(_): return "un_favourite"
        case .SubCategoryListing(_) : return "subcategory_listing"
        case .ProductListing(_) : return "get_products"
        case .ProductDetail(_) : return "get_product_details"
        case .MultiSearch(_): return "multi_search"
        case .LocationList(_, type:let type):
            switch type {
            case .Country:
                return "get_all_country"
            case .City:
                return "get_all_city"
            case .Zone:
                return "get_all_zone"
            case .Area:
                return "get_all_area"
            default:
                return ""
            }
        case .RegisterLastStep(_):
            return "customer_register_step_third"
        case .ResendOTP(_):
            return "resend_otp"
        case .Addresses(_):
            return "get_all_customer_address"
        case .AddNewAddress(_):
            return "add_new_address"
        case .EditAddress(_):
            return "edit_address"
        case .AddToCart(_):
            return "add_to_cart"
        case .PackageSupplierListing(_):
            return "package_category"
        case .PackageProductListing(_):
            return "package_product"
        case .UpdateCartInfo(_):
            return "update_cart_info"
        case .LoyalityPointScreen(_):
            return "get_loyality_product"
        case .MyFavorites(_):
            return "get_my_favourite"
        case .OrderHistory(_):
            return "history_order"
        case .OrderUpcoming(_):
            return "upcoming_order"
        case .OrderTrackingList(_):
            return "track_order_list"
        case .SupplierRating(_):
            return "supplier_rating"
        case .BarCodeSearch(_):
            return "bar_code"
        case .GenerateOrder(_):
            return "genrate_order"
        case .ScheduleOrder(_):
            return "schedule_order"
        case .RateOrderListing(_):
            return "rate_my_order_list"
        case .RateMyOrder(_):
            return "user_rate_order"
        case .PromotionProducts(_):
            return "get_promoation_product"
        case .DeleteAddress(_):
            return "delete_customer_address"
        case .OrderTrack(_):
            return "order_track"
            //Notifications
        case .AllNotifications(_):
            return "get_all_notification"
        case .ClearAllNotifications(_):
            return "clear_all_notification"
            // Laundry
        case .LaundryServices(_):
            return "get_laundry_data"
        case .LoyaltyPointsOrder:
            return "loyality_order"
        case .NotificationSwitch(_):
            return "on_off_notification"
        case .CancelOrder(_):
            return "cancel_order"
        case .LoginFacebook(_):
            return "facebook_login"
        case .LaundryProductListing(_):
            return "get_laundry_product"
        case .ChangeProfile(_):
            return "change_profile"
        case .ChangePassword(_):
            return "change_password"
        case .ForgotPassword(_):
            return "forget_password"
        case .ViewAllOffers(_):
            return "view_all_offer"
        case .SupplierImage(_):
            return "supplier_image"
        case .OrderDetails(_):
            return "customer_order_description"
        case .CompareProducts(_):
            return "product_acco_to_area"
        case .CompareProductResult(_):
            return "compare_product"
        case .LaundrySupplierList(_):
            return "laundary_supplier_list"
        case .ScheduleNewOrder(_):
            return "schedule_order_new"
        case .ScheduledOrders(_):
            return "schedule_orders"
        case .NotificationLanguage(_):
            return "notification_language"
        case .TotalPendingSchedule(_):
            return "get_total_pending_schedule"
        case .OrderDetail(_):
            return "user_order_details"
        case .ClearOneNotification(_):
            return "clear_notification"
        case .ConfirmScheduleOrder(_):
            return "confirm_order"
        }
    }
    
    var parameters: OptionalDictionary{
        
        switch self {
        case .Home(let params) :
            return params
        case .Register(let params):
            return params
        case .Login(let params):
            return params
        case .SendOTP(let params):
            return params
        case .CheckOTP(let params):
            return params
        case .SupplierListing(let params):
            return params
        case .SupplierInfo(let params):
            return params
        case .MarkSupplierFav(let params):
            return params
        case .UnFavoriteSupplier(let params):
            return params
        case .SubCategoryListing(let params):
            return params
        case .ProductListing(let params) :
            return params
        case .ProductDetail(let params):
            return params
        case .MultiSearch(let params):
            return params
        case .LocationList(let params, type: _):
            return params
        case .RegisterLastStep(let params):
            return params
        case .ResendOTP(let params):
            return params
        case .Addresses(let params):
            return params
        case .AddNewAddress(let params):
            return params
        case .EditAddress(let params):
            return params
        case .AddToCart(let params):
            return params
        case .PackageSupplierListing(let params):
            return params
        case .PackageProductListing(let params):
            return params
        case .UpdateCartInfo(let params):
            return params
        case .LoyalityPointScreen(let params):
            return params
        case .MyFavorites(let params):
            return params
        case .OrderHistory(let params):
            return params
        case .OrderUpcoming(let params):
            return params
        case .OrderTrackingList(let params):
            return params
        case .SupplierRating(let params):
            return params
        case .BarCodeSearch(let params):
            return params
        case .GenerateOrder(let params):
            return params
        case .ScheduleOrder(let params):
            return params
        case .RateOrderListing(let params):
            return params
        case .RateMyOrder(let params):
            return params
        case .PromotionProducts(let params):
            return params
        case .DeleteAddress(let params):
            return params
        case .OrderTrack(let params):
            return params
            //Notifications
        case .AllNotifications(let params):
            return params
        case .ClearAllNotifications(let params):
            return params
            // Laundry
        case .LaundryServices(let params):
            return params
        case .LaundryProductListing(let params):
            return params
        case .LoyaltyPointsOrder(let params):
            return params
        case .NotificationSwitch(let params):
            return params
        case .CancelOrder(let params):
            return params
        case .LoginFacebook(let params):
            return params
        case .ChangeProfile(let params):
            return params
        case .ChangePassword(let params):
            return params
        case .ForgotPassword(let params):
            return params
        case .ViewAllOffers(let params):
            return params
        case .SupplierImage(let params):
            return params
        case .OrderDetails(let params):
            return params
        case .CompareProducts(let params):
            return params
        case .CompareProductResult(let params):
            return params
        case .LaundrySupplierList(let params):
            return params
        case .ScheduleNewOrder(let params):
            return params
        case .ScheduledOrders(let params):
            return params
        case .NotificationLanguage(let params):
            return params
        case .TotalPendingSchedule(let params):
            return params
        case .OrderDetail(let params):
            return params
        case .ClearOneNotification(let params):
            return params
        case .ConfirmScheduleOrder(let params):
            return params
        }
    }
    
    
    var method : Alamofire.Method {
        switch self {
        default:
            return .POST
        }
    }
}



enum FormatAPIParameters  {
    
    case Home()
    case Register(email : String?,password : String?)
    case SupplierListing(categoryId: String?,subCategoryId : String?)
    case SupplierInfo(supplierId : String? ,branchId : String? , accessToken : String?,categoryId : String?)
    case MarkSupplierFavorite (supplierId : String?)
    case UnFavoriteSupplier(supplierId : String?)
    case SubCategoryListing(supplierId : String? , categoryId : String?)
    case DetailedSubCatProducts(supplierBranchId : String? , subCategoryId : String?)
    case ProductDetail(productId : String?,supplierBranchId : String?,offer : String?)
    case MultiSearch(supplierBranchId : String? , categoryId : String? , searchList : String?)
    case Country()
    case LocationList(type : SelectedLocation, id : String?)
    //Register & Login
    case SendOTP(accessToken : String?,mobileNumber : String?,countryCode : String?)
    case CheckOTP(accessToken : String?,OTP : String?)
    case RegisterLastStep(accessToken : String?,name : String?)
    case Login(email : String?,password : String?)
    case ResendOTP(token : String?)
    
    //Address
    case Addresses(supplierBranchId : String?,areaId : String?)
    case AddNewAddress(address : Address?)
    case EditAddress(address : Address?,addressId : String?)
    
    case AddToCart(cart : [Cart],supplierBranchId : String?,promotionType : String?,remarks : String?)
    
    
    case UpdateCartInfo(cartId : String?,deliveryAddressId : String?,deliveryType : String?,delivery : Delivery?,deliveryDate : NSDate?,netAmount : String?,remarks : String?)
    case PackageSupplierListing ()
    case PackageProductListing(supplierBranchId : String? , categoryId : String?)
    case LoyalityPointScreen()
    case MyFavorites()
    case OrderHistory()
    case OrderUpcoming()
    case OrderTrackingList()
    case SupplierRating(supplierId : String?,rating : String?,comment : String?)
    case BarCodeSearch(barCode : String? , supplierBranchId : String?)
    case GenerateOrder(cartId : String?,isPackage : String?,paymentType : String?)
    case ScheduleOrder(orderId : String?,status : CalendarMode,deliveryTime : String?,selectedArr : String?)
    case RateMyOrder(orderId : String?,rating : String?,comment : String?)
    case PromotionProducts()
    case DeleteAddress(addressId : String?)
    case OrderTrack(orderId : String?)

    //Notifications
    case AllNotifications()
    case ClearAllNotifications()
    case NotificationSwitch(status : String?)
    case OrderDetails(orderId : String?)
    
    //Laundry
    case LaundryServices(categoryId : String?)
    case LaundryProductListing(categoryId : String?,supplierBranchId : String?)
    case CancelOrder(orderId : String?,isScheduled : String?)
    case LaundrySupplierList(categoryId : String?,pickupDate : NSDate?)
    
    case LoyaltyPointsOrder(supplierBranchId : String?,deliveryAddressId : String?,deliveryType : String?,deliveryDate : NSDate?,totalPoints : String?,urgentPrice : String?,remarks : String?,cart : [Cart])
    
    case FacebookLogin(fbProfile : Facebook)
    case ChangeProfile()
    case ChangePassword(oldPassword : String?,newPassword : String?)
    case ForgotPassword(email : String?)
    
    case ViewAllOffers()
    
    case SupplierImage(supplierBranchId : String?)
    case CompareProducts(productName : String?,startValue : String?)
    case CompareProductResult(sku : String?)
    
    case ScheduleNewOrder(orderId : String?,deliveryDate : [NSDate]?,pickUpBuffer : String?,deliveryTime : String?)
    
    case NotificationLanguage(languageId : String?)
    case TotalPendingSchedule()
    case OrderDetail(orderId : String?)
    case ConfirmScheduleOrder(orderId : String?,paymentType : String?)
    
    case ClearOneNotification(notificationId : String?)
    
    func formatParameters () -> [String : AnyObject]?{
        
        let languageId = GDataSingleton.sharedInstance.languageId ?? ""
        let accessToken = GDataSingleton.sharedInstance.loggedInUser?.token ?? ""
        let countryId = LocationSingleton.sharedInstance.location?.countryEN?.id ?? ""
//        let cityId = LocationSingleton.sharedInstance.location?.city?.id ?? ""
//        let zoneId = LocationSingleton.sharedInstance.location?.currentZone?.id ?? ""
        let areaId = LocationSingleton.sharedInstance.location?.areaEN?.id ?? ""
        let deviceToken = GDataSingleton.sharedInstance.deviceToken ?? "asdf"
        
        if case .Home() = self{
            return ["countryId" : countryId , "accessToken" : accessToken , "areaId" : areaId ]
        }
        
        if case .SupplierListing(let categoryId,let subCategoryId) = self {
            var params = ["areaId" : areaId , "categoryId" : categoryId ?? "" , "languageId" : languageId]
            if subCategoryId != nil {
                params["subCat"] = subCategoryId
                return params
            }else {
                return params
            }
        }
        
        if case .SupplierInfo(let supplierId , let branchId ,let accessToken,let categoryId) = self {
            var params = [ "supplierId" : supplierId ?? "" , "branchId" : branchId ?? "" , "accessToken" : accessToken ?? "" , "languageId" : languageId,"area_id" : areaId ?? ""]
            
                params["categoryId"] = categoryId ?? GDataSingleton.sharedInstance.currentCategoryId
            
            return params
        }
        
        
        if case .MarkSupplierFavorite(let supplierId) = self{
            return ["accessToken" : accessToken , "supplierId" : supplierId ?? ""]
        }
        
        if case .UnFavoriteSupplier(let supplierId) = self {
            return ["accessToken" : accessToken , "supplierId" : supplierId ?? ""]
        }
        
        if case .DetailedSubCatProducts(let supplierBranchId, let subCategoryId) = self {
            return ["supplierBranchId" : supplierBranchId ?? "" , "subCategoryId" : subCategoryId ?? "" , "languageId" : GDataSingleton.sharedInstance.languageId, "countryId" : countryId , "areaId" : areaId]
        }
        
        if case .ProductDetail(let productId,let supplierBranchId,let offer) = self {
            return ["productId" : productId ?? "" , "languageId" : languageId,"supplierBranchId" : supplierBranchId ?? "","areaId" : areaId,"offer" : offer ?? ""]
        }
        
        if case .SubCategoryListing(let supplierId , let categoryId) = self {
            return ["supplierId" : supplierId ?? "" , "categoryId" : categoryId ?? "" , "languageId" : languageId]
        }
        
        if case .MultiSearch(let supplierBranchId , let categoryId , let searchList) = self {
            return ["supplierBranchId" : supplierBranchId ?? "" , "categoryId" : categoryId ?? "" , "languageId" : languageId , "searchList" : searchList ?? ""]
        }
        
        if case .Country() = self {
            return ["languageId" : languageId]
        }
        
        if case .LoyalityPointScreen() = self{
            return ["languageId" : languageId ,"accessToken" : accessToken , "areaId" : areaId ,"deviceType" : DeviceType.iOS]
        }
        
        if case .MyFavorites() = self{
            return ["languageId" : languageId ,"accessToken" : accessToken , "areaId" : areaId ]
        }
        
        if case .LocationList(let type ,let id) = self {
            var params : Dictionary<String,String> = ["":""]
            switch type {
            case .City:
                params["countryId"] = id ?? ""
            case .Zone:
                params["cityId"] = id ?? ""
            case .Area:
                params["zoneId"] = id ?? ""
                if let token = GDataSingleton.sharedInstance.loggedInUser?.token {
                    params["accessToken"] = token
                }
            default:
                break
            }
            return params
        }
        
        if case .Register(let email, let password) = self {
            return ["email":email ?? "","password":password ?? "","deviceToken" : deviceToken,"deviceType" : DeviceType.iOS ,"areaId" : areaId,"latitude" : LocationManager.sharedInstance.currentLocation?.currentLat ?? "","longitude" : LocationManager.sharedInstance.currentLocation?.currentLng ?? "","languageId" : languageId]
        }
        if case .SendOTP(let token, let mobileNumber,let countryCode) = self{
            return ["mobileNumber" : mobileNumber ?? "","accessToken" : token ?? "","countryCode" : countryCode ?? ""]
        }
        if case .CheckOTP(let token, let otp) = self {
            return ["accessToken" : token ?? "","otp" :otp ?? "","languageId" : languageId]
        }
        if case .RegisterLastStep(let token, let name) = self {
            return ["accessToken" : token ?? "","name" : name ?? ""]
        }
        if case .Login(let email, let password) = self {
            
            return ["email":email ?? "","password":password ?? "","deviceToken" : deviceToken ,"deviceType" : DeviceType.iOS ,"languageId" : languageId]
        }
        if case .ResendOTP(let token) = self {
            return ["accessToken" : token ?? ""]
        }
        if case .Addresses(let supplierBranchId,let areaid) = self {
            var params = Dictionary<String,String>()
            if let _ = supplierBranchId {
                params = ["supplierBranchId" : supplierBranchId ?? "", "accessToken" : accessToken,"languageId" : languageId]
            }else {
                params = ["accessToken" : accessToken,"languageId" : languageId]
            }
            params["areaId"] = areaid ?? "0"
            
            return params
        }
        
        if case .AddNewAddress(let address) = self {
            
            let pincode = UtilityFunctions.appendOptionalStrings(withArray: [address?.houseNo,address?.buildingName], separatorString: ",@#")
            let lineSecond = UtilityFunctions.appendOptionalStrings(withArray: [address?.city,address?.country], separatorString: ",@#")
            let params = ["accessToken" : accessToken,"areaId" : areaId,"landmark" : address?.landMark ?? "","addressLineFirst" : address?.area ?? "","addressLineSecond" : lineSecond ?? "","pincode" : pincode,"name" : address?.name ?? "","address_link" : address?.placeLink ?? "","customer_address" : address?.address ?? ""]
            return params
        }
        
        if case .EditAddress(let address,let addressId) = self {
            let pincode = UtilityFunctions.appendOptionalStrings(withArray: [address?.houseNo,address?.buildingName], separatorString: ",@#")
            let lineSecond = UtilityFunctions.appendOptionalStrings(withArray: [address?.city,address?.country], separatorString: ",@#")
            let params = ["accessToken" : accessToken,"areaId" : areaId,"landmark" : address?.landMark ?? "","addressLineFirst" : address?.area ?? "","addressLineSecond" : lineSecond ?? "","pincode" : pincode,"addressId" : addressId ?? "","name" : address?.name ?? "","address_link" : address?.placeLink ?? "","customer_address" : address?.address ?? ""]
            
            return params
        }
        
        if case .AddToCart(let cart,let supplierId,let promotionType,let remarks) = self {
            
            var tempArr : [Dictionary<String,String>] = []
           let maxAdm = Cart.getMaxAdmin(cart)
            let maxSupplier = Cart.getMaxSupplier(cart)
            for product in cart {
                
                let productDict = ["productId" : product.id ?? "","quantity" : product.quantity ?? "","handling_admin" : product.handlingAdmin == maxAdm ? product.handlingAdmin ?? "" : "0","handling_supplier" : product.handlingSupplier == maxSupplier ? product.handlingSupplier ?? "" : "0","price_type" : product.priceType.rawValue.toString ?? "0"]
                tempArr.append(productDict)
            }
            
            do {
                let data = try NSJSONSerialization.dataWithJSONObject(tempArr, options: .PrettyPrinted)
                var jsonString = String(data: data,encoding: NSUTF8StringEncoding)
                jsonString = jsonString?.stringByReplacingOccurrencesOfString("\n", withString: "")
                jsonString = "\"" + (jsonString ?? "") + "\""
                let params = ["accessToken" : accessToken,"supplierBranchId" : supplierId ?? "","productList" : /jsonString,"remarks" :  remarks ?? "0","deviceId" : "1","promoationType" : promotionType ?? "","area_id" : areaId]
                return params
            }catch{}
        }
        
        if case .PackageSupplierListing() = self {
            return ["areaId" :  areaId , "languageId" : languageId ]
        }
        if case .PackageProductListing(let supplierBranchId, let categoryId) = self {
            return ["supplierBranchId" : supplierBranchId ?? "" , "categoryId" : categoryId ?? "" , "languageId" : GDataSingleton.sharedInstance.languageId , "areaId" : areaId]
        }

        /*
         hanlding admin
         handling supplier
         delvery Charges
         min Order Delivery Crossed
         urgentPrice
         */
        
        if case .UpdateCartInfo(let cartId ,let deliveryAddressId ,let deliveryType,let delivery ,let deliveryDate ,let netAmount ,let remarks ) = self {
            //delivery Type 0 -> Standard 1-> Urgent 2 -> Postpone
            var params = ["accessToken": accessToken,"cartId" : cartId ?? "","deliveryId" : deliveryAddressId ?? "","deliveryType" : deliveryType ?? "","handlingAdmin" : delivery?.handlingAdmin ?? "","handlingSupplier" : delivery?.handlingSupplier ?? "","deliveryCharges" : delivery?.deliveryCharges ?? "","currencyId" : "1","minOrderDeliveryCrossed" : delivery?.minOrderDeliveryCrossed ?? "","netAmount" : netAmount ?? "","remarks" : remarks ?? "","languageId" : languageId]
            let deliveryDatestr = UtilityFunctions.getDateFormatted("yyyy-MM-dd", date: deliveryDate ?? NSDate())
            let deliveryTime = UtilityFunctions.getTimeFormatted("HH:mm:ss", date: deliveryDate ?? NSDate())
            params["deliveryDate"] = deliveryDatestr
            params["deliveryTime"] = deliveryTime
            
            let cal = NSCalendar.currentCalendar()
            let comp = cal.components(.Weekday,fromDate: deliveryDate ?? NSDate())
            if comp.weekday - 2 < 0 {
                params["day"] = "6"
            }else {
                params["day"] = (comp.weekday - 2).toString
            }
            if /delivery?.needPickup {
                params["pickupTime"] = UtilityFunctions.getTimeFormatted("HH:mm:ss", date: delivery?.pickupDate ?? NSDate())
                params["pickupId"] = /delivery?.pickupAddress?.id
                params["pickupDate"] = UtilityFunctions.getDateFormatted("yyyy-MM-dd", date: delivery?.pickupDate ?? NSDate())
            }
            if deliveryType == "1" {
                params["urgentPrice"] = /delivery?.urgentPrice
            }
            return params
        }
        
        if case .GenerateOrder(let cartId,let isPackage,let paymentType) = self {
            return ["accessToken" : accessToken,"languageId" : languageId,"cartId" : cartId ?? "","paymentType" : paymentType ?? "","isPackage" : isPackage ?? ""]
        }
        if case .ScheduleOrder(let orderId ,let status,let deliveryTime ,let selectedArr ) = self {
            var params = ["accessToken" : accessToken,"orderId" : orderId ?? "","delivery_time" : deliveryTime ?? ""]
            
            params["status"] = status == .Weekly ? "1" : "2"
            if status == .Weekly {
                params["weekly_arr"] = "[" + (selectedArr ?? "") + "]"
            }else {
                params["monthly_arr"] = "[" + (selectedArr ?? "") + "]"
            }
            return params
        }
        if case .RateMyOrder(let orderId ,let rating, let comment) = self {
            return ["accessToken" : accessToken,"orderId" : orderId ?? "","rating" : rating ?? "", "comment" : comment ?? "","languageId" : languageId]
        }
        
        if case .SupplierRating(let supplierId, let rating,let comment) = self{
            return ["accessToken" : accessToken,"supplierId" : supplierId ?? "","rating" : rating ?? "", "comment" : comment ?? ""]
        }
        
        if case .BarCodeSearch(let barCode , let supplierBranchId) = self {
            var params = ["barCode" : barCode ?? "" , "languageId" : GDataSingleton.sharedInstance.languageId,"areaId" : /areaId]
//            if let _ = supplierBranchId {
//                params["supplierBranchId"] = supplierBranchId ?? ""
//            }
            return params
        }
        if case .PromotionProducts() = self {
            return ["languageId" : languageId , "areaId" : areaId]
        }
        //Notifications
        if case .AllNotifications() = self{
            return ["accessToken" : accessToken]
        }
        
        if case .ClearAllNotifications() = self{
            return ["accessToken" : accessToken]
        }
        
        //ORDER RELATED
        if case .OrderHistory() = self  {
            return ["accessToken" : accessToken,"languageId" : languageId]
        }
      
        if case .OrderUpcoming() = self {
            return ["accessToken" : accessToken,"languageId" : languageId]
        }
        
        if case .OrderTrackingList() = self {
            return ["accessToken" : accessToken,"languageId" : languageId]
        }
        
        if case .DeleteAddress(let addressId) = self {
            return ["accessToken" : accessToken,"addressId" : addressId ?? ""]
        }
        
        //Laundry
        if case .LaundryServices(let categoryId) = self {
            return ["categoryId" : categoryId ?? "" , "languageId" : languageId]
        }
        
        //Edited
        if case .OrderTrack(let orderId) = self {
            return ["accessToken" : accessToken,"languageId" : languageId,"orderId" : orderId ?? ""]
        }
        
        if case .CancelOrder(let orderId , let isScheduled) = self {
            return ["accessToken" : accessToken,"languageId" : languageId,"orderId" : orderId ?? "" , "isScheduled" : isScheduled ?? ""]
        }
        
        if case .LoyaltyPointsOrder(let supplierBranchId ,let deliveryAddressId,let deliveryType,let deliveryDate,let totalPoints  ,let urgentPrice ,let remarks ,let cart ) = self {
            
            var params = ["accessToken": accessToken,"languageId" : languageId,"supplierBranchId" : supplierBranchId ?? "","deliveryAddressId" : deliveryAddressId ?? "","deliveryType" : deliveryType ?? "","remarks" : remarks ?? "0","totalPoints" : totalPoints ?? "","deviceType" : DeviceType.iOS]
            
            let deliveryDatestr = UtilityFunctions.getDateFormatted("yyyy-MM-dd", date: deliveryDate ?? NSDate())
            
            params["deliveryDate"] = deliveryDatestr ?? ""
            if deliveryType == "1" {
                params["urgentPrice"] = urgentPrice ?? ""
                params["urgent"] = "1"
            }
            var productList = [String]()
            for product in cart {
                
                productList.append(product.id ?? "")
            }
            do {
                let data = try NSJSONSerialization.dataWithJSONObject(productList, options: .PrettyPrinted)
                var jsonString = String(data: data,encoding: NSUTF8StringEncoding)
                jsonString = jsonString?.stringByReplacingOccurrencesOfString("\n", withString: "")
                params["productList"] = jsonString ?? ""
                return params
            }catch {}
            
        }
        
        if case .NotificationSwitch(let status) = self {
            return ["accessToken" : accessToken,"languageId" : languageId,"status" : status == "true" ? "1" : "0"]
        }
        
        if case .FacebookLogin(let profile) = self {
            return ["facebookToken":profile.fbId ?? "","email":profile.email ?? "","deviceToken" : deviceToken,"deviceType" : DeviceType.iOS ,"areaId" : areaId,"image" : profile.imageUrl ?? "","name" : profile.firstName ?? ""]
        }
        
        if case .LaundryProductListing(let categoryId, let supplierBranchId) = self{
            return ["categoryId" : categoryId ?? "","languageId" : languageId,"supplierBranchId" : supplierBranchId ?? "","areaId" : areaId]
        }
        
        if case .ChangeProfile() = self {
            return ["accessToken" : accessToken]
        }
        if case .ChangePassword(let oldPassword,let newPassword) = self {
            return ["accessToken" : accessToken,"languageId" : languageId,"oldPassword" : oldPassword ?? "","newPassword" : newPassword ?? ""]
        }
        if case .ForgotPassword(let email) = self {
            return ["emailId" : email ?? ""]
        }
        
        if case .ViewAllOffers() = self {
            return ["languageId" : languageId,"areaId" : areaId]
        }
        
        if case .SupplierImage(let branchId) = self {
            return ["branchId" : branchId ?? ""]
        }
        
        if case .OrderDetails(let orderId) = self {
            return ["accessToken" : accessToken,"orderId" : orderId ?? ""]
        }
        if case .CompareProducts(let productName,let startValue) = self {
            return ["languageId" : languageId,"areaId" : areaId,"productName" : productName.unwrap(),"startValue" : startValue.unwrap()]
        }
        if case .CompareProductResult(let sku) = self {
            return ["languageId" : languageId,"areaId" : areaId,"skuCode" : sku.unwrap()]
        }

        if case .LaundrySupplierList(let categoryId,let date) = self {
            var dayNumber = (UtilityFunctions.getDateFormatted("e",date : date ?? NSDate()))?.toInt()
            if dayNumber != 1 {
                dayNumber = (dayNumber ?? 2) - 2
            }else {
                dayNumber = 6
            }
            return ["categoryId" : categoryId ?? "", "date" : String(dayNumber!) ?? "","time" : UtilityFunctions.getTimeFormatted("HH:mm:ss",date : date ?? NSDate()) ?? "","areaId" : areaId,"languageId" : languageId]
        }
        
        if case .ScheduleNewOrder(let orderId ,let dates,let pickupBuffer,let deliveryTime) = self {
            var tempArr : Array<Dictionary<String,String>> = []
            for date in dates ?? [] {
                
                if let buffer = pickupBuffer?.toInt() {
                    let calendar = NSCalendar.currentCalendar()
                    let newDate = calendar.dateByAddingUnit(.Minute, value: -buffer, toDate: date, options: [])
                    let dict = ["deliveryDate" : newDate?.toString() ?? "","pickupDate" : date.toString() ?? "","pickupTime" : UtilityFunctions.getTimeFormatted("HH:mm:ss",date : date) ?? ""]
                    tempArr.append(dict)
                }else {
                    let dict = ["deliveryDate" : UtilityFunctions.getDateFormatted("EEE MM dd HH:mm:ss ZZZ yyyy", date: date ?? NSDate()) ?? "","pickupDate" : "","pickupTime" : "","delivery_time" : /deliveryTime]
                    tempArr.append(dict)
                }
            }
            
            do {
                let data = try NSJSONSerialization.dataWithJSONObject(tempArr, options: .PrettyPrinted)
                var jsonString = String(data: data,encoding: NSUTF8StringEncoding)
                jsonString = jsonString?.stringByReplacingOccurrencesOfString("\n", withString: "")
                jsonString =  (jsonString ?? "")
                let params = ["orderId" : orderId ?? "","orderDates" : jsonString ?? "","type" : DeviceType.iOS]
                return params
            }catch{ }
        }
        
        if case .NotificationLanguage(let language) = self {
            return ["accessToken" : accessToken, "languageId" : /language]
        }
        
        if case .TotalPendingSchedule() = self {
            return ["accessToken" : accessToken]
        }
        if case .OrderDetail(let orderId) = self {
            return ["accessToken" : accessToken,"languageId" : languageId,"orderId" : orderId ?? ""]
        }
        
        if case .ClearOneNotification(let notificationId) = self {
            return ["accessToken" : accessToken,"notificationId" : notificationId ?? ""]
        }
        
        if case .ConfirmScheduleOrder(let orderId,let payment) = self {
            
            return ["accessToken" : accessToken,"languageId" : languageId,"orderId" : orderId ?? "","paymentType" : payment ?? ""]
        }
        return nil
    }
}



