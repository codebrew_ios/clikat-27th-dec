//
//  APIManager.swift
//  Clikat
//
//  Created by cbl73 on 4/22/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import AFNetworking
import EZSwiftExtensions
import NVActivityIndicatorView

typealias APICompletion = (APIResponse) -> ()

class APIManager {
        
    static let sharedInstance = APIManager()
    private lazy var httpClient : HTTPClient = HTTPClient()

   
    let activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 0,y: 0,width: 40,height: 40), type: .Orbit, color: UIColor.whiteColor(), padding: 4)
    let overLayView = UIView(frame: UIScreen.mainScreen().bounds)
    
    func opertationWithRequest( withApi api : API , completion : APICompletion )  {
        
        if !Alamofire.NetworkReachabilityManager()!.isReachable {
            UtilityFunctions.sharedAppDelegateInstance().window?.makeToast(L10n.PleaseCheckYourInternetConnection.string)
            return
        }
        
        if isLoaderNeeded(api){
            showLoader()
        }
        print(api.route)
        print(api.parameters)
        httpClient.postRequest(withApi: api, success: { [unowned self] (data) in
            
             if self.isLoaderNeeded(api){
                self.hideLoader()
            }
            guard let response = data else {
                completion(APIResponse.Failure(.None))
                return
            }
            let json = JSON(response)
            print(json)
            let responseType  = APIValidation(rawValue: (json.dictionaryValue[APIConstants.Status]?.stringValue) ?? "") ?? .None
            
            if responseType == APIValidation.Success{
 
                var object : AnyObject?
                switch api{
                case .Home(_) :
                    let home = Home(sender: json.dictionaryValue)
                    object = home
                    GDataSingleton.sharedInstance.homeData = home
                case .Register(_) :
                    object = User(attributes: json[APIConstants.DataKey].dictionaryValue)
                    
                case .Login(_) :
                    let user = User(attributes: json[APIConstants.DataKey].dictionaryValue)
                    object = user
                    GDataSingleton.sharedInstance.loggedInUser = object as? User
                    
                case .SendOTP(_):
                    object = User(attributes: json[APIConstants.DataKey].dictionaryValue)
                    
                case .CheckOTP(_):
                    object = User(attributes: json[APIConstants.DataKey].dictionaryValue)
                case .SupplierListing(_):
                    object = SupplierListing(attributes: json.dictionaryValue,key: SupplierKeys.supplierList.rawValue)
                case .SupplierInfo(_):
                     object = Supplier(attributes: json[APIConstants.DataKey].dictionaryValue)
                    
                case .MarkSupplierFav(_):
                    print("Marked Successfully")
                
                case .SubCategoryListing(_):
                    object = SubCategoriesListing(attributes: json.dictionaryValue)
                case .ProductListing(_):
                    object = ProductListing(attributes: json.dictionaryValue)
                case .ProductDetail(_):
                    object = Product(attributes: json[APIConstants.DataKey].dictionaryValue)
                case .MultiSearch(_):
                    object = SearchResult(attributes: json.dictionaryValue)
                case .LocationList(_, type: _):
                    object = LocationResults(data: json[APIConstants.DataKey].dictionaryValue)
                case .ResendOTP(_):
                    break
                case .Addresses(_):
                    object = Delivery(attributes: json[APIConstants.DataKey].dictionaryValue)
                case .EditAddress(_):
                    object = Address(attributes: json[APIConstants.DataKey].dictionaryValue)
                case .PackageSupplierListing(_):
                    object = SupplierListing(attributes: json.dictionaryValue,key: APIConstants.listKey)
                case .PackageProductListing(_):
                    object = PackageProductListing(attributes: json[APIConstants.DataKey].dictionaryValue ,key:  APIConstants.listKey)
                case .AddToCart(_):
                    object = UtilityFunctions.appendOptionalStrings(withArray: [json[APIConstants.DataKey]["cartId"].stringValue,json[APIConstants.DataKey]["min_order"].stringValue], separatorString: "$")
                case .LoyalityPointScreen(_):
                    object = LoyalityPointsListing(attributes: json[APIConstants.DataKey].dictionaryValue)
                case .MyFavorites(_):
                    object = SupplierListing(attributes: json.dictionaryValue,key: SupplierKeys.favorites.rawValue)
                case .OrderHistory(_):
                    object = OrderListing(attributes: json[APIConstants.DataKey].dictionaryValue, key: OrderRelatedKeys.orderHistory.rawValue)
                case .OrderUpcoming(_),.ScheduledOrders(_):
                    object = OrderListing(attributes: json[APIConstants.DataKey].dictionaryValue, key: OrderRelatedKeys.orderHistory.rawValue)
                case .OrderTrackingList(_),.RateOrderListing(_):
                    object = OrderListing(attributes: json[APIConstants.DataKey].dictionaryValue, key: OrderRelatedKeys.orderList.rawValue)
                case .AddNewAddress(_):
                    object = Address(attributes: json[APIConstants.DataKey].dictionaryValue)
                case .GenerateOrder(_):
                    object = json[APIConstants.DataKey][APIConstants.id].stringValue
                case .BarCodeSearch(_):
                    object = BarCodeProductListing(attributes: json[APIConstants.DataKey].dictionaryValue, key: APIConstants.listKey)
                case .PromotionProducts(_):
                    object = PromotionListing(attributes: json[APIConstants.DataKey].dictionaryValue)
                case .LaundryServices(_):
                    object = LaundryServices(attributes: json[APIConstants.DataKey].dictionaryValue)
                case .AllNotifications(_):
                    object = NotificationListing(attributes : json[APIConstants.DataKey].dictionaryValue)
                case .OrderTrack(_):
                    object = json[APIConstants.DataKey]["msg"].stringValue
                case .LoginFacebook(_):
                    let user = User(attributes: json[APIConstants.DataKey].dictionaryValue,params: api.parameters)
                    object = user
                case .LaundryProductListing(_):
                    object = LaundryProductListing(attributes: json.dictionaryValue)
                case .ViewAllOffers(_):
                    object = OfferListing(attributes: json[APIConstants.DataKey].dictionaryValue)
                case .SupplierImage(_):
                    object = json[APIConstants.DataKey]["image"].stringValue + " " + json[APIConstants.DataKey][SupplierKeys.supplier_id.rawValue].stringValue
                case .OrderDetails(_):
                    object = OrderDetails(attributes: json[APIConstants.DataKey].dictionaryValue)
                case .CompareProducts(_):
                    object = CompareProductListing(attributes: json[APIConstants.DataKey].dictionaryValue)
                case .CompareProductResult(_):
                    object = CompareProductResult(attributes: json[APIConstants.DataKey].dictionaryValue)
                case .LaundrySupplierList(_):
                    object = SupplierListing(attributes: json.dictionaryValue,key: SupplierKeys.supplierList.rawValue)
                case .ScheduleNewOrder(_):
                    object = json.stringValue
                case .OrderDetail(_):
                    object = OrderDetails(attributes: json[APIConstants.DataKey]["orderHistory"].arrayValue.first?.dictionaryValue)
                case .TotalPendingSchedule(_):
                    object = UtilityFunctions.appendOptionalStrings(withArray:[json[APIConstants.DataKey]["pendingOrder"].stringValue,json[APIConstants.DataKey]["scheduleOrders"].stringValue],separatorString: "$$")
                    break
                default:
                    break
                }
                
                completion(APIResponse.Success(object))
                
                return
            }
            else if responseType == .InvalidAccessToken {
                guard let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate,window = appDelegate.window else { return }
                GDataSingleton.sharedInstance.loggedInUser = nil
                UtilityFunctions.showAlert(nil, message: L10n.SessionExpiredLoginToContinue.string, success: {
                    window.rootViewController?.sideMenuController()?.setContentViewController(StoryboardScene.Main.instantiateHomeViewController())
                    window.rootViewController?.toggleSideMenuView()
                    }, cancel: {
                        window.rootViewController?.sideMenuController()?.setContentViewController(StoryboardScene.Main.instantiateHomeViewController())
                        window.rootViewController?.toggleSideMenuView()
                })
            }else if responseType == .ParameterMissing {
                if self.isAlertNeeded(api){
                    UtilityFunctions.sharedAppDelegateInstance().window?.makeToast(json.dictionaryValue[APIConstants.message]?.stringValue)
                }
                return
            }else if responseType == .ApiError {
//                UtilityFunctions.sharedAppDelegateInstance().window?.makeToast(L10n.SomewhereSomehowSomethingWentWrong.string)
                return
            }else if responseType == .Validation {
                if self.isAlertNeeded(api) {
                    UtilityFunctions.sharedAppDelegateInstance().window?.makeToast(json.dictionaryValue[APIConstants.message]?.stringValue)
                }
                return
            }
            if responseType == .Delivery {
                UtilityFunctions.showSweetAlert(L10n.Warning.string, message: json.dictionaryValue[APIConstants.message]?.stringValue, success: {}, cancel: {})
                return
            }
            completion(APIResponse.Failure(responseType))
            
            }, failure: {[unowned self] (message) in
//                UtilityFunctions.stopLoader()
                 if self.isLoaderNeeded(api){
                    self.hideLoader()
                }
//                if self.isAlertNeeded(api){
//                    UtilityFunctions.sharedAppDelegateInstance().window?.makeToast(L10n.SomewhereSomehowSomethingWentWrong.string)
//                }
                completion(APIResponse.Failure(.None))
            })
    }
    
    
    func opertationWithRequest ( withApi api : API,image : UIImage? , completion : APICompletion )  {
        
        if isLoaderNeeded(api){
            showLoader()
        }

        print(api.parameters)
        
        httpClient.postRequest(withApi: api, image: image, success: { (data) in
            self.hideLoader()
            guard let response = data else {
                completion(APIResponse.Failure(.None))
                return
            }
            let json = JSON(response)
            print(json)
            let responseType  = APIValidation(rawValue: (json.dictionaryValue[APIConstants.Status]?.stringValue) ?? "") ?? .None
            if responseType == APIValidation.Success{
                var object : AnyObject?

            switch api {
            case .RegisterLastStep(_):
                object = User(attributes: json[APIConstants.DataKey].dictionaryValue)
                GDataSingleton.sharedInstance.loggedInUser = object as? User
            case .ChangeProfile(_):
                object = json[APIConstants.DataKey]["image"].stringValue
            default:
                break
                }
                completion(APIResponse.Success(object))
                return
            }
            
            
            completion(APIResponse.Failure(responseType))
        }) { (message) in
            
            
            self.hideLoader()
            
            completion(APIResponse.Failure(.None))
        }
    }

    
    func isAlertNeeded(api : API) -> Bool{
        switch api {
        case .SupplierImage(_):
            return false
        default :
            return true
        }
    }

    
    
    func isLoaderNeeded(api : API) -> Bool{
        switch api {
        //Login Flow
        case .Register(_), .Login(_), .SendOTP(_),.CheckOTP(_),.RegisterLastStep(_),.ChangeProfile(_),.LoginFacebook(_):
            return true
        //Cart Flow
        case .AddToCart(_),.UpdateCartInfo(_),.GenerateOrder(_),.BarCodeSearch(_):
            return true
        //Package Flow
        case .PackageProductListing(_),.PackageSupplierListing(_),.LaundryProductListing(_),.LaundrySupplierList(_):
            return true
        // GenericFlow
        case .SupplierListing(_),.SupplierInfo(_),.SubCategoryListing(_),.ProductListing(_),.ProductDetail(_),.MultiSearch(_):
            return true
        //Location Related
        case .CompareProducts(_),.CompareProductResult(_),.ViewAllOffers(_),.ScheduleNewOrder(_),.RateOrderListing(_),.ClearAllNotifications(_),.AllNotifications(_):
            return true
        //Side Panel
        case .LoyalityPointScreen(_),.MyFavorites(_),.OrderHistory(_),.OrderUpcoming(_),.OrderTrackingList(_),.PromotionProducts(_),.ScheduledOrders(_),.LoyaltyPointsOrder(_),.OrderDetail(_):
            return true
        case .Home(_):
            return GDataSingleton.sharedInstance.homeData == nil ? true : false
        default:
            return false
        }
    }
    
    func showLoader() {

        UtilityFunctions.sharedAppDelegateInstance().window?.rootViewController?.startAnimating(CGSize(width: 40,height: 40), message: L10n.Loading.string, type: .LineScalePulseOut, color: UIColor.whiteColor())
    }
    
    func hideLoader(){
        UtilityFunctions.sharedAppDelegateInstance().window?.rootViewController?.stopAnimating()
    }

}



extension UIViewController : NVActivityIndicatorViewable {
    
}
