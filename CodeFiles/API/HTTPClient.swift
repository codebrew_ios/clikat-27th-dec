//
//  APIClient.swift
//  Clikat
//
//  Created by cbl73 on 4/22/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation
import Alamofire
import AFNetworking
import SwiftyJSON

typealias HttpClientSuccess = (AnyObject?) -> ()
typealias HttpClientFailure = (String) -> ()

class HTTPClient {
    
    
    func JSONObjectWithData(data: NSData) -> AnyObject? {
        do { return try NSJSONSerialization.JSONObjectWithData(data, options: []) }
        catch { return .None }
    }
    
    func postRequest(withApi api : API  , success : HttpClientSuccess , failure : HttpClientFailure )  {
        
        let params = api.parameters
        let fullPath = APIConstants.BasePath + api.route
        let method = api.method
        
        Alamofire.request(method, fullPath, parameters: params, encoding: .URL, headers: nil)
            .responseJSON { (response) in
                
                switch response.result {
                case .Success(let data):
                    success(data)
                case .Failure(let error):
                    failure(error.localizedDescription)
                }
        }
        
        /*
         .responseString { (response) in
         print(response)
         }
         */
    }
    
    func postRequest(withApi api : API,image : UIImage?, success : HttpClientSuccess , failure : HttpClientFailure){
        
        let params = api.parameters
        let fullPath = APIConstants.BasePath + api.route
        // let method = api.method
        
        Alamofire.upload(.POST, fullPath, headers: nil, multipartFormData: { (formData) in
            if let ppic = image,data = UIImageJPEGRepresentation(ppic, 0.0){
                formData.appendBodyPart(data: data, name:"profilePic" , fileName:"temp" + "\(Int(arc4random_uniform(100000)))" + ".jpeg" , mimeType: "image/jpeg")
            }
            for (key, value) in params ?? [:] {
                formData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
            }
            }, encodingMemoryThreshold: Manager.MultipartFormDataEncodingMemoryThreshold) { (result) in
                switch result {
                case .Success(request:let req, streamingFromDisk: _, streamFileURL: _):
                    req.responseJSON(completionHandler: { (response) in
                        switch response.result {
                        case .Success(let data):
                            success(data)
                        case .Failure(let error):
                            failure(error.localizedDescription)
                        }
                    })
                    req.responseString(completionHandler: { (let response) in
                        print(response)
                    })
                case .Failure(let error):
                    failure(error.toString)
                }
        }
    }
    
    func postPayFortRequest(completion : APICompletion){
        
        let manager : AFHTTPSessionManager = AFHTTPSessionManager(sessionConfiguration: NSURLSessionConfiguration.defaultSessionConfiguration())
        manager.requestSerializer = AFJSONRequestSerializer()
        
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        
        let url = "https://paymentservices.payfort.com/FortAPI/paymentApi"
        
        var signatureStr = ""
        
        var params = [
            "service_command" : "SDK_TOKEN",
            "device_id" : PayFortController().getUDID(),
            "language" : "en",
            "access_code" : "ELvn2sPwsH5CXUFp0BjI",
            "merchant_identifier" : "UWQlGgoP",
            ]
        let req = params.sort({$0.0 < $1.0})
        for (key,value) in req {
            signatureStr += key + "=" + value
        }
        signatureStr = "CLIKATIN" + "access_code=ELvn2sPwsH5CXUFp0BjIdevice_id=" + PayFortController().getUDID() + "language=enmerchant_identifier=UWQlGgoPservice_command=SDK_TOKEN" + "CLIKATIN"
        
        signatureStr = signatureStr.sha256()
        params["signature"] = signatureStr

        Alamofire.request(.POST, url, parameters: params, encoding: .JSON, headers: ["Content-Type":"application/json","Accept" : "application/json"]).responseJSON { (response) in
            switch response.result {
            case .Success(let data):
                if JSON(data).dictionaryValue["status"] != "00" {
                    completion(APIResponse.Success(PayFort(attributes: JSON(data).dictionaryValue)))
                    return
                }
                print(JSON(data).dictionaryValue["response_message"])
                completion(APIResponse.Failure(APIValidation.ApiError))
            case .Failure(let error):
                print(error)
                completion(APIResponse.Failure(APIValidation.ApiError))
            }
        }
        
       
    }
  
}