//
//  FacebookManager.swift
//  Clikat
//
//  Created by cblmacmini on 4/27/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

typealias FacebookCallback = (facebook : Facebook) -> ()

class FacebookManager: NSObject {
    
    var viewController : UIViewController?
    let permissions = ["public_profile","email"]
    
    var facebookCallback : FacebookCallback?
    
    class var sharedManager: FacebookManager {
        struct Static {
            static var instance: FacebookManager?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = FacebookManager()
            
        }
        
        return Static.instance!
    }
    override init() {
        super.init()
    }
    
    
    func configureLoginManager(sender : UIViewController, success : FacebookCallback){
        
        facebookCallback = success
        self.viewController = sender
        let loginManager = FBSDKLoginManager()
        
        loginManager.logInWithReadPermissions(permissions, fromViewController: viewController) { (result, error) in
            weak var weakSelf = self
            if let err = error {
                print(err.localizedDescription)
            }else if result.isCancelled {
                print("Cancelled")
            }else{
                weakSelf?.sendGraphRequest()
            }
        }
    }
    
    
    func sendGraphRequest(){
        
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"first_name,last_name,picture.type(large),email"]).startWithCompletionHandler { (connection, result, error) in
            
            if let err = error {
                print(err.localizedDescription)
            }else if let block = self.facebookCallback {
                
                let fbProfile = Facebook(result: result)
                block(facebook: fbProfile)
            }
        }
    }
}

class Facebook : NSObject {
    
    var fbId : String?
    var firstName : String?
    var lastName : String?
    var imageUrl : String?
    var email : String?
    
    init(result : AnyObject?) {
        super.init()
        guard let fbResult = result else { return }
        
        fbId = FBSDKAccessToken.currentAccessToken().userID
        firstName = fbResult.valueForKey("first_name") as? String
        lastName = fbResult.valueForKey("last_name") as? String
        imageUrl = fbResult.valueForKeyPath("picture.data.url") as? String
        email = fbResult.valueForKey("email") as? String
        
        if email == nil || email?.trim().characters.count == 0 {
            email = /firstName + "@facebook.com"
        }
    }
    
    override init() {
        super.init()
    }
}



