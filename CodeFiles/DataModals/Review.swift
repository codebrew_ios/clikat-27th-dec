//
//  Review.swift
//  Clikat
//
//  Created by cblmacmini on 5/4/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import SwiftyJSON


enum ReviewKeys : String{
    
    case firstname = "firstname"
    case user_image = "user_image"
    case comment = "comment"
    case rating = "rating"
    case review_list = "review_list"
    
}

class Review: NSObject {

    var userImage : String?
    var userName : String?
    var comment : String?
    var rating : Int?
    
    init (attributes : SwiftyJSONParameter){
        self.userName = attributes?[ReviewKeys.firstname.rawValue]?.stringValue
        self.userImage = attributes?[ReviewKeys.user_image.rawValue]?.stringValue
        self.comment = attributes?[ReviewKeys.comment.rawValue]?.stringValue
        self.rating = attributes?[ReviewKeys.rating.rawValue]?.intValue
    }
    
    override init() {
        super.init()
    }
}

class ReviewListing: NSObject {
    
    var reviewListing : [Review]?
    
    init(attributes : SwiftyJSONParameter) {
        
        var arrayResults : [Review] = []
        guard let rawData = attributes else { return }
        let json = JSON(rawData)
        let results = json[ReviewKeys.review_list.rawValue].arrayValue
        for result in results{
                let product = Review(attributes: result.dictionaryValue)
                arrayResults.append(product)
        }
        self.reviewListing = arrayResults        
        
    }
    
    override init() {
        super.init()
    }
}

