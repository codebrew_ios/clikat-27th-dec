//
//  SubCategory.swift
//  Clikat
//
//  Created by Night Reaper on 09/05/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation
import SwiftyJSON

enum SubCategoryKeys : String {
    
    case subCategoryId = "sub_category_id"
    case icon = "icon"
    case image = "image"
    case name = "name"
    case description = "description"
}



class SubCategory : NSObject {
    
    var subCategoryId : String?
    var icon : String?
    var image : String?
    var name : String?
    var subCategoryDesc : String?
    
    
    init(attributes : SwiftyJSONParameter){
        
        self.subCategoryId = attributes?[SubCategoryKeys.subCategoryId.rawValue]?.stringValue
        self.icon = attributes?[SubCategoryKeys.icon.rawValue]?.stringValue
        self.image = attributes?[SubCategoryKeys.image.rawValue]?.stringValue
        self.name = attributes?[SubCategoryKeys.name.rawValue]?.stringValue
        self.subCategoryDesc = attributes?[SubCategoryKeys.description.rawValue]?.stringValue
    }
    
    override init() {
        super.init()
        
    }
}


class SubCategoriesListing {
    
    var subCategories : [SubCategory]?
    
    init(attributes : SwiftyJSONParameter){
        guard let rawData = attributes else { return }
        let json = JSON(rawData)
        
        let dict = json[APIConstants.DataKey]
        let subCategories = dict.arrayValue
        var arraySubcategories : [SubCategory] = []
        
        for element in subCategories{
            let supplier = SubCategory(attributes: element.dictionaryValue)
            arraySubcategories.append(supplier)
        }
        self.subCategories = arraySubcategories
    }
    
}
