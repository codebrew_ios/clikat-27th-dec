//
//  Banner.swift
//  Clikat
//
//  Created by Night Reaper on 20/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation
import SwiftyJSON
import RMMapper


enum BannerKeys : String {
    case image = "image"
    case category_id = "category_id"
    case name = "name"
    case supplier_id = "supplier_id"
    case branch_id = "branch_id"
    case supplier_placement_level = "supplier_placement_level"
    case category_flow = "category_flow"
}

class Banner : NSObject, RMMapping {
    
    var image : String?
    var category_id : String?
    var name : String?
    var supplierId : String?
    var supplierBranchId : String?
    var categoryFlow : String?
    var supplierPlacementLevel : String?
    
    init (attributes : Dictionary<String, JSON>?){
        self.category_id = attributes?[BannerKeys.category_id.rawValue]?.stringValue
        self.image = attributes?[BannerKeys.image.rawValue]?.stringValue
        self.image = self.image?.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        self.name = attributes?[BannerKeys.name.rawValue]?.stringValue
        self.supplierId = attributes?[BannerKeys.supplier_id.rawValue]?.stringValue
        self.supplierBranchId = attributes?[BannerKeys.branch_id.rawValue]?.stringValue
        self.categoryFlow = attributes?[BannerKeys.category_flow.rawValue]?.stringValue
        self.supplierPlacementLevel = attributes?[BannerKeys.supplier_placement_level.rawValue]?.stringValue
    }
 
    override init() {
        super.init()
    }
}

