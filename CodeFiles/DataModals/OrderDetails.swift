//
//  OrderDetails.swift
//  Clikat
//
//  Created by cblmacmini on 5/2/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import SwiftyJSON
import EZSwiftExtensions

enum OrderDateFormat : String {
    case From = "yyyy-MM-dd HH:mm:ss"
    case To = "MMM dd EEE hh:mm a"
    case ToLine = "MMM dd EEE\n hh:mm a"
}

enum OrderDeliveryStatus : String {
    
    case Delivered = "5" // Green - History
    case CustomerCancel = "8" // Red - History
    case Rejected = "2" // Red - History

    case Tracked = "7" // Yellow - Track
    case Shipped = "3" // Yellow
    case Nearby = "4" // Yellow

    
    case Confirmed = "1" //
    case Schedule = "9" // - Upcoming
    
    case Pending = "0"
    case FeedbackGiven = "6"
    
    func color() -> UIColor{
        
        
        switch self {
        case .Delivered :
            return Colors.GreenColor.color()
        
        case .CustomerCancel:
            return Colors.RedColor.color()
        case .Rejected :
            return Colors.RedColor.color()
            
        case .Tracked , .Shipped , .Nearby :
            return Colors.YellowColor.color()
        
        default:
            return Colors.MainColor.color()
        }
    
    
    }
    
    func stringValue () -> String {
        
        switch self {
        case .Pending:
            return L10n.PENDING.string
        case .Delivered:
            return L10n.DELIVERED.string
        case .Confirmed:
            return L10n.CONFIRMED.string
        case .FeedbackGiven:
            return L10n.FEEDBACKGIVEN.string
        case .Rejected:
            return L10n.REJECTED.string
        case .Shipped:
            return L10n.SHIPPED.string
        case .Nearby:
            return L10n.NEARBY.string
        case .Tracked:
            return L10n.TRACKED.string
        case .CustomerCancel:
            return L10n.CUSTOMERCANCELLED.string
        case .Schedule:
            return L10n.SCHEDULED.string
        
        }
        
    }
    
}

enum OrderRelatedKeys : String {
    
    case net_amount = "net_amount"
    case order_id = "order_id"
    case service_date = "service_date"
    case delivered_on = "delivered_on"
    case status = "status"
    case near_on = "near_on"
    case shipped_on = "shipped_on"
    case payment_type = "payment_type"
    case created_on = "created_on"
    case product_count = "product_count"
    case user_delivery_address = "user_delivery_address"
    case delivery_address = "delivery_address"
    case product = "product"
    case schedule_order = "schedule_order"
    case orderHistory = "orderHistory"
    case orderList = "orderList"
    case supplier_branch_id = "supplier_branch_id"
    case total_points = "total_points"
    case area_id = "area_id"
}

class OrderDetails: NSObject {

    var netAmount : String?
    var orderId : String?
    var serviceDate : String?
    var deliveredOn : String?
    var status : OrderDeliveryStatus = .Pending
    var nearOn : String?
    var shippedOn : String?
    var paymentType :PaymentMethod = .COD
    var createdOn : String?
    var productCount : String?
    var userDeliveryAddress : String?
    var scheduleOrder : String?
    var deliveryAddress : Address?
    var product : [Product]?
    var supplierBranchId : String?
    
    //New
    var nearOnLine : String?
    var shippedOnLine : String?
    var serviceDateLine : String?
    var deliveredOnLine : String?
    var createdOnLine : String?
    var areaId : String?
    
    var totalPoints : String?
    /*
     [10:01 AM] prince: schedule_order
     [10:02 AM] prince: 0: no schedule order , 1: schedule order
     */
    init(attributes : SwiftyJSONParameter) {
     
        self.netAmount = attributes?[OrderRelatedKeys.net_amount.rawValue]?.stringValue
        self.orderId = attributes?[OrderRelatedKeys.order_id.rawValue]?.stringValue
        self.status = OrderDeliveryStatus(rawValue: attributes?[OrderRelatedKeys.status.rawValue]?.stringValue ?? "") ?? .Pending
        self.paymentType = PaymentMethod(rawValue: attributes?[OrderRelatedKeys.near_on.rawValue]?.stringValue ?? "") ?? .COD
        self.productCount = attributes?[OrderRelatedKeys.product_count.rawValue]?.stringValue
        self.userDeliveryAddress = attributes?[OrderRelatedKeys.user_delivery_address.rawValue]?.stringValue
        
        self.scheduleOrder = attributes?[OrderRelatedKeys.schedule_order.rawValue]?.stringValue
        self.supplierBranchId = attributes?[OrderRelatedKeys.supplier_branch_id.rawValue]?.stringValue
        
        self.totalPoints = attributes?[OrderRelatedKeys.total_points.rawValue]?.stringValue
        self.areaId = attributes?[OrderRelatedKeys.area_id.rawValue]?.stringValue
        /*
        if let tempServiceDate = NSDate(fromString: (attributes?[OrderRelatedKeys.service_date.rawValue]?.stringValue ?? ""), format: "yyyy-MM-dd") {
            self.serviceDate = tempServiceDate.toString(format: "mmmm d, yyyy")
        }
        if let tempDeliveryDate = NSDate(fromString: (attributes?[OrderRelatedKeys.delivered_on.rawValue]?.stringValue ?? ""), format: "yyyy-MM-dd"){
            self.serviceDate = tempDeliveryDate.toString(format: "mmmm d, yyyy")
        }
    */
        var arrayProducts : [Product] = []
        for element in attributes?[OrderRelatedKeys.product.rawValue]?.arrayValue ?? [] {
            let product = Product(attributes: element.dictionaryValue)
            arrayProducts.append(product)
        }
        self.product = arrayProducts
        self.deliveryAddress = Address(attributes: attributes?[OrderRelatedKeys.delivery_address.rawValue]?.dictionaryValue)
        
        //Dates
        self.nearOn = attributes?[OrderRelatedKeys.near_on.rawValue]?.stringValue.convertDateStringFormat(.From,to:.To)
        self.shippedOn = attributes?[OrderRelatedKeys.shipped_on.rawValue]?.stringValue.convertDateStringFormat(.From,to:.To)
        self.createdOn = attributes?[OrderRelatedKeys.created_on.rawValue]?.stringValue.convertDateStringFormat(.From,to:.To)
        
        self.deliveredOn = attributes?[OrderRelatedKeys.delivered_on.rawValue]?.stringValue.convertDateStringFormat(.From,to:.To)
        self.serviceDate = attributes?[OrderRelatedKeys.service_date.rawValue]?.stringValue.convertDateStringFormat(.From,to:.To)
        
        self.nearOnLine = attributes?[OrderRelatedKeys.near_on.rawValue]?.stringValue.convertDateStringFormat(.From,to:.ToLine)
        self.shippedOnLine = attributes?[OrderRelatedKeys.shipped_on.rawValue]?.stringValue.convertDateStringFormat(.From,to:.ToLine)
        self.createdOnLine = attributes?[OrderRelatedKeys.created_on.rawValue]?.stringValue.convertDateStringFormat(.From,to:.ToLine)
        
        self.deliveredOnLine = attributes?[OrderRelatedKeys.delivered_on.rawValue]?.stringValue.convertDateStringFormat(.From,to:.ToLine)
        self.serviceDateLine = attributes?[OrderRelatedKeys.service_date.rawValue]?.stringValue.convertDateStringFormat(.From,to:.ToLine)

    }
    
    init(orderSummary : OrderSummary?,orderId : String?,scheduleOrder : String?) {
        self.orderId = orderId
        self.status = .Pending
        self.paymentType = orderSummary?.paymentMethod ?? .COD
        self.createdOn = UtilityFunctions.getDateFormatted(OrderDateFormat.To.rawValue, date: NSDate())
        self.deliveryAddress = orderSummary?.deliveryAddress
        self.product = []
        for product in orderSummary?.items ?? [] {
            self.product?.append(Product(cart:product))
        }
        self.netAmount = orderSummary?.totalAmount?.toString
        self.supplierBranchId = product?.first?.supplierBranchId
        self.scheduleOrder = scheduleOrder
        self.userDeliveryAddress = self.deliveryAddress?.addressString
        self.productCount = self.product?.count.toString
        
        self.deliveredOn = UtilityFunctions.getDateFormatted(OrderDateFormat.To.rawValue, date: orderSummary?.deliveryDate ?? NSDate())
        self.serviceDate = self.deliveredOn
    }
    
    init(orderId : String?) {
        self.orderId = orderId
    }
    
    override init() {
        super.init()
     
    }
}


class OrderListing {
    
    
    var orders : [OrderDetails]?
    
    init(attributes : SwiftyJSONParameter , key : String){
        guard let rawData = attributes else { return }
        let json = JSON(rawData)
        
        let dict = json[key]
        let tempOrders = dict.arrayValue
        var arrayOrders : [OrderDetails] = []
        
        for element in tempOrders {
            let order = OrderDetails(attributes: element.dictionaryValue)
            arrayOrders.append(order)
        }
        self.orders = arrayOrders
    }
    
}

extension String {
    
    func convertDateStringFormat(from : OrderDateFormat, to : OrderDateFormat) -> String? {
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeZone = NSTimeZone.localTimeZone()
        dateFormatter.dateFormat = from.rawValue
        var str = self.componentsSeparatedByString("+").first ?? ""
        str = str.stringByReplacingOccurrencesOfString("T", withString: " ")
        guard let date = dateFormatter.dateFromString(str) else { return nil }
        return UtilityFunctions.getDateFormatted(to.rawValue,date: date)
    }
}



