//
//  Language.swift
//  Clikat
//
//  Created by cbl73 on 4/23/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation
import SwiftyJSON
import RMMapper


enum Language : String {
    case id = "id"
    case language_code = "language_code"
    case language_name = "language_name"
    
    static let allValues = [ "en" , "ar" ]
    
}



class ApplicationLanguage : NSObject , RMMapping {
    
    var id : String?
    var language_code : String?
    var language_name : String?
    
    
    init (attributes : SwiftyJSONParameter){
        self.id = attributes?[Language.id.rawValue]?.stringValue
        self.language_code = attributes?[Language.language_code.rawValue]?.stringValue
        self.language_name = attributes?[Language.language_name.rawValue]?.stringValue
        
    }
    override init() {
        super.init()
    }
    
}

class LanguageListing : NSObject , RMMapping {
    
    var languages : [ApplicationLanguage]?
    
    
    init (attributes : SwiftyJSONParameter){
        guard let rawData = attributes else { return }
        let json = JSON(rawData)
        let languages = json[HomeKeys.languageList.rawValue].arrayValue
        
        var tempLanguages : [ApplicationLanguage] = []
        for language in languages {
            let lang = ApplicationLanguage(attributes: language.dictionaryValue)
            tempLanguages.append(lang)
        }
        self.languages = tempLanguages
        
    }
    
    override init() {
        super.init()
    }
    
}

