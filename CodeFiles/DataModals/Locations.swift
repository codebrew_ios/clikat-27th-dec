//
//  Locations.swift
//  Clikat
//
//  Created by cblmacmini on 5/12/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import SwiftyJSON

enum LocationType {
    case Country
    case City
    case Zone
    case Area
}

enum LocationKeys : String {
    
    case arabicList = "arabicList"
    case englishList = "englishList"
    case englistList = "englistList"
    case address = "address"
}

class Locations: NSObject {
    
    var id : String?
    var name : String?
    
    init(data : SwiftyJSONParameter) {
        self.id = data?["id"]?.stringValue
        self.name = data?["name"]?.stringValue
    }
    
    override init() {
        super.init()
    }
}

class LocationResults {
    
    var arrLocationEn : [Locations]?
    var arrLocationAr : [Locations]?
    
    var myAddress : [Address]?
    
    init(data : SwiftyJSONParameter) {
        var englishList = [JSON]()
        if let list = data?[LocationKeys.englishList.rawValue]?.arrayValue {
            englishList = list
        }else if let list  = data?[LocationKeys.englistList.rawValue]?.arrayValue {
            
            englishList = list
        }
        myAddress = []
        for address in data?[LocationKeys.address.rawValue]?.arrayValue ?? [] {
            myAddress?.append(Address(attributes: address.dictionaryValue))
        }
        guard let arabicList = data?[LocationKeys.arabicList.rawValue]?.arrayValue else { return }
        var tempArrEn : [Locations] = []
        var tempArrAr : [Locations] = []
        for (arabic,english) in zip(arabicList,englishList) {
            let locationen = Locations(data: english.dictionaryValue)
            let locationar = Locations(data: arabic.dictionaryValue)
            tempArrAr.append(locationar)
            tempArrEn.append(locationen)
        }
        arrLocationAr = tempArrAr
        arrLocationEn = tempArrEn
    }
    func getArrLocation() -> [Locations]? {
        return Localize.currentLanguage() == Languages.Arabic ? arrLocationAr : arrLocationEn
    }
    
}

class ApplicationLocation : NSObject {
    var countryEN : Locations?
    var cityEN : Locations?
    var areaEN : Locations?
    
    var countryAR : Locations?
    var cityAR : Locations?
    var areaAR : Locations?
    
    
    init(countryEn : Locations?,cityEn : Locations?,areaEn : Locations?,countryAr : Locations?,cityAr : Locations?,areaAr : Locations?){
        super.init()
        self.countryEN = countryEn
        self.cityEN = cityEn
        self.areaEN = areaEn
        
        self.countryAR = countryAr
        self.cityAR = cityAr
        self.areaAR = areaAr
    }
    override init() {
        super.init()
    }
    
    func getCountry() -> Locations? {
        return Localize.currentLanguage() == Languages.Arabic ? countryAR : countryEN
    }
    
    func getCity() -> Locations? {
        return Localize.currentLanguage() == Languages.Arabic ? cityAR : cityEN
    }
    
    func getArea() -> Locations? {
        return Localize.currentLanguage() == Languages.Arabic ? areaAR : areaEN
    }
}
