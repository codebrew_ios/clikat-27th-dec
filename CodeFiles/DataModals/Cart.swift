//
//  Cart.swift
//  Clikat
//
//  Created by cblmacmini on 5/11/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

typealias CartCompletionBlock = ([AnyObject]) -> ()

enum PriceType : Int {
    case Fixed = 0
    case Hourly
    case None
}

class Cart: NSObject {
    
    var id: String?
    var quantity: String?
    var dateModified: NSDate?
    var name: String?
    var productType: String?
    var handlingSupplier : String?
    var image : String?
    var deliveryCharges : String?
    var handlingAdmin : String?
    var supplierBranchId : String?
    var displayPrice : String?
    var measuringUnit : String?
    var sku : String?
    var canUrgent : String?
    var price: String?
    var loyalty_points : String?
    
    var priceType : PriceType = .None
    var fixedPrice : String?
    var hourlyPrice : [HourlyPrice]?
    var strHourlyPrice : String?
    var urgentPrice : String?
    var urgentValue : String?
    var urgentType : String?
    
    var category : String?
    var categoryId : String?
    
    
    
    init(attributes : SwiftyJSONParameter){
        super.init()
        self.name = attributes?[ProductKeys.name.rawValue]?.stringValue
        self.price = attributes?[ProductKeys.price.rawValue]?.stringValue
        self.productType = attributes?[ProductKeys.product_type.rawValue]?.stringValue
        self.deliveryCharges = attributes?[ProductKeys.delivery_charges.rawValue]?.stringValue
        self.supplierBranchId = attributes?[ProductKeys.supplier_branch_id.rawValue]?.stringValue
        self.loyalty_points = attributes?[LoyalityPointsKeys.loyalty_points.rawValue]?.stringValue
        if let identifier = attributes?[ProductKeys.id.rawValue]?.stringValue {
            self.id = identifier
        }
        else{
            self.id = attributes?[ProductKeys.product_id.rawValue]?.stringValue
        }
        self.urgentValue = attributes?[ProductKeys.urgent_value.rawValue]?.stringValue
        self.handlingSupplier = attributes?[ProductKeys.handling_supplier.rawValue]?.stringValue
        self.handlingAdmin = attributes?[ProductKeys.handling_admin.rawValue]?.stringValue
        self.canUrgent = attributes?[ProductKeys.can_urgent.rawValue]?.stringValue
        self.urgentType = attributes?[ProductKeys.urgent_type.rawValue]?.stringValue
        self.urgentPrice = attributes?[ProductKeys.urgent_price.rawValue]?.stringValue
        self.priceType = PriceType(rawValue: attributes?[ProductKeys.price_type.rawValue]?.intValue ?? 0) ?? .None
        self.fixedPrice = attributes?[ProductKeys.fixed_price.rawValue]?.stringValue
        hourlyPrice = []
        
        if urgentType == "1" {
            
            
        }
        for price in attributes?[ProductKeys.hourly_price.rawValue]?.arrayValue ?? [] {
            let tempPrice = HourlyPrice(attributes: price.dictionaryValue)
            hourlyPrice?.append(tempPrice)
        }
        self.strHourlyPrice = attributes?[ProductKeys.hourly_price.rawValue]?.rawString()
        let tempPrice = getDisplayPrice(0)?.toFloat() ?? 0.0
        self.displayPrice = String(tempPrice)
        
        if let image = attributes?[ProductKeys.product_image.rawValue]?.stringValue {
            
            self.image = image
            return
        }
        guard let productImages = attributes?[ProductKeys.image_path.rawValue]?.arrayValue where productImages.count > 0 else{
            self.image = attributes?[ProductKeys.image_path.rawValue]?.stringValue
            return
        }
        self.image = productImages.first?.stringValue
    }
    override init() {
        super.init()
    }
    class func initializePriceArray(rawStr : String?) -> [HourlyPrice]? {
       
        guard let jsonstr = rawStr?.parseJSONString else { return [] }
        let json = JSON(jsonstr)
        var arrPrice : [HourlyPrice] = []
        for price in json.arrayValue ?? [] {
            let tempPrice = HourlyPrice(attributes: price.dictionaryValue)
            arrPrice.append(tempPrice)
        }
        return arrPrice
    }
    
    func getDisplayPrice(quantity : Double?) -> String?{
        
        guard let price = Double(getPrice(quantity) ?? "0") else { return nil }
        
//        return (price + handlingSupplier + handlingAdmin).toString
        return price.toString
    }
    func getPrice(quantity : Double?) -> String? {
        switch priceType ?? .None {
        case .Fixed:
            return fixedPrice
        case .Hourly:
            if hourlyPrice?.count <= 0 { print("Hourly price nahi hai"); return fixedPrice }
            guard let price = Double(hourlyPrice?[getIndex(quantity)].pricePerHour ?? "0") else { return nil }
            return price.toString
        default:
            return nil
        }
    }
    
    func getPrice() -> Double? {
        switch priceType ?? .None {
        case .Fixed:
            return fixedPrice?.toDouble()
        case .Hourly:
            if hourlyPrice?.count <= 0 { print("Hourly price nahi hai"); return fixedPrice?.toDouble() }
            guard let price = Double(hourlyPrice?[getIndex(1)].pricePerHour ?? "0") else { return nil }
            return price
        default:
            return nil
        }
    }
    
    private func getIndex(quantity : Double?) -> Int{
        var priceIndex : Int = 0
        for (index,element) in (hourlyPrice ?? []).enumerate() {
            
            let min = element.minHour?.toDouble() ?? 0
            let max = element.maxHour?.toDouble() ?? 0
            if min...max ~= quantity ?? 0 {
                priceIndex = index
                break
            }else { continue }
        }
        return priceIndex
    }
    
    class func getMaxSupplier(cart : [Cart]) -> String?{
        var maxSupplier : Int = 0
        for product in cart {
            guard let supplier = product.handlingSupplier?.toInt() else { return nil }
            maxSupplier = maxSupplier > supplier ? maxSupplier : supplier
        }
        return maxSupplier.toString
        
    }
    
    class func getMaxAdmin(cart : [Cart]) -> String?{
        var maxAdmin : Int = 0
        for product in cart {
            guard let admin = product.handlingAdmin?.toInt() else { return nil }
            maxAdmin = maxAdmin > admin ? maxAdmin : admin
        }
        return maxAdmin.toString
    }
}

class PackageProduct : Product {
    
    override init(attributes : SwiftyJSONParameter) {
        super.init(attributes: attributes)
    }
    
}

class HourlyPrice {
    
    var minHour : String?
    var maxHour : String?
    var pricePerHour : String?
    
    init(attributes : SwiftyJSONParameter){
        minHour = attributes?[ProductKeys.min_hour.rawValue]?.stringValue
        maxHour = attributes?[ProductKeys.max_hour.rawValue]?.stringValue
        pricePerHour = attributes?[ProductKeys.price_per_hour.rawValue]?.stringValue
    }
}

extension String {
    
    var parseJSONString: AnyObject? {
        
        let data = dataUsingEncoding(NSUTF8StringEncoding)
        do {
            guard let jsonData = data else { return nil }
            return try NSJSONSerialization.JSONObjectWithData(jsonData, options: .MutableContainers)
        }catch let error{
            print(error)
            return nil
        }
    }
}
