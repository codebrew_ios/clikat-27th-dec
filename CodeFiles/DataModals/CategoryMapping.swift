//
//  CategoryMapping.swift
//  Clikat
//
//  Created by cbl73 on 5/3/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation
import UIKit
//"category_flow" : "Category>Services>PickUpTime>Suppliers>SupplierInfo>LaundryOrder",
//Category>Suppliers>SupplierInfo>SubCategory>Ds-Pl
enum CategoryFlowMap : String {
    
    case Category = "Category"
    case SubCategory = "SubCategory"
    case DetailedSubCategory = "Ds-Pl"
//    case DetailedSubCategory = "DetailedSubCategory"
    case Suppliers = "Suppliers"
    case SupplierInfo = "SupplierInfo"
    case ProductListing = "Pl"
    case Services = "Services"
    case PackageSupplierListing = "PackageSL"
    case PackageSupplierInfo = "PackageSI"
    case PackageProducts = "PackageProducts"
    
    
    case PickUpTime = "PickUpTime"
    case LaundryOrder = "LaundryOrder"
    case CategorySelection = "CategorySelectionController"
    
    
    func viewControllerIntance() -> UIViewController?{
        
        switch self {
        case .Category:
            return StoryboardScene.Main.instantiateHomeViewController()
        case .SubCategory:
            return StoryboardScene.Main.instantiateSubcategoryViewController()
        case .DetailedSubCategory:
            return StoryboardScene.Main.instantiateItemListingViewController()
        case .Suppliers:
            return StoryboardScene.Main.instantiateSupplierListingViewController()
        case .PackageSupplierListing:
            return StoryboardScene.Main.instantiatePackageSupplierListingViewController()
        case .SupplierInfo , .PackageSupplierInfo :
            let supplierInfoVC = StoryboardScene.Main.instantiateSupplierInfoViewController()
            return supplierInfoVC
        case .ProductListing:
            let productListingVc = StoryboardScene.Main.instantiateItemListingViewController()
            productListingVc.hideDetailedSubCategoriesBar = true
            return productListingVc
        case .PackageProducts:
            return StoryboardScene.Main.instantiatePackageProductListingViewController()
        case .Services :
            return StoryboardScene.Main.instantiateServicesViewController()
        case .PickUpTime :
            return StoryboardScene.Laundry.instantiatePickupDetailsController()
        case .LaundryOrder:
            return StoryboardScene.Laundry.instantiateOrderViewController()
        case .CategorySelection:
            return StoryboardScene.Options.instantiateCategorySelectionController()
        }
        
    }
    
    
}

enum InstanceException: ErrorType {
    case InstanceNotFound
    case CategoryNotFound
}

class CategoryMapping {
    
   class func nextViewController(withFlow flow : String? , currentViewController : UIViewController?) throws -> UIViewController? {
    
        guard let unwrappedFlow = flow else{
            throw InstanceException.CategoryNotFound
        }
        let flowComponents = unwrappedFlow.componentsSeparatedByString(">")
        guard let currentViewControllerIdentifier = CategoryMapping.mapViewControllerWithIdentifier(withViewController: currentViewController ,flowComponents: flowComponents) , nextItemIndex = flowComponents.indexOf(currentViewControllerIdentifier) else{
            throw InstanceException.CategoryNotFound
        }
    
        if flowComponents.count < nextItemIndex + 1{
            throw InstanceException.InstanceNotFound
        }
        let nextViewControllerInstance =  CategoryFlowMap(rawValue: flowComponents[nextItemIndex + 1])?.viewControllerIntance()
        return nextViewControllerInstance
    }

    
    class func mapViewControllerWithIdentifier(withViewController currentViewController : UIViewController? , flowComponents : [String])  -> String? {
        
        guard let viewController = currentViewController else{
            fatalError("Invalid current ViewController")
        }
        
        if let _ = viewController as? HomeViewController {
            return CategoryFlowMap.Category.rawValue
        }
        
        if let _ = viewController as? SupplierListingViewController {
            if flowComponents.contains(CategoryFlowMap.PackageSupplierListing.rawValue){
                return CategoryFlowMap.PackageSupplierListing.rawValue
            }
            return CategoryFlowMap.Suppliers.rawValue
        }
        
        if let _ = viewController as? SupplierInfoViewController {
            if flowComponents.contains(CategoryFlowMap.PackageSupplierInfo.rawValue){
                return CategoryFlowMap.PackageSupplierInfo.rawValue
            }
            return CategoryFlowMap.SupplierInfo.rawValue
        }
        if let _ = viewController as? PackageSupplierListingViewController {
            return CategoryFlowMap.PackageSupplierListing.rawValue
        }
        
        if let _ = viewController as? SubcategoryViewController {
            return CategoryFlowMap.SubCategory.rawValue
        }
        
        if let _ = viewController as? ItemListingViewController {
            if flowComponents.contains(CategoryFlowMap.PackageProducts.rawValue){
                return CategoryFlowMap.PackageProducts.rawValue
            }
            return CategoryFlowMap.DetailedSubCategory.rawValue
        }
        
        if let _ = viewController as? ProductDetailViewController {
            return CategoryFlowMap.ProductListing.rawValue
        }
        if let _ = viewController as? ServicesViewController {
            return CategoryFlowMap.Services.rawValue
        }
        if let _ = viewController as? PickupDetailsController {
            return CategoryFlowMap.PickUpTime.rawValue
        }
        
        if let _ = viewController as? OrderViewController {
            return CategoryFlowMap.LaundryOrder.rawValue
        }
        
        if viewController is CategorySelectionController {
            return CategoryFlowMap.Category.rawValue
        }
        return nil
    }
    
}


