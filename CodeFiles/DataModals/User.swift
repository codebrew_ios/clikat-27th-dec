//
//  User.swift
//  Clikat
//
//  Created by cblmacmini on 5/18/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import SwiftyJSON
enum UserKeys : String {
    case id = "id"
    case accessToken = "access_token"
    case email = "email"
    case user_image = "user_image"
    case firstname = "firstname"
    case mobile_no = "mobile_no"
    case otp_verified = "otp_verified"
    case facebookToken = "facebookToken"
}

class User: NSObject {
    
    var id : String?
    var token : String?
    var email : String?
    var userImage : String?
    var firstName : String?
    var mobileNo : String?
    var otpVerified : String?
    var fbId : String?
    
    init(attributes : SwiftyJSONParameter) {
        super.init()
        self.id = attributes?[UserKeys.id.rawValue]?.stringValue
        self.token = attributes?[UserKeys.accessToken.rawValue]?.stringValue
        self.email = attributes?[UserKeys.email.rawValue]?.stringValue
        self.userImage = attributes?[UserKeys.user_image.rawValue]?.stringValue
        self.firstName = attributes?[UserKeys.firstname.rawValue]?.stringValue
        self.mobileNo = attributes?[UserKeys.mobile_no.rawValue]?.stringValue
        self.otpVerified = attributes?[UserKeys.otp_verified.rawValue]?.stringValue
    }
    
    init(attributes : SwiftyJSONParameter,params : OptionalDictionary) {
        super.init()
        self.id = UserKeys.id.rawValue => attributes
        self.id = attributes?[UserKeys.id.rawValue]?.stringValue
        self.token = attributes?[UserKeys.accessToken.rawValue]?.stringValue
        self.email = attributes?[UserKeys.email.rawValue]?.stringValue
        self.userImage = attributes?[UserKeys.user_image.rawValue]?.stringValue
        self.firstName = attributes?[UserKeys.firstname.rawValue]?.stringValue
        self.mobileNo = attributes?[UserKeys.mobile_no.rawValue]?.stringValue
        self.otpVerified = attributes?[UserKeys.otp_verified.rawValue]?.stringValue
        self.fbId = params?[UserKeys.facebookToken.rawValue] as? String
        print(self.id|)
    }
    
    override init() {
        super.init()
    }
    
    class func validateChangePassword(passwords : [String]?) -> String?{
        guard let tempArr = passwords else { return nil }
        for str in tempArr {
            if str.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) == "" {
                return L10n.PleaseFillAllDetails.string
            }
        }
        
        if tempArr.first == tempArr[1] {
            return L10n.NewPasswordMustNotBeSameAsOldPassword.string
        }else if tempArr[1].characters.count < 6 {
         return L10n.PasswordShouldBeMinimum6Characters.string
        }else if tempArr[1] != tempArr.last {
            return L10n.PasswordsDoNotMatch.string
        }
        return ""
    }
    
    
}
infix operator => {associativity left precedence 160}
infix operator =| {associativity left precedence 160}
func =>(key : String, json : Dictionary<String, JSON>?) -> String?{
    return json?[key]?.stringValue
}
func =|(key : String, json : Dictionary<String, JSON>?) -> [JSON]?{
    return json?[key]?.arrayValue
}

postfix operator | {}
postfix func |(value : String?) -> String {
    return value.unwrap()
}
