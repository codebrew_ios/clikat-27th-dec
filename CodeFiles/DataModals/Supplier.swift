//
//  Supplier.swift
//  Clikat
//
//  Created by cbl73 on 4/21/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation
import SwiftyJSON

public typealias SwiftyJSONParameter = Dictionary<String, JSON>?

enum CommissionPackage : String {
    
    case Silver = "0"
    case Bronze = "1"
    case Gold = "2"
    case None = "4"
    case DoesntMatter
    
    func indexValue() -> Int {
        
        switch self {
        case .Silver:
            return 0
        case .Bronze:
            return 1
        case .Gold:
            return 2
        case .None:
            return 4
        default:
            return 0
        }
    }
    
    static let allValues = [L10n.Gold.string, L10n.Silver.string , L10n.Bronze.string]
    
    func commissionStringValue() -> String{
        
        switch self {
        case .Gold:
            return L10n.Gold.string
        case .Silver:
            return L10n.Silver.string
        case .Bronze:
            return L10n.Bronze.string
        default:
            return ""
        }
    }
    
    func medal() -> Asset? {
        
        switch self {
            
        case .Gold :
            return Asset.Ic_badge_mini_gold
        case .Silver :
            return Asset.Ic_badge_mini_silver
        case .Bronze :
            return Asset.Ic_badge_mini_bronze
        case .None :
            return Asset.Ic_np
        default:
            return nil
        }
        
    }
    
    func bigMedal () -> Asset? {
        
        switch self {
            
        case .Gold :
            return Asset.Ic_badge_gold_big
        case .Silver :
            return Asset.Ic_badge_silver_big
        case .Bronze :
            return Asset.Ic_badge_bronze_big
        case .None :
            return Asset.Ic_np
        default:
            return nil
        }
        
    }
    
}

enum Status : String {
    
    case Online = "1"
    case Busy = "2"
    case Closed = "0"
    case DoesntMatter
    
    
    func indexValue () -> Int {
        
        switch self {
        case .Online:
            return 1
        case .Busy:
            return 2
        case .Closed:
            return 0
        default:
            return 2
        }
    }
    static let allValues = [L10n.Closed.string , L10n.Open.string , L10n.Busy.string ]
    
    func statusStringValue() -> String{
        
        switch  self {
        case .Online:
            return L10n.Open.string
        case .Busy:
            return L10n.Busy.string
        case .Closed:
            return L10n.Closed.string
        default:
            return L10n.Open.string
        }
        
    }
    
    func status () -> Asset {
        
        switch self {
        case Online :
            return .Ic_status_online
        case Busy :
            return .Ic_status_busy
        case Closed :
            return .Ic_status_offline
        case DoesntMatter :
            return .Ic_status_offline
        }
        
    }
    
}

enum CommisionType : String {
    
    case ByValue = "0"
    case ByPercentage = "1"
    case DoesntMatter
    
}

enum PaymentMethod : String {
    
    case COD = "0"
    case Card = "1"
    case DoesntMatter = "2"
    
    
    func indexValue () -> Int {
        
        switch self {
        case .COD:
            return 0
        case .Card:
            return 1
        default:
            return 2
        }
    }
    
    static let allValues = [L10n.CashOnDelivery.string , L10n.Card.string , L10n.Both.string]
    
    func paymentMethodString() -> String{
        
        switch self {
        case .COD:
            return L10n.CashOnDelivery.string
        case .Card:
            return L10n.Card.string
        default:
            return L10n.Both.string
        }
        
    }
    
    
    func visibilityBasedOnDelivery (withImgCOD imgCOD : UIImageView? , imgCard : UIImageView?){
        
        
        switch self {
        case .DoesntMatter :
            imgCOD?.hidden = false
            imgCard?.hidden = false
            
        case .Card :
            imgCOD?.hidden = true
            imgCard?.hidden = false
            
        case .COD :
            imgCOD?.hidden = false
            imgCard?.hidden = true
        }
        
    }
    
}

enum SupplierKeys :String {
    
    case supplierList = "supplierList"
    
    case id = "id"
    case supplier_branch_id = "supplier_branch_id"
    case handling_fees = "handling_fees"
    case commission = "commission"
    case commission_type = "commission_type"
    case delivery_min_time = "delivery_min_time"
    case delivery_max_time = "delivery_max_time"
    case delivery_start_time = "delivery_start_time"
    case delivery_end_time = "delivery_end_time"
    case working_start_time = "working_start_time"
    case working_end_time = "working_end_time"
    case min_order = "min_order"
    case delivery_charges = "delivery_charges"
    case name = "name"
    case supplier_name = "supplier_name"
    case logo = "logo"
    case status = "status"
    case total_reviews = "total_reviews"
    case rating = "rating"
    case payment_method = "payment_method"
    case commission_package = "commission_package"
    case description = "description"
    case about = "about"
    case supplier_images = "supplier_image"
    case address = "address"
    case total_order = "total_order"
    case business_start_date = "business_start_date"
    case Favourite = "Favourite"
    case supplier_id = "supplier_id"
    case my_review = "my_review"
    case favorites = "favourites"
    case sponser = "sponser"
    case open_time = "open_time"
    case close_time = "close_time"
    case terms_and_conditions = "terms_and_conditions"
    case is_sponsor = "is_sponsor"
}


extension String {
    
    func percentEscapedString() -> String?{
        if self.contains("%20") {
            return self
        }
        return self.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
    }
}
class Supplier : NSObject {
    
    
    var id : String?
    var supplierBranchId : String?
    var handlingFees : String?
    var commission : String?
    var deliveryMinTime : String?
    var deliveryMaxTime : String?
    var deliveryStartTime : String?
    var deliveryEndTime : String?
    var workingStartTime : String?
    var workingEndTime : String?
    var minOrder : String?
    var deliveryCharges : String?
    var name : String?
    var logo : String?
    var totalReviews : String?
    var rating : String?
    var paymentMethod : PaymentMethod = .DoesntMatter
    var commissionPackage : CommissionPackage = .DoesntMatter
    var commissionType : CommisionType = .DoesntMatter
    var status : Status = .DoesntMatter
    var image : String?
    var address : String?
    var openingTime : String?
    var minimumDeliveryTime : String?
    var ordersDoneSoFar : String?
    var businessSince : String?
    var reviews : [Review]?
    var myReview : Review?
    var Favourite : String?
    var categories : [Categorie]?
    var descriptionHTML : String?
    var about : String?
    var supplierImages : [String]?
    var closeTime : String?
    var termsAndConditions : String?
    
    var displayDeliveryTime : String?
    var isSponsor : String?
    
    var categoryId : String?
    
    init(attributes : SwiftyJSONParameter){
        
        
        super.init()
        self.supplierBranchId = attributes?[SupplierKeys.supplier_branch_id.rawValue]?.stringValue
        self.handlingFees = attributes?[SupplierKeys.handling_fees.rawValue]?.stringValue
        self.commission = attributes?[SupplierKeys.commission.rawValue]?.stringValue
        self.deliveryMinTime = attributes?[SupplierKeys.delivery_min_time.rawValue]?.stringValue
        self.deliveryMaxTime = attributes?[SupplierKeys.delivery_max_time.rawValue]?.stringValue
        self.deliveryStartTime = attributes?[SupplierKeys.delivery_start_time.rawValue]?.stringValue
        self.deliveryEndTime = attributes?[SupplierKeys.delivery_end_time.rawValue]?.stringValue
        self.workingStartTime = attributes?[SupplierKeys.working_start_time.rawValue]?.stringValue
        self.workingEndTime = attributes?[SupplierKeys.working_end_time.rawValue]?.stringValue
        self.minOrder = attributes?[SupplierKeys.min_order.rawValue]?.stringValue
        self.deliveryCharges = attributes?[SupplierKeys.delivery_charges.rawValue]?.stringValue
        self.logo = attributes?[SupplierKeys.logo.rawValue]?.stringValue.percentEscapedString()
        self.totalReviews = attributes?[SupplierKeys.total_reviews.rawValue]?.stringValue
        self.rating = attributes?[SupplierKeys.rating.rawValue]?.stringValue
        self.address = attributes?[SupplierKeys.address.rawValue]?.stringValue
        self.Favourite = attributes?[SupplierKeys.Favourite.rawValue]?.stringValue
        self.isSponsor = attributes?[SupplierKeys.is_sponsor.rawValue]?.stringValue
        self.myReview = Review(attributes: attributes?[SupplierKeys.my_review.rawValue]?.dictionaryValue)
        self.commissionType = CommisionType(rawValue: attributes?[SupplierKeys.commission_type.rawValue]?.stringValue ?? "") ?? .DoesntMatter
        self.commissionPackage = CommissionPackage(rawValue: attributes?[SupplierKeys.commission_package.rawValue]?.stringValue ?? "") ?? .DoesntMatter
        self.status = Status(rawValue: attributes?[SupplierKeys.status.rawValue]?.stringValue ?? "") ?? .DoesntMatter
        self.paymentMethod = PaymentMethod(rawValue: attributes?[SupplierKeys.payment_method.rawValue]?.stringValue ?? "" ) ?? .DoesntMatter
        self.descriptionHTML = attributes?[SupplierKeys.description.rawValue]?.stringValue
        self.about = attributes?[SupplierKeys.about.rawValue]?.stringValue
        self.openingTime = attributes?[SupplierKeys.open_time.rawValue]?.stringValue
        self.closeTime = attributes?[SupplierKeys.close_time.rawValue]?.stringValue
        self.termsAndConditions = attributes?[SupplierKeys.terms_and_conditions.rawValue]?.stringValue
        self.descriptionHTML = UtilityFunctions.appendOptionalStrings(withArray: [self.descriptionHTML,self.termsAndConditions], separatorString: "\n")
        var images : [String] = []
        for element in attributes?[SupplierKeys.supplier_images.rawValue]?.arrayValue ?? []{
            if element.stringValue.characters.count == 0 { continue }
            images.append(element.stringValue.percentEscapedString() ?? "")
        }
        self.supplierImages = images
        
        self.reviews = ReviewListing(attributes: attributes).reviewListing
        self.ordersDoneSoFar = attributes?[SupplierKeys.total_order.rawValue]?.stringValue
        self.businessSince = attributes?[SupplierKeys.business_start_date.rawValue]?.stringValue
        businessSince = businessSince?.componentsSeparatedByString(",").last
        self.categories = CategoriesListing(attributes: attributes).arrayCategories
        
        self.displayDeliveryTime = self.getMinimumDeliveryTime(deliveryMaxTime, min: deliveryMinTime)
        if let suppName = attributes?[SupplierKeys.supplier_name.rawValue]?.stringValue {
            self.name = suppName
        }
        else{
            self.name = attributes?[SupplierKeys.name.rawValue]?.stringValue
        }

        guard let supplierId = attributes?[SupplierKeys.supplier_id.rawValue]?.stringValue else{
            self.id = attributes?[SupplierKeys.id.rawValue]?.stringValue
            return
        }
        self.id = supplierId
        
        
    }
    
    override init() {
        super.init()
        
    }
    
    func getMinimumDeliveryTime(max : String?,min : String?) -> String?{
        guard let minDelTime = min?.toInt(),maxDelTime = max?.toInt() else { return nil }
        if minDelTime == maxDelTime {
            return convertMinutes(minDelTime)
        }else if minDelTime > maxDelTime {
            return UtilityFunctions.appendOptionalStrings(withArray: [convertMinutes(maxDelTime),convertMinutes(minDelTime)], separatorString: " - ")
        }else{
            return UtilityFunctions.appendOptionalStrings(withArray: [convertMinutes(minDelTime),convertMinutes(maxDelTime)], separatorString: " - ")
        }
    }
    
    func convertMinutes(minutes : Int) -> String?{
        
        let hours = minutes.toDouble / 60
        let days = hours / 24
        if hours > 23 {
            return UtilityFunctions.appendOptionalStrings(withArray: [ceil(days).toInt.toString , L10n.Days.string], separatorString: " ")
        }else if minutes > 59 {
            return UtilityFunctions.appendOptionalStrings(withArray: [ceil(hours).toInt.toString , L10n.Hours.string], separatorString: " ")
        }else {
            return UtilityFunctions.appendOptionalStrings(withArray: [minutes.toString ,L10n.Mins.string], separatorString: " ")
        }
    }
}


class SupplierListing {
    
    var suppliers : [Supplier]?
    var sponsor : Supplier?
    
    init(attributes : SwiftyJSONParameter , key : String){
        guard let rawData = attributes else { return }
        let json = JSON(rawData)
        
        let dict = json[APIConstants.DataKey]

        let suppliers = dict[key].arrayValue
        var arraySuppliers : [Supplier] = []
        for element in suppliers{
            let supplier = Supplier(attributes: element.dictionaryValue)
            arraySuppliers.append(supplier)
        }
        self.suppliers = arraySuppliers
    }
    
}

class Sponsor {
    
    var id : String?
    var logo : String?
    var name : String?
    
    init(attributes : SwiftyJSONParameter){
        self.id = attributes?[SupplierKeys.id.rawValue]?.stringValue
        self.logo = attributes?[SupplierKeys.logo.rawValue]?.stringValue
        self.name = attributes?[SupplierKeys.name.rawValue]?.stringValue
    }
}



