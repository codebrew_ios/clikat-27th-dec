//
//  OrderSummary.swift
//  Clikat
//
//  Created by cblmacmini on 5/9/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import SwiftyJSON

prefix operator /{}

prefix func /(value: Int?) -> Int {
    return value ?? 0
}
prefix func /(value : String?) -> String {
    return value ?? ""
}
prefix func /(value : Bool?) -> Bool {
    return value ?? false
}
prefix func /(value : Double?) -> Double {
    return value ?? 0.0
}
prefix func /(value : Array<AnyObject>) -> Array<AnyObject> {
    return value ?? []
}

enum CartFlowType {
    case Normal
    case LoyaltyPoints
    case Laundry
}

class OrderSummary: NSObject {
    
    var pickupAddress : Address?
    var pickupDate : NSDate?
    var deliveryAddress : Address?
    var deliveryDate : NSDate?
    var items : [Cart]?
    var netTotal : String?
    var netTotalActualValue : String?
    var deliveryCharges : String?
    var handlingFee : String?
    var netPayableAmount : String?
    var displayNetTotal : String?
    var paymentMethod : PaymentMethod = .DoesntMatter
    var handlingCharges : String?
    var minOrderAmount : String?
    var totalAmount : Double?
    var pickupBuffer : String?
    var isPackage : String?
    var dDeliveryCharges : Double?
    var dTotalPrice : Double?
    
    var deliveryAdditionalCharges : Double?{
        didSet{
//            let delivery = deliveryAdditionalCharges?.toString
//            deliveryCharges = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string,delivery])
//            let total = /totalAmount  + /deliveryAdditionalCharges
//            netPayableAmount = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string ,total.toString])
        }
    }
    
    var cartId : String?
    
    override init() {
        super.init()
        
//        DBManager().getCart { (array) in
//            weak var weakSelf = self
//            weakSelf?.initalizeOrderSummary(array)
//        }
    }
    
    init(items : [Cart]?){
        super.init()
        
        initalizeOrderSummary(items)
    }
    init(items : [Cart]?,netTotal : String?) {
        self.items = items
        self.netTotal = netTotal
        self.netPayableAmount = netTotal
        self.displayNetTotal = netTotal
        self.deliveryCharges = "0.0" + L10n.Points.string
    }
    
    func initalizeOrderSummary(cart : [AnyObject]?){
        guard let arrCart = cart as? [Cart] else { return }
        
        let totalPrice = arrCart.reduce(0.0, combine: { (initial, cart) -> Double in
            let price = cart.getPrice(cart.quantity?.toDouble())?.toDouble() ?? 0.0
            let quantity = cart.quantity?.toDouble() ?? 0.0
            return initial + (price * (quantity < 0.0 ? 1 : quantity))
        })
        dTotalPrice = totalPrice
        
        let deliCharges = arrCart.reduce(0.0, combine: { (initial, cart) -> Double in
            let delivery = cart.deliveryCharges?.toDouble() ?? 0.0
            return delivery > initial ? delivery : initial
        })
        
        if let _ = deliveryAdditionalCharges {
            dDeliveryCharges = /deliveryAdditionalCharges
        }else {
           dDeliveryCharges = deliCharges
        }
        
        isPackage = arrCart.first?.category == "12" ? "1" : "0"
        
        items = arrCart
        
        guard let maxSupplier = Double(Cart.getMaxSupplier(arrCart) ?? "0"), maxAdmin = Double(Cart.getMaxAdmin(arrCart) ?? "0") else { return }
        
        let handling = maxSupplier + maxAdmin
        let total = totalPrice + /dDeliveryCharges + handling
        totalAmount = total
        handlingCharges = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string , handling.toString])
        displayNetTotal = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string , totalPrice.toString])
        netTotal = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string , totalPrice.toString])
        deliveryCharges = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string , dDeliveryCharges?.toString])
        netPayableAmount = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string ,total.toString])
        
    }
}


class LoyaltyPointsSummary : OrderSummary{
    
    var totalPoints : String?
    var deliveryData : Delivery?
    var cartFlowType : CartFlowType = .LoyaltyPoints
}
