//
//  Register.swift
//  Clikat
//
//  Created by cblmacmini on 4/28/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class Register: NSObject {

    class func validateCredentials(email : String, password : String?) -> String{
        
        var message = ""
        
        
        if email.characters.count == 0 {
            message = L10n.PleaseEnterYourEmailAddress.string
        }else if !isValidEmail(email){
            message = L10n.PleaseEnterAValidEmailAddress.string
        }else if password?.characters.count == 0 {
            message = L10n.PleaseEnterYourPassword.string
        }else if password?.characters.count < 6 {
            message = L10n.PasswordShouldBeMinimum6Characters.string
        }
        return message
    }
    
    class func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        return NSPredicate(format:"SELF MATCHES %@", emailRegEx).evaluateWithObject(testStr)
    }
    
    class func isValidPhoneNumber(testStr:String) -> Bool {
        
        return testStr.characters.count < 7 ? false : true
    }

}
