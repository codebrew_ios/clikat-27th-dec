//
//  Categorie.swift
//  Clikat
//
//  Created by Night Reaper on 23/05/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation
import SwiftyJSON

enum CategoryKeys : String {
    
    case category_id = "category_id"
    case category_name = "category_name"
    case order = "order"
    case category = "category"
    case category_flow = "category_flow"

}



class Categorie : NSObject {
    
    var category_name : String?
    var category_id : String?
    var order : String?
    var category_flow : String?
    
    init(attributes : SwiftyJSONParameter){
        
        self.category_id = attributes?[CategoryKeys.category_id.rawValue]?.stringValue
        self.category_name = attributes?[CategoryKeys.category_name.rawValue]?.stringValue
        self.order = attributes?[CategoryKeys.order.rawValue]?.stringValue
        self.category_flow = attributes?[CategoryKeys.category_flow.rawValue]?.stringValue

    }
    
    override init() {
        super.init()
        
    }
    
}

class CategoriesListing {
    
    var arrayCategories : [Categorie]?
    
    init(attributes : SwiftyJSONParameter){
        guard let rawData = attributes else { return }
        let json = JSON(rawData)
        
        let dict = json[CategoryKeys.category.rawValue]
        let cats = dict.arrayValue
        var arrayCategories : [Categorie] = []
        
        for element in cats{
            let supplier = Categorie(attributes: element.dictionaryValue)
            arrayCategories.append(supplier)
        }
        self.arrayCategories = arrayCategories
    }
    
}


