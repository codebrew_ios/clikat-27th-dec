//
//  Address.swift
//  Clikat
//
//  Created by cblmacmini on 5/2/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import SwiftyJSON

enum AddressKeys : String {
    
    case id = "id"
    case address_line_1 = "address_line_1"
    case pincode = "pincode"
    case directions_for_delivery = "directions_for_delivery"
    case address_line_2 = "address_line_2"
    case landmark = "landmark"
    case is_deleted = "is_deleted"
    case name = "name"
    case address_link = "address_link"
    case customer_address = "customer_address"
    case area_id = "area_id"
}

class Address: NSObject {

    var name : String?
    var landMark : String?
    var houseNo : String?
    var buildingName : String?
    var address : String?
    var area : String?
    var city : String?
    var country : String?
    var id : String?
    var isDeleted : String?
    
    var placeLink : String?
    var customerAddress : String?
    
    var areaId : String?
    
    var addressString : String?
    
    init(attributes : SwiftyJSONParameter) {
        
        self.name = attributes?[AddressKeys.name.rawValue]?.stringValue
        self.landMark = attributes?[AddressKeys.landmark.rawValue]?.stringValue
        self.address = attributes?[AddressKeys.customer_address.rawValue]?.stringValue
        self.area = attributes?[AddressKeys.address_line_1.rawValue]?.stringValue
        
        let pincodeStrings = attributes?[AddressKeys.pincode.rawValue]?.stringValue.componentsSeparatedByString(",@#")
        self.houseNo = pincodeStrings?.first
        self.buildingName = pincodeStrings?.last
        self.id = attributes?[AddressKeys.id.rawValue]?.stringValue
        self.isDeleted = attributes?[AddressKeys.is_deleted.rawValue]?.stringValue
        self.placeLink = attributes?[AddressKeys.address_link.rawValue]?.stringValue
        let addressLineSecond = attributes?[AddressKeys.address_line_2.rawValue]?.stringValue.componentsSeparatedByString(",@#")
        self.city = addressLineSecond?.first
        self.country = addressLineSecond?.last
        
        self.addressString = UtilityFunctions.appendOptionalStrings(withArray: [landMark,houseNo,buildingName,city,country])
        
        self.areaId = attributes?[AddressKeys.area_id.rawValue]?.stringValue
    }
    
    init(name : String?,addressString : String?) {
        self.addressString = addressString
        self.name = name
    }
    
    init(name : String?,address : String?,landmark : String?,houseNo : String?,buildingName : String?,city : String?,country : String?,placeLink : String?,area : String?) {
        self.address = address
        self.name = name
        self.landMark = landmark
        self.houseNo = houseNo
        self.buildingName = buildingName
        self.area = area
        self.city = city
        self.country = country
        self.placeLink = placeLink
        self.addressString = UtilityFunctions.appendOptionalStrings(withArray: [landMark,houseNo,buildingName,area,city,country])
    }
    
    class func validateAddress(name : UITextField?,houseNo : UITextField?,building : UITextField?,address : UILabel?,landmark : UITextField?,city : UITextField? , country : UITextField?) -> String{
        guard let name = name?.text where name.characters.count != 0 else {
            return L10n.PleaseEnterYourName.string
        }
        guard let houseno = houseNo?.text where houseno.characters.count != 0 else {
            return L10n.PleaseEnterYourHouseNo.string
        }
        guard let buildingName = building?.text where buildingName.characters.count != 0 else {
            return L10n.PleaseEnterYourBuildingName.string
        }
        guard let address = address?.text where address.characters.count != 0 else {
            return L10n.PleaseSelectYourLocation.string
        }
        guard let landmark = landmark?.text where landmark.characters.count != 0 else {
            return L10n.PleaseEnterALandmarkName.string
        }
        guard let city = city?.text where city.characters.count != 0 else {
            return L10n.PleaseEnterYourCity.string
        }
        guard let country = country?.text where country.characters.count != 0 else {
            return L10n.PleaseEnterYourCounrty.string
        }
        return ""
    }
    
    override init() {
        super.init()
    }
}
