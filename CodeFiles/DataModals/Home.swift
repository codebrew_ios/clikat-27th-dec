//
//  Home.swift
//  Clikat
//
//  Created by Night Reaper on 19/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation
import SwiftyJSON
import RMMapper

enum HomeKeys : String {
    case english = "english"
    case arabic = "arabic"
    case topBanner = "topBanner"
    case offerArabic = "offerArabic"
    case offerEnglish = "offerEnglish"
    case languageList = "languageList"
    case SupplierInEnglish = "SupplierInEnglish"
    case SupplierInArabic = "SupplierInArabic"
    
    case pendingOrder = "pendingOrder"
    case scheduleOrders = "scheduleOrders"
}


class Home : NSObject , RMMapping {
    
    
    
    var arrayBanners : [Banner]?
    var arrayServiceTypesEN : [ServiceType]?
    var arrayServiceTypesAR : [ServiceType]?
    var arrayOffersEN : [Product]?
    var arrayOffersAR : [Product]?    
    
    var arrayRecommendedEN : [Supplier]?
    var arrayRecommendedAR : [Supplier]?
    
    
    var arrayPurchaseHistory : [Product]?
    var arrayLanguages : [ApplicationLanguage]?
    
    var scheduleOrders : String?
    var pendingOrder : String?
    
    
    init(sender : SwiftyJSONParameter){
        
        guard let rawData = sender else { return }
        let json = JSON(rawData)
        
        let dict = json[APIConstants.DataKey]
        let arrCategoryEnglish = dict[HomeKeys.english.rawValue].arrayValue
        let arrCategoryArabic = dict[HomeKeys.arabic.rawValue].arrayValue
        let banners = dict[HomeKeys.topBanner.rawValue].arrayValue
        
        let arrOffersArabic = dict[HomeKeys.offerArabic.rawValue].arrayValue
        let arrOffersEnglish = dict[HomeKeys.offerEnglish.rawValue].arrayValue
        
        let arrRecArabic = dict[HomeKeys.SupplierInArabic.rawValue].arrayValue
        let arrRecEnglish = dict[HomeKeys.SupplierInEnglish.rawValue].arrayValue
                
        arrayLanguages = LanguageListing(attributes: dict.dictionaryValue).languages
        
        arrayServiceTypesEN = []
        arrayServiceTypesAR = []
        arrayBanners = []
        arrayOffersEN = []
        arrayOffersAR = []
        arrayRecommendedAR = []
        arrayRecommendedEN = []
        
        for (_ , element) in banners.enumerate(){
            let banner = Banner(attributes: element.dictionaryValue)
            arrayBanners?.append(banner)
        }
        
        if Localize.currentLanguage() == Languages.Arabic {
            arrayBanners = arrayBanners?.reverse()
        }
        
        for (recEN,recAR) in zip(arrRecEnglish,arrRecArabic) {
            let recTypeEnglish = Supplier(attributes: recEN.dictionaryValue)
            let recTypeArabic = Supplier(attributes: recAR.dictionaryValue)
            arrayRecommendedAR?.append(recTypeArabic)
            arrayRecommendedEN?.append(recTypeEnglish)
        }
        
        for (offersEN,offersAR) in zip(arrOffersEnglish,arrOffersArabic) {
            let offerTypeEnglish = Product(attributes: offersEN.dictionaryValue)
            let offerTypeArabic = Product(attributes: offersAR.dictionaryValue)
            arrayOffersAR?.append(offerTypeArabic)
            arrayOffersEN?.append(offerTypeEnglish)
        }
        
        for (categoryEN,categoryAR) in zip(arrCategoryEnglish,arrCategoryArabic) {
            let serviceTypeEnglish = ServiceType(attributes: categoryEN.dictionaryValue)
            let serviceTypeArabic = ServiceType(attributes: categoryAR.dictionaryValue)
            arrayServiceTypesEN?.append(serviceTypeEnglish)
            arrayServiceTypesAR?.append(serviceTypeArabic)
        }
        
    }
    
    override init(){
        super.init()
    }
    func  getProducts() -> [Product] {
        
        return [
            Product() , Product(), Product()
        ]
    }
    
    
    func getServiceData () -> [ServiceType] {
        
        let list = []
        return list as! [ServiceType]
    }
    
    
}