//
//  LoyalityPoints.swift
//  Clikat
//
//  Created by Night Reaper on 25/05/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation


enum LoyalityPointsKeys : String {
    case image_path = "image_path"
    case product_id = "product_id"
    case product_desc = "product_desc"
    case measuring_unit = "measuring_unit"
    case name = "name"
    case supplier_id = "supplier_id"
    case loyalty_points = "loyalty_points"
    case supplier_branch_id = "supplier_branch_id"
    case product = "product"
    case supplier_name = "supplier_name"
    case orders = "orders"
}

class LoyalityPoints : Product {
    
    
    var product_desc : String?
    var measuring_unit : String?
    var supplier_id : String?
    var supplier_branch_id : String?
  
    
    
    override init(attributes : SwiftyJSONParameter){
        super.init(attributes: attributes)
        self.product_desc = attributes?[LoyalityPointsKeys.product_desc.rawValue]?.stringValue
        self.measuring_unit = attributes?[LoyalityPointsKeys.measuring_unit.rawValue]?.stringValue
        self.supplier_id = attributes?[LoyalityPointsKeys.supplier_id.rawValue]?.stringValue
        self.supplier_branch_id = attributes?[LoyalityPointsKeys.supplier_branch_id.rawValue]?.stringValue
        self.supplierName = attributes?[LoyalityPointsKeys.supplier_name.rawValue]?.stringValue
    }
    
    override init() {
        super.init()
    }
    
}


class LoyalityPointsListing : NSObject {
    var arrProduct : [LoyalityPoints]?
    var totalPoints : String?
    var orders : [OrderDetails]?
    
    init(attributes : SwiftyJSONParameter) {
        super.init()
        self.totalPoints = attributes?[LoyalityPointsKeys.loyalty_points.rawValue]?.stringValue
        orders = []
        for order in attributes?[LoyalityPointsKeys.orders.rawValue]?.arrayValue ?? [] {
            orders?.append(OrderDetails(attributes: order.dictionaryValue))
        }
        
        guard let json = attributes,jsonArr = json[LoyalityPointsKeys.product.rawValue]?.arrayValue else { return }
        var tempArr : [LoyalityPoints] = []
        for product in jsonArr {
            let lPproduct = LoyalityPoints(attributes: product.dictionaryValue)
            tempArr.append(lPproduct)
        }
        arrProduct = tempArr
    }
    
    override init() {
        super.init()
    }
}
