//
//  Product.swift
//  Clikat
//
//  Created by Night Reaper on 20/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation
import SwiftyJSON
import RMMapper


enum ProductKeys : String {
    
    // Inner Packet
    case detailed_sub_category_id = "detailed_sub_category_id"
    case id = "id"
    case product_id = "product_id"
    case bar_code = "bar_code"
    case sku = "sku"
    case detailed_name = "detailed_name"
    case name = "name"
    case product_desc = "product_desc"
    case image_path = "image_path"
    case price = "price"
    case delivery_charges = "delivery_charges"
    case is_package = "is_package"
    case commission = "commission"
    case commission_type = "commission_type"
    case display_price = "display_price"
    case handling = "handling"
    case handling_supplier = "handling_supplier"
    case can_urgent = "can_urgent"
    case urgent_price = "urgent_price"
    case urgent_type = "urgent_type"
    case urgent_value = "urgent_value"
    
    case product_type = "product_type"
    case handling_admin = "handling_admin"
    case measuring_unit = "measuring_unit"
    case supplier_branch_id = "supplier_branch_id"
    case product = "product"
    case DetailSubName = "DetailSubName"
    case quantity = "quantity"
    //Laundry
    case charges_below_min_order = "charges_below_min_order"
    case min_order = "min_order"
    case category_id = "category_id"
    case sub_category_id = "sub_category_id"
    
    case fixed_price = "fixed_price"
    case hourly_price = "hourly_price"
    case price_type = "price_type"
    case min_hour = "min_hour"
    case max_hour = "max_hour"
    case price_per_hour = "price_per_hour"
    case supplier_name = "supplier_name"
    case product_image = "product_image"
}

class Product : Cart, RMMapping{
    
    var detailedSubCatId : String?
    var barCode : String?
    
    var detailedName : String?
    var desc : String?
    var images : [String]?
    var isPackage : String?
    var commission : String?
    var commissionType : String?
    var handling : String?
    
    
    //Laundry
    var subCategoryId : String?
    var chargesBelowMinimumOrder : String?
    var minOrder : String?
    
    //Offers
    var offerPrice : String?
    var supplierName : String?
    
    var isOffer : Bool? = false
    
    
    override init (attributes : SwiftyJSONParameter){
        
        super.init(attributes: attributes)
        self.detailedSubCatId = attributes?[ProductKeys.detailed_sub_category_id.rawValue]?.stringValue
        self.barCode = attributes?[ProductKeys.bar_code.rawValue]?.stringValue
        self.sku = attributes?[ProductKeys.sku.rawValue]?.stringValue
        self.detailedName = attributes?[ProductKeys.detailed_name.rawValue]?.stringValue
        self.desc = attributes?[ProductKeys.product_desc.rawValue]?.stringValue
        self.isPackage = attributes?[ProductKeys.is_package.rawValue]?.stringValue
        self.commission = attributes?[ProductKeys.commission.rawValue]?.stringValue
        self.commissionType = attributes?[ProductKeys.commission_type.rawValue]?.stringValue
        self.offerPrice = attributes?[ProductKeys.display_price.rawValue]?.stringValue
        self.handling = attributes?[ProductKeys.handling.rawValue]?.stringValue
        self.measuringUnit = attributes?[ProductKeys.measuring_unit.rawValue]?.stringValue
        self.supplierName = attributes?[ProductKeys.supplier_name.rawValue]?.stringValue
        self.chargesBelowMinimumOrder = attributes?[ProductKeys.charges_below_min_order.rawValue]?.stringValue
        self.minOrder = attributes?[ProductKeys.min_order.rawValue]?.stringValue
        self.categoryId = attributes?[ProductKeys.category_id.rawValue]?.stringValue
        self.subCategoryId = attributes?[ProductKeys.sub_category_id.rawValue]?.stringValue
        self.productType = attributes?[ProductKeys.product_type.rawValue]?.stringValue
        isOffer = false
        if self.offerPrice?.toDouble() > self.displayPrice?.toDouble() {
            isOffer = true
        }
        
        if let quantity = attributes?[ProductKeys.quantity.rawValue]?.stringValue {
            self.quantity = quantity
        }else {
            updateValuesFromDB()
        }
        
        var tempImages : [String] = []
        guard let productImages = attributes?[ProductKeys.image_path.rawValue]?.arrayValue where productImages.count > 0 else{
            self.images = [/attributes?[ProductKeys.image_path.rawValue]?.stringValue.percentEscapedString()]
            return
        }
        for image in productImages {
            tempImages.append(/image.stringValue.percentEscapedString())
        }
        self.images = tempImages
        
        
        
    }
    
    init(cart : Cart?) {
        super.init()
        id = cart?.id
        name = cart?.name
        image = cart?.image
        quantity = cart?.quantity
        handlingSupplier = cart?.handlingSupplier
        handlingAdmin = cart?.handlingAdmin
        price = cart?.price
        deliveryCharges = cart?.deliveryCharges
        supplierBranchId = cart?.supplierBranchId
        displayPrice = cart?.displayPrice
        sku = cart?.sku
        measuringUnit = cart?.measuringUnit
        canUrgent = cart?.canUrgent
        priceType = cart?.priceType ?? .None
        strHourlyPrice = cart?.strHourlyPrice
        fixedPrice = cart?.fixedPrice
        urgentType = cart?.urgentType
        urgentValue = cart?.urgentValue
        category = cart?.category
        categoryId = cart?.categoryId
    }

    func updateValuesFromDB(){
        DBManager.sharedManager.getCart { (array) in
            weak var weakSelf = self
            for product in array {
                guard let currentProduct = product as? Cart,isSameSuplier = weakSelf?.isSameSupplierId(self.supplierBranchId) else { return }
                
                if currentProduct.id == weakSelf?.id  && isSameSuplier{
                    self.quantity = currentProduct.quantity
                    self.dateModified = currentProduct.dateModified
                }
            }
        }
    }
    
    override init(){
        super.init()
    }
    
    func isSameSupplierId(currentSupplierId : String?) -> Bool {
        guard let suppId = GDataSingleton.sharedInstance.currentSupplierId else {
            return true
        }
        if currentSupplierId == nil {
            return true
        }
        return suppId == currentSupplierId ? true : false
    }
}



class DetailedSubCategories : NSObject {
    
    
    var strTitle : String?
    var arrProducts : [Product]?
    
    init(strTitle : String?,products : [Product]?) {
        self.strTitle = strTitle
        self.arrProducts = products
    }
}



class ProductListing : NSObject {
    
    var arrDetailedSubCategories : [DetailedSubCategories]?
    
    
    override init() {
        super.init()
        
    }
    
    
    init(attributes : SwiftyJSONParameter){
        
        var arrayDSCs : [DetailedSubCategories] = []
        guard let rawData = attributes else { return }
        let json = JSON(rawData)
        
        let detailedSubCategories = json[APIConstants.DataKey][ProductKeys.product.rawValue].arrayValue
        for dsc in detailedSubCategories{
            
            let dict = dsc[ProductKeys.product.rawValue]
            let products = dict.arrayValue
            var arrayProducts : [Product] = []
            for element in products{
                let supplier = Product(attributes: element.dictionaryValue)
                arrayProducts.append(supplier)
            }
            arrayDSCs.append(DetailedSubCategories(strTitle: dsc[ProductKeys.DetailSubName.rawValue].stringValue, products: arrayProducts))
        }
        
        arrayDSCs.sortInPlace({
            $0.strTitle < $1.strTitle
        })
        self.arrDetailedSubCategories = arrayDSCs
    }
    
}


class BarCodeProductListing : NSObject {
    
    var arrProduct : [Product]?
    
    init(attributes : SwiftyJSONParameter , key : String){
        super.init()
        guard let rawData = attributes else { return }
        let json = JSON(rawData)
        
        let productListing = json[key].arrayValue
        
        var arrayProducts : [Product] = []
        for element in productListing{
            let supplier = Product(attributes: element.dictionaryValue)
            arrayProducts.append(supplier)
        }
        self.arrProduct = arrayProducts
    }
}


class PackageProductListing : NSObject {
    var arrProduct : [PackageProduct]?
    
    init(attributes : SwiftyJSONParameter , key : String) {
        super.init()
        guard let json = attributes,jsonArr = json[key]?.arrayValue else { return }
        var tempArr : [PackageProduct] = []
        for product in jsonArr {
            
            //TODO: Add Supplier Branch Id for packages
            let packageProduct = PackageProduct(attributes: product.dictionaryValue)
            tempArr.append(packageProduct)
        }
        
        arrProduct = tempArr
    }
    
    override init() {
        super.init()
    }
}
enum LaundryProductKeys : String{
    case supplier_name = "supplier_name"
    case supplier_address = "supplier_address"
}

class LaundryProductListing : NSObject {
    
    var arrDetailedSubCategories : [DetailedSubCategories]?
    var totalAmount : String?
    var supplierName : String?
    var supplierAddress : String?
    
    override init() {
        super.init()
    }
    
    init(attributes : SwiftyJSONParameter){
        
        supplierName = attributes?[APIConstants.DataKey]?[LaundryProductKeys.supplier_name.rawValue].stringValue
        supplierAddress = attributes?[APIConstants.DataKey]?[LaundryProductKeys.supplier_address.rawValue].stringValue
        var arrayDSCs : [DetailedSubCategories] = []
        guard let rawData = attributes else { return }
        let json = JSON(rawData)
        
        let detailedSubCategories = json[APIConstants.DataKey]["list"].arrayValue
        for dsc in detailedSubCategories{
            
            let dict = dsc[ProductKeys.product.rawValue]
            let products = dict.arrayValue
            var arrayProducts : [Product] = []
            for element in products{
                let supplier = Product(attributes: element.dictionaryValue)
                arrayProducts.append(supplier)
            }
            arrayDSCs.append(DetailedSubCategories(strTitle: dsc[ProductKeys.name.rawValue].stringValue, products: arrayProducts))
        }
        self.arrDetailedSubCategories = arrayDSCs
    }
}

class OfferListing {
    
    var arrOffers : [Product]?
    
    init(attributes : SwiftyJSONParameter){
        let tempArr = attributes?["list"]?.arrayValue ?? []
        arrOffers = []
        for product in tempArr {
            arrOffers?.append(Product(attributes: product.dictionaryValue))
        }
    }
}

class CompareProductListing {
    
    var arrProducts : [Product]?
    
    init(attributes : SwiftyJSONParameter){
        let tempArr = attributes?["details"]?["products"].arrayValue ?? []
        arrProducts = []
        for product in tempArr {
            arrProducts?.append(Product(attributes: product.dictionaryValue))
        }
    }
}

