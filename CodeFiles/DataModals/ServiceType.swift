//
//  ServiceType.swift
//  Clikat
//
//  Created by Night Reaper on 20/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation
import SwiftyJSON
import RMMapper


enum ServiceTypeKeys : String {
    case id = "id"
    case supplier_placement_level = "supplier_placement_level"
    case image = "image"
    case icon = "icon"
    case name = "name"
    case description = "description"
    case category_flow = "category_flow"
    case order = "order"
}

class ServiceType : NSObject , RMMapping{
    
    var id : String?
    var supplier_placement_level : String?
    var image : String?
    var icon : String?
    var name : String?
    var desc : String?
    var category_flow : String?
    var order : String?
    
    
    init (attributes : SwiftyJSONParameter){
        self.id = attributes?[ServiceTypeKeys.id.rawValue]?.stringValue
        self.supplier_placement_level = attributes?[ServiceTypeKeys.supplier_placement_level.rawValue]?.stringValue
        self.image = attributes?[ServiceTypeKeys.image.rawValue]?.stringValue
        self.icon = attributes?[ServiceTypeKeys.icon.rawValue]?.stringValue
        self.name = attributes?[ServiceTypeKeys.name.rawValue]?.stringValue
        self.desc = attributes?[ServiceTypeKeys.description.rawValue]?.stringValue
        self.category_flow = attributes?[ServiceTypeKeys.category_flow.rawValue]?.stringValue
        self.order = attributes?[ServiceTypeKeys.order.rawValue]?.stringValue

    }
    
    override init() {
        super.init()
    }
}