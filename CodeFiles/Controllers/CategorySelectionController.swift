//
//  CategorySelectionController.swift
//  Clikat
//
//  Created by cblmacmini on 6/2/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class CategorySelectionController: CategoryFlowBaseViewController {

    // MARK: Vars
    
    var headerHeight: CGFloat = 236
    var transitionDriver: TransitionDriver?
    var supplier : Supplier?
    var arrCategory : [Categorie]?
    var scrollOffsetY : CGFloat = 0.0
    var ISPushAnimation : Bool = false
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewNavBar: UIView!{
        didSet{
            viewNavBar.alpha = 0.0
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if ISPushAnimation {
            view.backgroundColor = UIColor.whiteColor()
            tableView.tableHeaderView = UIView(frame: CGRectMake(0, 0, ScreenSize.SCREEN_WIDTH, 64.0))
        }
        
        // Do any additional setup after loading the view.
    }

  
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animateWithDuration(0.3) {
            weak var weakSelf = self
            weakSelf?.viewNavBar.alpha = 1.0
        }
    }
}



// MARK: Helpers

extension CategorySelectionController {
    
    private func getScreen() -> UIImage? {
        let height = (headerHeight - tableView.contentOffset.y) < 0 ? 0 : (headerHeight - tableView.contentOffset.y)
        let backImageSize = CGSize(width: view.bounds.width, height: view.bounds.height - height)
        let backImageOrigin = CGPoint(x: 0, y: height + tableView.contentOffset.y)
        return view.takeSnapshot(CGRect(origin: backImageOrigin, size: backImageSize))
    }
}
// MARK: Public

extension CategorySelectionController {
    
     func popTransitionAnimation() {
        guard let transitionDriver = self.transitionDriver else {
            return
        }
        
        let backImage = getScreen()
        let offset = tableView.contentOffset.y > headerHeight ? headerHeight : tableView.contentOffset.y
        transitionDriver.popTransitionAnimationContantOffset(offset, backImage: backImage)
        self.navigationController?.popViewControllerAnimated(false)
    }
}

extension CategorySelectionController : UIScrollViewDelegate{
     func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView.contentOffset.y < -40 {
            // buttonAnimation
            popTransitionAnimation()
        }
        scrollOffsetY = scrollView.contentOffset.y
    }
}

//MARK: - Button Actions

extension CategorySelectionController {
    
    @IBAction func actionBack(sender: UIButton) {
        
        if ISPushAnimation {
            popVC()
            return
        }
        popTransitionAnimation()
    }
    @IBAction func actionMenu(sender: UIButton) {
        toggleSideMenuView()
    }
}

//Category>SubCategory>Suppliers>SupplierInfo>Ds-Pl

//MARK: - TableView Delegate and datasource
extension CategorySelectionController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCategory?.count ?? 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifiers.CategorySelectionCell)! as UITableViewCell
        (cell as? CategorySelectionCell)?.category = arrCategory?[indexPath.row]
        cell.backgroundColor = indexPath.row % 2 == 0 ? Colors.MainColor.color().colorWithAlphaComponent(0.5) : UIColor.whiteColor()
        (cell as? CategorySelectionCell)?.labelCategoryTitle.textColor = indexPath.row % 2 == 0 ? UIColor.whiteColor() : Colors.MainColor.color()
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 48
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let category = arrCategory?[indexPath.row]
        let VC = StoryboardScene.Main.instantiateSupplierInfoViewController()
        
        let flow = (category?.order == "3" || category?.order == "5") ? "Category>Suppliers>SupplierInfo>SubCategory>Ds-Pl" : category?.category_flow
        VC.passedData = PassedData(withCatergoryId: category?.category_id, categoryFlow: flow,supplierId: supplier?.id ,subCategoryId: nil ,productId: nil,branchId: supplier?.supplierBranchId,subCategoryName: nil , categoryOrder: category?.order,categoryName : category?.category_name)
        pushVC(VC)
    }
    
}
