//
//  TrackMyOrderViewController.swift
//  Clikat
//
//  Created by cbl73 on 4/23/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class TrackMyOrderViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewPlaceholder : UIView!

    
    var dataSource : TableViewDataSource = TableViewDataSource(){
        didSet{
            tableView?.dataSource = dataSource
            tableView?.delegate = dataSource
        }
    }
    
    
    var orderListing : OrderListing? {
        didSet{
            dataSource.items = orderListing?.orders
            tableView?.reloadTableViewData(inView: view)
            
            guard let orders = orderListing?.orders where orders.count > 0 else{
                viewPlaceholder?.hidden = false
                return
            }
            viewPlaceholder?.hidden = true

        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerNib(UINib(nibName: CellIdentifiers.OrderParentCell, bundle: nil), forCellReuseIdentifier: CellIdentifiers.OrderParentCell)
        configureTableViewInitialization()
        webService()
    }
}


//MARK: - Webservice Methods
extension TrackMyOrderViewController {
    
    
    func webService (){
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.OrderTrackingList(FormatAPIParameters.OrderTrackingList().formatParameters())) { (response) in
            
            weak var weak : TrackMyOrderViewController? = self
            switch response{
                
            case .Success(let listing):
                weak?.orderListing = listing as? OrderListing
                
                break
            default :
                break
            }
        }
        
    }
    
}


//MARK: - TableView Configuration

extension TrackMyOrderViewController{
    
    
    func configureTableViewInitialization(){
        dataSource = TableViewDataSource(items: orderListing?.orders , height: 283, tableView: tableView, cellIdentifier: CellIdentifiers.OrderParentCell , configureCellBlock: { (cell, item) in
            
            weak var weakSelf : TrackMyOrderViewController? = self
            weakSelf?.configureCell(withCell : cell , item : item)
            
            }, aRowSelectedListener: { (indexPath) in
                weak var weak : TrackMyOrderViewController? = self
                let orderDetailVc = StoryboardScene.Order.instantiateOrderDetailController()
                orderDetailVc.orderDetails = weak?.orderListing?.orders?[indexPath.row]
                orderDetailVc.type = .OrderTracking
                weak?.pushVC(orderDetailVc)
        })
        tableView.reloadData()
    }
    
    
    func configureCell(withCell cell : AnyObject , item : AnyObject? ){
        
        (cell as? OrderParentCell)?.cellType = .OrderTracking
        (cell as? OrderParentCell)?.order = item as? OrderDetails
        (cell as? OrderParentCell)?.orderDelegate = self
        (cell as? OrderParentCell)?.btnOrderType.hidden = (item as? OrderDetails)?.status == .Tracked ? true : false
    }
    
}


//MARK: - Button Actions

extension TrackMyOrderViewController{
    
    @IBAction func actionCart(sender: AnyObject) {
        pushVC(StoryboardScene.Options.instantiateCartViewController())
    }

    
    @IBAction func actionMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
}

//MARK: Order cell delegate
extension TrackMyOrderViewController : OrderParentCellDelegate {
    
    func actionOrderTypeButton(cellType: OrderCellType, order: OrderDetails?) {
        let index = orderListing?.orders?.indexOf({ (orderDetails) -> Bool in
            orderDetails.orderId == order?.orderId
        })
        (dataSource.items as? [OrderDetails])?[/index].status = .Tracked
        tableView.reloadData()
        webServiceTrackOrder(order?.orderId)
    }
    func actionOrderTypeButton(cell: OrderParentCell, orderId: String?) {
    }
}

//MARK: - Track Order web service
extension TrackMyOrderViewController {
    
    func webServiceTrackOrder(orderId : String?){
        
        AdjustEvent.TrackOrder.sendEvent()
        APIManager.sharedInstance.opertationWithRequest(withApi: API.OrderTrack(FormatAPIParameters.OrderTrack(orderId: orderId).formatParameters())) { (response) in
            weak var weakSelf = self
            switch response {
            case .Success(let object):
                weakSelf?.view.makeToast(object as? String)
            case .Failure(_):
                break
            }
        }
    }
}