//
//  ScheduledOrderController.swift
//  Clikat
//
//  Created by cblmacmini on 8/16/16.
//  Copyright © 2016 Rajat. All rights reserved.
//

import UIKit

class ScheduledOrderController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewPlaceholder : UIView!
    
    var dataSource : TableViewDataSource = TableViewDataSource(){
        didSet{
            tableView?.dataSource = dataSource
            tableView?.delegate = dataSource
        }
    }
    var orderListing : OrderListing? {
        didSet{
            dataSource.items = orderListing?.orders
            tableView?.reloadTableViewData(inView: view)
            
            guard let orders = orderListing?.orders where orders.count > 0 else{
                viewPlaceholder?.hidden = false
                return
            }
            viewPlaceholder?.hidden = true
            
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AdjustEvent.ScheduledOrders.sendEvent()
        tableView.registerNib(UINib(nibName: CellIdentifiers.OrderParentCell, bundle: nil), forCellReuseIdentifier: CellIdentifiers.OrderParentCell)
        configureTableViewInitialization()
        webService()
    }
}


//MARK: - Webservice Methods
extension ScheduledOrderController {
    
    
    func webService (){
        
        let api = API.ScheduledOrders(FormatAPIParameters.OrderUpcoming().formatParameters())
        
        APIManager.sharedInstance.opertationWithRequest(withApi: api) {[weak self] (response) in
            
            switch response{

            case .Success(let listing):
                self?.orderListing = listing as? OrderListing
            default :
                break
            }
        }
        
    }
    
}


//MARK: - TableView Configuration

extension ScheduledOrderController{
    
    
    func configureTableViewInitialization(){
        dataSource = TableViewDataSource(items: orderListing?.orders , height: 283, tableView: tableView, cellIdentifier: CellIdentifiers.OrderParentCell , configureCellBlock: { [weak self] (cell, item) in
            
            
            self?.configureCell(withCell : cell , item : item)
            
            }, aRowSelectedListener: { [weak self] (indexPath) in
                self?.configureCellSelection(indexPath)
        })
        tableView.reloadData()
    }
    
    
    func configureCell(withCell cell : AnyObject , item : AnyObject? ){
        
        (cell as? OrderParentCell)?.cellType = .OrderScheduled
        (cell as? OrderParentCell)?.order = item as? OrderDetails
        (cell as? OrderParentCell)?.orderDelegate = self
        (cell as? OrderParentCell)?.btnOrderType.hidden = (item as? OrderDetails)?.status == .Confirmed ? true : false
    }
    
    func configureCellSelection(indexPath : NSIndexPath){
        let orderDetailVc = StoryboardScene.Order.instantiateOrderDetailController()
        orderDetailVc.orderDetails = self.orderListing?.orders?[indexPath.row]
        orderDetailVc.type = .OrderScheduled
        orderDetailVc.cancelOrder = { [unowned self] in
         self.handleCancelOrder(indexPath, orderId: self.orderListing?.orders?[indexPath.row].orderId , orderStatus: self.orderListing?.orders?[indexPath.row].status )
        }
        self.pushVC(orderDetailVc)
    }
    
    func handleCancelOrder(indexPath : NSIndexPath,orderId : String?, orderStatus : OrderDeliveryStatus?){
        
        tableView.beginUpdates()
        dataSource.items?.removeAtIndex(indexPath.row)
        orderListing?.orders?.removeAtIndex(indexPath.row)
        tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Top)
        tableView.endUpdates()
        viewPlaceholder?.hidden = dataSource.items?.count > 0 ? true : false
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.CancelOrder(FormatAPIParameters.CancelOrder(orderId: orderId , isScheduled: orderStatus == .Schedule ? "1" : "0").formatParameters())) { (response) in
            switch response {
            case .Success(_):
                UtilityFunctions.showSweetAlert(L10n.Success.string, message: L10n.YouHaveCancelledYourOrderSuccessfully.string, style: .Success)
            case .Failure(_):
                break
            }
        }
    }
    
}


//MARK: - Button Actions

extension ScheduledOrderController{
    
    @IBAction func actionCart(sender: AnyObject) {
        pushVC(StoryboardScene.Options.instantiateCartViewController())
    }
    
    @IBAction func actionMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
}

//MARK: - Order Parent Cell Delegate {

extension ScheduledOrderController : OrderParentCellDelegate {
    
    func actionOrderTypeButton(cell: OrderParentCell, orderId: String?) {
        if cell.btnOrderType.titleLabel?.text == L10n.CONFIRMORDER.string {
            let VC = StoryboardScene.Order.instantiatePaymentMethodController()
            VC.orderId = orderId
            VC.isConfirmOrder = true
            pushVC(VC)
            return
        }
    }
    
    func actionOrderTypeButton(cellType: OrderCellType, order: OrderDetails?) {
        
    }
    
    
    func cancelOrderWebservice(cell : OrderParentCell, orderId :String?){
        guard let indexpath = tableView.indexPathForCell(cell) else { return }
        tableView.beginUpdates()
        dataSource.items?.removeAtIndex(indexpath.row)
        orderListing?.orders?.removeAtIndex(indexpath.row)
        tableView.deleteRowsAtIndexPaths([indexpath], withRowAnimation: .Top)
        tableView.endUpdates()
        viewPlaceholder?.hidden = dataSource.items?.count > 0 ? true : false
        APIManager.sharedInstance.opertationWithRequest(withApi: API.CancelOrder(FormatAPIParameters.CancelOrder(orderId: orderId,isScheduled: "0").formatParameters())) { (response) in
            switch response {
            case .Success(_):
                break
            case .Failure(_):
                break
            }
        }
    }
    func handleCancelOrder(cell : OrderParentCell?){
        
    }
}
