//
//  MyFavoritesViewController.swift
//  Clikat
//
//  Created by cbl73 on 4/23/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class MyFavoritesViewController: ExpandingViewController {

//    @IBOutlet var collectionView: UICollectionView!
    var dataSource = CollectionViewDataSource(){
        didSet{
//            collectionView?.dataSource = dataSource
//            collectionView?.delegate = dataSource
        }
    }
    
    var arraySuppliers : [Supplier]? {
        didSet{
            dataSource.items = arraySuppliers
            viewPlaceholder.hidden = arraySuppliers?.count == 0 ? false : true
            collectionView?.reloadData()
        }
    }
    
    @IBOutlet weak var viewPlaceholder : UIView!
    @IBOutlet weak var imageViewbackground: UIImageView!{
        didSet{
            imageViewbackground.image = UIImageEffects.imageByApplyingDarkEffectToImage(imageViewbackground.image)
        }
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AdjustEvent.Favourites.sendEvent()
        let nib = UINib(nibName: CellIdentifiers.SupplierCollectionCell, bundle: nil)
        collectionView?.registerNib(nib, forCellWithReuseIdentifier: CellIdentifiers.SupplierCollectionCell)
        addGestureToView(collectionView!)
//        configureCollectionView()
        webServiceFavorites()
    }
    
}



//MARK: - Web Service
extension MyFavoritesViewController {
    func webServiceFavorites (){
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.MyFavorites(FormatAPIParameters.MyFavorites().formatParameters())) { (response) in
            
            weak var weak : MyFavoritesViewController? = self
            switch response{
                
            case .Success(let listing):
                weak?.arraySuppliers = (listing as? SupplierListing)?.suppliers
                
                weak?.collectionView?.reloadData()
                break
            default :
                break
            }
        }
    }
}
//MARK: - CollectionView delegates
extension MyFavoritesViewController {
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arraySuppliers?.count ?? 0
    }
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CellIdentifiers.SupplierCollectionCell, forIndexPath: indexPath)
        (cell as? SupplierCollectionCell)?.supplier = arraySuppliers?[indexPath.row]
        // configure cell
        return cell
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        guard let cell = collectionView.cellForItemAtIndexPath(indexPath) as? SupplierCollectionCell else { return }
        if !cell.isOpened {
            cell.cellIsOpen(true)
        }else {
            handleSelection(indexPath)
        }
    }
    
    func handleSelection(indexPath : NSIndexPath){
        let VC = StoryboardScene.Options.instantiateCategorySelectionController()
        VC.arrCategory = arraySuppliers?[indexPath.row].categories
        VC.supplier = arraySuppliers?[indexPath.row]
        
        if Localize.currentLanguage() == Languages.Arabic {
            VC.ISPushAnimation = true
            pushVC(VC)
            return
        }
        
        pushToViewController(VC)
    }
}




//MARK: - Button Actions
extension MyFavoritesViewController{
    
    @IBAction func actionMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    @IBAction func actionCart(sender: AnyObject) {
        pushVC(StoryboardScene.Options.instantiateCartViewController())
    }
    
}

//MARK: - Gesture Recognizer helpers

extension MyFavoritesViewController {
    
    private func addGestureToView(toView: UIView) {
        let gesutereUp = Init(UISwipeGestureRecognizer(target: self, action: #selector(MyFavoritesViewController.swipeHandler(_:)))) {
            $0.direction = .Up
        }
        
        let gesutereDown = Init(UISwipeGestureRecognizer(target: self, action: #selector(MyFavoritesViewController.swipeHandler(_:)))) {
            $0.direction = .Down
        }
        toView.addGestureRecognizer(gesutereUp)
        toView.addGestureRecognizer(gesutereDown)
    }
    
    func swipeHandler(sender: UISwipeGestureRecognizer) {
//        let indexPath = NSIndexPath(forRow: currentIndex, inSection: 0)
        guard let indexPath = collectionView?.indexPathForItemAtPoint(sender.locationInView(collectionView)) else { return }
        guard case let cell as SupplierCollectionCell = collectionView?.cellForItemAtIndexPath(indexPath) else {
            return
        }
        // double swipe Up transition
        if cell.isOpened == true && sender.direction == .Up {
           handleSelection(indexPath)
        }
        
        let open = sender.direction == .Up ? true : false
        cell.cellIsOpen(open)
    }
}
