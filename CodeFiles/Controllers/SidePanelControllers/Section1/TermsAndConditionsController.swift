//
//  TermsAndConditionsController.swift
//  Clikat
//
//  Created by cblmacmini on 7/20/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
enum AppInfo : String{
    case TermsAndConditions = "http://clikat.com/term.html"
    case AboutUs = "http://clikat.com/privacy.html"
    
    func url() -> NSURL?{
        
        return NSURL(string:self.rawValue)
    }
}

class TermsAndConditionsController: BaseViewController {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var labelTitle: UILabel!
    
    var type : AppInfo = .TermsAndConditions
    override func viewDidLoad() {
        super.viewDidLoad()
        labelTitle.text = type == .AboutUs ? L10n.AboutUs.string : L10n.TermsAndConditions.string
        guard let url = type.url() else { return }
        let requestObj = NSURLRequest(URL: url)
        webView.loadRequest(requestObj)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
    @IBAction func actionMenu(sender: UIButton) {
        toggleSideMenuView()
    }

}
