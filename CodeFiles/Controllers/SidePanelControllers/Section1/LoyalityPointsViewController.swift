//
//  LoyalityPointsViewController.swift
//  Clikat
//
//  Created by cbl73 on 4/23/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit


class LoyalityPointsViewController: BaseViewController {

    @IBOutlet var btnRedeem: UIButton!{
        didSet{
            btnRedeem?.kern(ButtonKernValue)
        }
    }
    
    @IBOutlet var collectionView: UICollectionView!{
        didSet{
            collectionView?.dataSource = self
            collectionView?.delegate = self
            collectionView?.allowsMultipleSelection = true
        }
    }
    
    var selectedProducts : [LoyalityPoints] = []
    var loyalityPointsListing : LoyalityPointsListing? {
        didSet{
            collectionView?.reloadData()
        }
    }

    var selectedIndexPath : [NSIndexPath] = []{
        didSet{
            
        }
    }
    var header : LoyalityPointsHeader?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AdjustEvent.LoyaltyPoints.sendEvent()
        let width = CGRectGetWidth(collectionView!.bounds)
        let layout = collectionView?.collectionViewLayout as! UICollectionViewFlowLayout
        layout.headerReferenceSize = CGSize(width: width, height: ScreenSize.SCREEN_WIDTH * 0.6)
        webServiceLoyalityPoints()
    }
    
}
//MARK: - Web Service
extension LoyalityPointsViewController {
    func webServiceLoyalityPoints(){
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.LoyalityPointScreen(FormatAPIParameters.LoyalityPointScreen().formatParameters())) { (response) in
            
            weak var weak : LoyalityPointsViewController? = self
            switch response{
                
            case .Success(let listing):
                weak?.loyalityPointsListing = (listing as? LoyalityPointsListing)
            default :
                break
            }
        }
        
    }
}

//MARK: - Button Actions
extension LoyalityPointsViewController {
    
    @IBAction func actionMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    @IBAction func actionRedeem(sender: AnyObject) {
        
        if selectedProducts.count == 0 {
           return
        }
        
        
        guard let strTotalPoints = loyalityPointsListing?.totalPoints where strTotalPoints != "0" else {
            view.makeToast(L10n.YouHavenTEarnedAnyLoyaltyPointsYet.string)
            return
        }
       
        var totalSelectedPoints = 0
        for product in selectedProducts {
            guard let points = Int(product.loyalty_points ?? "0") else { return }
            totalSelectedPoints += points
        }
        if Int(strTotalPoints) < totalSelectedPoints {
            view.makeToast(L10n.SorryNotEnoughPointsToRedeem.string)
            return
        }
        
        let VC = StoryboardScene.Order.instantiateDeliveryViewController()
        VC.cartFlowType = .LoyaltyPoints
        let summary = LoyaltyPointsSummary(items: selectedProducts, netTotal: String(totalSelectedPoints) + " " + L10n.Points.string)
        summary.totalPoints = totalSelectedPoints.toString
        VC.loyaltyPointsSummary = summary
        
        pushVC(VC)
    }
    
    @IBAction func actionBtnOrders(sender: UIButton) {
        
        let VC = StoryboardScene.Options.instantiateLoyaltyPointOrdersController()
        VC.orders = loyalityPointsListing?.orders
        pushVC(VC)
    }
    
}

//MARK: - CollectionView DataSource and Delegates
extension LoyalityPointsViewController : UICollectionViewDelegate , UICollectionViewDataSource {
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CellIdentifiers.LoyalityPointsCell ,forIndexPath: indexPath)
        (cell as? LoyalityPointsCell)?.loyalityPoints = loyalityPointsListing?.arrProduct?[indexPath.row]
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return loyalityPointsListing?.arrProduct?.count ?? 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        guard let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else{
            return CGSizeZero
        }
        let numberOfColoumns = 2
        let padding = (layout.sectionInset.left + layout.sectionInset.right + CGFloat(numberOfColoumns - 1) * (layout.minimumLineSpacing))
        let width = floor(((CGRectGetWidth(view.bounds) - padding)  / CGFloat(numberOfColoumns)))
        let height = width + 64.0
        return CGSizeMake(width, height)
    }


    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionElementKindSectionHeader:
            let headerView  = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: CellIdentifiers.LoyalityPointsHeader , forIndexPath: indexPath) as! LoyalityPointsHeader
            headerView.backgroundColor = UIColor.clearColor()
            headerView.lblLoyalityPoints?.text = loyalityPointsListing?.totalPoints ?? "0"
            updatePoints()
            header = headerView
            return headerView
            
        default: return UICollectionReusableView()
        }
    }
    
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        let cell = cell as? LoyalityPointsCell
        cell?.imgTick.hidden = selectedIndexPath.contains(indexPath) == true ? false : true
    }
    
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        handleCellSelection(indexPath)
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        handleCellSelection(indexPath)
    }
    
    func updatePoints() -> Bool{
        guard let totalPoints = loyalityPointsListing?.totalPoints?.toInt() else { return false}
        
        let price = selectedProducts.reduce(0, combine: { (initial, product) -> Int in
            
            return initial + (product.loyalty_points?.toInt() ?? 0)
        }) ?? 0
        
        if totalPoints < price {
            return false
        }
        header?.lblLoyalityPoints.text = (totalPoints - price).toString
        return true
    }
    
    
    func handleCellSelection(indexPath : NSIndexPath){
    
        guard let strTotalPoints = loyalityPointsListing?.totalPoints where strTotalPoints != "0" else {
            view.makeToast(L10n.YouHavenTEarnedAnyLoyaltyPointsYet.string)
            return
        }
        
        
        guard let currentProduct = loyalityPointsListing?.arrProduct?[indexPath.row],currentSupplierId = currentProduct.supplier_id else { return }
        
        
        
        
        
        
        for product in selectedProducts {
            if currentSupplierId != product.supplier_id {
                if selectedProducts.count > 0 {
                    view.makeToast(L10n.SelectProdutsFromSameSupplier.string)
                    return
                }
            }
        }
        
        
       

        
        if !selectedIndexPath.contains(indexPath){
            selectedIndexPath.append(indexPath)
            selectedProducts.append(currentProduct)
            
        }else {
            selectedIndexPath.removeObject(indexPath)
            selectedProducts.removeObject(currentProduct)
        }
        
        
        //Haspinder Singh
        if !updatePoints(){
            if selectedIndexPath.contains(indexPath){
                selectedIndexPath.removeObject(indexPath)
                selectedProducts.removeObject(currentProduct)
            }
            view.makeToast(L10n.SorryNotEnoughPointsToRedeem.string)
            return
        }
        
        guard let cell = collectionView.cellForItemAtIndexPath(indexPath) as? LoyalityPointsCell else{
            return
        }
        cell.imgTick?.hidden.toggle()
    }
    
}