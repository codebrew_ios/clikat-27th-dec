//
//  OrderHistoryViewController.swift
//  Clikat
//
//  Created by cbl73 on 4/23/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class OrderHistoryViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewPlaceholder : UIView!

    
    
    var dataSource : TableViewDataSource = TableViewDataSource(){
        didSet{
            tableView?.dataSource = dataSource
            tableView?.delegate = dataSource
        }
    }
     

    var orderListing : OrderListing? {
        didSet{
            dataSource.items = orderListing?.orders
            tableView?.reloadTableViewData(inView: view)
            
            guard let orders = orderListing?.orders where orders.count > 0 else{
                viewPlaceholder?.hidden = false
                return
            }
            viewPlaceholder?.hidden = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerNib(UINib(nibName: CellIdentifiers.OrderParentCell, bundle: nil), forCellReuseIdentifier: CellIdentifiers.OrderParentCell)
        configureTableViewInitialization()
        webService()
    }
}


//MARK: - Webservice Methods 
extension OrderHistoryViewController {

    
    func webService (){
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.OrderHistory(FormatAPIParameters.OrderHistory().formatParameters())) { (response) in
            
            weak var weak : OrderHistoryViewController? = self
            switch response{
                
            case .Success(let listing):
                weak?.orderListing = listing as? OrderListing
                
                break
            default :
                break
            }

            
        }
        
    }

}


//MARK: - TableView Configuration

extension OrderHistoryViewController{
    
    
    func configureTableViewInitialization(){
        dataSource = TableViewDataSource(items: orderListing?.orders, height: 283, tableView: tableView, cellIdentifier: CellIdentifiers.OrderParentCell , configureCellBlock: { (cell, item) in
            
            weak var weakSelf : OrderHistoryViewController? = self
            weakSelf?.configureCell(withCell : cell , item : item)
            
            }, aRowSelectedListener: { (indexPath) in
                weak var weak : OrderHistoryViewController? = self
                let orderDetailVc = StoryboardScene.Order.instantiateOrderDetailController()
                orderDetailVc.orderDetails = weak?.orderListing?.orders?[indexPath.row]
                orderDetailVc.type = .OrderHistory
                weak?.pushVC(orderDetailVc)
        })
        tableView.reloadData()
    }
    
    
    func configureCell(withCell cell : AnyObject , item : AnyObject? ){
        
        (cell as? OrderParentCell)?.cellType = .OrderHistory
        (cell as? OrderParentCell)?.order = item as? OrderDetails
        (cell as? OrderParentCell)?.orderDelegate = self
        
    }
    
    func itemClicked(withIndexPath : NSIndexPath){
        
        
        
    }
}



//MARK: - Button Actions


extension OrderHistoryViewController{
    

    
    @IBAction func actionCart(sender: AnyObject) {
        pushVC(StoryboardScene.Options.instantiateCartViewController())
    }

    
    @IBAction func actionMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
}
//MARK: - Order Parent cell delegate

extension OrderHistoryViewController : OrderParentCellDelegate {
    
    func actionOrderTypeButton(cellType: OrderCellType, order: OrderDetails?) {

        
        //Haspinder Singh
        //Check cart -> If cart == nil then direct reorder
        DBManager.sharedManager.getCart { (products) in
            if products.count > 0{
                
                UtilityFunctions.showAlert(nil, message: L10n.ReOrderingWillClearYouCart.string, success: {
                    weak var weakSelf = self
                    weakSelf?.reorderAllitems(order)
                }) {
                    
                }}
            else{
                
                self.reorderAllitems(order)
            }
        }
        
        
        
    }
    func actionOrderTypeButton(cell: OrderParentCell, orderId: String?) {
        
    }
    
    func reorderAllitems(orderDetails : OrderDetails?){
        
        AdjustEvent.Reorder.sendEvent()
        DBManager.sharedManager.deleteAllData(CoreDataEntity.Cart.rawValue)
        guard let products = orderDetails?.product else { return }
        GDataSingleton.sharedInstance.currentSupplierId = orderDetails?.supplierBranchId
        for product in products {
            guard let quantity = Int(product.quantity ?? "") else { return }
            product.supplierBranchId = orderDetails?.supplierBranchId
            DBManager.sharedManager.manageCart(product, quantity: quantity)
        }
        GDataSingleton.sharedInstance.currentCategoryId = products.first?.categoryId
        let VC = StoryboardScene.Options.instantiateCartViewController()
        pushVC(VC)
    }
}
