//
//  OrderSchedularViewController.swift
//  Clikat
//
//  Created by Night Reaper on 29/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material

enum CalendarMode  {
    case Weekly
    case Monthly
    
    static let allValues = [L10n.Monthly.string , L10n.Weekly.string]

}


enum Weekday : Int {
    
    case Sun = 0
    case Mon = 1
    case Tues = 2
    case Wed = 3
    case Thurs = 4
    case Fri = 5
    case Sat = 6
    
    func value () -> String{
        
        switch self {
        case .Sun:
            return "Sun"
        case .Mon:
           return "Mon"
        case .Tues:
           return "Tues"
        case .Wed:
           return "Wed"
        case .Thurs:
           return "Thurs"
        case .Fri:
           return "Fri"
        default:
          return  "Sat"
        }
        
    }
    
}

class OrderSchedularViewController: BaseViewController {

    @IBOutlet weak var calendarMenuView: JTCalendarMenuView!
    @IBOutlet var calendarContentView: JTHorizontalCalendarView!
    @IBOutlet var viewWeekly : UIView!
    @IBOutlet var btnWeekly: [UIButton]!
    @IBOutlet weak var btnBookingCycle: UIButton!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblBookingValues: UILabel!
    @IBOutlet var btnSetScheduled: MaterialButton!{
        didSet{
            btnSetScheduled.kern(ButtonKernValue)
        }
    }
    @IBOutlet weak var btnDate: UIButton! {
        didSet{
            btnDate.setTitle(UtilityFunctions.getTimeFormatted(DateFormat.TimeFormatUI, date: NSDate()), forState: .Normal)
        }
    }
    
    var selectedTime : NSDate? = NSDate()
    var calendarManager : JTCalendarManager!
    var dateSelectedArray : [NSDate] = []
    var mode : CalendarMode = .Monthly{
        didSet{
          
            if !(oldValue == mode){
                calendarContentView.hidden.toggle()
                viewWeekly.hidden.toggle()
                
                switch mode {
                case .Weekly:
                    selectedDayString()
                default:
                    selectedDateString()
                }
            }

        }
    }
    
    var orderSummary : OrderSummary?
    var orderId : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
//      addSupplierImage()
    }
 
    private func updateUI(){
        calendarManager = JTCalendarManager()
        calendarManager.delegate = self
        calendarManager.contentView = calendarContentView
        calendarManager.menuView = self.calendarMenuView
        calendarManager.setDate(NSDate())
        calendarContentView.scrollEnabled = true
//        lblMonth?.text = calendarManager.date().toString(format: "MMM, yyyy")
    }


}


extension OrderSchedularViewController : JTCalendarDelegate {
    
    
    func isInDateSelected (date : NSDate) -> Bool{
        for dateSelected in dateSelectedArray {
            if calendarManager.dateHelper.date(dateSelected, isTheSameDayThan : date){
                return true
            }
        }
        return false
    }
    
    func selectedDateString(){
        
        var dateSringArray : [String] = []
        for dateSelected in dateSelectedArray {
             dateSringArray.append(dateSelected.toString(format: "d"))
        }

        lblBookingValues?.text = ""
    }
    
    func selectedDayString(){
        
        var dayStringArray : [String] = []
        for (index , sender) in btnWeekly.enumerate() {
            if sender.selected{
                dayStringArray.append(Weekday(rawValue: index)?.value() ?? "")
                continue
            }
        }
//        lblBookingValues?.text =  dayStringArray.joinWithSeparator(", ")
    }

    func calendar(calendar: JTCalendarManager!, prepareDayView dayView: UIView!) {
        guard let currentDayView = dayView as? JTCalendarDayView else{
            return
        }
        
        if NSDate().compare(currentDayView.date) == .OrderedDescending{
            currentDayView.textLabel.textColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.6)
            currentDayView.circleView.hidden = true
            currentDayView.backgroundColor = UIColor.clearColor()
        }else if(isInDateSelected(currentDayView.date)){
            currentDayView.circleView.hidden = false
            currentDayView.circleView.backgroundColor = Colors.MainColor.color()
            currentDayView.textLabel.textColor = UIColor.whiteColor()
        }
            // Other month
        else if (!(calendarManager.dateHelper.date(calendarContentView.date, isTheSameMonthThan: currentDayView.date))){
            currentDayView.circleView.hidden = true
            currentDayView.backgroundColor = UIColor.clearColor()
            currentDayView.textLabel.textColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.6)
        }
            // Another day of the current month
        else{
            currentDayView.circleView.hidden = true
            currentDayView.backgroundColor = UIColor.clearColor()
            currentDayView.textLabel.textColor = UIColor.blackColor()
        }

    }
    
    func calendar(calendar: JTCalendarManager!, canDisplayPageWithDate date: NSDate!) -> Bool {
        
        let cal = NSCalendar.currentCalendar()
        let maxdate = cal.dateByAddingUnit(.Month, value: 2, toDate: NSDate(), options: [])
        return calendarManager.dateHelper.date(date, isEqualOrAfter: NSDate(), andEqualOrBefore: maxdate)
    }
    func calendar(calendar: JTCalendarManager!, didTouchDayView dayView: UIView!) {
        guard let currentDayView = dayView as? JTCalendarDayView else{return}
        
        
        // Previous or next month date clicked
        if (!calendarManager.dateHelper.date(calendarContentView.date, isTheSameMonthThan: currentDayView.date)){
           // calendarContentView.date.compare(dayVIew.date) == NSComparisonResult.OrderedAscending ? calendarContentView.loadNextPageWithAnimation() : calendarContentView.loadPreviousPageWithAnimation()
            return
        }
        if isInDateSelected(currentDayView.date){
            dateSelectedArray.removeObject(currentDayView.date)
            dateSelectedArray.sortInPlace({ $0.compare($1) == NSComparisonResult.OrderedAscending })
            selectedDateString()
            UIView.transitionWithView(currentDayView, duration: 0.3, options: UIViewAnimationOptions.CurveEaseInOut , animations: {
                currentDayView.circleView.transform =  CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1)
                weak var weak : OrderSchedularViewController? = self
                weak?.calendarManager.reload()
                }, completion: nil)
        }
        else{
            dateSelectedArray.append(currentDayView.date)
            dateSelectedArray.sortInPlace({ $0.compare($1) == NSComparisonResult.OrderedAscending })
            selectedDateString()
            UIView.transitionWithView(currentDayView, duration: 0.3, options: UIViewAnimationOptions.CurveEaseInOut , animations: {
                currentDayView.circleView.transform = CGAffineTransformIdentity
                weak var weak : OrderSchedularViewController? = self
                weak?.calendarManager.reload()
                }, completion: nil)
            
        }
    }
    
//    func calendar(calendar: JTCalendarManager!, canDisplayPageWithDate date: NSDate!) -> Bool {
//        return true
//    }
}



//MARK: - Button Actions

extension OrderSchedularViewController{
    
    @IBAction func actionSetSchedule(sender: UIButton) {
        
        webServiceScheduleOrder()
    }
    @IBAction func actionCalendarMode(sender: AnyObject) {
       
        UtilityFunctions.showActionSheet(withTitle: L10n.ChooseBookingCycle.string , subTitle: nil, vc: self, senders: CalendarMode.allValues) { (sender,index) in
            weak var weak : OrderSchedularViewController? = self
            weak?.bookingCycleHandler(withSender: sender)
        }
    }
    
    func bookingCycleHandler (withSender sender : AnyObject?){

        guard let title = sender as? String else{
            return
        }
        btnBookingCycle?.setTitle(title, forState: .Normal)
        switch title {
        case L10n.Monthly.string:
            mode = .Monthly
        case L10n.Weekly.string:
            mode = .Weekly
        default:
            break
        }
    }
    
    @IBAction func actionWeekMode(sender: UIButton) {
        
        sender.selected.toggle()
        selectedDayString()
    }
    
    @IBAction func actionBack(sender: UIButton) {
        popVC()
    }
    @IBAction func actionTimePick(sender: UIButton) {
        UtilityFunctions.showDatePicker(self, datePickerMode: .Time, minDate: NSDate(), title: L10n.SelectBookingTime.string, message: nil, selectedDate: { (date) in
            weak var weakSelf = self
            weakSelf?.selectedTime = date
            weakSelf?.btnDate.setTitle(UtilityFunctions.getTimeFormatted(DateFormat.TimeFormatUI, date: date), forState: .Normal)
            }) {
                
        }
    }
    
    @IBAction func actionSkip(sender: UIButton) {
        getOrderDetails("0")
    }
    
}

//MARK: - Schedule Order Web Service 

extension OrderSchedularViewController {
    func webServiceScheduleOrder(){
        
        
        var selectedSchedule : String = ""
        var tempArr = [String?]()
        switch mode {
        case .Weekly:
            
            for (index,btn) in btnWeekly.enumerate() {
                if btn.selected {
                    tempArr.append(String(index))
                }
            }
            selectedSchedule = UtilityFunctions.appendOptionalStrings(withArray: tempArr, separatorString: ",")
        case .Monthly:
            
            for date in dateSelectedArray {
                tempArr.append(UtilityFunctions.getDateFormatted("dd", date: date))
            }
            selectedSchedule = UtilityFunctions.appendOptionalStrings(withArray: tempArr, separatorString: ",")
        }
        if dateSelectedArray.count == 0 {
            UtilityFunctions.showSweetAlert(L10n.Warning.string, message: L10n.PleaseSelectDatesToSchedule.string, success: {}, cancel: {})
            return
        }
        
        
        AdjustEvent.ScheduleOrder.sendEvent()
        let params = FormatAPIParameters.ScheduleNewOrder(orderId: orderId, deliveryDate: dateSelectedArray, pickUpBuffer: orderSummary?.pickupBuffer,deliveryTime: UtilityFunctions.getTimeFormatted("HH:mm", date: orderSummary?.deliveryDate)).formatParameters()
        APIManager.sharedInstance.opertationWithRequest(withApi: API.ScheduleNewOrder(params)) { (response) in
            weak var weakSelf = self
            switch response{
            case .Success(_):
                weakSelf?.handleScheduleOrder()
            case .Failure(_):break
            }
        }
    }
    
    func handleScheduleOrder(){
        
        UtilityFunctions.showSweetAlert(L10n.Success.string, message: L10n.YourOrderHaveBeenSheduledSuccessfully.string, style: .Success, success: { [weak self] in
            self?.getOrderDetails("1")
            })
    }
    
    func getOrderDetails(scheduleOrder : String?){
        
        let orderDetailVc = StoryboardScene.Order.instantiateOrderDetailController()
        orderDetailVc.orderDetails = OrderDetails(orderSummary: orderSummary,orderId: orderId,scheduleOrder: scheduleOrder)
        orderDetailVc.type = .OrderUpcoming
        orderDetailVc.isOrderCompletion = true
        self.pushVC(orderDetailVc)
    }
}

extension OrderSchedularViewController {
    
    func addSupplierImage(){
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.SupplierImage(FormatAPIParameters.SupplierImage(supplierBranchId: orderSummary?.items?.first?.supplierBranchId).formatParameters())) {[weak self] (response) in
            switch response{
            case .Success(let object):
                guard let image = object as? String else { return }
                self?.addFloatingButton(false, image: image.componentsSeparatedByString(" ").first,supplierId:image.componentsSeparatedByString(" ").last,supplierBranchId: GDataSingleton.sharedInstance.currentSupplierId )
            default :
                break
            }
        }
    }
}

