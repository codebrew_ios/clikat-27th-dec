//
//  SettingsViewController.swift
//  Clikat
//
//  Created by cbl73 on 4/23/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Photos
import PhotosUI
import YYWebImage


protocol LogoutDelegate : class {
    func userLoggedOut()
}

class SettingsViewController: BaseViewController {

    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var imageViewCover: UIImageView!
    @IBOutlet weak var imageViewProfile: UIImageView!{
        didSet{
            imageViewProfile.layer.cornerRadius = 8.0
            imageViewProfile.layer.borderWidth = 2.0
            imageViewProfile.layer.borderColor = UIColor.whiteColor().CGColor
        }
    }
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelPlace: UILabel!
    
    let headerHeight = ScreenSize.SCREEN_WIDTH * 0.6
    var headerView = UIView(frame: CGRectZero)
    
    var delivery : Delivery?
    
    weak var delegate : LogoutDelegate?
    var tableDataSource = SettingsDataSource(){
        didSet{
            tableView.delegate = tableDataSource
            tableView.dataSource = tableDataSource
        }
    }
    var arrAddress : [Address]?{
        didSet{
            configureTableView()
            tableView.reloadTableViewData(inView: view)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        getAllAdresses()
        setupUI()
      //checkCameraPermission()
    }
    
    func setupUI(){
        configureTableHeader()
        labelName.text = GDataSingleton.sharedInstance.loggedInUser?.firstName
        labelPlace.text = UtilityFunctions.appendOptionalStrings(withArray: [LocationSingleton.sharedInstance.location?.getCity()?.name,LocationSingleton.sharedInstance.location?.getCountry()?.name],separatorString: " , ")
        guard let image = GDataSingleton.sharedInstance.loggedInUser?.userImage,imageUrl = NSURL(string: image) else { return }        
        imageViewCover.yy_setImageWithURL(imageUrl, placeholder: UIImage(), options: [.SetImageWithFadeAnimation]) { (image, url, imageformtype, imageStage, error) in
            weak var weakSelf = self
            weakSelf?.imageViewCover.image = UIImageEffects.imageByApplyingDarkEffectToImage(image)
        }

        imageViewProfile.yy_setImageWithURL(imageUrl, options: .SetImageWithFadeAnimation)
    }
}

//MARK: - Webservice Get all Adresses
extension SettingsViewController {
    func getAllAdresses(){
        APIManager.sharedInstance.showLoader()
        APIManager.sharedInstance.opertationWithRequest(withApi: API.Addresses(FormatAPIParameters.Addresses(supplierBranchId: nil,areaId: nil).formatParameters())) { [weak self] (result) in
            APIManager.sharedInstance.hideLoader()
            switch result {
            case .Success(let object):
                guard let delivery = object as? Delivery else { return }
                self?.delivery = delivery
                self?.arrAddress = delivery.addresses
            case .Failure(let validation):
                print(validation)
            }
        }
    }
}

//MARK: - Configure tableview
extension SettingsViewController {
    
    func configureTableView(){
        weak var weakSelf : SettingsViewController? = self
        tableDataSource = SettingsDataSource(tableView: tableView, configurecell: { (cell, section, indexPath) in
            
            weakSelf?.configureTableCell(cell, section: section, indexPath: indexPath)
            }, rowSelectedBlock: { (indexPath) in
                weakSelf?.configureCellSelection(indexPath)
            }, scrollListener: { (scrollView) in
               
                weakSelf?.updateHeaderView(weakSelf?.headerView)
        })
    }
    
    func configureTableCell(cell : AnyObject, section : SettingsSection, indexPath : NSIndexPath){
        (cell as? DeliveryAddressCell)?.arrAddress = arrAddress
        cell.textLabel?.font = UIFont(name: Fonts.ProximaNova.SemiBold, size: Size.Large.rawValue)
        switch section {
        case .Notifications:
            
            cell.textLabel?.text = indexPath.row == 0 ? L10n.Notifications.string : L10n.NotificationsLanguage.string
            if cell.textLabel?.text == L10n.Notifications.string {
                
                let switchView = UISwitch(frame: CGRectZero)
                switchView.onTintColor = Colors.MainColor.color()
                switchView.on = delivery?.notificationStatus == "1" ? true : false
                (cell as? SettingsCell)?.accessoryView = switchView
                switchView.addTarget(self, action:  #selector(SettingsViewController.actionSwitch(_:)), forControlEvents: .ValueChanged)
            }else {
                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, w: 24, h: 24))
                imageView.image = Localize.currentLanguage() == Languages.Arabic ? UIImage(asset: Asset.Flag_ead) : UIImage(asset: Asset.Ic_UK_flag)
                (cell as? SettingsCell)?.accessoryView = imageView
            }
        case .Other:
            if let _ = GDataSingleton.sharedInstance.loggedInUser?.fbId {
            cell.textLabel?.text = L10n.Logout.string
            }else {
                cell.textLabel?.text = indexPath.row == 0 ? L10n.ChangePassword.string : L10n.Logout.string
            }
        default :
            break
        }
    }
    
    func configureCellSelection(indexPath : NSIndexPath){
        
        let row = SettingsSection.allValues[indexPath.section]
        
        switch row {
        case .Other:
            if let _ = GDataSingleton.sharedInstance.loggedInUser?.fbId {
                logOut()
            }else {
                if indexPath.row == 1 {
                    logOut()
                }else {
                    showAlertChangePassword()
                }
            }
            
        case .Notifications:
            if indexPath.row == 1 {
                let cell = tableView.cellForRowAtIndexPath(indexPath)
                UtilityFunctions.showActionSheet(withTitle: L10n.SelectNotificationLanguage.string, subTitle: nil, vc: self, senders: [L10n.English.string,L10n.Arabic.string], success: { (text, index) in
                    let title = text as! String
                    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, w: 24, h: 24))
                    switch title {
                    case L10n.English.string:
                        imageView.image = UIImage(asset: Asset.Ic_UK_flag)
                        self.changeNotificationLanguage("14")
                    case L10n.Arabic.string:
                        imageView.image = UIImage(asset: Asset.Flag_ead)
                        self.changeNotificationLanguage("15")
                    default : break
                    }
                    DBManager.sharedManager.deleteAllData(CoreDataEntity.Cart.rawValue)
                    (cell as? SettingsCell)?.accessoryView = imageView
                    
                })
            }
        default:
            break
        }
    }
    
    func changeNotificationLanguage(languageId : String?){
        if let _ = GDataSingleton.sharedInstance.loggedInUser?.token {
            APIManager.sharedInstance.opertationWithRequest(withApi: API.NotificationLanguage(FormatAPIParameters.NotificationLanguage(languageId: languageId).formatParameters())) { (response) in
                UtilityFunctions.showSweetAlert(L10n.Success.string, message: L10n.NotificationLanguageChangedSuccessfully.string, success: {}, cancel: {})
            }
        }
    }
}



//MARK: - Setup table Header

extension SettingsViewController {
    
    func configureTableHeader(){
        headerView = tableView.tableHeaderView!
        tableView.tableHeaderView = nil
        tableView.addSubview(headerView)
        tableView.contentInset = UIEdgeInsets(top: headerHeight, left: 0, bottom: 0, right: 0)
        tableView.contentOffset = CGPoint(x: 0, y: -headerHeight)
        updateHeaderView(headerView)
    }
    
    func updateHeaderView(headerView : UIView?){
        
        var headerRect = CGRect(x: 0, y: -headerHeight, width: ScreenSize.SCREEN_WIDTH, height: headerHeight )
        if (tableView.contentOffset.y < -headerHeight) {
            headerRect.origin.y = tableView.contentOffset.y
            headerRect.size.height = -tableView.contentOffset.y
        }
        headerView?.frame = headerRect
    }
}


//MARK: - Button Actions
extension SettingsViewController {
    
    @IBAction func actionToggleMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    @IBAction func actionChangeProfilePic(sender : AnyObject){
        
        if UtilityFunctions.isCameraPermission() {
            UtilityFunctions.showActionSheet(withTitle: nil, subTitle: L10n.SelectPicture.string, vc: self, senders: [L10n.Camera.string,L10n.PhotoLibrary.string]) { (text, index) in
                
                CameraGalleryPickerBlock.sharedInstance.pickerImage(type: text as! String, presentInVc: self, pickedListner: { [weak self] (image) in
                    
                    self?.changeProfileWebservice(image)
                }) {
                    
                    //Cancelled
                }
            }
        }else {
            UtilityFunctions.alertToEncourageCameraAccessWhenApplicationStarts(self)
        }
        
    }
    
    func actionSwitch(sender : UISwitch!){
        let status = String(sender.on)
        APIManager.sharedInstance.opertationWithRequest(withApi: API.NotificationSwitch(FormatAPIParameters.NotificationSwitch(status: status).formatParameters())) { (result) in
//            weak var weakSelf = self
            
        }
    }
}

//MARK: - change profile pic & change password
extension SettingsViewController {
    
    func changeProfileWebservice(image : UIImage?){
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.ChangeProfile(FormatAPIParameters.ChangeProfile().formatParameters()), image: image?.resize(toWidth:300)) {[weak self] (response) in
            switch response {
            case .Success(let object):
                self?.imageViewProfile.image = image
                self?.imageViewCover.image = UIImageEffects.imageByApplyingDarkEffectToImage(image)
                let user = GDataSingleton.sharedInstance.loggedInUser
                user?.userImage = object as? String
                GDataSingleton.sharedInstance.loggedInUser = user
            default:
                break
            }
        }
    }
    func changePasswordWebservice(old : String?,new : String?){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.ChangePassword(FormatAPIParameters.ChangePassword(oldPassword: old, newPassword: new).formatParameters())) { (response) in
            
        }
    }
}

//MARK: - ChangePassword

extension SettingsViewController {
    
    func showAlertChangePassword(){
        
        
        let alert = TYAlertView(title: L10n.ChangePassword.string , message: L10n.EnterYourDetails.string)
        let alertController = TYAlertController(alertView: alert, preferredStyle: .Alert, transitionAnimation: .ScaleFade)
        
        let action = TYAlertAction(title: L10n.Save.string, style: .Default) {[weak self] (action) in
            
            self?.handleAlertAction(alertController)
        }
        alert.buttonDefaultBgColor = Colors.MainColor.color()
        alert.textFieldFont = UIFont(name: Fonts.ProximaNova.Regular, size: Size.Small.rawValue)
        alert.addAction(TYAlertAction(title: L10n.Cancel.string , style: .Destructive, handler: { (action) in
            alert.hideView()
        }))
        alert.addAction(action)
        let tempPlaceholderArr = [L10n.OldPassword.string,L10n.NewPassword.string,L10n.ConfirmPassword.string]
        for (index,element) in tempPlaceholderArr.enumerate() {
            alert.addTextFieldWithConfigurationHandler { (textField) in
                textField.placeholder = element
                textField.tag = index + 1
                textField.returnKeyType = .Done
                textField.secureTextEntry = true
            }
        }
        
        presentVC(alertController)
        
    }
    
    func handleAlertAction(alertController : TYAlertController){
        alertController.view.endEditing(true)
        var arrTextFieldText : [String] = []
        let arrTextfieldNames = [L10n.OldPassword.string,L10n.NewPassword.string,L10n.ConfirmPassword.string]
        for (index,_) in arrTextfieldNames.enumerate() {
            
            guard let tf = alertController.alertView.viewWithTag(index + 1) as? UITextField,text = tf.text else { continue }
            arrTextFieldText.append(text.characters.count == 0 ? "" : text)
        }
        
        let message = User.validateChangePassword(arrTextFieldText) ?? ""
        if message.characters.count == 0 {
             alertController.dismissViewControllerAnimated(true)
            changePasswordWebservice(arrTextFieldText.first, new: arrTextFieldText.last)
        }else{
            alertController.view.endEditing(true)
            alertController.view.makeToast(message)
        }
       
    }
}
//MARK: - Utility
import AVFoundation
extension SettingsViewController {
    func logOut(){
        UtilityFunctions.showSweetAlert(L10n.LogOut.string, message: L10n.AreYouSureYouWantToLogout.string, success: { [weak self] in
            LocationSingleton.sharedInstance.scheduledOrders = "0"
            GDataSingleton.sharedInstance.loggedInUser = nil
            
            let cache = YYWebImageManager.sharedManager().cache
            cache?.diskCache.removeAllObjects()
            cache?.memoryCache.removeAllObjects()
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
            self?.delegate?.userLoggedOut()
        }) {
        }
    }
}





