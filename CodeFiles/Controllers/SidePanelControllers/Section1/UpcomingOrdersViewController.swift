//
//  UpcomingOrdersViewController.swift
//  Clikat
//
//  Created by cbl73 on 4/23/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class UpcomingOrdersViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewPlaceholder : UIView!

    var dataSource : TableViewDataSource = TableViewDataSource(){
        didSet{
            tableView?.dataSource = dataSource
            tableView?.delegate = dataSource
        }
    }
    var orderListing : OrderListing? {
        didSet{
            dataSource.items = orderListing?.orders
            tableView?.reloadTableViewData(inView: view)
            
            guard let orders = orderListing?.orders where orders.count > 0 else{
                viewPlaceholder?.hidden = false
                return
            }
            viewPlaceholder?.hidden = true

        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AdjustEvent.PendingOrders.sendEvent()
        tableView.registerNib(UINib(nibName: CellIdentifiers.OrderParentCell, bundle: nil), forCellReuseIdentifier: CellIdentifiers.OrderParentCell)
        configureTableViewInitialization()
        webService()
    }
}


//MARK: - Webservice Methods
extension UpcomingOrdersViewController {
    
    
    func webService (){
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.OrderUpcoming(FormatAPIParameters.OrderUpcoming().formatParameters())) { (response) in
            
            weak var weak : UpcomingOrdersViewController? = self
            switch response{
                
            case .Success(let listing):
                weak?.orderListing = listing as? OrderListing
                
                break
            default :
                break
            }
        }
        
    }
    
}


//MARK: - TableView Configuration

extension UpcomingOrdersViewController{
    
    
    func configureTableViewInitialization(){
        dataSource = TableViewDataSource(items: orderListing?.orders , height: 283, tableView: tableView, cellIdentifier: CellIdentifiers.OrderParentCell , configureCellBlock: { (cell, item) in
            
            weak var weakSelf : UpcomingOrdersViewController? = self
            weakSelf?.configureCell(withCell : cell , item : item)
            
            }, aRowSelectedListener: { (indexPath) in
                weak var weak : UpcomingOrdersViewController? = self
                let orderDetailVc = StoryboardScene.Order.instantiateOrderDetailController()
                orderDetailVc.orderDetails = weak?.orderListing?.orders?[indexPath.row]
                orderDetailVc.type = .OrderUpcoming
                weak?.pushVC(orderDetailVc)
        })
        tableView.reloadData()
    }
    
    
    func configureCell(withCell cell : AnyObject , item : AnyObject? ){
        
        (cell as? OrderParentCell)?.cellType = .OrderUpcoming
        (cell as? OrderParentCell)?.order = item as? OrderDetails
        (cell as? OrderParentCell)?.orderDelegate = self
        (cell as? OrderParentCell)?.btnOrderType.hidden = ((item as? OrderDetails)?.status != .Pending) ? true : false
        
    }
    
}


//MARK: - Button Actions

extension UpcomingOrdersViewController{
    
    @IBAction func actionCart(sender: AnyObject) {
        pushVC(StoryboardScene.Options.instantiateCartViewController())
    }
    
    @IBAction func actionMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
}

//MARK: - Order Parent Cell Delegate {

extension UpcomingOrdersViewController : OrderParentCellDelegate {
    
    func actionOrderTypeButton(cell: OrderParentCell, orderId: String?) {
        if cell.btnOrderType.titleLabel?.text == L10n.CONFIRMORDER.string {
            let VC = StoryboardScene.Order.instantiatePaymentMethodController()
            VC.orderId = orderId
            pushVC(VC)
            return
        }
        UtilityFunctions.showSweetAlert(L10n.CancelOrder.string, message: L10n.DoYouReallyWantToCancelThisOrder.string, success: { [weak self] in
            UtilityFunctions.showSweetAlert(L10n.Success.string, message: L10n.YouHaveCancelledYourOrderSuccessfully.string, style: .Success)
            self?.cancelOrderWebservice(cell, orderId: orderId)
            }) {
        }
    }
    
    func actionOrderTypeButton(cellType: OrderCellType, order: OrderDetails?) {
        
    }
    
    
    func cancelOrderWebservice(cell : OrderParentCell, orderId :String?){
        guard let indexpath = tableView.indexPathForCell(cell) else { return }
        tableView.beginUpdates()
        dataSource.items?.removeAtIndex(indexpath.row)
        orderListing?.orders?.removeAtIndex(indexpath.row)
        tableView.deleteRowsAtIndexPaths([indexpath], withRowAnimation: .Top)
        tableView.endUpdates()
        viewPlaceholder?.hidden = dataSource.items?.count > 0 ? true : false
        APIManager.sharedInstance.opertationWithRequest(withApi: API.CancelOrder(FormatAPIParameters.CancelOrder(orderId: orderId,isScheduled: "0").formatParameters())) { (response) in
            switch response {
            case .Success(_):
                break
            case .Failure(_):
                break
            }
        }
    }
    func handleCancelOrder(cell : OrderParentCell?){
        
    }
}