//
//  LiveSupportControllerExtension.swift
//  Clikat
//
//  Created by cblmacmini on 6/9/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import EZSwiftExtensions
extension LiveSupportViewController {

    //MARK: -  Keyboard Notifications
    
    func setUpKeyboardNotifications(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LiveSupportViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: self.view?.window)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LiveSupportViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: self.view?.window)
    }
    
    func keyboardWillShow(n : NSNotification){
        if let userInfo = n.userInfo{
            let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue)
            let rect = keyboardFrame.CGRectValue()
            self.setOffset(hide: false,height: CGFloat(rect.height))
        }
    }
    func keyboardWillHide (n : NSNotification){
        self.setOffset(hide: true,height: 0)
    }
    
    func setOffset (hide hide : Bool,height : CGFloat){
        
        if hide{
            
            self.constraintBottomTableView?.constant = 0
            UIView.animateWithDuration(0.25, animations: { () -> Void in
                 weak var weakSelf = self
                weakSelf?.view.layoutIfNeeded()
                }, completion: { (animated) -> Void in
            })
        }
        else{
            self.constraintBottomTableView?.constant = height ;
            UIView.animateWithDuration(0.25, animations: { () -> Void in
                 weak var weakSelf = self
                weakSelf?.view.layoutIfNeeded()
                }, completion: { (animated) -> Void in
                    weak var weakSelf = self
                    weakSelf?.scrollToBottom(animate: true)
            })
        }
    }
    
    func scrollToBottom(animate animate : Bool){
        guard let count = messages?.count else { return }
        if count > 0 {
            tableView?.scrollToRowAtIndexPath(NSIndexPath(forRow: count - 1, inSection: 0), atScrollPosition: .Top, animated: animate)
        }
    }
    
}
