//
//  OrderSummaryController.swift
//  Clikat
//
//  Created by cblmacmini on 5/9/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class OrderSummaryController: BaseViewController {

    var tableDataSource = OrderSummaryDataSource(){
        didSet{
            tableView.delegate = tableDataSource
            tableView.dataSource = tableDataSource
        }
    }
    var remarks : String?
    var orderSummary : OrderSummary?
    var selectedPaymentMehod : PaymentMethod?
    var cartFlowType : CartFlowType = .Normal
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var btnPlaceOrder: UIButton!
    
    @IBOutlet weak var deliveryDetailStackView: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnPlaceOrder.kern(ButtonKernValue)
        ez.runThisAfterDelay(seconds: 0.1) {
            weak var weakSelf = self
            weakSelf?.configureTableView()
            weakSelf?.tableView.reloadTableViewData(inView: weakSelf?.view)
        }
        updateUI()
    }

}

extension OrderSummaryController  {
    
    func configureTableView() {
        tableDataSource = OrderSummaryDataSource(items: orderSummary?.items, height: UITableViewAutomaticDimension, tableView: tableView, cellIdentifier: nil, configureCellBlock: { (cell, item) in
            weak var weakSelf = self
            weakSelf?.configureTableViewCell(cell, item: item)
            }, aRowSelectedListener: { (indexPath) in
                
        })
    }
    
    func configureTableViewCell(cell : AnyObject?,item : AnyObject?){
    
        (cell as? OrderSummaryCell)?.cart = item as? Cart
        (cell as? OrderBillCell)?.orderSummary = orderSummary
        (cell as? OrderBillCell)?.tvRemarks.text = remarks ?? L10n.NoRemarks.string
    }
}





//MARK: - Update UI
extension OrderSummaryController {
    func updateUI(){
        
        if let pickAddress = orderSummary?.pickupAddress {
            let pickupDetailView = OrderDeliveryDetailView(frame: CGRect(x:0,y:0,w:50,h:50), type: .Pickup, address: pickAddress,date: orderSummary?.pickupDate)
            deliveryDetailStackView.addArrangedSubview(pickupDetailView)
        }
        
        guard let delAddress = orderSummary?.deliveryAddress else { return }
        let deliveryDetailView = OrderDeliveryDetailView(frame: CGRect(x:0,y:0,w:50,h:50), type: .Delivery, address: delAddress, date: orderSummary?.deliveryDate)
        deliveryDetailStackView.addArrangedSubview(deliveryDetailView)
    }
}

//MARK: - Button Actions

extension OrderSummaryController {
    
    @IBAction func actionPlaceOrder(sender: AnyObject) {
        if orderSummary?.items?.first?.category != "Promotion" && orderSummary?.dTotalPrice < orderSummary?.minOrderAmount?.toDouble() {
            
             view.makeToast(UtilityFunctions.appendOptionalStrings(withArray: [L10n.SorryYourOrderIsBelowMinimumOrderPrice.string,orderSummary?.minOrderAmount]))
        }else {
            handleGenerateOrder()
        }
    }
    
    @IBAction func actionBack(sender: UIButton) {
        popVC()
    }
    
    @IBAction func actionMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
  
}

//MARK: - GenerateOrder Web service

extension OrderSummaryController {
    
    func generateOrder(){
//        APIManager.sharedInstance.opertationWithRequest(withApi: API.GenerateOrder(FormatAPIParameters.GenerateOrder(cartId: orderSummary?.cartId).formatParameters())) { [weak self] (response) in
//            
//            switch response {
//            case .Success(let object):
//                self?.handleGenerateOrder()
//            case .Failure(_):
//                break
//            }
//        }
    }
    
    func handleGenerateOrder(){
        let VC = StoryboardScene.Order.instantiatePaymentMethodController()
        VC.orderSummary = orderSummary
        pushVC(VC)
    }
}
