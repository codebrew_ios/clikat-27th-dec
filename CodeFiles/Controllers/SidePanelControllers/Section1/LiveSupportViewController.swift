//
//  LiveSupportViewController.swift
//  Clikat
//
//  Created by cbl73 on 4/21/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import SZTextView
import IQKeyboardManager

class LiveSupportViewController: BaseViewController {

    @IBOutlet weak var imageViewUser: UIImageView!{
        didSet{
            imageViewUser.layer.cornerRadius = MidPadding
            imageViewUser.yy_setImageWithURL(NSURL(string: GDataSingleton.sharedInstance.loggedInUser?.userImage ?? ""), options: .SetImageWithFadeAnimation)
        }
    }
    @IBOutlet weak var labelResponseTime: UILabel!
    @IBOutlet weak var tvMessage: SZTextView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintBottomTableView: NSLayoutConstraint!
    
    let group = dispatch_group_create()
    let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)
    
    var tableDataSource = ChatDataSource(){
        didSet{
            tableView.delegate = tableDataSource
            tableView.dataSource = tableDataSource
        }
    }
    
    var messages : [Message]? = []
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureChatDataSource()
//        SocketIOManager.sharedInstance.establishConnection()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
       
        setupLiveSupport()
    }
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
//        SocketIOManager.sharedInstance.closeConnection()
        NSNotificationCenter.defaultCenter().removeObserver(self)
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        messages = []
    }
    func setupLiveSupport(){
        IQKeyboardManager.sharedManager().enable = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        setUpKeyboardNotifications()
        fetchMessages()
        labelResponseTime.text = L10n.Online.string
    }
}

//MARK: - Configure chat Data Source
extension LiveSupportViewController {
    
    func fetchMessages(){
        
//        SocketIOManager.sharedInstance.getMessagesFromServer { [unowned self] (chat) in
//            
//            var tempmessages : [Message] = []
//            for message in chat?.messages ?? [] {
//                if message.userId != GDataSingleton.sharedInstance.loggedInUser?.id { return }
//                message.myMessage = false
//                tempmessages.append(message)
//            }
//            dispatch_group_notify(self.group, self.queue, {
//                 self.addNewMessages(tempmessages)
//            })
//           
//        }
    }
    
    func configureChatDataSource(){
        tableDataSource = ChatDataSource(items: messages, height: UITableViewAutomaticDimension, tableView: tableView, cellIdentifier: nil, configureCellBlock: { (cell, item) in
                weak var weakSelf = self
            weakSelf?.configureCell(cell, message: item)
            }, aRowSelectedListener: { (indexPath) in
                
        })
        tableView.reloadData()
    }
    
    
    func configureCell(cell : AnyObject,message : AnyObject?){
        (cell as? LiveSupportChatCell)?.message = message as? Message
    }
}
//MARK: - Button Actions
extension LiveSupportViewController {
    
    @IBAction func actionMenu(sender: UIButton) {
        toggleSideMenuView()
    }
    
    @IBAction func actionSend(sender: UIButton) {
        
        if tvMessage?.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()) == "" {
            return
        }
        sendMessage(tvMessage?.text)
    }
}

//MARK: - New Messages Handlers

extension LiveSupportViewController {
    
    func addNewMessages(arrMessage : [Message]?){
        
        var indexPaths : [NSIndexPath] = []
        guard let arrMessages = arrMessage where arrMessages.count > 0 else { return }
        for msg in arrMessages {
            guard let _ = msg as? Message else { continue }
            messages?.append(msg)
            tableDataSource.items?.append(msg)
            indexPaths.append(NSIndexPath(forRow: (messages?.count ?? 0) - 1, inSection: 0))
        }
        
        if indexPaths.count > 0{
            ez.runThisInMainThread({
                weak var weakSelf = self
                weakSelf?.tableView.beginUpdates()
                weakSelf?.tableView.insertRowsAtIndexPaths(indexPaths , withRowAnimation: .Fade)
                weakSelf?.tableView.endUpdates()
                weakSelf?.scrollToBottom(animate: true)
            })
        }
    }
    
    func sendMessage(message : String?){
//        let params = SocketIOManager.sharedInstance.formatParameters(message?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()), adminId: messages?.last?.adminId)
//        
//        guard let sentMessage = SocketIOManager.sharedInstance.convertDictToMessage(params) else { return }
//        sentMessage.myMessage = true
//        self.addNewMessages([sentMessage])
//        
//        SocketIOManager.sharedInstance.sendMessage(params)
//        tvMessage.text = ""
    }
    
}

//MARK: - Start/Stop Typing

extension LiveSupportViewController {
    func configureStartStopTypinghandlers(){
//        SocketIOManager.sharedInstance.startTypingBlock = { [weak self]() in
//            
//            self?.labelResponseTime.text = L10n.Typing.string
//        }
//        
//        SocketIOManager.sharedInstance.stopTypingBlock = {[weak self] () in
//            
//            self?.labelResponseTime.text = L10n.Online.string
//        }
    }
}
