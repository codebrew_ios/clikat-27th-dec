//
//  OrderDetailController.swift
//  Clikat
//
//  Created by cblmacmini on 5/2/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class OrderDetailController: BaseViewController {
 
//    let supplierView = FloatingSupplierView(frame: CGRect(x: Localize.currentLanguage() == Languages.Arabic ? 16 : ScreenSize.SCREEN_WIDTH - 80, y: ScreenSize.SCREEN_HEIGHT - 80, w: 64, h: 64))
    typealias ActionCancelOrder = () -> ()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnScheduleOrder: UIButton!
    @IBOutlet weak var constraintButtonContainer: NSLayoutConstraint!
    @IBOutlet weak var btnReOrder: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    
    var tableDataSource = OrderDetailDataSource()
    var orderDetails : OrderDetails?
    
    var type : OrderCellType?
    var isOrderCompletion = false
    var isPush = false
    var isConfirmOrder = false
    
    var cancelOrder : ActionCancelOrder?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getOrderDetails(orderDetails?.orderId)
        setupButtons()
        AdjustEvent.OrderDetail.sendEvent()
//        setupUI()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        addSupplierImage()
    }
    
    func setupUI(){
        
        let color = orderDetails?.status.color()
        btnReOrder.layer.borderColor = color?.CGColor
        btnReOrder.setTitleColor(color, forState: .Normal)
//        btnScheduleOrder.layer.borderColor = color?.CGColor
//        btnScheduleOrder.setTitleColor(color, forState: .Normal)
        configureOrderDetailDataSource()
    }
    
    func setupButtons(){
        guard let cellType = type else { return }
        switch cellType {
        case .OrderTracking,.OrderUpcoming:
            constraintButtonContainer?.constant = 0
        case .OrderScheduled:
            btnReOrder?.setTitle(L10n.Cancel.string, forState: .Normal)
        case .OrderHistory,.RateOrder:
            constraintButtonContainer?.constant = LocationSingleton.sharedInstance.location?.areaEN?.id == orderDetails?.areaId ? 48 : 0
        default:
            break
        }
        
//        if let schedule = orderDetails?.scheduleOrder where schedule == "1"{
//            stackView.removeArrangedSubview(btnScheduleOrder)
//        }
        view.layoutIfNeeded()
    }

}

//MARK: - Get Order Detail 

extension OrderDetailController {
    
    func getOrderDetails(orderId : String?) {
        APIManager.sharedInstance.opertationWithRequest(withApi: API.OrderDetail(FormatAPIParameters.OrderDetail(orderId: orderId).formatParameters())) { [weak self] (response) in
            switch response {
            case .Success(let object): 
                self?.orderDetails = object as? OrderDetails
                self?.setupUI()
            default: break
            }
        }
    }
}

extension OrderDetailController {
    
    func configureOrderDetailDataSource(){
        
        tableDataSource = OrderDetailDataSource(items: [], height: UITableViewAutomaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.OrderStatusCell, configureCellBlock: { (cell, item) in
            weak var weakSelf : OrderDetailController? = self
            weakSelf?.configureOrderStatusCell(cell as? OrderStatusCell)
            weakSelf?.configureOrderDetailCell(cell as? OrderDetailCell)
        }, aRowSelectedListener: { (indexPath) in
            
        })
        tableView.delegate = tableDataSource
        tableView.dataSource = tableDataSource
        tableView.reloadTableViewData(inView: view)
    }
    
    func configureOrderDetailCell(cell : OrderDetailCell?){
        guard let c = cell else { return }
        c.cellType = type ?? .OrderUpcoming
        c.orderDetails = orderDetails
       
    }
    
    func configureOrderStatusCell(cell : OrderStatusCell?){
        guard let c = cell,products = orderDetails?.product else { return }
        c.cellType = type ?? .OrderUpcoming
        c.orderDetails = orderDetails
        for product in products {
            let productView = ProductView(frame: CGRect(x: 0, y: 0, w: c.stackView.size.width, h: 100))
            productView.configureProductView(product)
            c.stackView.addArrangedSubview(productView)
        }
    }
}



extension OrderDetailController{
    
    @IBAction func actionBack(sender: AnyObject) {
        if isConfirmOrder {
            sideMenuController()?.setContentViewController(StoryboardScene.Order.instantiateScheduledOrderController())
            toggleSideMenuView()
        }else if isOrderCompletion {
            sideMenuController()?.setContentViewController(StoryboardScene.Main.instantiateHomeViewController())
            toggleSideMenuView()
            DBManager.sharedManager.deleteAllData(CoreDataEntity.Cart.rawValue)
        }else if isPush {
            sideMenuController()?.setContentViewController(StoryboardScene.Main.instantiateHomeViewController())
            toggleSideMenuView()
        }else{
            popVC()
        }
    }
    
    @IBAction func actionScheduleOrder(sender: AnyObject) {
        
        let schedularVC = StoryboardScene.Order.instantiateOrderSchedularViewController()
        schedularVC.orderId = orderDetails?.orderId
        pushVC(schedularVC)
    }
    
    @IBAction func actionReorder(sender: AnyObject) {
        guard let button = sender as? UIButton else { return }
        
        if button.titleLabel?.text == L10n.Cancel.string {
            UtilityFunctions.showSweetAlert(L10n.AreYouSure.string, message: L10n.DoYouReallyWantToCancelThisOrder.string, success: { [weak self] in
                guard let block = self?.cancelOrder else { return }
                self?.popVC()
                block()
                }, cancel: {
                    
            })
            
        }else {
            UtilityFunctions.showAlert(nil, message: L10n.ReOrderingWillClearYouCart.string, success: {
                weak var weakSelf = self
                weakSelf?.reorderAllitems()
            }) {
                
            }
        }
    }
    
}
//MARK: - Configure Reorder
extension OrderDetailController{
    func reorderAllitems(){
        
        DBManager.sharedManager.deleteAllData(CoreDataEntity.Cart.rawValue)
        guard let products = orderDetails?.product else { return }
        for product in products {
            guard let quantity = Int(product.quantity ?? "") else { continue }
            product.supplierBranchId = orderDetails?.supplierBranchId
            DBManager.sharedManager.manageCart(product, quantity: quantity)
        }
        GDataSingleton.sharedInstance.currentCategoryId = products.first?.categoryId
        let VC = StoryboardScene.Options.instantiateCartViewController()
        
        pushVC(VC)
    }
}

//MARK: - Add Supplier Image
extension OrderDetailController {
    func addSupplierImage(){
        
    
        APIManager.sharedInstance.opertationWithRequest(withApi: API.SupplierImage(FormatAPIParameters.SupplierImage(supplierBranchId: orderDetails?.supplierBranchId).formatParameters())) {[weak self] (response) in
            switch response{
            case .Success(let object):
                guard let image = object as? String else { return }
                self?.addFloatingButton(false, image: image.componentsSeparatedByString(" ").first,supplierId:image.componentsSeparatedByString(" ").last,supplierBranchId: self?.orderDetails?.supplierBranchId )
            default :
                break
            }
        }
    }
    
    
    override func addFloatingButton(isCategoryFlow : Bool ,image : String?,supplierId : String?,supplierBranchId : String?){
        supplierView.imageSupplier.yy_setImageWithURL(NSURL(string:image ?? ""), options: .SetImageWithFadeAnimation)
        supplierView.supplierBranchId = supplierBranchId
        supplierView.supplierId = supplierId
        supplierView.floatingViewTapped = { [weak self] in
            let VC = StoryboardScene.Main.instantiateSupplierInfoViewController()
            VC.passedData.supplierBranchId = supplierBranchId
            VC.passedData.supplierId = supplierId
            VC.passedData.categoryId = self?.orderDetails?.product?.first?.categoryId
            VC.showButton = false
            self?.pushVC(VC)
        }
        self.view.addSubview(supplierView)
        self.view.bringSubviewToFront(supplierView)
    }
}

