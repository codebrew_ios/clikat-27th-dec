//
//  CartViewController.swift
//  Clikat
//
//  Created by cbl73 on 4/21/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit


class CartViewController: BaseViewController {
    
    @IBOutlet var lblStaticText: [UILabel]!{
        didSet{
            for lbl in lblStaticText {
                lbl.kern(ButtonKernValue)
            }
        }
    }

    @IBOutlet weak var btnBack : UIButton!
    @IBOutlet var imgPlaceholderNotext: UIView!
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.registerNib(UINib(nibName: CellIdentifiers.ProductListingCell,bundle: nil), forCellReuseIdentifier: CellIdentifiers.ProductListingCell)
        }
    }
    @IBOutlet var btnProceedCheckout: UIButton!{
        didSet{
            btnProceedCheckout?.kern(ButtonKernValue)
        }
    }
    var FROMSIDEPANEL : Bool = false
    
    var tableDataSource = CartDataSource(){
        didSet{
            tableView.delegate = tableDataSource
            tableView.dataSource = tableDataSource
        }
    }
    
    var cartProdcuts : [AnyObject]?
    
    var remarks : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if FROMSIDEPANEL {
            btnBack?.hidden = true
        }
        if let _ = cartProdcuts {
            configureTableDataSource(cartProdcuts)
        }else {
            
            getCartFromDB()
        }
        AdjustEvent.Cart.sendEvent()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CartViewController.handleCartQuantity(_:)), name: CartNotification, object: nil)
        guard let _ = cartProdcuts else { return }
        getCartFromDB()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
}


//MARK: - Button Actions
extension CartViewController{
    
    
    @IBAction func actionMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    @IBAction func actionBack(sender: AnyObject) {
        popVC()
    }
    
    @IBAction func actionProceedCheckout(sender: AnyObject) {
        
        guard let _ = GDataSingleton.sharedInstance.loggedInUser?.token else {
            presentVC(StoryboardScene.Register.instantiateLoginViewController())
            return
        }
        if !tableView.hidden {
            webServiceAddToCart()
        }else {
            UtilityFunctions.showAlert(L10n.YourCartHasNoItemsPleaseAddItemsToCartToProceed.string)
        }
    }
    
}

//MARK: - Add all products to cart webservice 
extension CartViewController {
    
    func webServiceAddToCart(){
        guard let products = tableDataSource.items as? [Cart] else { return }
       let promotionType = products.reduce("0") { (initial, product) -> String in
        return product.category == "Promotion" ? "1" : initial
        }
        let params = FormatAPIParameters.AddToCart(cart: products, supplierBranchId: products[0].supplierBranchId,promotionType: promotionType, remarks: remarks).formatParameters()
        APIManager.sharedInstance.opertationWithRequest(withApi: API.AddToCart(params)) { (response) in

            weak var weakSelf = self
            switch response {
            case .Success(let object):
                
            weakSelf?.handleWebService(object)
            case .Failure(_):
                break
            }
        }
    }
    
    func handleWebService(tempCartId : AnyObject?){
        guard let _ = GDataSingleton.sharedInstance.loggedInUser?.token else {
            let loginVc = StoryboardScene.Register.instantiateLoginViewController()
            presentVC(loginVc)
            return
        }
        
        
        guard let cartId = (tempCartId as? String)?.componentsSeparatedByString("$").first else { return }
        let VC = StoryboardScene.Order.instantiateDeliveryViewController()
        VC.orderSummary = OrderSummary(items: tableDataSource.items as? [Cart])
        VC.orderSummary?.cartId = cartId
        VC.orderSummary?.minOrderAmount = (tempCartId as? String)?.componentsSeparatedByString("$").last
        VC.remarks = remarks
        pushVC(VC)
    }
}

//MARK: - LoginScreen Delegates
extension CartViewController : LoginViewControllerDelegate {
    
    func userFailedLoggedIn() {
        // Show Alert here
        
    }
    func userSuccessfullyLoggedIn(withUser user: User?) {
    
    }
}


//MARK: - Table View Methods
extension CartViewController {
    
    
    func getCartFromDB(){
        DBManager().getCart { [weak self] (array) in
            
            self?.configureTableDataSource(array)
        }
    }
    
    func configureTableDataSource(products : [AnyObject]?){
        
        if products?.count == 0{
            tableView?.hidden = true
            imgPlaceholderNotext?.hidden = false
            btnProceedCheckout?.hidden = true
            btnProceedCheckout?.enabled = false
            return
        }
        cartProdcuts = products
        configureTableView(cartProdcuts)
        tableView.reloadTableViewData(inView: view)
        btnProceedCheckout.hidden = false
        btnProceedCheckout?.enabled = true
        tableView?.hidden = false
        imgPlaceholderNotext?.hidden = true
    }
    
    func configureTableView(arrayProducts : Array<AnyObject>?){
        guard let array = arrayProducts else { return }
        tableDataSource = CartDataSource(items: array, height: UITableViewAutomaticDimension, tableView: tableView, cellIdentifier: nil, configureCellBlock: { (cell, item) in
            
            weak var weakSelf = self
            weakSelf?.configureCell(cell, indexPath: item, array: array)
            }, aRowSelectedListener: { [weak self] (indexPath) in
//            self?.handleCellSelction(indexPath)
        })
        
    }
    
    func configureCell(cell : AnyObject,indexPath : AnyObject?,array : Array<AnyObject>){
        
        guard let index = indexPath as? NSIndexPath else { return }
        if index.row != array.count {
            (cell as? ProductListingCell)?.cart = array[/index.row] as? Cart
            (cell as? ProductListingCell)?.contentView.backgroundColor = UIColor.whiteColor()
//            (cell as? ProductListingCell)?.label
            configureCellSwipe(cell as? ProductListingCell)
            (cell as? ProductListingCell)?.productClicked = { [weak self] product in
               self?.handleCellSelction(index)
            }
        }else {
            (cell as? CartBillCell)?.cart = array as? [Cart]
            (cell as? CartBillCell)?.tvRemarks.delegate = self
        }
    }
    
    func handleCellSelction(indexPath : NSIndexPath){
        if tableView.cellForRowAtIndexPath(indexPath) is ProductListingCell {
            
            let productDetailVc = StoryboardScene.Main.instantiateProductDetailViewController()
            productDetailVc.passedData.productId = (tableDataSource.items?[indexPath.row] as? Cart)?.id
            productDetailVc.suplierBranchId = (tableDataSource.items?[indexPath.row] as? Cart)?.supplierBranchId
            pushVC(productDetailVc)
            
        }
    }
}

extension CartViewController {
    
    func handleCartQuantity(sender : NSNotification){
        
        let visibleCells = tableView.visibleCells
        if visibleCells.first is CartBillCell {
            tableView?.hidden = true
            imgPlaceholderNotext?.hidden = false
            btnProceedCheckout.hidden = true
            return
        }
        for visibleCell in visibleCells {
            guard let cell = visibleCell as? ProductListingCell else { return }
            if cell.stepper.value == 0 {
                guard let indexpath = tableView.indexPathForCell(cell) else { return }
                tableView.beginUpdates()
                if tableDataSource.items?.count != 0 {
                    tableDataSource.items?.removeAtIndex(indexpath.row)
                }
                tableView.deleteRowsAtIndexPaths([indexpath], withRowAnimation: .Right)
                tableView.endUpdates()
                if /tableDataSource.items?.count == 0{
                    tableView?.hidden = true
                    imgPlaceholderNotext?.hidden = false
                    btnProceedCheckout.enabled = true
                }
                else{
                    tableView?.hidden = false
                    imgPlaceholderNotext?.hidden = true
                    btnProceedCheckout.enabled = false
                    tableView.reloadData()
                }
            } else {
                
                getCartFromDB()
            }
        }
       
    }
}
//MARK: - TextView Delegate
extension CartViewController : UITextViewDelegate {
    
    func textViewDidEndEditing(textView: UITextView) {
        remarks = textView.text
    }
}

//MARK: - Product Listing Gesture Recognizer
extension CartViewController {
    
    func configureCellSwipe(cell : UITableViewCell?){
        
        let slideGesture = DRCellSlideGestureRecognizer()
        let (slideAction,pullAction) = (DRCellSlideAction(forFraction: 0.45),DRCellSlideAction(forFraction: 0.45))
        pullAction.behavior = .PushBehavior
        pullAction.didTriggerBlock = {[weak self] (tableView,indexpath) in
            guard let arrCart = self?.tableDataSource.items as? [Cart] else { return }
            tableView.beginUpdates()
            self?.tableDataSource.items?.removeAtIndex(indexpath.row)
            self?.cartProdcuts?.removeAtIndex(indexpath.row)
            tableView.deleteRowsAtIndexPaths([indexpath], withRowAnimation: .Right)
            tableView.endUpdates()
            DBManager.sharedManager.manageCart(Product(cart: arrCart[indexpath.row]), quantity: 0)
        }
        slideGesture.addActions([slideAction,pullAction])
        cell?.addGestureRecognizer(slideGesture)
    }
}
