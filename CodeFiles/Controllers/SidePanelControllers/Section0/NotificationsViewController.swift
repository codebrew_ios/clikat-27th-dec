//
//  NotificationsViewController.swift
//  Clikat
//
//  Created by cbl73 on 4/21/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import EZSwiftExtensions
class NotificationsViewController: BaseViewController {
    
    
    @IBOutlet weak var viewPlaceholder : UIView!
    @IBOutlet var btnClearAll: UIButton!{
        didSet{
            btnClearAll?.enabled = false
        }
    }
    
    @IBOutlet var tableView: UITableView!{
        didSet{
            tableView?.estimatedRowHeight = 44.0
            tableView?.rowHeight = UITableViewAutomaticDimension
        }
    }
    var dataSource : TableViewDataSource = TableViewDataSource(){
        didSet{
            tableView?.dataSource = dataSource
            tableView?.delegate = dataSource
        }
    }
    
    var notificationListing : NotificationListing? {
        didSet{
            dataSource.items = notificationListing?.arrayNotifications
            ez.runThisInMainThread {
                weak var weakSelf : NotificationsViewController? = self
                weakSelf?.tableView.reloadTableViewData(inView: weakSelf?.view)
                guard let notifications = weakSelf?.notificationListing?.arrayNotifications where notifications.count > 0 else{
                    weakSelf?.viewPlaceholder?.hidden = false
                    weakSelf?.btnClearAll?.enabled = false
                    return
                }
                weakSelf?.btnClearAll?.enabled = true
                weakSelf?.viewPlaceholder?.hidden = true

            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        webServiceGetAllNotifications()
    }
}


//MARK: - WebService
extension NotificationsViewController{
    
    func webServiceGetAllNotifications(){
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.AllNotifications(FormatAPIParameters.AllNotifications().formatParameters())) { (response) in
            
            weak var weakSelf : NotificationsViewController? = self
            switch response{
            case .Success(let listing):
                weakSelf?.notificationListing = listing as? NotificationListing
            default :
                break
            }
        }
    }
    
    func webServiceClearAllNotifications(){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.ClearAllNotifications(FormatAPIParameters.ClearAllNotifications().formatParameters())) { (response) in
            
            weak var weakSelf : NotificationsViewController? = self
            switch response{
            case .Success(_):
                weakSelf?.notificationListing?.arrayNotifications = []
                weakSelf?.dataSource.items = []
                weakSelf?.tableView.reloadData()
            default :
                break
            }
        }
    }
}

//MARK: - TableView configuration Methods
extension NotificationsViewController{
    
    func configureTableView (){
        
        dataSource = TableViewDataSource(items: notificationListing?.arrayNotifications , height: UITableViewAutomaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.NotificationsCell , configureCellBlock: { (cell, item) in
            weak var weakSelf = self
            weakSelf?.configureTableCell(cell, item: item)
            }, aRowSelectedListener: { [weak self] (indexPath) in
                self?.configureCellSelection(indexPath)
        })
        
    }
    
    func configureTableCell(cell : AnyObject?, item : AnyObject?){
        (cell as? NotificationsCell)?.notification = item as? Notification
    }
    
    func configureCellSelection(indexPath : NSIndexPath) {
        guard let notifications = dataSource.items as? [Notification] else { return }
        let order = OrderDetails(orderId: notifications[indexPath.row].orderId)
        tableView.beginUpdates()
        dataSource.items?.removeAtIndex(indexPath.row)
        tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Right)
        tableView.endUpdates()
        
        let VC = StoryboardScene.Order.instantiateOrderDetailController()
        VC.orderDetails = order
        VC.type = .OrderUpcoming
        pushVC(VC)
    }
}


//MARK: - Button Actions
extension NotificationsViewController{
    
    @IBAction func actionMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    @IBAction func actionClearAll(sender: AnyObject) {
        btnClearAll.enabled = true
        webServiceClearAllNotifications()
    }
}