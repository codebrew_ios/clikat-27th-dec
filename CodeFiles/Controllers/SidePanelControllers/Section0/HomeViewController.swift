//
//  DrawerViewController.swift
//  Clikat
//
//  Created by Night Reaper on 14/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import EZSwiftExtensions
//import LNNotificationsUI

extension UITableView {
    
    func reloadTableViewData(inView view : UIView?){
        
        guard let tempView = view else{ return  }
        UIView.transitionWithView(tempView,
                                  duration: 0.1,
                                  options: [.CurveEaseInOut, .TransitionCrossDissolve],
                                  animations: { () -> Void in
                                    weak var weakTableView : UITableView? = self
                                    weakTableView?.reloadData()
            }, completion: nil)
        
    }
    
}


class HomeViewController : CategoryFlowBaseViewController {
    
    @IBOutlet weak var btnArea : UIButton!
    @IBOutlet var btn_language: UIButton!
    @IBOutlet var tableView : UITableView!
    @IBOutlet var imageViewBg: UIImageView!{
        didSet{
            UtilityFunctions.addParallaxToView(imageViewBg)
        }
    }
    
    
    var home : Home? = GDataSingleton.sharedInstance.homeData{
        didSet{
            homeDataSource?.home = home
            tableView?.reloadTableViewData(inView: view)
        }
    }
    var homeDataSource : HomeDataSource? = HomeDataSource(){
        didSet{
            tableView.dataSource = homeDataSource
            tableView.delegate = homeDataSource
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btn_language.selected = Localize.currentLanguage() == Languages.Arabic ? true : false
        btnArea.setTitle(LocationSingleton.sharedInstance.location?.getArea()?.name ?? "", forState: .Normal)
        configureTableViewInitialization()
        webserviceHomeData()
        AdjustEvent.Home.sendEvent()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        GDataSingleton.sharedInstance.currentSupplier = nil
        if !LocationSingleton.sharedInstance.isLocationSelected(){
            let VC = StoryboardScene.Register.instantiateSplashViewController()
            VC.delegate = self
            presentVC(VC)
        }
        checkRating()
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    
    func checkRating(){
        
        guard let _ = GDataSingleton.sharedInstance.loggedInUser?.token else { return }
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.TotalPendingSchedule(FormatAPIParameters.TotalPendingSchedule().formatParameters())) { (response) in
            var arrStr : [String]? = [String]()
            switch response {
            case .Success(let object):
                arrStr = (object as? String)?.componentsSeparatedByString("$$")
                LocationSingleton.sharedInstance.scheduledOrders = arrStr?.last
            default:
                break
            }
            if let _ = GDataSingleton.sharedInstance.showRatingPopUp where arrStr?.first == "1" {
                GDataSingleton.sharedInstance.showRatingPopUp = nil
                
                
                UtilityFunctions.showSweetAlert(L10n.RateOrder.string, message: L10n.LooksLikeYourOrderHasBeenDeliveredWouldYouLikeToRateYourOrder.string,style: AlertStyle.None, success: { [weak self] in
                    self?.sideMenuController()?.setContentViewController(StoryboardScene.Order.instantiateRateMyOrderController())
                    self?.toggleSideMenuView()
                    }, cancel: {
                        
                })
            }
        }
    }
}



//MARK: - WebService Configuration

extension HomeViewController{
    
    func webserviceHomeData()  {
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.Home(FormatAPIParameters.Home().formatParameters())) { (response) in
            weak var weakSelf : HomeViewController? = self
            switch response{
            case APIResponse.Success(let object):
                weakSelf?.home = object as? Home
            default :
                break
            }
        }
    }
    
}


//MARK: - TableView Configuration

extension HomeViewController{
    
    func configureTableViewInitialization(){
       
        homeDataSource = HomeDataSource(home: home, tableView: tableView, configureCellBlock: { (cell, item , type) in
            
            weak var weak : HomeViewController? = self
            weak?.configureCell(withCell: cell, item: item, type: type)
            
            }, aRowSelectedListener: { (indexPath , type) in
                weak var weak : HomeViewController? = self
                weak?.itemClicked(atIndexPath: indexPath, type: type)
        })
    }
    
    
    func configureCell(withCell cell : AnyObject , item : AnyObject , type : HomeScreenSection ){
        
        homeDataSource?.sectionHeader?.actionViewAll = {[weak self] (sender) in
            self?.viewAllOffers(sender)
        }
        
        switch type{
        case .ServiceTypes :
            if let tempCell = cell as? ServiceTypeParentCell{
                tempCell.serviceTypes = Localize.currentLanguage() == Languages.Arabic ? (item as? Home)?.arrayServiceTypesAR : (item as? Home)?.arrayServiceTypesEN
                tempCell.delegate = self
            }
        case .Banners :
            (cell as? BannerParentCell)?.addPageControl()
            (cell as? BannerParentCell)?.banners = (item as? Home)?.arrayBanners
            (cell as? BannerParentCell)?.bannerClickedBlock = { [weak self](banner) in
                let VC = StoryboardScene.Main.instantiateSupplierInfoViewController()
                VC.passedData.supplierId = banner?.supplierId
                VC.passedData.categoryId = banner?.category_id
                VC.passedData.supplierBranchId = banner?.supplierBranchId
                VC.passedData.categoryFlow = banner?.categoryFlow
                self?.pushVC(VC)
            }
        case .Offers:
            if let tempCell = cell as? HomeProductParentCell{
                tempCell.products = Localize.currentLanguage() == Languages.Arabic ? (item as? Home)?.arrayOffersAR : (item as? Home)?.arrayOffersEN
                tempCell.delegate = self
            }
        case .Search:
            (cell as? HomeSearchCell)?.tfSearch.delegate = self
            (cell as? HomeSearchCell)?.actionBarcode = { [weak self] in
                self?.openBarCodeScanner()
            }
            (cell as? HomeSearchCell)?.actionSearch = { [weak self](searchKey) in
                self?.handleSearch(searchKey)
            }
        default :
            if let tempCell = cell as? HomeProductParentCell{
                tempCell.suppliers = Localize.currentLanguage() == Languages.Arabic ? (item as? Home)?.arrayRecommendedAR : (item as? Home)?.arrayRecommendedEN
                tempCell.delegate = self
            }
            break
        }
    }
    
    
    func itemClicked(atIndexPath indexPath : NSIndexPath , type : HomeScreenSection){
        
        switch type{
        case .ServiceTypes :
            break
        case .Banners :
            break
        default :
            break
        }
    }
}

//MARK: - Product Click Listerner
extension HomeViewController : HomeProductCollectionViewDelegate {
    
    func collectionViewItemClicked(atIndexPath indexPath: NSIndexPath, type: AnyObject?) {
        
        if let tempSupplier = type as? Supplier {
            ez.runThisInMainThread {
                weak var weakSelf : HomeViewController? = self
                let catSelectionVc = StoryboardScene.Options.instantiateCategorySelectionController()
                catSelectionVc.supplier = tempSupplier
                catSelectionVc.ISPushAnimation = true
                catSelectionVc.arrCategory = tempSupplier.categories
                weakSelf?.pushVC(catSelectionVc)
            
            }
        }
        if let tempProduct = type as? Product {
            ez.runThisInMainThread {
                weak var weakSelf : HomeViewController? = self
                let productDetail = StoryboardScene.Main.instantiateProductDetailViewController()
                productDetail.passedData.productId = tempProduct.id
                productDetail.suplierBranchId = tempProduct.supplierBranchId
                weakSelf?.pushVC(productDetail)
            }
        }
    }
    
}


//MARK: - Service Type Delegate Conformance
extension HomeViewController : ServiceTypeDelegate{
    
    func categoryClicked(atIndexPath indexPath: NSIndexPath , service : ServiceType?) {
        
        AdjustEvent.CategoryDetail.sendEvent()
        let category = home?.arrayServiceTypesEN?[indexPath.row]
        
        switch category?.order ?? ""{
        case Category.Laundry.rawValue:
            guard let _ = GDataSingleton.sharedInstance.loggedInUser?.id else {
                presentVC(StoryboardScene.Register.instantiateLoginViewController())
                return
            }
        case Category.BeautySalon.rawValue:
            view.makeToast("Coming Soon!")
            return
//            guard let _ = GDataSingleton.sharedInstance.loggedInUser?.id else {
//                presentVC(StoryboardScene.Register.instantiateLoginViewController())
//                return
//            }
//            UtilityFunctions.showActionSheet(withTitle: L10n.SelectService.string, subTitle: nil, vc: self, senders: [L10n.HOMESERVICE.string,L10n.ATPLACESERVICE.string], success: {[weak self] (text, index) in
//                guard let title = text as? String else { return }
//               self?.configureBeautySalonFlow(title,indexPath: indexPath)
//            })
        default:
            break
        }
        if indexPath.row == 2 {
            guard let _ = GDataSingleton.sharedInstance.loggedInUser?.id else {
                presentVC(StoryboardScene.Register.instantiateLoginViewController())
                return
            }
        }
        ez.runThisInMainThread {[weak self] in
            
            let category = Localize.currentLanguage() == Languages.Arabic ? self?.home?.arrayServiceTypesAR?[indexPath.row] : self?.home?.arrayServiceTypesEN?[indexPath.row]
            
            self?.passedData = PassedData(withCatergoryId: category?.id, categoryFlow: category?.category_flow,supplierId: nil ,subCategoryId: nil ,productId: nil,branchId: nil,subCategoryName: nil , categoryOrder: category?.order,categoryName : category?.name)
            self?.pushNextVc()
        }
    }
    
    func configureBeautySalonFlow(title : String,indexPath : NSIndexPath) {
        let category = home?.arrayServiceTypesEN?[indexPath.row]
        
        passedData = PassedData(withCatergoryId: category?.id, categoryFlow: category?.category_flow,supplierId: nil ,subCategoryId: nil ,productId: nil,branchId: nil,subCategoryName: nil , categoryOrder: category?.order,categoryName : category?.name)
        switch title {
        case L10n.HOMESERVICE.string:
            let VC = StoryboardScene.Laundry.instantiatePickupDetailsController()
            VC.isBeautySalon = true
            VC.passedData = passedData
            pushVC(VC)
            return
        case L10n.ATPLACESERVICE.string:
            
            pushNextVc()
        default:
            break
        }
    }
}


//MARK: - Action Methods
extension HomeViewController {
    
    @IBAction func actionCart(sender: AnyObject) {
        pushVC(StoryboardScene.Options.instantiateCartViewController())
    }
    
    @IBAction func actionLanguage(sender: UIButton) {
        
        if DBManager.sharedManager.isCartEmpty() {
            changeLanguage(sender)
        }else {
            UtilityFunctions.showSweetAlert(L10n.AreYouSure.string, message: L10n.ChangingTheLanguageWillClearYourCart.string, success: { [weak self] in
                self?.changeLanguage(sender)
            }) {
                
            }
        }
    }
    
    @IBAction func actionArea(sender : UIButton) {
        
        let VC = StoryboardScene.Register.instantiateSplashViewController()
        VC.delegate = self
        presentVC(VC)
    }
    
    @IBAction func actionMenu(sender: UIButton) {
        toggleSideMenuView()
//        LNNotificationCenter.defaultCenter().registerApplicationWithIdentifier("123", name: "Bka", icon: UIImage(asset: Asset.Ic_lp_medal), defaultSettings: LNNotificationAppSettings.defaultNotificationAppSettings())
//        let notification = LNNotification(message: "A Notification", title: "Clikat", icon: UIImage(asset:Asset.Bg_splash), date: NSDate())
//        
//        notification.defaultAction = LNNotificationAction(title: "", handler: { (action) in
//            
//        })
        
        
    }
    
    func viewAllOffers(sender : UIButton?){

        let VC = StoryboardScene.Main.instantiateItemListingViewController()
        VC.isOffers = true
        pushVC(VC)
    }
    
    func changeLanguage(sender : UIButton){
        let delegate = UtilityFunctions.sharedAppDelegateInstance()
        sender.selected ? delegate.switchViewControllers(isArabic: false) : delegate.switchViewControllers(isArabic: true)
        sender.selected.toggle()
        DBManager.sharedManager.deleteAllData(CoreDataEntity.Cart.rawValue)
    }
}

//MARK: - Splash Delegate
extension HomeViewController : SplashViewControllerDelegate {
    func locationSelected() {
        btnArea.setTitle(LocationSingleton.sharedInstance.location?.getArea()?.name ?? "", forState: .Normal)
        webserviceHomeData()
    }
}

//MARK: - TextField delegate 
extension HomeViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        handleSearch(textField.text)
        return true
    }
    
    func openBarCodeScanner(){
        if UtilityFunctions.isCameraPermission() {
            let barCodeVc = StoryboardScene.Options.instantiateBarCodeScannerViewController()
            barCodeVc.isCompareProducts = true
            pushVC(barCodeVc)
        }else {
            UtilityFunctions.alertToEncourageCameraAccessWhenApplicationStarts(self)
        }
    }
    
    func handleSearch(searchKey : String?){
        view.endEditing(true)
        let VC = StoryboardScene.Options.instantiateCompareProductsController()
        VC.searchKey = searchKey
        VC.isHomeSearch = true
        pushVC(VC)
    }
}