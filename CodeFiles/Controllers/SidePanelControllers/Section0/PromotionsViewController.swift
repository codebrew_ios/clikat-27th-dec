//
//  PromotionsViewController.swift
//  Clikat
//
//  Created by cbl73 on 4/21/16.
//  Copyright © 2016 Gagan. All rights reserved.
// https://invis.io/FC70NOKR5#/151930110_Bg_Splash-3x

import UIKit
import EZSwiftExtensions

class PromotionsViewController: BaseViewController {

    @IBOutlet weak var viewPlaceholder : UIView!
    
    var promotionListing : PromotionListing? {
        didSet{
            dataSource.items = promotionListing?.arrayPromotions
            ez.runThisInMainThread { 
                weak var weakSelf : PromotionsViewController? = self
                weakSelf?.tableView.reloadTableViewData(inView: weakSelf?.view)
                
                guard let promotions = weakSelf?.promotionListing?.arrayPromotions where promotions.count > 0 else{
                    weakSelf?.viewPlaceholder?.hidden = false
                    return
                }
                weakSelf?.viewPlaceholder?.hidden = true
            }
        }
    }
    
    @IBOutlet var tableView: UITableView!{
        didSet{
            tableView?.estimatedRowHeight = 44.0
            tableView?.rowHeight = UITableViewAutomaticDimension
        }
    }
    var dataSource : TableViewDataSource = TableViewDataSource(){
        didSet{
            tableView?.dataSource = dataSource
            tableView?.delegate = dataSource
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        webService()
        AdjustEvent.Promotions.sendEvent()
    }
    
}

//MARK: - Web Service Methods
extension PromotionsViewController {
    func webService(){
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.PromotionProducts(FormatAPIParameters.PromotionProducts().formatParameters())) { (response) in
            weak var weakSelf : PromotionsViewController? = self
            switch response{
            case .Success(let listing):
                weakSelf?.promotionListing = listing as? PromotionListing
            default :
                break
            }
        }
        
    }
}

//MARK: - TableView configuration Methods
extension PromotionsViewController{
    
    func configureTableView(){
        
        dataSource = TableViewDataSource(items: promotionListing?.arrayPromotions , height: UITableViewAutomaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.PromotionsCell , configureCellBlock: { (cell, item) in
              (cell as? PromotionsCell)?.promotion = item as? Promotion
            }, aRowSelectedListener: { [weak self] (indexPath) in
                
                
                self?.handleCellSelection(indexPath)
        })
    }
    
    func handleCellSelection(indexPath : NSIndexPath){
        let VC = StoryboardScene.Main.instantiateProductDetailViewController()
        let promotion = promotionListing?.arrayPromotions?[indexPath.row]
        APIManager.sharedInstance.showLoader()
        APIManager.sharedInstance.opertationWithRequest(withApi: API.SupplierImage(FormatAPIParameters.SupplierImage(supplierBranchId: promotion?.supplierBranchId).formatParameters())) { [weak self] (response) in
            APIManager.sharedInstance.hideLoader()
            switch response{
            case .Success(let object):
                guard let image = object as? String else { return }
                let supplier = Supplier()
                supplier.id = promotion?.supplierId
                supplier.supplierBranchId = promotion?.supplierBranchId
                supplier.logo = image.componentsSeparatedByString(" ").first?.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
                GDataSingleton.sharedInstance.currentSupplier = supplier
                VC.passedData.supplierId = promotion?.supplierId
                VC.suplierBranchId = promotion?.supplierBranchId
                VC.passedData.categoryId = promotion?.categoryId
                VC.passedData.productId = promotion?.id
                VC.isPromotion = true
                self?.pushVC(VC)
            default :
                break
            }
        }
    }
}
 
//MARK: - Button Actions

extension PromotionsViewController{
    
    @IBAction func actionCart(sender: AnyObject) {
        pushVC(StoryboardScene.Options.instantiateCartViewController())
    }
    @IBAction func actionMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
}