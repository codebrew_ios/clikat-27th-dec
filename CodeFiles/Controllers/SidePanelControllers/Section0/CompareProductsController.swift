//
//  CompareProductsController.swift
//  Clikat
//
//  Created by cblmacmini on 7/18/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import ESPullToRefresh

class CompareProductsController: BaseViewController {

    @IBOutlet weak var constraintTableViewBottom : NSLayoutConstraint!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var viewPlaceholder : UIView!
    var pageNo : Int = 0
    
    var isHomeSearch = false
    
    var searchKey : String?
    
    @IBOutlet weak var tableView: UITableView!
    var tableDataSource : TableViewDataSource?{
        didSet{
            
            tableView.reloadData()
        }
    }
    
    var productListing : CompareProductListing?{
        didSet{
            viewPlaceholder?.hidden = productListing?.arrProducts?.count == 0 ? false : true
            configureTableView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AdjustEvent.CompareProducts.sendEvent()
        
        if isHomeSearch {
            tfSearch?.text = searchKey
            getAllProducts(searchKey)
        }
        tableView.es_addInfiniteScrolling { [weak self] in
            self?.tableView.es_stopLoadingMore()
            self?.pageNo = /self?.tableDataSource?.items?.count + 1
            self?.getAllProductsPaging(self?.pageNo.toString)
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

//MARK: - Products Web Service
extension CompareProductsController {
    
    func getAllProducts(name : String?){
        if name == "" { return }
        APIManager.sharedInstance.opertationWithRequest(withApi: API.CompareProducts(FormatAPIParameters.CompareProducts(productName: name, startValue: "0").formatParameters())) {[weak self] (response) in
            switch response {
            case .Success(let object):
                self?.productListing  = object as? CompareProductListing
            default:
                break
            }
        }
    }
    
    func getAllProductsPaging(startValue : String?){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.CompareProducts(FormatAPIParameters.CompareProducts(productName: self.searchKey, startValue: startValue).formatParameters())) {[weak self] (response) in
            switch response {
            case .Success(let object):
                var products = (self?.tableDataSource?.items as? [Product]) ?? []
                guard let newProducts = (object as? CompareProductListing)?.arrProducts where newProducts.count > 0 else {
                    self?.tableView.es_noticeNoMoreData()
                    return
                }
                products.appendContentsOf(newProducts)
                self?.tableDataSource?.items = products
                self?.tableView.reloadTableViewData(inView: self?.view)
            default:
                break
            }
        }
    }
    
    func configureTableView(){
        
        
        tableDataSource = TableViewDataSource(items: productListing?.arrProducts, height: 105, tableView: tableView, cellIdentifier: CellIdentifiers.CompareProductsCell, configureCellBlock: { [weak self] (cell, item) in
            
            
            self?.configureCell(cell, item: item)
            
            }, aRowSelectedListener: { [weak self] (indexPath) in
                
                
                
                self?.configureCellSelection(indexPath)
                
                
        })
        tableView.delegate = tableDataSource
        tableView.dataSource = tableDataSource
    }
    
    func configureCellSelection(indexPath : NSIndexPath){
        
        self.view.endEditing(true)
        let VC = StoryboardScene.Options.instantiateCompareProductResultController()
        VC.product = tableDataSource?.items?[indexPath.row] as? Product
        pushVC(VC)
    }
    func configureCell(cell : AnyObject?,item : AnyObject?){
        (cell as? CompareProductsCell)?.product = item as? Product
    }
}

//MARK: - Button Actions

extension CompareProductsController {
    
    
    @IBAction func actionMenu(sender: UIButton) {
        toggleSideMenuView()
    }
    @IBAction func actionBarcodeScan(sender: UIButton) {
        if UtilityFunctions.isCameraPermission() {
            let barCodeVc = StoryboardScene.Options.instantiateBarCodeScannerViewController()
            barCodeVc.isCompareProducts = true
            pushVC(barCodeVc)
        }else {
            UtilityFunctions.alertToEncourageCameraAccessWhenApplicationStarts(self)
        }
    }
    @IBAction func actionSearch(sender: UIButton) {
        searchProduct(tfSearch)
        view.endEditing(true)
    }
}

//MARK: - TextFieldAction
extension CompareProductsController {
    
    @IBAction func textFieldEditingChanged(sender: UITextField) {
        
    }

    func searchProduct(sender : UITextField) {
        guard let searchKey = sender.text where searchKey.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()).characters.count != 0 else {
            return
        }
        if self.searchKey != searchKey {
            tableDataSource?.items = []
            pageNo = 0
        }
        self.searchKey = searchKey
        getAllProducts(searchKey)

//        let tempArr = productListing?.arrProducts?.filter({ (product) -> Bool in
//            guard let name = product.name?.lowercaseString else { return false }
//            return name.contains(searchKey.lowercaseString)
//        })
//        
//        
//        if tempArr?.count > 0 {
//            tableDataSource?.items = tempArr
//            tableView.reloadData()
//        }else {
//            tableDataSource?.items = []
//            productListing?.arrProducts = []
//            tableView.reloadData()
//        }

    }
}

//MARK: - TextField Delegates

extension CompareProductsController : UITextFieldDelegate {
    func textFieldDidBeginEditing(textField: UITextField) {
        constraintTableViewBottom.constant = 260
        view.layoutIfNeeded()
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        constraintTableViewBottom.constant = 0
        view.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        view.endEditing(true)
        searchProduct(textField)
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
}
