//
//  LoginViewController.swift
//  Clikat
//
//  Created by Night Reaper on 20/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import TYAlertController
import Material
import EZSwiftExtensions
import Adjust

protocol LoginViewControllerDelegate : class {
    
    func userSuccessfullyLoggedIn(withUser user : User?)
    func userFailedLoggedIn()
    
}

class LoginViewController: LoginRegisterBaseViewController {


    weak var delegate : LoginViewControllerDelegate?
    
    @IBOutlet weak var tfEmail: TextField!
    @IBOutlet weak var tfPassword: TextField!
  
    
    var isFromSideMenu : Bool = false
    
    
}


//MARK: - Button Actions
extension LoginViewController{

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }

    
    @IBAction func actionBack(sender: UIButton) {
        dismissVC {
    
        }
    }
    
    
    @IBAction func actionSignUp(sender: UIButton) {
        presentVC(StoryboardScene.Register.instantiateRegisterFirstStepController())
    }
    @IBAction func actionForgotPass(sender: AnyObject) {
        
        let alert = TYAlertView(title: L10n.ForgotPassword.string , message: nil)
        let alertController = TYAlertController(alertView: alert, preferredStyle: .Alert, transitionAnimation: .ScaleFade)
        
        let action = TYAlertAction(title: L10n.Send.string, style: .Default) {[weak self] (action) in
            alertController.view.endEditing(true)
            alert.hideView()
            self?.handleForgotPassword((alert.textFieldArray?.first as? UITextField)?.text)
        }
        alert.buttonDefaultBgColor = Colors.MainColor.color()
        alert.textFieldFont = UIFont(name: Fonts.ProximaNova.Regular, size: Size.Small.rawValue)
        alert.addAction(TYAlertAction(title: L10n.Cancel.string , style: .Destructive, handler: { (action) in
            alert.hideView()
        }))
        alert.addAction(action)
            alert.addTextFieldWithConfigurationHandler { (textField) in
                textField.placeholder = L10n.Email.string
                textField.tag = 1
                textField.returnKeyType = .Done
                textField.autocorrectionType = .No
                textField.spellCheckingType = .No
            }
        presentVC(alertController)

    }
    
    @IBAction func actionLogin(sender: AnyObject) {
        
        guard let email = tfEmail.text, password = tfPassword.text else { return }
        let message = Register.validateCredentials(email, password: password)
        
        if message.characters.count == 0 {
            
            
            APIManager.sharedInstance.opertationWithRequest(withApi: API.Login(FormatAPIParameters.Login(email: email, password: password).formatParameters())) { (response) in
                
                weak var weakSelf : LoginViewController? = self
                switch response {
                case APIResponse.Success(_):
                    QGSdk.getSharedInstance().setEmail(email)
                    QGSdk.getSharedInstance().setCustomKey("language", withValue: Localize.currentLanguage() == Languages.Arabic ? "arabic" : "english")
                    if GDataSingleton.sharedInstance.tokenData != nil {
                        QGSdk.getSharedInstance().setToken(GDataSingleton.sharedInstance.tokenData)
                    }
                    AdjustEvent.Login.sendEvent()
                    ez.runThisInMainThread({
                        weakSelf?.dismissVC(completion: {
                           // weakSelf?.delegate?.userSuccessfullyLoggedIn(withUser: user as? User)
                        })
                    })
                case APIResponse.Failure(let validation):
                    weakSelf?.view.makeToast(validation.message)
                }
                
            }
        }else{
            view.makeToast(message)
        }
    }
    
    @IBAction func actionLoginFb(sender: AnyObject) {
        
        FacebookManager.sharedManager.configureLoginManager(self) { (facebook) in
            weak var weakSelf = self
            UtilityFunctions.startLoader()
            weakSelf?.handleFacebookLogin(facebook)
        }
    }
}

extension LoginViewController {

    func handleFacebookLogin(facebookProfile : Facebook?){
        
        guard let profile = facebookProfile else { return }
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.LoginFacebook(FormatAPIParameters.FacebookLogin(fbProfile: profile).formatParameters())) { (response) in
            UtilityFunctions.stopLoader()
                weak var weakSelf = self
            switch response {
            case .Success(let object) :
                weakSelf?.facebookLoginWebServicehandler(object)
            default:
                break
            }
        }
    }
    
    func facebookLoginWebServicehandler(object : AnyObject?){
        guard let user = object as? User else { return }
        GDataSingleton.sharedInstance.loggedInUser = object as? User
        QGSdk.getSharedInstance().setEmail(user.email)
        QGSdk.getSharedInstance().setCustomKey("language", withValue: Localize.currentLanguage() == Languages.Arabic ? "arabic" : "english")
        if GDataSingleton.sharedInstance.tokenData != nil {
            QGSdk.getSharedInstance().setToken(GDataSingleton.sharedInstance.tokenData)
        }
        if let otpVerified = user.otpVerified where otpVerified == "0"{
            let VC = StoryboardScene.Register.instantiatePhoneNoViewController()
            VC.user = user
            presentVC(VC)
        }else {
            dismissVC(completion: nil)
        }
    }
}

//MARK: - Forgot password
extension LoginViewController {

    func handleForgotPassword(email : String?){
        guard let emailId = email where Register.isValidEmail(emailId) else {
            UtilityFunctions.sharedAppDelegateInstance().window?.makeToast(L10n.PleaseEnterAValidEmailAddress.string)
            return
        }
        
        APIManager.sharedInstance.showLoader()
        APIManager.sharedInstance.opertationWithRequest(withApi: API.ForgotPassword(FormatAPIParameters.ForgotPassword(email: email).formatParameters())) {[weak self] (response) in
            APIManager.sharedInstance.hideLoader()
            switch response {
            case .Success(_):
                self?.view.makeToast(L10n.PasswordRecoveryHasBeenSentToYourEmailId.string)
            default:
                break
            }
        }
    }
}
