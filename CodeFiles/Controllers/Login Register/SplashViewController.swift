//
//  SplashViewController.swift
//  Clikat
//
//  Created by cblmacmini on 5/12/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material

enum SelectedLocation : Int {
    case Country = 1
    case City
    case Zone
    case Area
    case None
}

protocol SplashViewControllerDelegate {
    func locationSelected()
}

class SplashViewController: UIViewController {

    @IBOutlet weak var imageViewSplash: UIImageView!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var logoBgView: UIView!
    @IBOutlet weak var bgView: MaterialView!
    
    @IBOutlet weak var btnCountry: UIButton!
    @IBOutlet weak var btnCity: UIButton!
    @IBOutlet weak var btnZone: UIButton!
    @IBOutlet weak var labelArea: UILabel!
    
    var delegate : SplashViewControllerDelegate?
    var countryAr : Locations?
    var cityAr : Locations?
    var areaAr : Locations?
    
    var countryEn : Locations?
    var cityEn : Locations?
    var areaEn : Locations?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
}

//MARK: - Setup UI and Animation
extension SplashViewController {
    
    func setupUI(){
        let image = UtilityFunctions.blendImage(imageViewSplash.image, color: UIColor(white: 1.0, alpha: 0.1))
        btnContinue.setBackgroundImage(image, forState: .Normal)
        btnContinue.backgroundColor = UIColor(hexString: Colors.MainColor.rawValue)?.colorWithAlphaComponent(0.1)
        setupAnimation()
        configureLanguage()
        if !LocationSingleton.sharedInstance.isLocationSelected() {
            webServiceForLocation()
        }
        guard let location = LocationSingleton.sharedInstance.location else { return }
        btnCountry.setTitle(location.getCountry()?.name, forState: .Normal)
        btnCity.setTitle(location.getCity()?.name, forState: .Normal)
        btnZone.setTitle(location.getArea()?.name, forState: .Normal)
        
        btnCountry.setTitleColor(UIColor.blackColor(), forState: .Normal)
        btnCity.setTitleColor(UIColor.blackColor(), forState: .Normal)
        btnZone.setTitleColor(UIColor.blackColor(), forState: .Normal)
        self.countryEn = location.countryEN
        self.cityEn = location.cityEN
        self.areaEn = location.areaEN
        
        self.countryAr = location.countryAR
        self.cityAr = location.cityAR
        self.areaAr = location.areaAR
    }
    
    func setupAnimation(){
        logoBgView.transform = CGAffineTransformMakeTranslation(0, -logoBgView.frame.height)
        bgView.transform = CGAffineTransformMakeScale(0, 0)
        
        UIView.animateWithDuration(1.5, delay: 0.4, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.3, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
            weak var weakSelf = self
            weakSelf?.logoBgView.transform = CGAffineTransformIdentity
            weakSelf?.bgView.transform = CGAffineTransformIdentity
            }) { (complete) in
        }
    }
    
    func configureLanguage(){
        if Localize.currentLanguage() == Languages.Arabic {
            btnCity.contentHorizontalAlignment = .Right
            btnCountry.contentHorizontalAlignment = .Right
            btnZone.contentHorizontalAlignment = .Right
            btnCountry.titleEdgeInsets = UIEdgeInsets(top: 0, left: 60, bottom: 0, right: 0)
        }
    }
}

extension SplashViewController {
    
    func stringArrayForDataSource(data : AnyObject?) -> [String]{
        
//        guard let location = data as? LocationResults,arrLocation = location.arrLocation else { return [] }
//        var arrString : [String] = []
//        for result in arrLocation {
//            arrString.append(result.name ?? "")
//        }
//        return arrString
        return [""]
    }
}

//MARK: - Button Actions
extension SplashViewController {
   
    
    @IBAction func actionCountry(sender: UIButton) {
        
        let VC = StoryboardScene.Register.instantiateLocationViewController()
        VC.delegate = self
        VC.locationType = .Country
        presentVC(VC)
    }
    
    @IBAction func actionCity(sender: UIButton) {
        let message = validateType(.City)
        if message.characters.count == 0 {
            let VC = StoryboardScene.Register.instantiateLocationViewController()
            VC.delegate = self
            VC.countryId = countryEn?.id
            VC.locationType = .City
            presentVC(VC)
        }else { self.view.makeToast(message) }
    }
    @IBAction func actionZone(sender: UIButton) {
        let message = validateType(.Area)
        
        if message.characters.count == 0{
            let VC = StoryboardScene.Register.instantiateLocationViewController()
            VC.delegate = self
            VC.countryId = countryEn?.id
            VC.cityId = cityEn?.id
            VC.locationType = .Area
            presentVC(VC)
        }else {
            view.makeToast(message)
        }
    }
    
    @IBAction func actionContinue(sender: UIButton) {
        
        guard let _ = countryEn, _ = cityEn, _ = areaEn,_ = countryAr, _ = cityAr, _ = areaAr else {
            view.makeToast(L10n.PleaseSelectAllFieldsAbove.string)
            return
        }
        let applicationLocation = ApplicationLocation(countryEn: countryEn,cityEn : cityEn,areaEn : areaEn,countryAr : countryAr,cityAr : cityAr,areaAr : areaAr)
        LocationSingleton.sharedInstance.location = applicationLocation
        dismissVC {[weak self] in
            
            guard let nav = self?.topMostController() as? UINavigationController where !(nav.topViewController is  HomeViewController) else {
                self?.delegate?.locationSelected()
               return
            }
            
            
            //self?.topMostController()
            self?.sideMenuController()?.setContentViewController(StoryboardScene.Main.instantiateHomeViewController())
            self?.toggleSideMenuView()
        }
    }
}

extension SplashViewController : LocationViewControllerDelegate {
    
    func locationSelected(location: Locations?,locationEnglish : Locations?, type: SelectedLocation?) {
        
        guard let selectedLocationType = type else { return }
        resetFields(selectedLocationType)
        switch selectedLocationType {
        case .Country:
            countryEn = locationEnglish
            countryAr = location
            btnCountry.setTitle(Localize.currentLanguage() == Languages.Arabic ? location?.name : locationEnglish?.name, forState: .Normal)
            btnCountry.setTitleColor(UIColor.blackColor(), forState: .Normal)
            case .City:
            cityAr = location
            cityEn = locationEnglish
            btnCity.setTitle(Localize.currentLanguage() == Languages.Arabic ? location?.name : locationEnglish?.name, forState: .Normal)
            btnCity.setTitleColor(UIColor.blackColor(), forState: .Normal)
            case .Area:
            areaAr = location
            areaEn = locationEnglish
            btnZone.setTitle(Localize.currentLanguage() == Languages.Arabic ? location?.name : locationEnglish?.name, forState: .Normal)
            btnZone.setTitleColor(UIColor.blackColor(), forState: .Normal)
        default:
            break
        }
    }
}

//MARK: - Reset and Validate

extension SplashViewController {
    func resetFields(type : SelectedLocation){
        switch type {
        case .Country:
            btnCity.setTitle(L10n.City.string, forState: .Normal)
            btnZone.setTitle(L10n.Area.string, forState: .Normal)
            btnCity.setTitleColor(UIColor.blackColor().colorWithAlphaComponent(0.25), forState: .Normal)
            btnZone.setTitleColor(UIColor.blackColor().colorWithAlphaComponent(0.25), forState: .Normal)
            cityEn = nil
            areaEn = nil
            cityAr = nil
            areaAr = nil
        case .City:
            btnZone.setTitle(L10n.Area.string, forState: .Normal)
            btnZone.setTitleColor(UIColor.blackColor().colorWithAlphaComponent(0.25), forState: .Normal)
            areaEn = nil
            areaAr = nil
        case .Zone:
            areaEn = nil
            areaAr = nil
        default:
            break
        }
    }
    
    func validateType(selectedType : SelectedLocation) -> String{
        
        switch selectedType {
        case .City:
            return countryEn == nil ? L10n.PleaseSelectACountry.string : ""
        case .Zone:
            
            guard let _ = countryEn else {
                return L10n.PleaseSelectACountry.string
            }
            guard let _ = cityEn else { return L10n.PleaseSelectACity.string }
            
            return ""
        default:
            return ""
        }
    }
}

//MARK: - Web service for Area

extension SplashViewController {
    
    func webServiewForArea(){
        
        let params = FormatAPIParameters.LocationList(type: .Area, id: areaEn?.id).formatParameters()
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.LocationList(params, type: .Area)) { (response) in
            
            weak var weakSelf = self
            switch response {
            case .Success(let locationResults):
            weakSelf?.handleAreaWebService(locationResults)
            default:
                break
            }
        }
    }
    
    func handleAreaWebService(locationResults : AnyObject?){
    
        guard let results = locationResults as? LocationResults else { return }
        
        UtilityFunctions.showActionSheet(withTitle: L10n.SelectArea.string, subTitle: nil, vc: self, senders: stringArrayForDataSource(locationResults)) { (selectedResult,index) in
//            weak var weakSelf = self
//            guard let area = selectedResult as? String else { return }
//            weakSelf?.labelArea.text = area
//            weakSelf?.labelArea.textColor = UIColor.blackColor()
//            weakSelf?.area = results.arrLocation?[index]
        }
    }
}

//MARK: - Set Default Country

extension SplashViewController {
    func webServiceForLocation(){
        let params = FormatAPIParameters.LocationList(type: .Country, id: nil).formatParameters()

        APIManager.sharedInstance.opertationWithRequest(withApi: API.LocationList(params, type: .Country)) { [weak self] (response) in
            
            switch response {
            case .Success(let locationResults):
                let location = (locationResults as? LocationResults)
                self?.countryEn = location?.arrLocationEn?.first
                self?.countryAr = location?.arrLocationAr?.first
               self?.resetFields(.Country)
                self?.locationSelected(self?.countryAr,locationEnglish:self?.countryEn , type: .Country)
            default:
                break
            }
        }
    }
}

