//
//  OTPViewController.swift
//  Clikat
//
//  Created by cbl73 on 4/21/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material

class OTPViewController: LoginRegisterBaseViewController {

    var user : User?
    
    @IBOutlet weak var btnSubmit : UIButton!{
        didSet{
            btnSubmit.kern(ButtonKernValue)
        }
    }
    @IBOutlet weak var tfOTP: TextField!
    @IBOutlet weak var btnResend: UIButton!{
        didSet{
            btnResend?.enabled = false
        }
    }
    
    var timer : NSTimer?
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        timer = NSTimer.scheduledTimerWithTimeInterval(180, target: self, selector: #selector(OTPViewController.startTimer), userInfo: nil, repeats: false)
        
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        timer?.invalidate()
    }
    
    func startTimer(){
        btnResend?.enabled = false
    }
}


//MARK: - Button Actions

extension OTPViewController{
    
    @IBAction func actionBack(sender: UIButton) {
        dismissVC{}
    }
    @IBAction func actionSubmitOTP(sender: AnyObject) {
        
        let message = validateOTP()
        if message.characters.count == 0 {
            APIManager.sharedInstance.opertationWithRequest(withApi: API.CheckOTP(FormatAPIParameters.CheckOTP(accessToken :user?.token,OTP: tfOTP?.text).formatParameters())) { (response) in
            
            weak var weakSelf = self
            switch response {
            case APIResponse.Success(_):
                weakSelf?.handleCheckOTPResponse()
                break
            case APIResponse.Failure(_):
                break
            }
 
        }
        }else {
            view.makeToast(message)
        }
    }
    
    @IBAction func actionResendOTP(sender: UIButton) {
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.ResendOTP(FormatAPIParameters.ResendOTP(token: user?.token).formatParameters())) { (response) in
            weak var weakSelf = self
            switch response {
            case .Success(_):
                weakSelf?.view.makeToast(L10n.OTPSent.string)
            case .Failure(_): break
//                weakSelf?.view.makeToast("Please try again.")
            }
        }
    }
}

extension OTPViewController {
    
    func handleCheckOTPResponse(){
        let VC = StoryboardScene.Register.instantiateRegisterViewController()
        VC.user = user
        presentVC(VC)
    }
    
    func validateOTP() -> String{
        guard let otp = tfOTP.text else {
            return "Please enter OTP."
        }
        guard let _ = Int(otp) where otp.characters.count == 5 else {
            return "Please enter a valid OTP."
        }
        return ""
    }
}