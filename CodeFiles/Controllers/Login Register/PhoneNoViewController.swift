//
//  PhoneNoViewController.swift
//  Clikat
//
//  Created by cbl73 on 4/21/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material
import SwiftyJSON

enum CountryKeys : String {
    case name = "name"
    case dial_code = "dial_code"
    case code = "code"
}

class PhoneNoViewController: LoginRegisterBaseViewController {


    @IBOutlet weak var btnOTP : UIButton!{
        didSet{
            btnOTP.kern(ButtonKernValue)
        }
    }
    
    @IBOutlet weak var tfCountryCode: UITextField!
    @IBOutlet weak var tfPhoneNumber: TextField!
    
    var user : User?
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
}


//MARK: - Button Actions
extension PhoneNoViewController{
    
    @IBAction func actionSelectCountryCode(sender: UIButton) {
        
//        let countryController = CountryListViewController(nibName: "CountryListViewController", delegate: self)
//        presentVC(countryController)
    }
    @IBAction func actionBack(sender: UIButton) {
        dismissVC{}
    }
    
    
    @IBAction func actionSendOTP(sender: AnyObject) {
        
        guard let phoneNumber = tfPhoneNumber.text else { return }
        if Register.isValidPhoneNumber(phoneNumber) {
            let mobileNumber = phoneNumber
            APIManager.sharedInstance.opertationWithRequest(withApi: API.SendOTP(FormatAPIParameters.SendOTP(accessToken: user?.token,mobileNumber: mobileNumber,countryCode: "+971").formatParameters()), completion: { (response) in
                
                weak var weakSelf = self
                
                switch response {
                case APIResponse.Success(let object):
                    weakSelf?.handleOTPSentResponse(object)
                    break
                default:
                    break
                }
            })
        }else{
            view.makeToast(L10n.PleaseEnterValidCountryCodeFollwoedByPhoneNumber.string)
        }
    }
    
    
    func handleOTPSentResponse(tempUser : AnyObject?){
        
        let VC = StoryboardScene.Register.instantiateOTPViewController()
        VC.user = tempUser as? User
        presentVC(VC)
    }
    
}
//MARK: - Country Controller Delegate
extension PhoneNoViewController : CountryListViewDelegate {
    
    func didSelectCountry(countryName: String!, dialCode DialCode: String!, countryCode: String!) {
        
    }
}