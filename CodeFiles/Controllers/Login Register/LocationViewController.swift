//
//  LocationViewController.swift
//  Clikat
//
//  Created by cblmacmini on 5/12/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

protocol LocationViewControllerDelegate {
    func locationSelected(locationArabic : Locations?,locationEnglish : Locations?,type : SelectedLocation?)
}

class LocationViewController: LoginRegisterBaseViewController {
    
    
    @IBOutlet weak var constraintTableBottom: NSLayoutConstraint!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tfSearch: UITextField!{
        didSet{
            tfSearch.delegate = TextFieldDataSource(textField: tfSearch, sender: self)
        }
    }
    
    var delegate : LocationViewControllerDelegate?
    
    var locationType : SelectedLocation?
    var countryId : String?
    var cityId : String?
    var zoneId : String?
    
    var locations : [Locations]?
    
    var searchResultsEn : [Locations]? = []
    var searchResultsAr : [Locations]? = []
    var myLocations : [Address]?
    
    var tableDataSource = LocationDataSource(){
        didSet{
            tableView.dataSource = tableDataSource
            tableView.delegate = tableDataSource
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        webServiceForLocation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
//MARK: - Setup User Interface
extension LocationViewController {
    
    func setupUI(){
        
        guard let type = locationType else { return }
        switch type {
        case .Country:
            labelTitle.text = L10n.SelectCountry.string
            tfSearch.placeholder = L10n.SelectCountry.string
        case .City:
            labelTitle.text = L10n.SelectCity.string
            tfSearch.placeholder = L10n.SelectCity.string
        case .Area:
            labelTitle.text = L10n.SelectArea.string
            tfSearch.placeholder = L10n.SelectArea.string
        default:
            break
        }
    }
}

extension LocationViewController {
    
    func webServiceForLocation(){
        var params : OptionalDictionary = ["" : ""]
        
        guard let type = locationType else { return }
        switch type {
        case .Country:
            params = FormatAPIParameters.LocationList(type: type, id: nil).formatParameters()
        case .City:
            params = FormatAPIParameters.LocationList(type: type, id: countryId).formatParameters()
        case .Area:
            params = FormatAPIParameters.LocationList(type: type, id: cityId).formatParameters()
        default:
            break
        }
        APIManager.sharedInstance.showLoader()
        APIManager.sharedInstance.opertationWithRequest(withApi: API.LocationList(params, type: locationType ?? .None)) { [weak self] (response) in
            APIManager.sharedInstance.hideLoader()
            switch response {
            case .Success(let locationResults):
                self?.locations = (locationResults as? LocationResults)?.getArrLocation()
                self?.searchResultsEn = (locationResults as? LocationResults)?.arrLocationEn
                self?.searchResultsAr = (locationResults as? LocationResults)?.arrLocationAr
                self?.tableDataSource.items = (locationResults as? LocationResults)?.getArrLocation()
                self?.myLocations = (locationResults as? LocationResults)?.myAddress
                self?.configureTableView(locationResults)
            default:
                break
            }
        }
    }
    
    func configureTableView(searchResults : AnyObject?){
        guard let results = searchResults as? LocationResults else { return }
        tableDataSource = LocationDataSource(items: results.getArrLocation(),myLocations: results.myAddress, height: UITableViewAutomaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.LocationSearchCell, configureCellBlock: { [weak self] (cell, item) in
            guard let tempCell = cell as? LocationTableCell
                else { return }
           self?.configureCell(tempCell, indexPath: item)
            }, aRowSelectedListener: { (indexPath) in
                weak var weakSelf = self
                weakSelf?.locationSelectedAtIndex(indexPath)
        })
        tableView.reloadTableViewData(inView: view)
    }
    
    func configureCell(cell : AnyObject?,indexPath : AnyObject?){
        
        guard let indexpath = indexPath as? NSIndexPath,locations = tableDataSource.items else { return }
        if tableDataSource.myLocations?.count > 0 {
            if indexpath.section == 0{
            let address = UtilityFunctions.appendOptionalStrings(withArray: [myLocations?[indexpath.row].name,myLocations?[indexpath.row].area], separatorString: ", ")
                (cell as? LocationTableCell)?.labelTitle.text =  address
                return
            }
            (cell as? LocationTableCell)?.labelTitle.text = locations[indexpath.row].name
        }else {
            (cell as? LocationTableCell)?.labelTitle.text = locations[indexpath.row].name
        }
    }
    
    func locationSelectedAtIndex(indexPath : NSIndexPath){
        
        switch locationType {
        case .Some(let type):
            if LocationSingleton.sharedInstance.location?.areaEN?.id == nil {
               fallthrough
            }else if type == .Area && searchResultsAr?[indexPath.row].id != LocationSingleton.sharedInstance.location?.areaEN?.id {
                if DBManager.sharedManager.isCartEmpty() {
                    handleLocationSelection(indexPath)
                    DBManager.sharedManager.deleteAllData(CoreDataEntity.Cart.rawValue)
                }else {
                    UtilityFunctions.showSweetAlert(L10n.AreYouSure.string, message: L10n.ChangingTheCurrentAreaWillClearYouCart.string, success: { [weak self] in
                        self?.handleLocationSelection(indexPath)
                        DBManager.sharedManager.deleteAllData(CoreDataEntity.Cart.rawValue)
                        }, cancel: {
                            
                    })
                }
                return
            }
            fallthrough
        default:
            handleLocationSelection(indexPath)
        }
    }
    
    func handleLocationSelection(indexPath : NSIndexPath){
        dismissVC { [weak self] in
            if self?.myLocations?.count > 0 && indexPath.section == 0 && !(self?.tfSearch.text?.trim().characters.count > 0) {
                let index = self?.searchResultsEn?.indexOf({ $0.id == self?.myLocations?[indexPath.row].areaId })
                let arabicIndex = self?.searchResultsAr?.indexOf({ $0.id == self?.myLocations?[indexPath.row].areaId })
                self?.delegate?.locationSelected( self?.searchResultsAr?[arabicIndex ?? 0],locationEnglish: self?.searchResultsEn?[index ?? 0] , type: self?.locationType)
                return
            }
            
            if Localize.currentLanguage() == Languages.Arabic {
                let index = self?.searchResultsEn?.indexOf({ $0.id == self?.searchResultsAr?[indexPath.row].id })
                self?.delegate?.locationSelected( self?.searchResultsAr?[indexPath.row],locationEnglish: self?.searchResultsEn?[index
                     ?? 0] , type: self?.locationType)
            }
            else{
                let index = self?.searchResultsAr?.indexOf({ $0.id == self?.searchResultsEn?[indexPath.row].id })
                self?.delegate?.locationSelected( self?.searchResultsAr?[index ?? 0],locationEnglish: self?.searchResultsEn?[indexPath.row] , type: self?.locationType)
            }
            
        }
    }
}

extension LocationViewController {
    
    func configureLocationSelection(data : AnyObject?){
        
        guard let locationResults = data as? LocationResults,arrResults = locationResults.getArrLocation() else { return }
        var arrStrings = [String]()
        for result in arrResults {
            
            arrStrings.append(result.name ?? "")
        }
    }
}

//MARK: - Actions
extension LocationViewController {
    
    @IBAction func textFieldChanged(sender: UITextField) {
        tableDataSource.myLocations = sender.text?.trim().characters.count > 0 ? [] : myLocations
        guard let tempLocations = locations else { return }
        let filterSuppliers = tempLocations.filter({ (location) -> Bool in
            guard let match = location.name?.lowercaseString.contains(sender.text!.lowercaseString) else { return false }
            return match
        })
        tableDataSource.items = sender.text?.characters.count == 0 ? tempLocations : filterSuppliers
        
        if Localize.currentLanguage() == Languages.Arabic {
            searchResultsAr = sender.text?.characters.count == 0 ? tempLocations : filterSuppliers
        }
        else{
            searchResultsEn = sender.text?.characters.count == 0 ? tempLocations : filterSuppliers
        }
        tableView.reloadData()
    }
    
    @IBAction func actionBack(sender: UIButton) {
        dismissVC { }
    }
}

extension LocationViewController : UITextFieldDelegate {
    
    override func textFieldDidBeginEditing(textField: UITextField) {
        constraintTableBottom.constant = 216
    }
    
    override func textFieldDidEndEditing(textField: UITextField) {
        constraintTableBottom.constant = 0
    }
}