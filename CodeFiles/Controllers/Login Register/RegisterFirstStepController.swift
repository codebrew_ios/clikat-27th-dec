//
//  RegisterFirstStepController.swift
//  Clikat
//
//  Created by cblmacmini on 4/27/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material

class RegisterFirstStepController: LoginRegisterBaseViewController {


    @IBOutlet weak var labelPrivacyPolicy: UILabel!
    @IBOutlet weak var tfEmail: TextField!
    @IBOutlet weak var tfPassword: TextField!
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        let normalTextS = L10n.BySigningUpYouAgreeToThe.string
        let boldPrivacy  = L10n.PrivacyPolicy.string
        let boldTerms = L10n.TermsAndConditionsSignUp.string
        
        let attributedString1 = NSMutableAttributedString(string:normalTextS)
        let attributedString2 = NSMutableAttributedString(string: L10n.And.string)
        let attrs = [NSFontAttributeName : UIFont(name: Fonts.ProximaNova.Bold,size: 12)!]
        let boldStringP = NSMutableAttributedString(string:boldPrivacy, attributes:attrs)
        let boldStringT = NSMutableAttributedString(string:boldTerms, attributes:attrs)
        
        attributedString1.appendAttributedString(boldStringP)
        attributedString1.appendAttributedString(attributedString2)
        attributedString1.appendAttributedString(boldStringT)
        
        labelPrivacyPolicy?.attributedText = attributedString1
    }
}

//MARK: - Button Actions

extension RegisterFirstStepController {
    
    
    @IBAction func actionBack(sender: UIButton) {
        dismissVC(completion: nil)
    }
    @IBAction func actionContinue(sender: UIButton) {
        
        let message = Register.validateCredentials(tfEmail.text ?? "", password: tfPassword.text ?? "")
        
        guard let email = tfEmail.text,password = tfPassword.text else { return }
        
        if message.characters.count == 0 {
            
            APIManager.sharedInstance.opertationWithRequest(withApi: API.Register(FormatAPIParameters.Register(email: email, password: password).formatParameters()), completion: { (response) in
                weak var weakSelf = self
            
                switch response {
                case APIResponse.Success(let object):
                    
                    weakSelf?.handleLoginResponse(object as? User)
                    break
                case APIResponse.Failure(_):
                    break
                }
                
            })
        }else{
             view.makeToast(message)
        }
        
    }
    
    @IBAction func actionRegisterFacebook(sender: UIButton) {
        
        FacebookManager.sharedManager.configureLoginManager(self) { (facebook) in
            weak var weakSelf = self
            weakSelf?.handleFacebookLogin(facebook)
        }
    }
    
    func handleLoginResponse(user : User?){
       let VC = StoryboardScene.Register.instantiatePhoneNoViewController()
        VC.user = user
        presentVC(VC)
    }
    
    
    func handleFacebookLogin(facebookProfile : Facebook?){
        
        guard let profile = facebookProfile else { return }
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.LoginFacebook(FormatAPIParameters.FacebookLogin(fbProfile: profile).formatParameters())) { (response) in
            UtilityFunctions.stopLoader()
            weak var weakSelf = self
            switch response {
            case .Success(let object) :
                weakSelf?.facebookLoginWebServicehandler(object)
            default:
                break
            }
        }
    }
    
    func facebookLoginWebServicehandler(object : AnyObject?){
        guard let user = object as? User else { return }
        
        if let otpVerified = user.otpVerified where otpVerified == "0"{
            let VC = StoryboardScene.Register.instantiatePhoneNoViewController()
            VC.user = user
            presentVC(VC)
        }else {
            self.presentingViewController?.presentingViewController?.dismissVC(completion: nil)
        }
    }
}
