//
//  RegisterViewController.swift
//  Clikat
//
//  Created by cbl73 on 4/21/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material
import EZSwiftExtensions

class RegisterViewController: LoginRegisterBaseViewController {
    
    var user : User?
    
    @IBOutlet var btnFinish: MaterialButton!{
        didSet{
            btnFinish.kern(ButtonKernValue)
        }
    }
    @IBOutlet weak var btnProfilePic: MaterialButton!
    
    @IBOutlet weak var imageViewProfilePic: UIImageView!{
        didSet{
            imageViewProfilePic.layer.cornerRadius = 8.0
        }
    }
    @IBOutlet weak var tfName: TextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfName?.text = GDataSingleton.sharedInstance.loggedInUser?.firstName
        guard let image = GDataSingleton.sharedInstance.loggedInUser?.userImage,url = NSURL(string:image) else {
            imageViewProfilePic.image = UIImage(asset: Asset.User_placeholder)
            return }
        imageViewProfilePic.yy_setImageWithURL(url, options: .SetImageWithFadeAnimation)
        btnProfilePic.setImage(nil, forState: .Normal)
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
       view.endEditing(true)
    }
}


//MARK: - Button Actions
import Adjust
extension  RegisterViewController{
    
    
    @IBAction func actionBack(sender: UIButton) {
        dismissVC { }
    }
    @IBAction func actionFinishSignUp(sender: AnyObject) {
        
        
        let message = validateCredentials()
        if message.characters.count == 0 {
            
            APIManager.sharedInstance.showLoader()
            APIManager.sharedInstance.opertationWithRequest(withApi: API.RegisterLastStep(FormatAPIParameters.RegisterLastStep(accessToken : user?.token, name: tfName.text ?? "").formatParameters()), image: imageViewProfilePic.image?.resizeWith(200, height: 200)) { [weak self] (response) in
                APIManager.sharedInstance.hideLoader()
                switch response {
                case .Success(_):
                    QGSdk.getSharedInstance().setEmail(self?.user?.email ?? "")
                    QGSdk.getSharedInstance().setCustomKey("language", withValue: Localize.currentLanguage() == Languages.Arabic ? "arabic" : "english")
                    if GDataSingleton.sharedInstance.tokenData != nil {
                        QGSdk.getSharedInstance().setToken(GDataSingleton.sharedInstance.tokenData)
                    }
                    AdjustEvent.SignUp.sendEvent()
                    ez.runThisInMainThread({
                        var VC = self?.presentingViewController
                        while ((VC?.presentingViewController) != nil) {
                            VC = VC?.presentingViewController
                        }
                        VC?.dismissVC{}
                    })
                default:
                    break
                }
            }
        }else {
            view.makeToast(message)
        }
    }
    
    @IBAction func actionAddDP(sender: UIButton) {
        
        if UtilityFunctions.isCameraPermission() {
        UtilityFunctions.showActionSheet(withTitle: nil, subTitle: L10n.SelectPicture.string, vc: self, senders: [L10n.Camera.string,L10n.PhotoLibrary.string]) { (text, index) in
            
            CameraGalleryPickerBlock.sharedInstance.pickerImage(type: text as! String, presentInVc: self, pickedListner: {[weak self] (image) in
                
                self?.imageViewProfilePic.image = image
                sender.setImage(nil, forState: .Normal)
            }) {
                
                //Cancelled
            }
        }
        }else {
            UtilityFunctions.alertToEncourageCameraAccessWhenApplicationStarts(self)
        }
    }
    
}

extension RegisterViewController {
    
    func validateCredentials() -> String{
        guard let name = tfName.text where name.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()).characters.count != 0 else {
            
            return L10n.PleaseEnterYourName.string
        }
        guard let _ = imageViewProfilePic.image else {
            return L10n.PleaseSelectYourProfilePicture.string
        }
        return ""
    }
}

