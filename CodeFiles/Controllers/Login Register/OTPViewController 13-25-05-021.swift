//
//  OTPViewController.swift
//  Clikat
//
//  Created by cbl73 on 4/21/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material

class OTPViewController: UIViewController {

    @IBOutlet weak var tfOTP: TextField!
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
}


//MARK: - Button Actions

extension OTPViewController{
    
    @IBAction func actionSubmitOTP(sender: AnyObject) {
        guard let otp = tfOTP.text else { return }
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.CheckOTP(FormatAPIParameters.CheckOTP(OTP: otp).formatParameters())) { (response) in
            
            weak var weakSelf = self
            switch response {
            case APIResponse.Success(let object):
                weakSelf?.handleCheckOTPResponse()
                break
            case APIResponse.Failure(let validation):
                weakSelf?.view.makeToast(validation.message)
                break
            }
 
        }
    }
    
    @IBAction func actionResendOTP(sender: UIButton) {
        
        
    }
}

extension OTPViewController {
    
    func handleCheckOTPResponse(){
        pushVC(StoryboardScene.Register.instantiateRegisterViewController())
    }
}