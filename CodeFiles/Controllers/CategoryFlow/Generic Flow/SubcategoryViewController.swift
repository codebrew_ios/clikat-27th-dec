//
//  SubcategoryViewController.swift
//  Clikat
//
//  Created by Night Reaper on 27/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class SubcategoryViewController: CategoryFlowBaseViewController {

    @IBOutlet weak var btnBarCode: UIButton!
    @IBOutlet weak var viewBg: UIView!{
        didSet{
            viewBg.layer.borderWidth = 0.5
            viewBg.layer.borderColor = UIColor.lightGrayColor().CGColor
        }
    }
    @IBOutlet weak var constraintTableViewBottom: NSLayoutConstraint!
    @IBOutlet weak var tfSearch: UITextField!
    
    @IBOutlet var tableView: UITableView!{
        didSet{
            tableView.registerNib(UINib(nibName: CellIdentifiers.SubCategoryListingCell, bundle: nil), forCellReuseIdentifier: CellIdentifiers.SubCategoryListingCell)
            configureTableViewInitialization()
        }
    }
    var tableViewDataSource = TableViewDataSource(){
        didSet{
            tableView?.dataSource = tableViewDataSource
            tableView?.delegate = tableViewDataSource
        }
    }
    
    var subCategories : [SubCategory]? = []{
        didSet{
            tableViewDataSource.items = subCategories
            tableView?.reloadTableViewData(inView: view)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webServiceSubCategoriesListing()
        btnBarCode.hidden = (passedData.categoryOrder == Category.Grocery.rawValue || passedData.categoryOrder == Category.Electronics.rawValue) ? false : true
    }

}



//MARK: - WebService Methods
extension SubcategoryViewController{
    
    func webServiceSubCategoriesListing (){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.SubCategoryListing(FormatAPIParameters.SubCategoryListing(supplierId: passedData.supplierId, categoryId: passedData.categoryId).formatParameters())) { (response) in
            
            weak var weak : SubcategoryViewController? = self
            switch response{
                
            case .Success(let listing):
                weak?.subCategories = (listing as? SubCategoriesListing)?.subCategories

            default :
                break
            }
        }
    }
    
}


//MARK: - TableView Configuration Methods
extension SubcategoryViewController {
    
    func configureTableViewInitialization(){
        weak var weakSelf : SubcategoryViewController? = self
        tableViewDataSource = TableViewDataSource(items: subCategories, height: 120.0 , tableView: tableView, cellIdentifier: CellIdentifiers.SubCategoryListingCell , configureCellBlock: { (cell, item) in
            weakSelf?.configureCell(withCell: cell, item: item)
            }, aRowSelectedListener: { (indexPath) in
                weakSelf?.itemClicked(atIndexPath: indexPath)
        })
        
    }
    
    
    func configureCell(withCell cell : AnyObject , item : AnyObject? ){
        
        guard let tempCell = cell as? SubCategoryListingCell else{
            return
        }
        tempCell.subCategory = item as? SubCategory
    }
    
    func itemClicked(atIndexPath indexPath : NSIndexPath){
        guard let subCatId = subCategories?[indexPath.row].subCategoryId ,subCatName = subCategories?[indexPath.row].name else{
            ez.runThisInMainThread {
                weak var weakSelf : SubcategoryViewController? = self
                weakSelf?.pushNextVc()
            }
            return
        }
        
        ez.runThisInMainThread { [unowned self] in
            
            self.passedData.subCategoryId = subCatId
            self.passedData.subCategoryName = subCatName
            self.pushNextVc()
        }
        
    }
    
}





//MARK: - Button Actions
extension SubcategoryViewController{
    
    @IBAction func actionBack(sender: AnyObject) {
        popVC()
    }
    
    @IBAction func actionMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    @IBAction func actionMultiSearch(sender: AnyObject) {
        view.endEditing(true)
        guard let _ = passedData.supplierBranchId else { return }
        let searchVc = StoryboardScene.Main.instantiateSearchViewController()
        searchVc.passedData = passedData
        pushVC(searchVc)
    }

    @IBAction func actionCart(sender: AnyObject) {
        pushVC(StoryboardScene.Options.instantiateCartViewController())
    }
    
    @IBAction func actionBarCode(sender : UIButton!){
        if UtilityFunctions.isCameraPermission() {
            let barCodeVc = StoryboardScene.Options.instantiateBarCodeScannerViewController()
            barCodeVc.passedData = passedData
            pushVC(barCodeVc)
        }else {
            UtilityFunctions.alertToEncourageCameraAccessWhenApplicationStarts(self)
        }
    }

}

//MARK: - TextField handlers

extension SubcategoryViewController : UITextFieldDelegate {
    
    
    @IBAction func textFieldValueChanged(sender: UITextField) {
        
        let filterSuppliers = subCategories?.filter({ (supplier) -> Bool in
            guard let match = supplier.name?.contains(sender.text!, compareOption: .CaseInsensitiveSearch) else { return false }
            return match
        })
        tableViewDataSource.items = sender.text?.characters.count == 0 ? subCategories : filterSuppliers
        tableView.reloadData()
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        constraintTableViewBottom.constant = 260
        view.layoutIfNeeded()
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        constraintTableViewBottom.constant = 0
        view.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
}