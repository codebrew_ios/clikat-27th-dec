//
//  CategoryFlowViewController.swift
//  Clikat
//
//  Created by cbl73 on 5/6/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class PassedData {
    
    var productId : String?
    var supplierId : String?
    var supplierBranchId : String?
    var categoryId : String?
    var subCategoryId : String?
    var subCategoryName : String?
    var categoryFlow : String?
    var categoryOrder : String?
    var categoryName : String?

    convenience init(withCatergoryId categoryId : String? , categoryFlow : String? , supplierId : String? , subCategoryId : String? , productId : String? , branchId : String?,subCategoryName : String? ,categoryOrder : String?,categoryName : String? ){
        self.init()
        self.categoryId = categoryId
        self.categoryFlow = categoryFlow
        self.supplierId = supplierId
        self.categoryId = categoryId
        self.subCategoryId = subCategoryId
        self.supplierBranchId = branchId
        self.productId = productId
        self.subCategoryName = subCategoryName
        self.categoryOrder = categoryOrder
        self.categoryName = categoryName

    }
    init(){
    }
}

class CategoryFlowBaseViewController : BaseViewController{
    
    var passedData : PassedData = PassedData()
    var laundryData : OrderSummary?
    func pushNextVc(){
        do {
            let nextVC = try CategoryMapping.nextViewController(withFlow: passedData.categoryFlow, currentViewController: self) as? CategoryFlowBaseViewController
            nextVC?.passedData = PassedData(withCatergoryId: passedData.categoryId , categoryFlow:  passedData.categoryFlow ,supplierId: passedData.supplierId ,subCategoryId: passedData.subCategoryId , productId: passedData.productId , branchId: passedData.supplierBranchId ,subCategoryName : passedData.subCategoryName ,categoryOrder:  passedData.categoryOrder, categoryName:passedData.categoryName)
            nextVC?.laundryData = laundryData
            guard let tempNextVc = nextVC else{
                return
            }
            pushVC(tempNextVc)
        }
        catch let exception{
            print(exception)
        }
    }
}



