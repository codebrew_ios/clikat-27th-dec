//
//  ItemListingViewController.swift
//  Clikat
//
//  Created by cblmacmini on 4/24/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import EZSwiftExtensions

enum Category : String {
    
    case Laundry = "2"
    case BeautySalon = "10"
    case Grocery = "0"
    case Electronics = "1"
}

class ItemListingViewController: CategoryFlowBaseViewController {
    
    @IBOutlet weak var viewPlaceholder: UIView!
    @IBOutlet weak var btnSearch : UIButton!
    @IBOutlet weak var btnBarCode : UIButton!
    @IBOutlet weak var buttonBarView: ButtonBarView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var lblNavTitle: UILabel!
    @IBOutlet weak var barViewHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintTableviewBottom: NSLayoutConstraint!
    
    var barViewDataSource = BarButtonDataSource()
    var tableDataSource = ItemListingDataSource()
    var isOffers = false
    var productListing : ProductListing?{
        didSet{
            viewPlaceholder?.hidden = productListing?.arrDetailedSubCategories?.count == 0 ? false : true
            if let count = productListing?.arrDetailedSubCategories?.count {
                
                barViewHeight?.constant = count <= 1 ? 0 : 48
                view.layoutIfNeeded()
            }
        }
    }
    
    var changeCurrentIndexProgressive: ((oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void)?
    var changeCurrentIndex: ((oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, animated: Bool) -> Void)?
    
    var hideDetailedSubCategoriesBar : Bool = false
    
    lazy var buttonBarItemSpec: ButtonBarItemSpec<ButtonBarViewCell> = .NibFile(nibName: "ButtonCell", bundle: NSBundle(forClass: ButtonBarViewCell.self), width:{ [weak self] (childItemInfo) -> CGFloat in
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        
        label.text = childItemInfo.title
        let labelSize = label.intrinsicContentSize()
        return labelSize.width + (LowPadding) * 2
        })
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        if isOffers {
            lblNavTitle?.text = L10n.Offers.string
            btnSearch?.hidden = true
        }
        guard let _ = productListing else {
            webServiceItemListing()
            return
        }
        if let count = productListing?.arrDetailedSubCategories?.count {
            barViewHeight?.constant = count <= 1 ? 0 : 48
            view.layoutIfNeeded()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        lblNavTitle?.text = passedData.subCategoryName ?? productListing?.arrDetailedSubCategories?.first?.strTitle
        reloadVisibleCells()
        btnBarCode.hidden = (passedData.categoryOrder == Category.Grocery.rawValue || passedData.categoryOrder == Category.Electronics.rawValue) ? false : true
    }
}


//MARK: - WebService Methods
extension ItemListingViewController{
    
    func webServiceItemListing (){
        let params : [String : AnyObject]?
        if isOffers {
            params = FormatAPIParameters.ViewAllOffers().formatParameters()
            lblNavTitle?.text = L10n.Offers.string
        }else {
            params = FormatAPIParameters.DetailedSubCatProducts(supplierBranchId: passedData.supplierBranchId, subCategoryId: passedData.subCategoryId).formatParameters()
        }
        
        let api = isOffers ? API.ViewAllOffers(params) : API.ProductListing(params)
        APIManager.sharedInstance.opertationWithRequest(withApi: api) { [weak self](response) in
           
            switch response{
            case .Success(let listing):
                if listing is ProductListing {
                    self?.productListing = listing as? ProductListing
                }else {
                    
                    let offerListing = ProductListing()
                    let detailsubcategory = DetailedSubCategories(strTitle: L10n.Offers.string, products: (listing as? OfferListing)?.arrOffers)
                    offerListing.arrDetailedSubCategories = [detailsubcategory]
                        self?.productListing = offerListing
                }
                defer{
                    self?.tableDataSource.items = self?.productListing?.arrDetailedSubCategories
                    self?.configureBarbuttonView()
                    self?.tableView.reloadTableViewData(inView: self?.view)
                }
                guard let items = self?.productListing?.arrDetailedSubCategories where items.count > 0 else{
                    return
                }
            default :
                break
            }
            defer{
                ez.runThisAfterDelay(seconds: 0.0, after: {
                    guard let array = self?.productListing?.arrDetailedSubCategories else { return}
                    self?.configureBarbuttonView()
                    self?.tableView.reloadTableViewData(inView: self?.view)
                })
            }
        }
    }
    
}


extension ItemListingViewController {
    
    func configureBarbuttonView(){
        
        buttonBarView.scrollsToTop = false
        let flowLayout = buttonBarView.collectionViewLayout as! UICollectionViewFlowLayout
        flowLayout.scrollDirection = .Horizontal
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.minimumLineSpacing = 0
        let sectionInset = flowLayout.sectionInset
        flowLayout.sectionInset = UIEdgeInsetsMake(sectionInset.top,sectionInset.left, sectionInset.bottom, sectionInset.right)
        
        buttonBarView.showsHorizontalScrollIndicator = false
        buttonBarView.backgroundColor = UIColor.clearColor()
        buttonBarView.selectedBar.backgroundColor = Colors.MainColor.color()
        
        buttonBarView.selectedBarHeight = 2
        // register button bar item cell
        switch buttonBarItemSpec {
        case .NibFile(let nibName, let bundle, _):
            buttonBarView.registerNib(UINib(nibName: nibName, bundle: bundle), forCellWithReuseIdentifier:"Cell")
        case .CellClass:
            buttonBarView.registerClass(ButtonBarViewCell.self, forCellWithReuseIdentifier:"Cell")
        }
        
        barViewDataSource = BarButtonDataSource(items: productListing?.arrDetailedSubCategories ?? [], barButtonView: buttonBarView,tableView: tableView)
        buttonBarView.delegate = barViewDataSource
        buttonBarView.dataSource = barViewDataSource
        if productListing?.arrDetailedSubCategories?.count > 0{
            buttonBarView.moveToIndex(0, animated: false, swipeDirection: .None, pagerScroll: .ScrollOnlyIfOutOfScreen)
        }
    }
    
    func configureTableView(){
        
        tableDataSource = ItemListingDataSource(items: productListing?.arrDetailedSubCategories, tableView: tableView, buttonBarView: buttonBarView, aRowSelectedListener: { [weak self] (indexPath) in
            
            self?.itemClickListner(withIndexPath: indexPath)
        })
        
        tableDataSource.configureCellBlock = { [weak self] (cell,indexPath) in
            self?.configureCell(cell, indexPath: indexPath)
        }
        tableDataSource.changeSectionListener = { [weak self] section in
            self?.barViewDataSource.selected = Array(count: self?.productListing?.arrDetailedSubCategories?.count ?? 0, repeatedValue: false)
            self?.barViewDataSource.selected[section] = true
            self?.buttonBarView.reloadData()
        }
        tableView.delegate = tableDataSource
        tableView.dataSource = tableDataSource
        
    }
    
    func itemClickListner(withIndexPath indexPath : NSIndexPath){
        
        guard let dsc = productListing?.arrDetailedSubCategories?[indexPath.section].arrProducts , productId = dsc[indexPath.row].id  else{
            return
        }
        ez.runThisInMainThread { [weak self] in
            let productDetailVc = StoryboardScene.Main.instantiateProductDetailViewController()
            productDetailVc.passedData.productId = productId
            productDetailVc.suplierBranchId = dsc[indexPath.row].supplierBranchId
            self?.pushVC(productDetailVc)
        }
    }
    
    //MARK: - Configure Cell
    
    func configureCell(cell : AnyObject?,indexPath : AnyObject?){
        
        guard let cell = cell as? ProductListingCell else { return }
        cell.category = passedData.categoryOrder
        cell.categoryId = passedData.categoryId
    }
    
}

//MARK: - Reload Visible Cells
extension ItemListingViewController {
    
    func reloadVisibleCells(){
        
        DBManager.sharedManager.getCart { [unowned self] (array) in
            
            self.productListing?.arrDetailedSubCategories?.forEach({ (index, dsc) in
                dsc.arrProducts?.forEach({ (index, product) in
                    array.forEach({ (index, savedProduct) in
                        
                        if (savedProduct as? Cart)?.id == product.id {
                            product.quantity = (savedProduct as? Cart)?.quantity
                            product.dateModified = (savedProduct as? Cart)?.dateModified
                        }
                    })
                })
            })
            self.tableView.reloadData()
        }

        
        DBManager.sharedManager.getCart { (cartProducts) in
            weak var weakSelf = self
            let visibleCells = weakSelf?.tableView.visibleCells ?? []
        
            for visibleCell in visibleCells {
                guard let cell = visibleCell as? ProductListingCell else { return }
                cell.stepper.value = 0
                for cartProduct in cartProducts {
                    guard let product = cartProduct as? Cart,quantity = product.quantity where cell.product?.id == product.id  else { return }
                    
                    cell.stepper.value = Double(quantity) ?? 0
                }
            }
        }
    }
}



extension ItemListingViewController{
    
    @IBAction func actionCart(sender: AnyObject) {
        pushVC(StoryboardScene.Options.instantiateCartViewController())
    }
    
    @IBAction func actionSearchLocal(sender: UIButton) {
        view.endEditing(true)
    }
    @IBAction func actionBack(sender: AnyObject) {
        popVC()
    }
    
    @IBAction func actionMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    @IBAction func actionSearch(sender: AnyObject) {
        let searchVc = StoryboardScene.Main.instantiateSearchViewController()
        searchVc.passedData = passedData
        pushVC(searchVc)
    }
    
    @IBAction func actionBacrCode(sender: AnyObject) {
        let barCodeVc = StoryboardScene.Options.instantiateBarCodeScannerViewController()
        barCodeVc.passedData = passedData
        pushVC(barCodeVc)
    }
}

//MARK: - Search field 

extension ItemListingViewController : UITextFieldDelegate {
    
    @IBAction func textFieldValueChanged(sender: UITextField) {
        
        var tempDetailSubCat : [DetailedSubCategories] = []
        for detailSubCat in productListing?.arrDetailedSubCategories ?? [] {
            let filterProducts = detailSubCat.arrProducts?.filter({ (product) -> Bool in
                guard let match = product.name?.contains(sender.text!, compareOption: .CaseInsensitiveSearch) else { return false }
                return match
            })
            
            tempDetailSubCat.append(DetailedSubCategories(strTitle: detailSubCat.strTitle, products: filterProducts))
        }
        
        tableDataSource.items = sender.text?.characters.count == 0 ? productListing?.arrDetailedSubCategories : tempDetailSubCat
        barViewHeight.constant = sender.text?.characters.count == 0 ? 48 : 0
        view.updateConstraints()
        tableView.reloadData()
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        constraintTableviewBottom.constant = 260
        view.layoutIfNeeded()
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        constraintTableviewBottom.constant = 0
        view.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
}