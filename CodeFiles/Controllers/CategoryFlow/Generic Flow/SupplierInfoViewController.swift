//
//  SupplierInfoViewController.swift
//  Clikat
//
//  Created by cblmacmini on 4/20/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import SKPhotoBrowser

class SupplierInfoViewController: CategoryFlowBaseViewController {
    
    let headerHeight = ScreenSize.SCREEN_WIDTH * 0.7
    var transitionDriver: TransitionDriver?
    
    var startingPosition : CGFloat = 0.0
    var currentPosition : CGFloat = 0.0
    var maxYOffset : Int = 150
    var startingTouchLocation : CGPoint?
    var imageOfBackgroundLayer : UIImage?
    
    //PromotionsFlow 
    var promotion : Promotion?
    
    let headerView = SupplierInfoHeaderView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_WIDTH * 0.7))
    var rateSupplierView = SupplierRatingPopUp(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))
    @IBOutlet weak var constraintLabel: NSLayoutConstraint!
    @IBOutlet weak var constraintBtnMakeOrder: NSLayoutConstraint!
    @IBOutlet weak var labelTitle: UILabel!{
        didSet{
            labelTitle.layer.shadowRadius = 2
            labelTitle.layer.shadowOffset = CGSize(width: 0, height: 3)
            labelTitle.layer.shadowOpacity = 0.2
        }
    }
    @IBOutlet weak var imageViewFade: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnFav: DOFavoriteButton!{
        didSet{
            btnFav.imageColorOff = UIColor.whiteColor()
            btnFav.imageColorOn = UIColor.redColor()
            btnFav.circleColor = UIColor.redColor()
            btnFav.lineColor = UIColor.redColor()
            btnFav.duration = 2.0 // default: 1.0
            btnFav?.image = UIImage(asset : Asset.Ic_favorite_white_normal)
        }
    }
    @IBOutlet var btnMakeOrder: UIButton!{
        didSet{
            btnMakeOrder.kern(ButtonKernValue)
        }
    }
    @IBOutlet weak var pageControl: UIPageControl!{
        didSet{
            pageControl.transform = CGAffineTransformMakeScale(0.8, 0.8)
        }
    }
    
    var supplier : Supplier?{
        didSet{
            labelTitle?.text = supplier?.name
            //            configureTableView()
            
            if let tempPromotion = promotion {
                supplier?.supplierImages = [tempPromotion.promotionImage ?? ""]
            }
            if passedData.categoryOrder == "2" {
                supplier?.status = .Online
            }
            configureTableHeaderView()
            setupUI()
            
        }
    }
    var tableDataSource = SupplierInfoDataSource(){
        didSet{
            tableView.reloadData()
        }
    }
    var showButton = true
    override func viewDidLoad() {
        super.viewDidLoad()
        webServiceSupplierInfo(.About)
        setupUI()
    }
    
    func setupUI(){
        if !showButton {
            constraintBtnMakeOrder.constant = 0
            self.view.layoutIfNeeded()
        }
        guard let _ = GDataSingleton.sharedInstance.loggedInUser?.id else{
            btnFav?.hidden = true
            return
        }
        btnFav?.hidden = false
        guard let favorite = supplier?.Favourite where favorite == "1" else{
            btnFav?.selected = false
            return
        }
        btnFav?.image = UIImage(asset : Asset.Heart)
        btnFav?.select()
    }
}


//MARK: - WebService Methods
extension SupplierInfoViewController{
    
    func webServiceSupplierInfo(selectedTab : SelectedTab){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.SupplierInfo(FormatAPIParameters.SupplierInfo(supplierId: passedData.supplierId, branchId: passedData.supplierBranchId, accessToken: GDataSingleton.sharedInstance.loggedInUser?.token,categoryId : passedData.categoryId).formatParameters())) { [weak self] (response) in
            switch response{
                
            case .Success(let listing):
                self?.supplier = listing as? Supplier
                self?.configureTableView()
                self?.tableDataSource.selectedTab = .About
                break
            default :
                break
            }
        }
    }
    
    func webServiceRateSupplier(rating : String?,comment : String?){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.SupplierRating(FormatAPIParameters.SupplierRating(supplierId: passedData.supplierId, rating: rating , comment: comment).formatParameters())) { (response) in
            weak var weakSelf = self
            switch response {
            case .Success(_):
                weakSelf?.handleWebServiceRateSupplier(rating)
            case .Failure(_):
                break
            }
        }
    }
    
}

extension SupplierInfoViewController {
    
    func configureTableView(){
        tableDataSource = SupplierInfoDataSource(supplier: supplier, height: UITableViewAutomaticDimension, tableView: tableView, cellIdentifier:CellIdentifiers.SupplierInfoCell , configureCellBlock: { (cell, indexPath,selectedTab) in
            guard let c = cell as? UITableViewCell,s = selectedTab else { return }
            self.configureTableViewCell(c,indexPath: indexPath,selectedTab: s)
            }, aRowSelectedListener: { (indexPath) in
                
            }, scrollViewListener: { (scrollView) in
                self.updateHeaderView(self.headerView)
        })
        tableView.delegate = tableDataSource
        tableView.dataSource = tableDataSource
    }
    
    func configureTableHeaderView(){
        
        headerView.supplier = supplier
        tableView.addSubview(headerView)
        headerView.imageDelegate = self
        tableView.contentInset = UIEdgeInsets(top: headerHeight, left: 0, bottom: 0, right: 0)
        tableView.contentOffset = CGPoint(x: 0, y: -headerHeight)
        pageControl.numberOfPages = supplier?.supplierImages?.count ?? 0
        pageControl.hidden = pageControl.numberOfPages > 5 ? true : false
        headerView.collectionDataSource.scrollViewListener = { (scrollView) in
            weak var weakSelf = self
            weakSelf?.configurePageControl(scrollView)
        }
        updateHeaderView(headerView)

    }
    func configurePageControl(scrollView : UIScrollView){
        
        let visibleRect = CGRect(origin: headerView.collectionView.contentOffset, size: headerView.collectionView.bounds.size)
        let visiblePoint = CGPoint(x: CGRectGetMidX(visibleRect), y: CGRectGetMidY(visibleRect))
        let visibleIndexPath = headerView.collectionView.indexPathForItemAtPoint(visiblePoint)
        pageControl.currentPage = visibleIndexPath?.row ?? 0
    }
    
    func updateHeaderView(headerView : SupplierInfoHeaderView){
        
        var headerRect = CGRect(x: 0, y: -headerHeight, width: ScreenSize.SCREEN_WIDTH, height: headerHeight )
        if (tableView.contentOffset.y < -headerHeight) {
            headerRect.origin.y = tableView.contentOffset.y
            headerRect.size.height = -tableView.contentOffset.y <= 64 ? headerRect.size.height : -tableView.contentOffset.y
        }else {
            headerRect.origin.y = tableView.contentOffset.y
            headerRect.size.height = -tableView.contentOffset.y <= 114 ? 114 : -tableView.contentOffset.y
        }
        headerView.frame = headerRect
        
        headerView.collectionView.collectionViewLayout.invalidateLayout()
        
        configureBlurImageHeader()
    }
    
    func configureTableViewCell(cell : UITableViewCell,indexPath : NSIndexPath,selectedTab : SelectedTab){
        
        if let currentCell = cell as? SupplierInfoCell {
            currentCell.passedData = passedData
            currentCell.supplier = supplier
        }else if let currentCell = cell as? SupplierInfoTabCell {
            currentCell.delegate = tableDataSource
            if selectedTab == .Uniqueness {
                currentCell.adjustViewHighlight(selectedTab)
            }
        }else if let currentCell = cell as? SupplierDescriptionCell {
            if selectedTab == .Review {
                currentCell.htmlString = ""
                return
            }
            currentCell.htmlString = selectedTab == .About ? supplier?.descriptionHTML : supplier?.about
        }else if let currentCell = cell as? MyReviewCell {
            currentCell.myReview = supplier?.myReview
            let tap = UITapGestureRecognizer(target: self, action: #selector(SupplierInfoViewController.handleReviewCellTap))
            guard let _ = supplier?.myReview?.rating else {
                currentCell.viewRating.userInteractionEnabled = true
                currentCell.viewRating.addGestureRecognizer(tap)
                return
            }
            currentCell.viewRating.userInteractionEnabled = false
            
        }else if let currentCell = cell as? OtherReviewCell {
            currentCell.currentReview = supplier?.reviews?[indexPath.row - 2]
        }
    }
    
    func handleReviewCellTap(){
        
        guard let _ = GDataSingleton.sharedInstance.loggedInUser?.id else {
            presentVC(StoryboardScene.Register.instantiateLoginViewController())
            return
        }
        self.view.addSubview(rateSupplierView)
        rateSupplierView.presentingViewController = self
        rateSupplierView.supplier = supplier
        rateSupplierView.presentRatingView { (rating, comment) in
            weak var weakSelf = self
            weakSelf?.webServiceRateSupplier(String(rating ?? 0), comment: comment)
            weakSelf?.rateSupplierView.dismissRatingView()
        }
    }
    
    func handleWebServiceRateSupplier(rating : String?){
        
        self.webServiceSupplierInfo(.Review)
    }
}


//MARK: - Image Click Delegate
extension SupplierInfoViewController : ImageClickListenerDelegate {
    
    func imageCliked(atIndexPath indexPath : NSIndexPath, cell: UICollectionViewCell? , images : [SKPhoto]) {
        
        guard let currentCell = cell as? SupplierInfoHeaderCollectionCell , originImage = currentCell.imageViewCover.image where images.count > 0 else{
            return
        }
        let browser = SKPhotoBrowser(originImage: originImage, photos: images, animatedFromView: currentCell)
        browser.initializePageIndex(indexPath.row)
        presentVC(browser)
    }
}


//MARK: - Button Actions

extension SupplierInfoViewController{
    
    
    @IBAction func actionFav(sender: DOFavoriteButton) {

        if sender.selected {
            btnFav?.image = UIImage(asset : Asset.Ic_favorite_white_normal)
            sender.deselect()
            APIManager.sharedInstance.opertationWithRequest(withApi: API.UnFavoriteSupplier(FormatAPIParameters.UnFavoriteSupplier(supplierId: passedData.supplierId).formatParameters()), completion: { (response) in })
            //Mark Unfavorite
            
        } else {
            btnFav?.image = UIImage(asset : Asset.Heart)
            sender.select()
            APIManager.sharedInstance.opertationWithRequest(withApi: API.MarkSupplierFav(FormatAPIParameters.MarkSupplierFavorite(supplierId: passedData.supplierId).formatParameters()), completion: { (response) in })
        }
    }
    
    @IBAction func actionBack(sender: AnyObject) {
        popVC()
    }
    
    @IBAction func actionMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    @IBAction func actionShare(sender: AnyObject) {
        let iosLink = "clikat://www.clikat.com/supplier?supplierId=\(supplier?.id ?? "0")&branchId=\(/passedData.supplierBranchId)&categoryId=\(/passedData.categoryId) (iOS)"
        
        UtilityFunctions.shareContentOnSocialMedia(withViewController: self, message: UtilityFunctions.appendOptionalStrings(withArray: [L10n.IWouldLikeToRecommendUsing.string,supplier?.name,L10n.ViaClikat.string,iosLink],separatorString: " "))
    }
    
    @IBAction func actionMakeOrder(sender: AnyObject) {
        if supplier?.status == .Busy || supplier?.status == .Closed {
            UtilityFunctions.showSweetAlert(UtilityFunctions.appendOptionalStrings(withArray: [supplier?.name, L10n.NotAvailable.string]), message: L10n.YourOrderWillBeConfirmedDuringNextSupplierWorkingHoursDay.string, success: { [weak self] in
                self?.makeOrder()
                }, cancel: {  
                
            })
        }else { makeOrder() }
    }
    
    
    func makeOrder(){
        if let currentPromotion = promotion {
            let product = addPromotionToCart(currentPromotion)
            
            UtilityFunctions.showSweetAlert(L10n.AreYouSure.string, message: L10n.AddingProductsFromPromotionsWillClearYourCart.string, success: { [weak self] in
                DBManager.sharedManager.deleteAllData(CoreDataEntity.Cart.rawValue)
                GDataSingleton.sharedInstance.currentSupplierId = product.supplierBranchId
                let VC = StoryboardScene.Options.instantiateCartViewController()
                DBManager.sharedManager.manageCart(Product(cart:product), quantity: -1)
                GDataSingleton.sharedInstance.currentSupplierId = "-1"
                self?.pushVC(VC)
                }, cancel: {
                    
            })
            
        }else {
            
            pushNextVc()
        }
    }
    
    func addPromotionToCart(currentPromotion : Promotion) -> Cart{
        let cartProduct = Cart()
        cartProduct.id = currentPromotion.id
        cartProduct.name = currentPromotion.promotionName
        cartProduct.quantity = "-1"
        cartProduct.image = promotion?.promotionImage
        cartProduct.priceType = .Fixed
        cartProduct.fixedPrice = currentPromotion.promotionPrice
        cartProduct.displayPrice = currentPromotion.promotionPrice
        cartProduct.deliveryCharges = currentPromotion.deliveryCharges
        cartProduct.supplierBranchId = currentPromotion.supplierBranchId
        cartProduct.handlingAdmin = currentPromotion.handlingAdmin
        cartProduct.handlingSupplier = currentPromotion.handlingSupplier
        
        return cartProduct
    }
}


extension SupplierInfoViewController {
    
    func configureBlurImageHeader(){
        let scrollOffset = -tableView.contentOffset.y
        let yPos = scrollOffset - headerHeight
        currentPosition = yPos
        headerView.collectionView.scrollEnabled =  yPos < -1 ? false : true
        currentPosition = currentPosition > CGFloat(maxYOffset) ? CGFloat(maxYOffset) : currentPosition
        let alpha = 1.0 - ((-yPos * 3.5)/headerHeight) > 1.0 ? 1.0 : 1.0 - ((-yPos * 3.5)/headerHeight)
        headerView.imageViewSupplier.alpha = alpha
        headerView.imageViewSupplier.transform = CGAffineTransformMakeScale(alpha, alpha)
        pageControl.alpha = alpha
        constraintLabel?.constant = scrollOffset - MidPadding < 0 ? 0 : scrollOffset - MidPadding
    }
}



// MARK: Helpers

extension SupplierInfoViewController {
    
    private func getScreen() -> UIImage? {
        let height = (headerHeight - tableView.contentOffset.y) < 0 ? 0 : (headerHeight - tableView.contentOffset.y)
        let backImageSize = CGSize(width: view.bounds.width, height: view.bounds.height - height)
        let backImageOrigin = CGPoint(x: 0, y: height + tableView.contentOffset.y)
        return view.takeSnapshot(CGRect(origin: backImageOrigin, size: backImageSize))
    }
}
// MARK: Public

extension SupplierInfoViewController {
    
    func popTransitionAnimation() {
        guard let transitionDriver = self.transitionDriver else {
            return
        }
        
        let backImage = getScreen()
        let offset = tableView.contentOffset.y > headerHeight ? headerHeight : tableView.contentOffset.y
        transitionDriver.popTransitionAnimationContantOffset(offset, backImage: backImage)
        self.navigationController?.popViewControllerAnimated(false)
    }
    
}


