//
//  SupplierViewController.swift
//  Clikat
//
//  Created by cbl73 on 4/21/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class SupplierListingViewController: CategoryFlowBaseViewController {

    @IBOutlet weak var labelPlaceholder: UILabel!
    @IBOutlet weak var viewPlaceholder: UIView!
    @IBOutlet var lblNavTitle: UILabel!
    @IBOutlet var btnCart: CartButton!
    @IBOutlet weak var viewBg: UIView!{
        didSet{
            viewBg.layer.borderWidth = 0.5
            viewBg.layer.borderColor = UIColor.lightGrayColor().CGColor
        }
    }
    @IBOutlet weak var constraintTableViewBottom: NSLayoutConstraint!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.registerNib(UINib(nibName: CellIdentifiers.SupplierListingCell, bundle: nil), forCellReuseIdentifier: CellIdentifiers.SupplierListingCell)
            configureTableViewInitialization()
        }
    }
        
    var filters : FilterAssocitedValue = .Filter([],[],[],[],.DoesntMatter)

    var tableViewDataSource = TableViewDataSource(){
        didSet{
            tableView?.dataSource = tableViewDataSource
            tableView?.delegate = tableViewDataSource
        }
    }
    var suppliers : [Supplier]? = []{
        didSet{
            tableViewDataSource.items = suppliers
            viewPlaceholder.hidden = suppliers?.count == 0 ? false : true
            labelPlaceholder.text = laundryData?.pickupDate == nil ? L10n.NoSupplierFound.string : L10n.ChangePickUpTimeNoSuppliersAreAvailableForThisPickupTiming.string
            tableView?.reloadTableViewData(inView: view)
        }
    }
    
    var sponsor : Sponsor?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        lblNavTitle.text = passedData.subCategoryName ?? passedData.categoryName
        webServiceSupplierListing()
    }
}

//MARK: - WebService Methods
extension SupplierListingViewController {
    
    func webServiceSupplierListing (){
        
        var parameters : Dictionary<String,AnyObject>? = [:]
        if let pickup = laundryData?.pickupDate {
            parameters = FormatAPIParameters.LaundrySupplierList(categoryId: passedData.categoryId, pickupDate: pickup).formatParameters()
        }else {
            parameters = FormatAPIParameters.SupplierListing(categoryId: passedData.categoryId,subCategoryId: passedData.subCategoryId).formatParameters()
        }
        let api = laundryData?.pickupDate == nil ? API.SupplierListing(parameters) : API.LaundrySupplierList(parameters)
        APIManager.sharedInstance.opertationWithRequest(withApi: api) { [weak self] (response) in
            
            weak var weak : SupplierListingViewController? = self
            switch response{
                
            case .Success(let listing):
                
                self?.tableViewDataSource.headerHeight = weak?.sponsor?.id != nil ? 72 : 0
                self?.suppliers = (listing as? SupplierListing)?.suppliers
                default :
                break
            }
        }
    }
    
}

//MARK: - TableView Configuration Methods
extension SupplierListingViewController {
    
    func configureTableViewInitialization(){
        
        tableViewDataSource = TableViewDataSource(items: suppliers, height: 124.0 , tableView: tableView, cellIdentifier: CellIdentifiers.SupplierListingCell , configureCellBlock: { (cell, item) in
            weak var weakSelf : SupplierListingViewController? = self
            
            weakSelf?.configureCell(withCell: cell, item: item)
            }, aRowSelectedListener: { (indexPath) in
                weak var weakSelf : SupplierListingViewController? = self
                weakSelf?.itemClicked(atIndexPath: indexPath)
        })
       
//        tableViewDataSource.viewforHeaderInSection = { (section) in
//            weak var weakSelf = self
//            let sponsorView = SponsorView(frame: CGRect(x: 0, y: 0, w: ScreenSize.SCREEN_WIDTH, h: 48))
//            guard let image = weakSelf?.sponsor?.logo,url = NSURL(string: image) else { return nil }
//            sponsorView.imageViewSponsor.yy_setImageWithURL(url, options: .ProgressiveBlur)
//            return sponsorView
//        }
        
    }
    
    
    func configureCell(withCell cell : AnyObject , item : AnyObject? ){
      
        guard let tempCell = cell as? SupplierListingCell else{
            return
        }
        tempCell.supplier = item as? Supplier
    }

    func itemClicked(atIndexPath indexPath : NSIndexPath){
        
        guard let supplierId = (tableViewDataSource.items?[indexPath.row] as? Supplier)?.id , supplierBranchId = (tableViewDataSource.items?[indexPath.row] as? Supplier)?.supplierBranchId else{
            return
        }
        GDataSingleton.sharedInstance.currentSupplier = tableViewDataSource.items?[indexPath.row] as? Supplier
        let supp = GDataSingleton.sharedInstance.currentSupplier
        supp?.categoryId = passedData.categoryId
        GDataSingleton.sharedInstance.currentSupplier = supp
        ez.runThisInMainThread {
            weak var weakSelf : SupplierListingViewController? = self
            weakSelf?.passedData.supplierId = supplierId
            weakSelf?.passedData.supplierBranchId = supplierBranchId
            weakSelf?.pushNextVc()
        }
    }
}


//MARK: - Button Actions
extension SupplierListingViewController{
    
    @IBAction func actionCart(sender: AnyObject) {
        pushVC(StoryboardScene.Options.instantiateCartViewController())
    }
    
    @IBAction func actionSearch(sender: UIButton) {
        view.endEditing(true)
    }
    
    @IBAction func actionMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    @IBAction func actionBack(sender: AnyObject) {
        popVC()
    }
    
    @IBAction func actionFilter(sender: AnyObject) {
        let filterVC = StoryboardScene.Options.instantiateFilterViewController()
        filterVC.delegate  = self
        filterVC.filters = filters
        pushVC(filterVC)
    }

}


extension SupplierListingViewController : FilterViewControllerDelegate {
    func filterApplied (withFilter filter : FilterAssocitedValue){
        
        filters = filter
        if case .Filter(let status ,let delivery , let commission , let rating,let sortBy) = filter {
            var filterSuppliers = suppliers?.filter({ (supplier) -> Bool in                
                                

                var statusBool = false
                var paymentBool = false
                var commissionBool = false
                var ratingBool = false
                
                if status.isEmpty || status.contains(supplier.status) || supplier.status == .DoesntMatter {
                    statusBool = true
                }
                if delivery.isEmpty || delivery.contains(supplier.paymentMethod) || supplier.paymentMethod == .DoesntMatter {
                    paymentBool = true
                }
                if commission.isEmpty || commission.contains(supplier.commissionPackage) {
                    commissionBool = true
                }
                let supplierRating = supplier.rating?.toInt() ?? 0
                
                if rating.isEmpty || rating.contains(Rating(rawValue: supplierRating) ?? .DoesntMatter) || (supplierRating >= 4 && rating.contains(.Star4Above)){
                    ratingBool = true
                }
                return statusBool && paymentBool && commissionBool && ratingBool
            })
            filterSuppliers?.sortInPlace({
                switch sortBy {
                case .MinOrderAmount:
                    return $0.minOrder?.toInt() < $1.minOrder?.toInt()
                case .MinDelTime:
                    return $0.deliveryMinTime?.toInt() < $1.deliveryMinTime?.toInt()
                default:
                    break
                }
                return false
            })
            viewPlaceholder?.hidden = filterSuppliers?.count == 0 ? false : true
            tableViewDataSource.items = filterSuppliers
            tableView.reloadData()
        }
    }

}

//MARK: - SearchField handlers

extension SupplierListingViewController : UITextFieldDelegate {    
    
    @IBAction func textFieldValueChanged(sender: UITextField) {
        let filterSuppliers = suppliers?.filter({ (supplier) -> Bool in
            guard let match = supplier.name?.contains(sender.text!, compareOption: .CaseInsensitiveSearch) else { return false }
            return match
        })
        
        tableViewDataSource.items = sender.text?.characters.count == 0 ? suppliers : filterSuppliers
        tableView.reloadData()
    }
    

    
    func textFieldDidBeginEditing(textField: UITextField) {
        constraintTableViewBottom.constant = 260
        view.layoutIfNeeded()
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        constraintTableViewBottom.constant = 0
        
        view.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
}
