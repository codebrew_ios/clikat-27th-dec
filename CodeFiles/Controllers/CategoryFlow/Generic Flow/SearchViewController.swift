//
//  SearchViewController.swift
//  Clikat
//
//  Created by cblmacmini on 4/29/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import EZSwiftExtensions

let SearchTableViewCellHeight : CGFloat = 240.0

class SearchViewController: CategoryFlowBaseViewController {

    @IBOutlet var viewPlaceholder: UIView!
    @IBOutlet var lblPlaceholder: UILabel!{
        didSet{
            lblPlaceholder.kern(ButtonKernValue)
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tagsView: TLTagsControl!{
        didSet{
            tagsView.tagPlaceholder = L10n.Search.string
            tagsView.tagsBackgroundColor = Colors.MainColor.color()
            tagsView.tagsTextColor = UIColor.whiteColor()
            tagsView.tagInputField?.returnKeyType = .Done
        }
    }
    
    @IBOutlet weak var viewBg: UIView!{
        didSet{
            viewBg.layer.borderWidth = 1.0
            viewBg.layer.borderColor = Colors.lightGrayBackgroundColor.color().CGColor
        }
    }
    var tableDataSource = SearchDataSource(){
        didSet{
            tableView.delegate = tableDataSource
            tableView.dataSource = tableDataSource
        }
    }
    
    var searchResults : SearchResult? {
        didSet{
            ez.runThisAfterDelay(seconds: 0.2, after: {
                weak var weakSelf : SearchViewController? = self
                weakSelf?.tableDataSource.items = weakSelf?.searchResults?.arrSearchResults
                weakSelf?.tableView?.reloadTableViewData(inView: weakSelf?.view)
                
                guard let results = weakSelf?.searchResults?.arrSearchResults where results.count > 0 else{
                    weakSelf?.tableView?.hidden = true
                    weakSelf?.viewPlaceholder?.hidden = false
                    return
                }
                weakSelf?.tableView?.hidden = false
                weakSelf?.viewPlaceholder?.hidden = true
                
            })
        }
    }
    
  
}
//MARK: - View Hierarchy Methods

extension SearchViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        reloadVisibleCells()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        tagsView?.tagInputField?.becomeFirstResponder()
    }
}



extension SearchViewController {
    
    func reloadVisibleCells(){
        DBManager.sharedManager.getCart { (cartProducts) in
            weak var weakSelf = self
            let tableVisibleCells = weakSelf?.tableView.visibleCells ?? []
            
            for tableVisibleCell in tableVisibleCells {
                guard let cell = tableVisibleCell as? SearchTableCell else { return }
                
                let collectionVisibleCells = cell.collectionView.visibleCells()
                
                for collecitonVisibleCell in collectionVisibleCells {
                    guard let cell = collecitonVisibleCell as? ProductCollectionCell else { return }
                    cell.stepper.value = 0
                    for cartProduct in cartProducts {
                        guard let product = cartProduct as? Cart,quantity = product.quantity where cell.product?.id == product.id  else { break }
                        
                        cell.stepper.value = Double(quantity) ?? 0
                    }
                }
            }
        }
    }
}

//MARK: - WebService Methods
extension SearchViewController {
    
    func webServiceMultiSearch (withKeys keys : String?){
        
        guard let tempKeys = keys else{
            return
        }
        
        let parameters = FormatAPIParameters.MultiSearch(supplierBranchId: passedData.supplierBranchId ?? "" , categoryId: passedData.categoryId ?? "", searchList: tempKeys).formatParameters()
        APIManager.sharedInstance.opertationWithRequest(withApi: API.MultiSearch(parameters)) { (response) in
            
            weak var weak : SearchViewController? = self
            switch response{
                
            case .Success(let listing):
                weak?.searchResults = (listing as? SearchResult)
                break
            default :
                break
            }
        }
    }
    
}


extension SearchViewController {
    
    func configureTableView(){

        tableDataSource = SearchDataSource(items: searchResults?.arrSearchResults, height: SearchTableViewCellHeight , tableView: tableView, cellIdentifier: CellIdentifiers.SearchTableCell, configureCellBlock: { (cell, item) in
            
            guard let c = cell as? SearchTableCell,searchResult = item as? Search else { return }
            c.delegate = self
            c.configureCollectionView(searchResult.arrProducts)
            }, aRowSelectedListener: { (indexPath) in
                
        })
    }
}




//MARK: - Button Actions 

extension SearchViewController {
    
    @IBAction func actionMultiSearch(sender: AnyObject) {
        view.endEditing(true)
        
        if (tagsView.tagInputField.text ?? "").characters.count > 0{
            tagsView?.addTag(tagsView.tagInputField.text)
        }
        guard let tags = tagsView?.tags where tags.count > 0 else{
            return
        }
        webServiceMultiSearch(withKeys: tags.componentsJoinedByString(","))
        tagsView?.tagInputField?.text = ""
    }
    
    @IBAction func actionSideMenu(sender: UIButton) {
        toggleSideMenuView()
    }
    
    @IBAction func actionFilter(sender: UIButton) {
    }
    
    @IBAction func actionCart(sender: UIButton) {
        pushVC(StoryboardScene.Options.instantiateCartViewController())
    }
    
    @IBAction func actionBack(sender: UIButton) {
        popVC()
    }
}

extension SearchViewController : SearchProductDelegate {
    
    func productClicked(atIndexPath indexPath: NSIndexPath, product: Product?) {
        let productDetailVc = StoryboardScene.Main.instantiateProductDetailViewController()
        productDetailVc.passedData.productId = product?.id
        productDetailVc.suplierBranchId = product?.supplierBranchId
        pushVC(productDetailVc)
    }
}




