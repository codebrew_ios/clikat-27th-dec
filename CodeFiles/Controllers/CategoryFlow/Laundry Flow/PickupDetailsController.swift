//
//  PickupDetailsController.swift
//  Clikat
//
//  Created by cblmacmini on 5/21/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class PickupDetailsController: CategoryFlowBaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet var btnServicePartner: UIButton!{
        didSet{
            btnServicePartner.kern(ButtonKernValue)
        }
    }
    
    var tableDataSource = PickupDetailsDataSource(){
        didSet{
            tableView.delegate = tableDataSource
            tableView.dataSource = tableDataSource
        }
    }
    var pickupDetails : PickupDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getAllAdresses()
    }

    var isBeautySalon = false
    var tempPickDetails : NSDate?

}

extension PickupDetailsController {
    
    func configureTableView(){
        
        tableDataSource = PickupDetailsDataSource(pickUpDetails: pickupDetails, height: UITableViewAutomaticDimension, tableView: tableView, cellIdentifier: nil, configureCellBlock: { (cell, indexPath) in
            weak var weakSelf = self
            weakSelf?.configureCell(cell)
            }, aRowSelectedListener: { (indexPath) in
                weak var weakSelf = self
                weakSelf?.handleCellSelection(indexPath)
        })
    }
    
    func configureCell(cell : AnyObject?){
        
        let nextDate = NSDate().dateByAddingTimeInterval(60*60*24)
        let strDisplayDate = UtilityFunctions.appendOptionalStrings(withArray: [UtilityFunctions.getDateFormatted(DateFormat.DateFormatGeneric, date: nextDate),"10 : 00 AM"])
        
        (cell as? PickupDateCell)?.pickupDate = pickupDetails?.date ?? NSDate(fromString: strDisplayDate,format: DateFormat.DateFormatGeneric + " " + DateFormat.TimeFormatUI)
        (cell as? DeliveryAddressCell)?.arrAddress = pickupDetails?.arrAddress
        tempPickDetails = pickupDetails?.date ?? NSDate(fromString: strDisplayDate,format: DateFormat.DateFormatGeneric + " " + DateFormat.TimeFormatUI)
    }
    
    func handleCellSelection(indexPath : NSIndexPath){
        guard let cell = tableView.cellForRowAtIndexPath(indexPath) as? PickupDateCell else { return }
       
        let dayComponent = NSDateComponents()
        dayComponent.day = 1
        
        let theCalendar = NSCalendar.currentCalendar()
        let nextDate = theCalendar.dateByAddingComponents(dayComponent, toDate: NSDate(), options: .WrapComponents)
//        let strDisplayDate = UtilityFunctions.appendOptionalStrings(withArray: [UtilityFunctions.getDateFormatted("yyyy-mm-dd", date: nextDate),"10 : 00 AM"])
        let comps = theCalendar.components([.Day,.Month,.Year], fromDate: nextDate ?? NSDate())
        comps.hour   = 10
        comps.minute = 00
        comps.second = 00
        let newDate = theCalendar.dateFromComponents(comps)
        UtilityFunctions.showDatePicker(self, minDate: NSDate(), title: L10n.SelectPickupDateAndTime.string, message: nil, selectedDate: { (date) in
            weak var weakSelf = self
            cell.pickupDate = date
            weakSelf?.pickupDetails?.date = date
            }) { 
                
        }
    }
}

//MARK: - Webservices 

extension PickupDetailsController {
    
    func getAllAdresses(){
        APIManager.sharedInstance.showLoader()
        APIManager.sharedInstance.opertationWithRequest(withApi: API.Addresses(FormatAPIParameters.Addresses(supplierBranchId: nil,areaId: LocationSingleton.sharedInstance.location?.areaEN?.id).formatParameters())) { [weak self] (result) in
            
            APIManager.sharedInstance.hideLoader()
            weak var weakSelf = self
            switch result {
            case .Success(let object):
                guard let delivery = object as? Delivery else { return }
                
                self?.pickupDetails = PickupDetails(arrAddress: delivery.addresses)
                self?.configureTableView()
                self?.tableView.reloadTableViewData(inView: weakSelf?.view)
                break
            case .Failure(_):
                break
            }
        }
    }
}
//MARK: - Button Actions
extension PickupDetailsController {
    
    @IBAction func actionServicePartners(sender: AnyObject) {
        
        guard let cell = tableView.visibleCells.last as? DeliveryAddressCell,selectedIndex = cell.selectedIndexPath?.row where cell.collectionDataSource.items?.count > 0 else {
            view.makeToast(L10n.PleaseSelectAnAddress.string)
            return
        }
        
        GDataSingleton.sharedInstance.pickupDate = pickupDetails?.date ?? tempPickDetails
        GDataSingleton.sharedInstance.pickupAddress = cell.collectionDataSource.items?[selectedIndex] as? Address
        if isBeautySalon{
            let VC = StoryboardScene.Main.instantiateSupplierListingViewController()
            laundryData = OrderSummary()
            laundryData?.pickupDate = pickupDetails?.date ?? tempPickDetails
            VC.laundryData = laundryData
            VC.passedData = passedData
            pushVC(VC)
        }else {
            
            laundryData = OrderSummary()
            laundryData?.pickupAddress = cell.collectionDataSource.items?[selectedIndex] as? Address
            laundryData?.pickupDate = pickupDetails?.date ?? tempPickDetails
            pushNextVc()
        }
        
    }
    
    @IBAction func actionMenu(sender: UIButton) {
        toggleSideMenuView()
    }
    @IBAction func actionCart(sender: UIButton) {
        
    }
    @IBAction func actionBack(sender: UIButton) {
        popVC()
    }
}
