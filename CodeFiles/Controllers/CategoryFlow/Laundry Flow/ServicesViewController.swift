//
//  ServicesViewController.swift
//  Clikat
//
//  Created by cbl73 on 5/7/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class ServicesViewController: CategoryFlowBaseViewController {

    
    let headerHeight = ScreenSize.SCREEN_WIDTH * 0.7
    let headerView = LaundryServiceHeaderView(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_WIDTH * 0.7))
    
    @IBOutlet weak var tableView: SKSTableView!{
        didSet{
        }
    }

  
    var services : [Laundry]?{
        didSet{
            guard let _ = tableView.SKSTableViewDelegate else{
                tableView.SKSTableViewDelegate = self
                tableView.registerNib(UINib(nibName: CellIdentifiers.ProductListingCell,bundle: nil), forCellReuseIdentifier: CellIdentifiers.ProductListingCell)
                tableView.tableHeaderView = headerView
                return
            }
            tableView?.reloadData()
        }
    }
    
    
    //MARK: - View Hierarchy Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        webService()
    }
    
   
}
//MARK: - WebService
extension ServicesViewController {
    
    func webService(){
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.LaundryServices(FormatAPIParameters.LaundryServices(categoryId: passedData.categoryId).formatParameters())) { (response) in
            
            weak var weakSelf = self
            switch response {
            case .Success(let object):
                weakSelf?.services = (object as? LaundryServices)?.services
                break
            case .Failure(_):
                
                break
            }
        }
    }
}

//MARK: - Button Actions
extension ServicesViewController {
    
    
    @IBAction func actionBack(sender: AnyObject) {
        popVC()
    }
    
    @IBAction func actionMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    @IBAction func actionCart(sender: AnyObject) {
        pushVC(StoryboardScene.Options.instantiateCartViewController())
    }
}

extension ServicesViewController : SKSTableViewDelegate {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return services?.count ?? 0
    }
    
    func tableView(tableView: SKSTableView!, numberOfSubRowsAtIndexPath indexPath: NSIndexPath!) -> Int {
        return services?[indexPath.row].products?.count ?? 0
    }
    
    func tableView(tableView: SKSTableView!, shouldExpandSubRowsOfCellAtIndexPath indexPath: NSIndexPath!) -> Bool {
        return false
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifiers.ServiceCell) as? ServiceCell else {
            fatalError("Missing ServiceCell identifier")
        }
        print(indexPath.row)
        cell.selectionStyle = .None
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        cell.expandable = true
        cell.service = services?[indexPath.row]
        return cell
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 84.0
    }
    
    func tableView(tableView: SKSTableView!, heightForSubRowAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        return 100.0
    }
    
    func tableView(tableView: SKSTableView!, cellForSubRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
      
        guard let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifiers.ProductListingCell) else {
            fatalError("Missing Product identifier")
        }
        let products = services?[indexPath.row].products
        (cell as? ProductListingCell)?.product = products?[indexPath.row]
        return cell
        

    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        pushNextVc()
        
    }
    
    func tableView(tableView: SKSTableView!, didSelectSubRowAtIndexPath indexPath: NSIndexPath!) {
        
        pushVC(StoryboardScene.Laundry.instantiatePickupDetailsController())
        
    }
    
    
    
}