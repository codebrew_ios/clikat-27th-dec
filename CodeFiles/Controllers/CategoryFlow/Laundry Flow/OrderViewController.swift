//
//  OrderViewController.swift
//  Clikat
//
//  Created by cblmacmini on 6/4/16.
//  Copyright © 2016 Rajat. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class OrderViewController: CategoryFlowBaseViewController {

    var laundryOrder : LaundryProductListing?{
        didSet{
            
            viewPlaceHolder?.hidden = laundryOrder?.arrDetailedSubCategories?.count == 0 ? false : true
            viewSearch?.hidden = laundryOrder?.arrDetailedSubCategories?.count == 0 ? true : false
            tableView?.hidden = laundryOrder?.arrDetailedSubCategories?.count == 0 ? true : false
            viewHeader.hidden = tableView.hidden
            if laundryOrder?.arrDetailedSubCategories?.count > 0{
                configureTableDataSource()
                configureBarbuttonView()
                refreshNetAmount()
            }
            labelSupplierName.text = laundryOrder?.supplierName
            labelSupplierAddress.text = laundryOrder?.supplierAddress
        }
    }
    var barViewDataSource = BarButtonDataSource()
    var tableDataSource = LaundryOrderDataSource(){
        didSet{
            tableView.delegate = tableDataSource
            tableView.dataSource = tableDataSource
        }
    }
    
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var constraintHeader : NSLayoutConstraint!
    
    @IBOutlet weak var labelNetAmount: UILabel!{
        didSet{
            labelNetAmount.text = "AED 0.0"
        }
    }
    @IBOutlet weak var buttonBarView: ButtonBarView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var labelSupplierName : UILabel!
    @IBOutlet weak var labelSupplierAddress : UILabel!
    @IBOutlet weak var viewPlaceHolder : UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnContinue: UIButton!{
        didSet{
            btnContinue.kern(ButtonKernValue)
        }
    }
    @IBOutlet weak var constraintBarViewHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintTableviewBottom: NSLayoutConstraint!
    
    //MARK: - ButtonBarView Properties
    var changeCurrentIndexProgressive: ((oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void)?
    var changeCurrentIndex: ((oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, animated: Bool) -> Void)?
    
    lazy var buttonBarItemSpec: ButtonBarItemSpec<ButtonBarViewCell> = .NibFile(nibName: "ButtonCell", bundle: NSBundle(forClass: ButtonBarViewCell.self), width:{ [weak self] (childItemInfo) -> CGFloat in
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        
        label.text = childItemInfo.title
        let labelSize = label.intrinsicContentSize()
        return labelSize.width + (LowPadding) * 2
        })

    
    override func viewDidLoad() {
        super.viewDidLoad()
        getAllLaundryProducts()
    }
}

//MARK: - Laundry Products Web Service 
extension OrderViewController {
    
    func getAllLaundryProducts(){
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.LaundryProductListing(FormatAPIParameters.LaundryProductListing(categoryId: passedData.categoryId, supplierBranchId: passedData.supplierBranchId).formatParameters())) { [weak self] (response) in
            
            switch response {
            case .Success(let object):
                ez.runThisAfterDelay(seconds: 0.2, after: { 
                    self?.laundryOrder = (object as? LaundryProductListing)
                })
            default:
                break
            }
        }
    }
}

//MARK: - Configure Button Bar View
extension OrderViewController {
    func configureBarbuttonView(){
        
        buttonBarView.scrollsToTop = false
        let flowLayout = buttonBarView.collectionViewLayout as! UICollectionViewFlowLayout
        flowLayout.scrollDirection = .Horizontal
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.minimumLineSpacing = 0
        let sectionInset = flowLayout.sectionInset
        flowLayout.sectionInset = UIEdgeInsetsMake(sectionInset.top,sectionInset.left, sectionInset.bottom, sectionInset.right)
        
        buttonBarView.showsHorizontalScrollIndicator = false
        buttonBarView.backgroundColor = UIColor.clearColor()
        buttonBarView.selectedBar.backgroundColor = Colors.MainColor.color()
        
        buttonBarView.selectedBarHeight = 2
        // register button bar item cell
        switch buttonBarItemSpec {
        case .NibFile(let nibName, let bundle, _):
            buttonBarView.registerNib(UINib(nibName: nibName, bundle: bundle), forCellWithReuseIdentifier:"Cell")
        case .CellClass:
            buttonBarView.registerClass(ButtonBarViewCell.self, forCellWithReuseIdentifier:"Cell")
        }
        
        barViewDataSource = BarButtonDataSource(items: laundryOrder?.arrDetailedSubCategories ?? [], barButtonView: buttonBarView,tableView: tableView)
        buttonBarView.delegate = barViewDataSource
        buttonBarView.dataSource = barViewDataSource
        buttonBarView.moveToIndex(0, animated: false, swipeDirection: .None, pagerScroll: .ScrollOnlyIfOutOfScreen)
    }
}
//MARK: - Configure table data source 

extension OrderViewController {
    
    func configureTableDataSource(){
        
        tableDataSource = LaundryOrderDataSource(laundryOrder: laundryOrder, tableView: tableView, configureCellBlock: { (cell, item) in
            weak var weakSelf = self
            weakSelf?.configureTableCell(cell, item: item)
            }, aRowSelectedListener: { (indexPath) in
                
        })
        tableView.reloadTableViewData(inView: view)
    }
    
    func configureTableCell(cell : AnyObject?,item : AnyObject?){
        
        guard let indexPath = item as? NSIndexPath else { return }
        
        if let currentCell = cell as? LaundryProductCell {
            let currentProduct = tableDataSource.laundryOrder?.arrDetailedSubCategories?[indexPath.section].arrProducts?[indexPath.row]
            currentCell.product = currentProduct
            currentCell.stepper.stepperValueListener = { [weak self] (value) in
                
                self?.tableDataSource.laundryOrder?.arrDetailedSubCategories?[indexPath.section].arrProducts?[indexPath.row].quantity = String(value ?? 0)
                self?.refreshNetAmount()
                self?.tableView.reloadData()
            }
        }
        
    }
}

//MARK: - Button Actions
extension OrderViewController {
    
    @IBAction func actionCart(sender: UIButton) {
        pushVC(StoryboardScene.Options.instantiateCartViewController())
    }
    @IBAction func actionBack(sender: AnyObject) {
        popVC()
    }
    
    @IBAction func actionMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    @IBAction func actionContinue(sender: AnyObject) {
        
        guard let arrDSC = tableDataSource.laundryOrder?.arrDetailedSubCategories else { return }
        var cartProducts : [Cart] = []
        for sc in arrDSC {
            guard let products = sc.arrProducts else { continue }
            for product in products {
                if let quantity = product.quantity where quantity != "0"{
                    cartProducts.append(product)
                }
            }
        }
        
        addToCartWebService(cartProducts)
    }
}

extension OrderViewController {

    func addToCartWebService(cart : [Cart]){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.AddToCart(FormatAPIParameters.AddToCart(cart: cart, supplierBranchId: cart[0].supplierBranchId,promotionType: nil, remarks: nil).formatParameters())) { (response) in
            weak var weakSelf = self
            switch response {
            case .Success(let object):
                weakSelf?.handleAddToCart(cart, cartId: object)
            default: break
            }
        }
    }
    
    func handleAddToCart(cartProducts : [Cart],cartId : AnyObject?){
        guard let tempCartId = cartId as? String else { return }
        let VC = StoryboardScene.Order.instantiateDeliveryViewController()
        VC.orderSummary = OrderSummary(items: cartProducts)
        VC.orderSummary?.pickupAddress = laundryData?.pickupAddress
        VC.orderSummary?.pickupDate = laundryData?.pickupDate
        VC.orderSummary?.cartId = tempCartId
        pushVC(VC)
    }
}

//MARK: - update Net Total

extension OrderViewController {
    func refreshNetAmount(){
        var amount : Double = 0.0
        guard let arrDetailCategories = tableDataSource.laundryOrder?.arrDetailedSubCategories else { return }
        for subCategory in arrDetailCategories {
            guard let products = subCategory.arrProducts else { continue }
            for product in products {
                guard let price = Double(product.price ?? "0.0"),quantity = Double(product.quantity ?? "0") else { continue }
                amount += price * quantity
            }
        }
        
        labelNetAmount.text = L10n.AED.string + amount.toString
    }
}

//MARK: - TextField Methods

extension OrderViewController : UITextFieldDelegate {
    
    @IBAction func textFieldValueChanged(sender: UITextField) {

        var tempDetailSubCat : [DetailedSubCategories] = []
        for detailSubCat in laundryOrder?.arrDetailedSubCategories ?? [] {
            let filterProducts = detailSubCat.arrProducts?.filter({ (product) -> Bool in
                return /product.name?.lowercaseString.trim().hasPrefix(/sender.text?.lowercaseString)
            })
            
            tempDetailSubCat.append(DetailedSubCategories(strTitle: detailSubCat.strTitle, products: filterProducts))
        }
        let laundryListing = LaundryProductListing()
        laundryListing.arrDetailedSubCategories = sender.text?.characters.count == 0 ? laundryOrder?.arrDetailedSubCategories : tempDetailSubCat
        
        tableDataSource.laundryOrder = laundryListing
        view.updateConstraints()
        refreshNetAmount()
        tableView.reloadData()
    }
    
    @IBAction func actionSearch(sender: UIButton) {
        view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        constraintTableviewBottom?.constant = 260
        constraintHeader?.constant = 0
        view.layoutIfNeeded()
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        constraintTableviewBottom.constant = 0
        if textField.text?.trim().characters.count == 0 {
            constraintHeader?.constant = 136
        }
        view.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
}

