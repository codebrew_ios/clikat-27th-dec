//
//  PackageSupplierListingViewController.swift
//  Clikat
//
//  Created by Night Reaper on 23/05/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class PackageSupplierListingViewController: CategoryFlowBaseViewController {

    @IBOutlet weak var constraintTableViewBottom: NSLayoutConstraint!
    @IBOutlet weak var tfSearch: UITextField!
    var filters : FilterAssocitedValue = .Filter([],[],[],[],.DoesntMatter)
    
    @IBOutlet var tableView: UIExpandableTableView!{
        didSet{
            tableView.registerNib(UINib(nibName: CellIdentifiers.SupplierListingCell,bundle: nil), forCellReuseIdentifier: CellIdentifiers.SupplierListingCell)

        }
    }
    var tableViewDataSource = TableViewDataSource(){
        didSet{
            tableView?.dataSource = tableViewDataSource
            tableView?.delegate = tableViewDataSource
        }
    }
    
    @IBOutlet weak var viewBg: UIView!{
        didSet{
            viewBg.layer.borderWidth = 0.5
            viewBg.layer.borderColor = UIColor.lightGrayColor().CGColor
        }
    }
    @IBOutlet weak var viewPlaceholder: UIView!
    var suppliers : [Supplier]? = []{
        didSet{
            tableViewDataSource.items = suppliers
            viewPlaceholder?.hidden = suppliers?.count > 0 ? true : false
            tableView?.reloadTableViewData(inView: view)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        webServiceSupplierListing()
    }
}

//MARK: - WebService Methods
extension PackageSupplierListingViewController {
    
    func webServiceSupplierListing (){
        
        let parameters = FormatAPIParameters.PackageSupplierListing().formatParameters()
        APIManager.sharedInstance.opertationWithRequest(withApi: API.PackageSupplierListing(parameters)) { (response) in
            
            weak var weak : PackageSupplierListingViewController? = self
            switch response{
                
            case .Success(let listing):
                weak?.suppliers = (listing as? SupplierListing)?.suppliers
                
            default :
                break
            }
        }
    }
    
}

//MARK: - Button Actions
extension PackageSupplierListingViewController {
    
    @IBAction func actionMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    @IBAction func actionBack(sender: AnyObject) {
        popVC()
    }
    
    @IBAction func actionFilter(sender: UIButton) {
        
        let filterVC = StoryboardScene.Options.instantiateFilterViewController()
        filterVC.delegate  = self
        filterVC.filters = filters
        pushVC(filterVC)
    }
    
}




//MARK: - SearchField handlers

extension PackageSupplierListingViewController : UITextFieldDelegate {
    
    @IBAction func textFieldValueChanged(sender: UITextField) {
        let filterSuppliers = suppliers?.filter({ (supplier) -> Bool in
            guard let match = supplier.name?.contains(sender.text!, compareOption: .CaseInsensitiveSearch) else { return false }
            return match
        })
        tableViewDataSource.items = sender.text?.characters.count == 0 ? suppliers : filterSuppliers
        tableView.reloadData()
    }
    
    
    
    func textFieldDidBeginEditing(textField: UITextField) {
        constraintTableViewBottom.constant = 260
        view.layoutIfNeeded()
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        constraintTableViewBottom.constant = 0
        
        view.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
}