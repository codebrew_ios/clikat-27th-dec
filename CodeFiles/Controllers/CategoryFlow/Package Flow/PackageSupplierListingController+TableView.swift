//
//  PackageSupplierListingController+TableView.swift
//  Clikat
//
//  Created by Night Reaper on 23/05/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation
import EZSwiftExtensions

//MARK: - TableView Configuration Methods
extension PackageSupplierListingViewController {
    
    
    // MARK: UITableViewDataSource
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 124.0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return suppliers?.count ?? 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let items = suppliers else{
            return 0
        }
        if (!items.isEmpty) {
            if (self.tableView.sectionOpen != NSNotFound && section == self.tableView.sectionOpen) {
                return items[section].categories?.count ?? 0
            }
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CellIdentifier", forIndexPath: indexPath)
        let categories = suppliers?[indexPath.section].categories
        cell.textLabel?.text = categories?[indexPath.row].category_name
        cell.textLabel?.font = UIFont(name: Fonts.ProximaNova.Regular, size: Size.Medium.rawValue)
        cell.textLabel?.backgroundColor = UIColor.clearColor()
        return cell
    }
    
    // MARK: UITableViewDelegate
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = HeaderView(tableView: self.tableView, section: section)

        let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifiers.SupplierListingCell) as! SupplierListingCell
        cell.frame = headerView.bounds
        cell.supplier = suppliers?[section]
        headerView.addSubview(cell)

        return headerView
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        guard let supplier = suppliers?[indexPath.section] , supplierId = supplier.id , supplierBranchId = supplier.supplierBranchId  , category = supplier.categories?[indexPath.row] else{
            return
        }
        GDataSingleton.sharedInstance.currentSupplier = supplier
        ez.runThisInMainThread { [weak self] in
            self?.passedData.supplierId = supplierId
            self?.passedData.categoryId = category.category_id
            self?.passedData.categoryOrder = category.order
            self?.passedData.supplierBranchId = supplierBranchId
            self?.pushNextVc()
        }

        
    }

}

extension PackageSupplierListingViewController : FilterViewControllerDelegate {
    func filterApplied (withFilter filter : FilterAssocitedValue){
        
        filters = filter
        if case .Filter(let status ,let delivery , let commission , let rating,let sortBy) = filter {
            var filterSuppliers = suppliers?.filter({ (supplier) -> Bool in
                
                
                var statusBool = false
                var paymentBool = false
                var commissionBool = false
                var ratingBool = false
                
                if status.isEmpty || status.contains(supplier.status) || supplier.status == .DoesntMatter {
                    statusBool = true
                }
                if delivery.isEmpty || delivery.contains(supplier.paymentMethod) || supplier.paymentMethod == .DoesntMatter {
                    paymentBool = true
                }
                if commission.isEmpty || commission.contains(supplier.commissionPackage) || supplier.commissionPackage == .DoesntMatter {
                    commissionBool = true
                }
                let supplierRating = supplier.rating?.toInt() ?? 0
                
                if rating.isEmpty || rating.contains(Rating(rawValue: supplierRating) ?? .DoesntMatter) || (supplierRating >= 4 && rating.last == .Star4Above){
                    ratingBool = true
                }
                return statusBool && paymentBool && commissionBool && ratingBool 
            })
            filterSuppliers?.sortInPlace({
                switch sortBy {
                case .MinOrderAmount:
                    return $0.minOrder?.toInt() < $1.minOrder?.toInt()
                case .MinDelTime:
                    return $0.minimumDeliveryTime?.toInt() < $1.minimumDeliveryTime?.toInt()
                default:
                    break
                }
                return false
            })
            tableViewDataSource.items = filterSuppliers
            tableView.reloadData()
        }
    }
    
}