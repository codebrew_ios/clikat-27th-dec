//
//  PaymentMethodController.swift
//  Clikat
//
//  Created by cblmacmini on 5/23/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material
import EZSwiftExtensions
import SwiftyJSON
import CryptoSwift
import Adjust

class PaymentMethodController: BaseViewController {
    @IBOutlet weak var btnPayment: UIButton!
    @IBOutlet weak var btnCOD: UIButton!{
        didSet{            
            btnCOD.setAttributedTitle(attributedStringWithImage( L10n.CashOnDelivery.string , image: Asset.Ic_payment_cash), forState: .Normal)
        }
    }
    @IBOutlet weak var btnCreditCard: UIButton!{
        didSet{
            btnCreditCard.setAttributedTitle(attributedStringWithImage(L10n.CreditDebitCard.string, image: Asset.Ic_payment_card), forState: .Normal)
        }
    }
    @IBOutlet weak var viewBgCardDetails: MaterialView!
    @IBOutlet weak var viewCardHolderName: MaterialView!
    @IBOutlet weak var viewCardNumber: MaterialView!
    @IBOutlet weak var viewExpiry: MaterialView!
    
    @IBOutlet weak var tfCardHoldersName : UITextField!
    @IBOutlet weak var tfCardNumber: UITextField!
    @IBOutlet weak var tfCVV: UITextField!
    @IBOutlet weak var tfExpiryDate: UITextField!
    
    var selectedPaymentMethod : PaymentMethod = .COD
    var orderId : String?
    var orderSummary : OrderSummary?
    
    var isConfirmOrder : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnPayment.kern(ButtonKernValue)
        btnCOD.selected = true
        viewBgCardDetails.alpha = 0.0
        viewCardHolderName.transform = CGAffineTransformMakeTranslation(viewBgCardDetails.frame.size.width, 0)
        viewCardNumber.transform = CGAffineTransformMakeTranslation(-viewBgCardDetails.frame.size.width, 0)
        viewExpiry.transform = CGAffineTransformMakeTranslation(viewBgCardDetails.frame.size.width, 0)
        
        guard let method = orderSummary?.paymentMethod else { return }
        
        switch method {
        case .Card:
            btnCOD?.hidden = true
            btnCreditCard?.hidden = false
            selectedPaymentMethod = .Card
        case .COD:
            btnCOD?.hidden = false
            btnCreditCard?.hidden = true
            selectedPaymentMethod = .COD
        case .DoesntMatter:
            btnCOD?.hidden = false
            btnCreditCard?.hidden = false
            selectedPaymentMethod =  .COD
        }
        
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }

}

extension PaymentMethodController {
    
    func attributedStringWithImage(string : String ,image : Asset) -> NSMutableAttributedString {
        let attachment = NSTextAttachment()
        attachment.image = UIImage(asset : image)
        let attachmentString = NSAttributedString(attachment: attachment)
        let myString = NSMutableAttributedString(string: "")
        myString.appendAttributedString(attachmentString)
        myString.appendAttributedString(NSAttributedString(string: "   "))
        myString.appendAttributedString(NSAttributedString(string: string))
        return myString
    }
}

//MARK: - Button Actions
extension PaymentMethodController {
    
    @IBAction func actionMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    @IBAction func actionBack(sender: AnyObject) {
        popVC()
    }
    
    @IBAction func actionCod(sender: UIButton) {
        if sender.selected { return }
        sender.selected = !sender.selected
        btnCreditCard.selected = !sender.selected
        toggleInfoView()
    }
    
    @IBAction func actionCard(sender: UIButton) {
        if sender.selected { return }
        sender.selected.toggle()
        btnCOD.selected.toggle()
        
        toggleInfoView()
    }
    
    @IBAction func actionFinish(sender: UIButton) {

        if isConfirmOrder {
            APIManager.sharedInstance.opertationWithRequest(withApi: API.ConfirmScheduleOrder(FormatAPIParameters.ConfirmScheduleOrder(orderId: orderId, paymentType: selectedPaymentMethod.rawValue).formatParameters()), completion: { (response) in
                switch response {
                case .Success(_):
                    
                    UtilityFunctions.showSweetAlert(L10n.Success.string, message: L10n.OrderConfirmedSuccessfully.string, style: .Success, success: {
                        let VC = StoryboardScene.Order.instantiateOrderDetailController()
                        VC.orderDetails = OrderDetails(orderId: self.orderId)
                        VC.type = .OrderUpcoming
                        VC.isConfirmOrder = true
                        self.pushVC(VC)
                        })
                    
                default: break
                }
            })
            return
        }else if selectedPaymentMethod != .Card {
            generateOrder()
            return
        }
        APIManager.sharedInstance.showLoader()
        HTTPClient().postPayFortRequest { [weak self] (response) in
            APIManager.sharedInstance.hideLoader()
            switch response {
            case .Success(let object):
                self?.startPayfortRequest((object as? PayFort)?.sdkToken)
            case .Failure(let validation):
                print(validation.message)
            }
        }
    }
}

//MARK: - Credit Card Info Animation
extension PaymentMethodController {
    
    func toggleInfoView(){
        
        if btnCOD.selected {
            selectedPaymentMethod = .COD
        }else {
            selectedPaymentMethod = .Card
        }
    }
}
//MARK: - TextField Actions

extension PaymentMethodController : UITextFieldDelegate {
    
    @IBAction func textFieldEditingChanged(sender: BKCardNumberField) {

        let formatedtext = formatCreditCardNumber(sender.text ?? "")
        
        if formatedtext != tfCardNumber.text {
            tfCardNumber.text = formatedtext
        }
        if tfCardNumber.text?.characters.count == 19 {
            tfCardNumber.resignFirstResponder()
        }
    }
    
    func formatCreditCardNumber(inputText : String) -> String{
        
        var output = ""
        switch inputText.characters.count {
        case 1...4:
            output = inputText
        case 5...8:
            let firstStr = inputText.substringToIndex(inputText.startIndex.advancedBy(4))
            let lastStr = inputText.substringFromIndex(inputText.startIndex.advancedBy(4))
            output = String(format: "%@-%@", arguments: [firstStr,lastStr])
        case 9...12:
            let firstStr = inputText.substringToIndex(inputText.startIndex.advancedBy(4))
            
            let middleStr = inputText.substringWithRange(inputText.startIndex.advancedBy(4) ..< inputText.startIndex.advancedBy(8))
            let lastStr = inputText.substringFromIndex(inputText.startIndex.advancedBy(8))
            output = String(format: "%@-%@-%@", arguments: [firstStr,middleStr,lastStr])
        case 13...16:
            let firstStr = inputText.substringToIndex(inputText.startIndex.advancedBy(4))
            
            let middleStr1 = inputText.substringWithRange(inputText.startIndex.advancedBy(4) ..< inputText.startIndex.advancedBy(8))
            let middleStr2 = inputText.substringWithRange(inputText.startIndex.advancedBy(8) ..< inputText.startIndex.advancedBy(12))
            let lastStr = inputText.substringFromIndex(inputText.startIndex.advancedBy(12))
            output = String(format: "%@-%@-%@-%@", arguments: [firstStr,middleStr1,middleStr2,lastStr])
        default:
            output = ""
        }
        return output
    }
}

//MARK: - GenerateOrder Web service

extension PaymentMethodController {
    
    func generateOrder(){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.GenerateOrder(FormatAPIParameters.GenerateOrder(cartId: orderSummary?.cartId,isPackage: orderSummary?.isPackage,paymentType:selectedPaymentMethod.rawValue).formatParameters())) { (response) in
            weak var weakSelf = self
            switch response {
            case .Success(let object):
                weakSelf?.handleGenerateOrder(object)
            case .Failure(_):
                break
            }
        }
    }
    func handleGenerateOrder(orderId : AnyObject?){
        AdjustEvent.Order.sendEvent(orderSummary?.totalAmount)
        DBManager.sharedManager.deleteAllData(CoreDataEntity.Cart.rawValue)
        UtilityFunctions.showSweetAlert(L10n.OrderPlacedSuccessfully.string, message: L10n.YourOrderHaveBeenPlacedSuccessfully.string, style: .Success, success: { [unowned self] in
            
            let VC = StoryboardScene.Order.instantiateOrderSchedularViewController()
            VC.orderId = orderId as? String
            VC.orderSummary = self.orderSummary
            VC.orderSummary?.paymentMethod = self.selectedPaymentMethod
            self.pushVC(VC)
            })
    }
}

extension PaymentMethodController : PayFortDelegate {
    
    func sdkResult(response: AnyObject!) {
        
        let response = JSON(response)
        if response["response_message"].stringValue == "Success"{
            generateOrder()
            AdjustEvent.Purchase.sendEvent()
        }
        print(response)
    }
}

extension PaymentMethodController {
    
   
    func randomStringWithLength (len : Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for _ in 0...len {
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.characterAtIndex(Int(rand)))
        }
    
        return randomString as String
    }
}

//MARK: - Payfort
extension PaymentMethodController {
    func startPayfortRequest(sdkToken : String?){
        let payfortController = PayFortController(enviroment: .Production)
        payfortController.delegate = self
        var request	= [String : String]()
        
        request["command"] = "PURCHASE"
        request["currency"] = "AED"
        request["customer_email"] = GDataSingleton.sharedInstance.loggedInUser?.email ?? ""
        request["language"] = Localize.currentLanguage() == Languages.Arabic ? "ar" : "en"
        request["merchant_reference"] = randomStringWithLength(8) + "-" + /orderId
        request["sdk_token"] = sdkToken ?? ""
        request["amount"] = Int(/orderSummary?.totalAmount * 100).toString
        request["customer_name"] = /GDataSingleton.sharedInstance.loggedInUser?.firstName
        payfortController.IsShowResponsePage = true
        payfortController.setPayFortRequest(NSMutableDictionary(dictionary: request))
        payfortController.callPayFort(self)
        
    }
}
