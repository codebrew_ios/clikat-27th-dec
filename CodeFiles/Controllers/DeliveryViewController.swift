//
//  DeliveryViewController.swift
//  Clikat
//
//  Created by cblmacmini on 5/19/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class DeliveryViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnContinue : UIButton!
    var tableDataSource = DeliveryDataSource(){
        didSet{
            tableView.dataSource = tableDataSource
            tableView.delegate = tableDataSource
        }
    }
    //LoyaltyPoints
    var cartFlowType : CartFlowType = .Normal
    var loyaltyPointsSummary : LoyaltyPointsSummary?
    
    var deliveryData = Delivery(){
        didSet{
            deliveryData.initalizeDelivery(orderSummary?.items)
            ez.runThisAfterDelay(seconds: 0.2) { [weak self] in
                
                self?.configureTableView()
                APIManager.sharedInstance.hideLoader()
                self?.tableView.reloadTableViewData(inView: self?.view)
            }
        }
    }
    
    var orderSummary : OrderSummary?
    
    var cartId : String?
    var selectedDate : NSDate?
    var remarks : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getAllAdresses()
        AdjustEvent.Delivery.sendEvent()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        updateUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func updateUI(){
        
        btnContinue.kern(ButtonKernValue)
    }
    
}

//MARK: - Webservice Get all Adresses
extension DeliveryViewController {
    func getAllAdresses(){
        
        var branchId = orderSummary?.items?.first?.supplierBranchId
        if let _ = loyaltyPointsSummary {
            branchId = loyaltyPointsSummary?.items?.first?.supplierBranchId
        }
        APIManager.sharedInstance.showLoader()
         APIManager.sharedInstance.opertationWithRequest(withApi: API.Addresses(FormatAPIParameters.Addresses(supplierBranchId:branchId ,areaId: LocationSingleton.sharedInstance.location?.areaEN?.id).formatParameters())) { (result) in
            
            weak var weakSelf = self
            switch result {
            case .Success(let object):
                guard let delivery = object as? Delivery else { return }
                weakSelf?.deliveryData = delivery
                break
            case .Failure(_):
                break
            }
        }
    }
}

extension DeliveryViewController {
    
    func configureTableView(){
        
        if let _ = loyaltyPointsSummary ,standardSpeed = deliveryData.deliverySpeeds?.first{
            deliveryData.deliverySpeeds = [standardSpeed]
        }
        tableDataSource = DeliveryDataSource(items: deliveryData, height: UITableViewAutomaticDimension, tableView: tableView, cellIdentifier: nil, configureCellBlock: { (cell, indexPath, selectedDeliverySpeed) in
            weak var weakSelf = self
            weakSelf?.configureTableCell(cell, indexpath: indexPath, selectedDeliveryType: selectedDeliverySpeed)
            }, aRowSelectedListener: { (indexPath) in
                weak var weakSelf = self
                weakSelf?.handleTableCellSelection(indexPath)
        })
        tableDataSource.loyaltyPointsSummary = loyaltyPointsSummary
    }
    
}

extension DeliveryViewController {
    
    
    func configureTableCell(cell : AnyObject?,indexpath : NSIndexPath?,selectedDeliveryType : DeliverySpeedType?){
        guard let indexPath = indexpath else { return }
        if let tempCell = cell as? DeliveryAddressCell {
            tempCell.arrAddress = deliveryData.addresses
        }
        
        if indexPath.row < deliveryData.deliverySpeeds?.count {
            
            guard let tempCell = cell as? DeliverySpeedCell, deliverySpeed = deliveryData.deliverySpeeds?[indexPath.row] else { return }
            tempCell.deliveryData = deliveryData
            tempCell.deliverySpeed = deliverySpeed
        }else {
           
            guard let maxDays = Int(deliveryData.deliveryMaxTime ?? "0") else { return }
            let timeInterval = Double(60 * maxDays)
            
            (cell as? TimeAndDateCell)?.selectedDate = selectedDate ?? (deliveryData.pickupDate ?? NSDate()).dateByAddingTimeInterval(timeInterval)
        }
    }
}

extension DeliveryViewController {
    
    func handleTableCellSelection(indexPath : NSIndexPath?){
        
        guard let deliverySpeeds = deliveryData.deliverySpeeds,indexpath = indexPath,index = indexPath?.row where indexPath?.section > 0 else { return }
        for speed in deliverySpeeds {
            speed.selected = false
        }
        if indexPath?.row < deliverySpeeds.count {
            deliverySpeeds[index].selected = true
            tableDataSource.selectedDeliverySpeed = deliverySpeeds[index].type
            tableDataSource.items = deliverySpeeds
            selectedDate = nil
            deliveryData.addresses = (tableView.visibleCells.first as? DeliveryAddressCell)?.arrAddress
            tableView.reloadTableViewData(inView: view)
        }else{
            guard let timeCell = tableView.cellForRowAtIndexPath(indexpath) as? TimeAndDateCell else { return }
            guard let maxDays = Int(deliveryData.deliveryMaxTime ?? "0"), var timeInterval = Double(deliveryData.urgentDeliveryTime ?? "0") else { return }
            timeInterval = Double(60*maxDays) 
            UtilityFunctions.showDatePicker(self ,datePickerMode: .DateAndTime, initialDate:  deliveryData.pickupDate ?? NSDate() ,minDate: (deliveryData.pickupDate ?? NSDate()).dateByAddingTimeInterval(timeInterval), title: L10n.SelectTimeAndDate.string , message: "", selectedDate: { (date) in
                
                weak var weakSelf = self
                timeCell.selectedDate = date
                weakSelf?.selectedDate = date
                }, cancel: { 
                    
            })
        }
        
    }
}


//MARK: - Button Actions
extension DeliveryViewController {
    
    @IBAction func actionContinue(sender: UIButton) {
        
        
        if cartFlowType == .LoyaltyPoints {
            pushLoyaltyPointsSummary()
        }else {
            
            webServiceForUpdateCartInfo()
        }
    }
    
    @IBAction func actionMenu(sender: UIButton) {
        toggleSideMenuView()
    }
    @IBAction func actionBack(sender: UIButton) {
        popVC()
    }
}

//MARK: - webservice for Update Cart Info


extension DeliveryViewController {
    func webServiceForUpdateCartInfo(){
        
        guard let visibleCell = tableView.visibleCells.first as? DeliveryAddressCell,selectedIndexPath = visibleCell.selectedIndexPath where visibleCell.arrAddress?.count > 0  else {
            view.makeToast(L10n.PleaseSelectAnAddress.string)
            return
        }
        
        
        //Haspinder Singh
        var netTotal = orderSummary?.totalAmount ?? 0.0
        switch tableDataSource.selectedDeliverySpeed {
        case .Urgent:
            netTotal = (netTotal - (deliveryData.deliveryCharges?.toDouble() ?? 0.0)) + ((deliveryData.urgentPrice?.toDouble()) ?? 0.0)
        default:
            break
        }
        
        let deliveryAddressId = visibleCell.arrAddress?[selectedIndexPath.row].id
        let deliveryDate = selectedDate ?? Delivery.getDeliveryDate(deliveryData, selectedType: tableDataSource.selectedDeliverySpeed)
        
   
        let parameters = FormatAPIParameters.UpdateCartInfo(cartId: orderSummary?.cartId, deliveryAddressId: deliveryAddressId, deliveryType: tableDataSource.selectedDeliverySpeed.rawValue.toString,delivery: deliveryData, deliveryDate: deliveryDate, netAmount: netTotal.toString , remarks: remarks).formatParameters()

        APIManager.sharedInstance.opertationWithRequest(withApi: API.UpdateCartInfo(parameters)) { (response) in
            weak var weakSelf = self
            switch response {
            case .Success(_):
                weakSelf?.handleUpdateCartInfo()
            case .Failure(_):
                break
            }
        }
    }
    
    func handleUpdateCartInfo(){
        
        guard let visibleCell = tableView.visibleCells.first as? DeliveryAddressCell,selectedIndexPath = visibleCell.selectedIndexPath where visibleCell.arrAddress?.count > 0 else {
            return
        }

        let deliveryAddress = visibleCell.arrAddress?[selectedIndexPath.row]

        let VC = StoryboardScene.Order.instantiateOrderSummaryController()
        
        orderSummary?.dDeliveryCharges = deliveryData.deliveryCharges?.toDouble()
        
        if tableDataSource.selectedDeliverySpeed == .Urgent {
            orderSummary?.deliveryAdditionalCharges = deliveryData.urgentPrice?.toDouble()
        }
        
        orderSummary?.initalizeOrderSummary(orderSummary?.items)
        orderSummary?.deliveryAddress = deliveryAddress
        orderSummary?.deliveryDate = selectedDate ?? Delivery.getDeliveryDate(deliveryData, selectedType: tableDataSource.selectedDeliverySpeed)
        orderSummary?.pickupAddress = deliveryData.pickupAddress
        orderSummary?.pickupDate = deliveryData.pickupDate
        if orderSummary?.items?.first?.category == Category.Laundry.rawValue || orderSummary?.items?.first?.category == Category.BeautySalon.rawValue {
            orderSummary?.pickupBuffer = deliveryData.deliveryMaxTime
        }
        orderSummary?.paymentMethod = deliveryData.paymentMethod
        VC.orderSummary = orderSummary
        VC.remarks = remarks
        pushVC(VC)
    }
}

//MARK: - Loyalty Points Summary
extension DeliveryViewController {
    
    func pushLoyaltyPointsSummary(){
        let VC = StoryboardScene.Order.instantiateLoyaltyPointsSummaryController()
        VC.cartFlowType = cartFlowType
        guard let visibleCell = tableView.visibleCells.first as? DeliveryAddressCell,selectedIndexPath = visibleCell.selectedIndexPath where visibleCell.arrAddress?.count > 0 else  {
            return
        }
        
        let deliveryAddress = visibleCell.arrAddress?[selectedIndexPath.row]
        loyaltyPointsSummary?.deliveryAddress = deliveryAddress
        loyaltyPointsSummary?.deliveryDate = selectedDate
        loyaltyPointsSummary?.deliveryData = deliveryData
        VC.orderSummary = loyaltyPointsSummary
        pushVC(VC)
    }
}