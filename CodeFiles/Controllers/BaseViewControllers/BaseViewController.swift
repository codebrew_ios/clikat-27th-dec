//
//  BaseViewController.swift
//  Clikat
//
//  Created by Night Reaper on 27/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import Material

class LoginRegisterBaseViewController : UIViewController {
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - UITextfield Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        let tag = textField.tag + 1
        let nextTextfield : UITextField? = self.view.viewWithTag(tag) as? UITextField
        nextTextfield != nil ? nextTextfield?.becomeFirstResponder() : textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
    }
}

class BaseViewController: UIViewController {
    let supplierView = FloatingSupplierView(frame: CGRect(x: Localize.currentLanguage() == Languages.Arabic ? 16 : ScreenSize.SCREEN_WIDTH - 80, y: ScreenSize.SCREEN_HEIGHT - 96, w: 64, h: 64))
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch self{
        case is SubcategoryViewController,is ItemListingViewController,is PackageProductListingViewController,is ProductDetailViewController,is SearchViewController:
            addSupplierImage(true)
        case is CartViewController,is DeliveryViewController, is PaymentMethodController:
            addSupplierImage(false)
        default:
            break
        }
    }

    func addSupplierImage(isCategoryFlow : Bool){
        if !isCategoryFlow {
            
            APIManager.sharedInstance.opertationWithRequest(withApi: API.SupplierImage(FormatAPIParameters.SupplierImage(supplierBranchId: GDataSingleton.sharedInstance.currentSupplierId).formatParameters())) {[weak self] (response) in
                switch response {
                case .Success(let object):
                    guard let image = object as? String else { return }
                    self?.addFloatingButton(isCategoryFlow,image: image.componentsSeparatedByString(" ").first,supplierId:image.componentsSeparatedByString(" ").last,supplierBranchId: GDataSingleton.sharedInstance.currentSupplierId )
                default :
                    break
                }
            }
        }else {
            guard let supplier = GDataSingleton.sharedInstance.currentSupplier,image = supplier.logo,id = supplier.id else {
                supplierView.removeFromSuperview()
                return
            }
            addFloatingButton(isCategoryFlow,image: image,supplierId: id,supplierBranchId: supplier.supplierBranchId)
        }
    }
    
    func addFloatingButton(isCategoryFlow : Bool,image : String?,supplierId : String?,supplierBranchId : String?){
        supplierView.imageSupplier.yy_setImageWithURL(NSURL(string:image?.percentEscapedString() ?? ""), options: .SetImageWithFadeAnimation)
        supplierView.supplierBranchId = supplierBranchId
        supplierView.supplierId = supplierId
        supplierView.floatingViewTapped = { [weak self] in
            let VC = StoryboardScene.Main.instantiateSupplierInfoViewController()
            VC.passedData.supplierBranchId = supplierBranchId
            VC.passedData.supplierId = supplierId
            if isCategoryFlow {
                VC.passedData.categoryId = GDataSingleton.sharedInstance.currentSupplier?.categoryId
            }else {
                VC.passedData.categoryId = GDataSingleton.sharedInstance.currentCategoryId
            }
            VC.showButton = false
            self?.pushVC(VC)
        }
        self.view.addSubview(supplierView)
        self.view.bringSubviewToFront(supplierView)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(BaseViewController.handleUrlScheme(_:)), name: UrlSchemeNotification, object: nil)
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
}



//MARK: - Handle User Scheme
extension BaseViewController{

    func handleUrlScheme(notification : NSNotification) {
        
        AdjustEvent.DeepLink.sendEvent()
        let VC = StoryboardScene.Main.instantiateSupplierInfoViewController()
       // VC.passedData.supplierBranchId = notification.userInfo?["branchId"]?.stringValue
        VC.passedData.supplierId = notification.userInfo?["supplierId"] as? String
        VC.passedData.supplierBranchId = notification.userInfo?["branchId"] as? String
        VC.passedData.categoryId = notification.userInfo?["categoryId"] as? String
        VC.showButton = false
        self.pushVC(VC)
    }
}
