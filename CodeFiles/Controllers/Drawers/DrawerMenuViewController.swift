//
//  MenuViewController.swift
//  Clikat
//
//  Created by Night Reaper on 15/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import ZDCChat

class DrawerMenuViewController: BaseViewController {

  
    @IBOutlet var imageViewProfilePic: UIImageView!{
        didSet{
            imageViewProfilePic.layer.borderWidth = 2.0
            imageViewProfilePic.layer.borderColor = UIColor.whiteColor().CGColor
        }
    }
    @IBOutlet var labelUserName: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var btnLocation: UIButton!
    @IBOutlet weak var labelWelcome : UILabel!
    
    let menuModal : Menu = Menu()
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerNib(UINib(nibName: CellIdentifiers.SideMenuCell, bundle: nil), forCellReuseIdentifier: CellIdentifiers.SideMenuCell)
    }
  
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
        updateUI()
    }
    
    func updateUI(){
        labelWelcome?.text = L10n.Welcome.string
        btnLocation.setTitle(UtilityFunctions.appendOptionalStrings(withArray: [LocationSingleton.sharedInstance.location?.getArea()?.name ,LocationSingleton.sharedInstance.location?.getCity()?.name],separatorString: " , "), forState: .Normal)

        labelUserName?.text = GDataSingleton.sharedInstance.loggedInUser?.firstName ?? L10n.Guest.string
        guard let image = GDataSingleton.sharedInstance.loggedInUser?.userImage , url = NSURL(string : image) else{
            imageViewProfilePic.image = UIImage(asset: Asset.User_placeholder)
            return
        }
        imageViewProfilePic?.yy_setImageWithURL(url, placeholder: UIImage(asset: Asset.User_placeholder))
        tableView.reloadData()
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
}

//MARK: - Button Actions
extension DrawerMenuViewController {
    
    @IBAction func actionProfile(sender: UIButton) {
        
        guard let _ = GDataSingleton.sharedInstance.loggedInUser?.firstName else{
            UtilityFunctions.sharedAppDelegateInstance().window?.rootViewController?.presentVC(StoryboardScene.Register.instantiateLoginViewController())
            return
        }
        let settingsVC = StoryboardScene.Options.instantiateSettingsViewController()
        settingsVC.delegate = self
        sideMenuController()?.setContentViewController(settingsVC)
    }
    @IBAction func actionBtnLocation(sender: UIButton) {
        let splashVC = StoryboardScene.Register.instantiateSplashViewController()
        guard let window = UtilityFunctions.sharedAppDelegateInstance().window else { return }
        toggleSideMenuView()
        var delegateVC : HomeViewController? = HomeViewController()
        if let navVC = window.rootViewController as? LeftNavigationViewController {
           delegateVC = navVC.topViewController as? HomeViewController
        }else if let navVC = window.rootViewController as? RightNavigationViewController {
            delegateVC = navVC.topViewController as? HomeViewController
        }
        splashVC.delegate = delegateVC
        window.rootViewController?.presentVC(splashVC)
    }
    
    @IBAction func actionFb(sender: AnyObject) {
        openWebURL(withUrlString: ApplicationWebLinks.FacebookLink)
    }
    
    
    @IBAction func actionTwitter(sender: AnyObject) {
        openWebURL(withUrlString: ApplicationWebLinks.TwitterLink)
    }
    
    
    @IBAction func actionInsta(sender: AnyObject) {
        openWebURL(withUrlString: ApplicationWebLinks.InstagramLink)
    }
    
    @IBAction func actionYoutube(sender: UIButton) {
        openWebURL(withUrlString: ApplicationWebLinks.YoutubeLink)
    }
    
    func openWebURL(withUrlString urlString : String?){
        guard let string = urlString , url = NSURL(string: string) else{
            return
        }
        UIApplication.sharedApplication().openURL(url)
    }

}

extension DrawerMenuViewController : UITableViewDataSource , UITableViewDelegate{
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return menuModal.numberOfSections
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuModal.sectionData[section].count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifiers.SideMenuCell) as! SideMenuCell
        cell.selectionStyle = .None
        cell.menuItem = menuModal.sectionData[indexPath.section][indexPath.row]
        if cell.menuItem?.getTitle() == L10n.ScheduledOrders.string{
            let badgeView = M13BadgeView(frame: CGRect(x: 0, y: MidPadding, w: 20, h: 20))
            badgeView.animateChanges = false
            badgeView.badgeBackgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.25)
            badgeView.hidesWhenZero = true
            badgeView.horizontalAlignment = M13BadgeViewHorizontalAlignmentCenter
            badgeView.font = UIFont(name: Fonts.ProximaNova.Light, size: Size.Small.rawValue)
            badgeView.shadowBadge = true
            badgeView.text = LocationSingleton.sharedInstance.scheduledOrders ?? "0"
            if let _ = GDataSingleton.sharedInstance.loggedInUser where LocationSingleton.sharedInstance.scheduledOrders != "0" {
                cell.accessoryView = badgeView
            }else {
                cell.accessoryView = nil
            }
        }
        return cell
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == menuModal.numberOfSections - 1{
            let view = UIView(x: 0, y: 0, w: tableView.bounds.width, h: 32)
            view.backgroundColor = UIColor.clearColor()
            let label = UILabel(x: 20, y: 0 , w: tableView.bounds.width - 40, h: 32)
            label.textColor = UIColor.whiteColor()
            label.font = UIFont(name: Fonts.ProximaNova.SemiBold , size: 12)
            label.text = L10n.MyAccount.string
            label.textAlignment = Localize.currentLanguage() == Languages.Arabic ? .Right : .Left
            label.backgroundColor = UIColor.clearColor()
            label.font = UIFont(name: Fonts.ProximaNova.SemiBold , size: Size.Small.rawValue)
            let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light))
            visualEffectView.frame = view.bounds
            view.addSubview(visualEffectView)
            view.addSubview(label)
            return view
        }
        
        return nil
        
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == menuModal.numberOfSections - 1{
            return 32
        }
        return 0.0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        var topVC : UIViewController?
        if topMostController() is LeftNavigationViewController {
            topVC = (topMostController() as? LeftNavigationViewController)?.topViewController
        }else if topMostController() is RightNavigationViewController {
            topVC = (topMostController() as? RightNavigationViewController)?.topViewController
        }
        sideMenuController()
        let menu = menuModal.sectionData[indexPath.section][indexPath.row]
        guard let title = menu.getTitle() else{return}
        switch title {
        case L10n.Home.string :
            if topVC is HomeViewController { toggleSideMenuView() ; return }
            sideMenuController()?.setContentViewController(StoryboardScene.Main.instantiateHomeViewController())
            return
        case L10n.ShareApp.string :
            
            UtilityFunctions.shareContentOnSocialMedia(withViewController: UtilityFunctions.sharedAppDelegateInstance().window?.rootViewController, message: L10n.TheLeadingOnlineHomeServicesInUAE.string)
            return
        case L10n.ShareApp.string :
            if topVC is LiveSupportViewController { toggleSideMenuView() ; return }
            sideMenuController()?.setContentViewController(StoryboardScene.Options.instantiateLiveSupportViewController())
        case L10n.TermsAndConditions.string,L10n.AboutUs.string:
            let VC = StoryboardScene.Options.instantiateTermsAndConditionsController()
            VC.type = title == L10n.AboutUs.string ? .AboutUs : .TermsAndConditions
            sideMenuController()?.setContentViewController(VC)
        default: break
        }
        
        if GDataSingleton.sharedInstance.loggedInUser == nil && title != L10n.Cart.string {
            let loginVc = StoryboardScene.Register.instantiateLoginViewController()
            
            ez.runThisInMainThread({
                weak var weakSelf : DrawerMenuViewController? = self
                weakSelf?.view.window?.rootViewController?.presentVC(loginVc)
            })
            return
        }
        
        
        // User Based Functionality
        switch title {
        case L10n.Cart.string:
            if topVC is CartViewController { toggleSideMenuView() ; return }
            let cartVC = StoryboardScene.Options.instantiateCartViewController()
            cartVC.FROMSIDEPANEL = true
            sideMenuController()?.setContentViewController(cartVC)
            
        case L10n.LiveSupport.string:
            toggleSideMenuView()
            AdjustEvent.LiveSupport.sendEvent()
            ZDCChat.instance().api.trackEvent("")
            ZDCChat.updateVisitor({ (visitor) in
                visitor.phone = GDataSingleton.sharedInstance.loggedInUser?.mobileNo ?? ""
                visitor.name = GDataSingleton.sharedInstance.loggedInUser?.firstName ?? ""
                visitor.email = GDataSingleton.sharedInstance.loggedInUser?.email ?? ""
            })
            ZDCChat.startChat({ (config) in
                config.preChatDataRequirements.name = ZDCPreChatDataRequirement.Required
                config.preChatDataRequirements.email = ZDCPreChatDataRequirement.Required
                config.preChatDataRequirements.phone = ZDCPreChatDataRequirement.Required
                config.preChatDataRequirements.department = ZDCPreChatDataRequirement.Required
                config.preChatDataRequirements.message = ZDCPreChatDataRequirement.Required
            })
//            UtilityFunctions.sharedAppDelegateInstance().window?.makeToast("Coming Soon!")
            
        case L10n.Promotions.string:
            if topVC is PromotionsViewController { toggleSideMenuView() ; return }
            sideMenuController()?.setContentViewController(StoryboardScene.Options.instantiatePromotionsViewController())
            
        case L10n.Notifications.string:
            if topVC is NotificationsViewController { toggleSideMenuView() ; return }
            sideMenuController()?.setContentViewController(StoryboardScene.Options.instantiateNotificationsViewController())
            
        case L10n.MyFavorites.string :
            if topVC is MyFavoritesViewController { toggleSideMenuView() ; return }
            sideMenuController()?.setContentViewController(StoryboardScene.Options.instantiateMyFavoritesViewController())
            
        case L10n.OrderHistory.string :
            if topVC is OrderHistoryViewController { toggleSideMenuView() ; return }
            sideMenuController()?.setContentViewController(StoryboardScene.Order.instantiateOrderHistoryViewController())
            
        case L10n.TrackMyOrder.string :
            if topVC is TrackMyOrderViewController { toggleSideMenuView() ; return }
            sideMenuController()?.setContentViewController(StoryboardScene.Order.instantiateTrackMyOrderViewController())
            
        case L10n.RateMyOrder.string :
            if topVC is RateMyOrderController { toggleSideMenuView() ; return }
            sideMenuController()?.setContentViewController(StoryboardScene.Order.instantiateRateMyOrderController())
            
        case L10n.PendingOrders.string :
            if topVC is UpcomingOrdersViewController { toggleSideMenuView() ; return }
            sideMenuController()?.setContentViewController(StoryboardScene.Order.instantiateUpcomingOrdersViewController())
            
        case L10n.ScheduledOrders.string :
            if topVC is ScheduledOrderController { toggleSideMenuView() ; return }
            
            guard let VC = UIStoryboard(name: "Order", bundle: NSBundle.mainBundle()).instantiateViewControllerWithIdentifier("ScheduledOrderController") as? ScheduledOrderController else { return }
            sideMenuController()?.setContentViewController(VC)
            
        case L10n.LoyalityPoints.string :
            if topVC is LoyalityPointsViewController { toggleSideMenuView() ; return }
            sideMenuController()?.setContentViewController(StoryboardScene.Options.instantiateLoyalityPointsViewController())
            
        case L10n.Settings.string :
            if topVC is SettingsViewController { toggleSideMenuView() ; return }
            let settingsVC = StoryboardScene.Options.instantiateSettingsViewController()
            settingsVC.delegate = self
            sideMenuController()?.setContentViewController(settingsVC)
        case L10n.CompareProducts.string :
            if topVC is CompareProductsController { toggleSideMenuView() ; return }
            sideMenuController()?.setContentViewController(StoryboardScene.Options.instantiateCompareProductsController())
        default:
            break
        }
        
    }
}

//MARK: - User Logged Out
extension DrawerMenuViewController : LogoutDelegate {
    
    func userLoggedOut() {
        updateUI()
        sideMenuController()?.setContentViewController(StoryboardScene.Main.instantiateHomeViewController())
        toggleSideMenuView()
    }
    
}
