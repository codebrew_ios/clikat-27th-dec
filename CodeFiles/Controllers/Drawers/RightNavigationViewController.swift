//
//  RightNavigationViewController.swift
//  Clikat
//
//  Created by Night Reaper on 15/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import ENSwiftSideMenu

class RightNavigationViewController: ENSideMenuNavigationController {
    
    var overLayView = UIView(frame: UIScreen.mainScreen().bounds)

    var menuViewController : UIViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        overLayView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.22)
        let tap = UITapGestureRecognizer(target: self, action: #selector(LeftNavigationViewController.handleTap(_:)))
        overLayView.addGestureRecognizer(tap)
        
        let rightDrawerVc = StoryboardScene.Main.instantiateDrawerMenuViewController()
        menuViewController = rightDrawerVc
        sideMenu = ENSideMenu(sourceView: self.view, menuViewController: rightDrawerVc, menuPosition:.Right)
        sideMenu?.menuWidth = (ScreenSize.SCREEN_WIDTH * 4.0)/5.0 // optional, default is 160
        sideMenu?.delegate = self
        sideMenu?.bouncingEnabled = false
        view.bringSubviewToFront(navigationBar)
    }
    
    func handleTap(sender : UITapGestureRecognizer){
        toggleSideMenuView()
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }

}


// MARK: - ENSideMenu Delegate

extension RightNavigationViewController : ENSideMenuDelegate{
    
    func sideMenuWillOpen() {
        view.endEditing(true)
        if let VC = menuViewController as? DrawerMenuViewController {
            VC.updateUI()
        }
        UIView.animateWithDuration(0.3) {
            weak var weakSelf : RightNavigationViewController? = self
            weakSelf?.topViewController?.view.addSubview(self.overLayView)
        }
    }
    
    func sideMenuWillClose() {
        
        UIView.animateWithDuration(0.4) {
            weak var weakSelf : RightNavigationViewController? = self
            weakSelf?.overLayView.removeFromSuperview()
        }
    }
        
}
