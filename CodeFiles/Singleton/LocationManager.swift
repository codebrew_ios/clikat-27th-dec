//
//  RKLocationManager.swift
//  InPerson
//
//  Created by CB Macmini_3 on 17/12/15.
//  Copyright © 2015 Rico. All rights reserved.
//

import Foundation
import CoreLocation
import AFNetworking

struct Location {
    var currentLat : String?
    var currentLng : String?
    var currentFormattedAddr : String?
    var currentCity : String?
}

struct DeviceTime {
    
    var currentTime : String {
        get{
            return  localTimeFormatter.stringFromDate(NSDate())
        }
    }
    
    var currentDate : String{
        get{
            return localDateFormatter.stringFromDate(NSDate())
        }
    }
    
    var currentZone : String {
        get {
            return NSTimeZone.localTimeZone().name
        }
    }
    
    
    
    func convertCreatedAt (createdAt : String?) -> NSDate?{
        return localDateTimeFormatter.dateFromString(createdAt ?? "")
    }
    
    
    var localTimeZoneFormatter = NSDateFormatter()
    var localTimeFormatter = NSDateFormatter()
    var localDateFormatter = NSDateFormatter()
    
    var localDateTimeFormatter = NSDateFormatter()
    
    func setUpTimeFormatters(){
        localTimeFormatter.dateFormat = "h:mm a"
        localDateFormatter.dateFormat = "MMM dd yyyy"
        localDateTimeFormatter.dateFormat = "MMM dd yyyy h:mm a Z"
        
    }
}

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    var currentLocation : Location? = Location()
    var currentTime : DeviceTime? = DeviceTime()
    class var sharedInstance: LocationManager {
        struct Static {
            static var instance: LocationManager?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = LocationManager()
        }
        
        return Static.instance!
    }
    

    
    var isReachable : Bool = true
    
    var isLocationServicesEnabled : Bool {
        get {
            return CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() == .Denied
        }
    }
    
    func startTrackingUser(){
        
        currentTime?.setUpTimeFormatters()
        // Ask for Authorisation from the User.
//        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        
       
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager.startUpdatingLocation()
        }
    }
    
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print(error.localizedDescription)
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locValue = (manager.location?.coordinate) ?? CLLocationCoordinate2D()
        self.currentLocation?.currentLat = "\(locValue.latitude)"
        self.currentLocation?.currentLng = "\(locValue.longitude)"
        
        guard let location = locations.last else{
            return
        }
        self.currentLocation?.currentLat = "\(location.coordinate.latitude)"
        self.currentLocation?.currentLng = "\(location.coordinate.longitude)"
        
        CLGeocoder().reverseGeocodeLocation(location) { (placemarks, error) -> Void in
            if let mark = placemarks?.last , addrList = mark.addressDictionary {
                self.formatAddress(addrList)
            }
        }
    }
    
       
    private func formatAddress (parameters : [NSObject : AnyObject]){
        
        self.currentLocation?.currentFormattedAddr = (parameters["FormattedAddressLines"] as? [String])?.joinWithSeparator(", ")
        
        guard let city = parameters["City"] as? String else{
            if let city = parameters["SubAdministrativeArea"] as? String{
                self.currentLocation?.currentCity = city
            }
            else if let city = parameters["State"] as? String{
                self.currentLocation?.currentCity = city
            }
            else if let city = parameters["Country"] as? String{
                self.currentLocation?.currentCity = city
            }
            return
        }
        self.currentLocation?.currentCity = city
        
    }
    
 
    
    
}

extension LocationManager  {
    

}
