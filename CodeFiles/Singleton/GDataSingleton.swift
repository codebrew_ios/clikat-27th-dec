//
//  GDataSingleton.swift
//  Real Estate
//
//  Created by Gagan on 08/04/15.
//  Copyright (c) 2015 Gagan. All rights reserved.
//

import Foundation

enum SingletonKeys : String {
    case Home = "ClikatHome"
    case User = "ClikatUser"
    case SupplierId = "ClikatSupplierId"
    case supplier = "ClikatSupplier"
    case PickupDate = "ClikatPickupDate"
    case PickupAddress = "ClikatPickupAddress"
    case DeviceToken = "ClikatDeviceToken"
    case ShowPopUp = "ClikatShowPopUp"
    case categoryId = "ClikatCategoryId"
    case tokenData = "ClikatDeviceTokenData"
}

class GDataSingleton{
    
    class var sharedInstance: GDataSingleton {
        struct Static {
            static var instance: GDataSingleton?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = GDataSingleton()
        }
        
        return Static.instance!
    }
    
    
    var languageId : String {
        return homeData?.arrayLanguages?[Localize.currentLanguage() == Languages.Arabic ? 1 : 0].id ?? ""
    }
    
    var homeData : Home?{
        get{
            var home : Home?
            if let data = NSUserDefaults.standardUserDefaults().rm_customObjectForKey(SingletonKeys.Home.rawValue) as? Home{
                home = data
            }
            return home
        }
        set{
            let defaults = NSUserDefaults.standardUserDefaults()
            if let value = newValue{
                defaults.rm_setCustomObject(value, forKey: SingletonKeys.Home.rawValue)
            }
            else{
                defaults.removeObjectForKey(SingletonKeys.Home.rawValue)
            }
        }
    }
    
    
    var loggedInUser : User?{
        get{
            return NSUserDefaults.standardUserDefaults().rm_customObjectForKey(SingletonKeys.User.rawValue) as? User
        }
        set{
            if let value = newValue {
                NSUserDefaults.standardUserDefaults().rm_setCustomObject(value, forKey: SingletonKeys.User.rawValue)
            }else{
                NSUserDefaults.standardUserDefaults().removeObjectForKey(SingletonKeys.User.rawValue)
            }
        }
    }
    
    var currentSupplierId : String?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey(SingletonKeys.SupplierId.rawValue) as? String
        }
        set{
            if let value = newValue {
                NSUserDefaults.standardUserDefaults().setObject(value, forKey: SingletonKeys.SupplierId.rawValue)
            }else{
                NSUserDefaults.standardUserDefaults().removeObjectForKey(SingletonKeys.SupplierId.rawValue)
            }
        }
    }
    
    var currentCategoryId : String?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey(SingletonKeys.categoryId.rawValue) as? String
        }
        set{
            if let value = newValue {
                NSUserDefaults.standardUserDefaults().setObject(value, forKey: SingletonKeys.categoryId.rawValue)
            }else{
                NSUserDefaults.standardUserDefaults().removeObjectForKey(SingletonKeys.categoryId.rawValue)
            }
        }
    }
    
    
    var currentSupplier : Supplier?{
        get{
            return NSUserDefaults.standardUserDefaults().rm_customObjectForKey(SingletonKeys.supplier.rawValue) as? Supplier
        }
        set{
            if let value = newValue {
                NSUserDefaults.standardUserDefaults().rm_setCustomObject(value, forKey: SingletonKeys.supplier.rawValue)
            }else{
                NSUserDefaults.standardUserDefaults().removeObjectForKey(SingletonKeys.supplier.rawValue)
            }
        }
    }
    
    
    //MARK: - Pickup Details
    var pickupDate : NSDate?{
        get{
            return NSUserDefaults.standardUserDefaults().rm_customObjectForKey(SingletonKeys.PickupDate.rawValue) as? NSDate
        }
        set{
            if let value = newValue {
                NSUserDefaults.standardUserDefaults().rm_setCustomObject(value, forKey: SingletonKeys.PickupDate.rawValue)
            }else{
                NSUserDefaults.standardUserDefaults().removeObjectForKey(SingletonKeys.PickupDate.rawValue)
            }
        }
    }
    
    var pickupAddress : Address?{
        get{
            return NSUserDefaults.standardUserDefaults().rm_customObjectForKey(SingletonKeys.PickupAddress.rawValue) as? Address
        }
        set{
            if let value = newValue {
                NSUserDefaults.standardUserDefaults().rm_setCustomObject(value, forKey: SingletonKeys.PickupAddress.rawValue)
            }else{
                NSUserDefaults.standardUserDefaults().removeObjectForKey(SingletonKeys.PickupAddress.rawValue)
            }
        }
    }
    
    //MARK: - Device Token
    
    var deviceToken : String?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey(SingletonKeys.DeviceToken.rawValue) as? String
        }
        set{
            if let value = newValue {
                NSUserDefaults.standardUserDefaults().setObject(value, forKey: SingletonKeys.DeviceToken.rawValue)
            }else {
                NSUserDefaults.standardUserDefaults().removeObjectForKey(SingletonKeys.DeviceToken.rawValue)
            }
        }
    }
    
    var tokenData : NSData?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey(SingletonKeys.tokenData.rawValue) as? NSData
        }
        set{
            if let value = newValue {
                NSUserDefaults.standardUserDefaults().setObject(value, forKey: SingletonKeys.tokenData.rawValue)
            }else {
                NSUserDefaults.standardUserDefaults().removeObjectForKey(SingletonKeys.tokenData.rawValue)
            }
        }
    }
    
    var pushDict : AnyObject? {
        get{
            return NSUserDefaults.standardUserDefaults().rm_customObjectForKey(RemoteNotification)
        }
        set{
            if let value = newValue {
                NSUserDefaults.standardUserDefaults().rm_setCustomObject(value, forKey: RemoteNotification)
            }else{
                NSUserDefaults.standardUserDefaults().removeObjectForKey(RemoteNotification)
            }
        }
    }
    
    //MARK: - Rate Order
    
    var showRatingPopUp : String?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey(SingletonKeys.ShowPopUp.rawValue) as? String
        }
        set{
            if let value = newValue {
                NSUserDefaults.standardUserDefaults().setObject(value, forKey: SingletonKeys.ShowPopUp.rawValue)
            }else {
                NSUserDefaults.standardUserDefaults().removeObjectForKey(SingletonKeys.ShowPopUp.rawValue)
            }
        }
        
    }
}
