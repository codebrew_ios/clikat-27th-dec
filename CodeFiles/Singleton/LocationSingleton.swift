//
//  LocationSingleton.swift
//  Clikat
//
//  Created by cblmacmini on 5/26/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class LocationSingleton: NSObject {
    
    class var sharedInstance: LocationSingleton {
        struct Static {
            static var instance: LocationSingleton?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = LocationSingleton()
        }
        
        return Static.instance!
    }
    
    var location : ApplicationLocation?{
        get{
            return NSUserDefaults.standardUserDefaults().rm_customObjectForKey("ClikatApplicationLocation") as? ApplicationLocation
        }
        set{
            if let value = newValue {
                NSUserDefaults.standardUserDefaults().rm_setCustomObject(value, forKey: "ClikatApplicationLocation")
            }else{
                NSUserDefaults.standardUserDefaults().removeObjectForKey("ClikatApplicationLocation")
            }
        }
    }
    
    func isLocationSelected() -> Bool{
        guard let _ = location?.countryEN, _ = location?.cityEN, _ = location?.areaEN,_ = location?.countryAR, _ = location?.cityAR, _ = location?.areaAR else {
            return false
        }
        return true
    }
    
    var scheduledOrders : String?{
        get {
            return NSUserDefaults.standardUserDefaults().objectForKey("ClikatScheduledOrders") as? String
        }
        set{
            guard let value = newValue else {
                NSUserDefaults.standardUserDefaults().removeObjectForKey("ClikatScheduledOrders")
                return
            }
            
            NSUserDefaults.standardUserDefaults().setObject(value, forKey: "ClikatScheduledOrders")
        }
    }
    
}
