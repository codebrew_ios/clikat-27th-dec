//
//  DBManager.swift
//  Clikat
//
//  Created by cblmacmini on 5/10/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import CoreData

enum CartKeys : String{
    case id = "id"
    case name = "name"
    case dateModified = "dateModified"
    case quantity = "quantity"
    case price = "price"
    case productType = "productType"
    case image = "image"
    case deliveryCharges = "deliveryCharges"
    case handlingAdmin = "handlingAdmin"
    case handlingSupplier = "handlingSupplier"
    case supplierBranchId = "supplierBranchId"
    case displayPrice = "displayPrice"
    case sku = "sku"
    case measuringUnit = "measuringUnit"
    
    case priceType = "priceType"
    case fixedPrice = "fixedPrice"
    case hourlyPrice = "hourlyPrice"
    case canUrgent = "canUrgent"
    case urgentValue = "urgentValue"
    case urgentType = "urgentType"
    case category = "category"
    case categoryId = "categoryId"
}

class DBManager: NSObject {
    
    var coreDataStack : CoreDataStack{
        get{
            let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
            return delegate.coreDataStack
        }
    }
    
    class var sharedManager: DBManager {
        struct Static {
            static var instance: DBManager?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = DBManager()
        }
        
        return Static.instance!
    }
    
    func getCart(result : CartCompletionBlock){
        
        let fetchRequest = NSFetchRequest(entityName: CoreDataEntity.Cart.rawValue)
        
        coreDataStack.managedObjectContext.performBlock {
            
            do {
                weak var weakSelf = self
                guard let results = try weakSelf?.coreDataStack.managedObjectContext.executeFetchRequest(fetchRequest) as? [NSManagedObject], cartArray = weakSelf?.convertManagedObjectArrayToCartArray(results) else { return }
                
                result(cartArray)
            }catch let error {
                print(error)
            }
        }
    }
    
    func convertManagedObjectArrayToCartArray(arrManagedObject : [NSManagedObject]) -> [Cart]{
        
        var arrCart : [Cart] = []
        for mc in arrManagedObject {
            let cart = Cart()
            cart.id = mc.valueForKey(CartKeys.id.rawValue) as? String
            cart.name = mc.valueForKey(CartKeys.name.rawValue) as? String
            cart.dateModified = mc.valueForKey(CartKeys.dateModified.rawValue) as? NSDate
            cart.quantity = mc.valueForKey(CartKeys.quantity.rawValue) as? String
            cart.handlingSupplier = mc.valueForKey(CartKeys.handlingSupplier.rawValue) as? String
            cart.productType = mc.valueForKey(CartKeys.productType.rawValue) as? String
            cart.price = mc.valueForKey(CartKeys.price.rawValue) as? String
            cart.image = mc.valueForKey(CartKeys.image.rawValue) as? String
            cart.deliveryCharges = mc.valueForKey(CartKeys.deliveryCharges.rawValue) as? String
            cart.handlingAdmin = mc.valueForKey(CartKeys.handlingAdmin.rawValue) as? String
            cart.supplierBranchId = mc.valueForKey(CartKeys.supplierBranchId.rawValue) as? String
            cart.displayPrice = mc.valueForKey(CartKeys.displayPrice.rawValue) as? String
            cart.sku = mc.valueForKey(CartKeys.sku.rawValue) as? String
            cart.measuringUnit = mc.valueForKey(CartKeys.measuringUnit.rawValue) as? String
            cart.canUrgent = mc.valueForKey(CartKeys.canUrgent.rawValue) as? String
            cart.priceType = PriceType(rawValue: (mc.valueForKey(CartKeys.priceType.rawValue) as? String)?.toInt() ?? 2) ?? .None
            cart.fixedPrice = mc.valueForKey(CartKeys.fixedPrice.rawValue) as? String
            cart.strHourlyPrice = mc.valueForKey(CartKeys.hourlyPrice.rawValue) as? String
            cart.urgentValue = mc.valueForKey(CartKeys.urgentValue.rawValue) as? String
            cart.urgentType = mc.valueForKey(CartKeys.urgentType.rawValue) as? String
            cart.category = mc.valueForKey(CartKeys.category.rawValue) as? String
            cart.hourlyPrice = Cart.initializePriceArray(cart.strHourlyPrice)
            cart.categoryId = mc.valueForKey(CartKeys.categoryId.rawValue) as? String
            arrCart.append(cart)
        }
        
        return arrCart
    }
    
    func saveProductsToCart(entityName : String, product : Product?, quantity : Int){
        
        guard let entity = NSEntityDescription.entityForName(entityName, inManagedObjectContext: (coreDataStack.managedObjectContext)),currentProduct = product else {
            fatalError("Could not find entity descriptions!")
        }
        
        coreDataStack.managedObjectContext.mergePolicy = NSErrorMergePolicy
        let cartProduct = NSManagedObject(entity: entity, insertIntoManagedObjectContext: coreDataStack.managedObjectContext)
        cartProduct.setValue(currentProduct.id, forKey: CartKeys.id.rawValue)
        cartProduct.setValue(currentProduct.dateModified, forKey: CartKeys.dateModified.rawValue)
        cartProduct.setValue(currentProduct.handlingSupplier, forKey: CartKeys.handlingSupplier.rawValue)
        cartProduct.setValue(currentProduct.name, forKey: CartKeys.name.rawValue)
        cartProduct.setValue(quantity.toString, forKey: CartKeys.quantity.rawValue)
        cartProduct.setValue(currentProduct.price, forKey: CartKeys.price.rawValue)
        cartProduct.setValue(currentProduct.productType, forKey: CartKeys.productType.rawValue)
        cartProduct.setValue(currentProduct.image, forKey: CartKeys.image.rawValue)
        cartProduct.setValue(currentProduct.deliveryCharges, forKey: CartKeys.deliveryCharges.rawValue)
        cartProduct.setValue(currentProduct.handlingAdmin, forKey: CartKeys.handlingAdmin.rawValue)
        cartProduct.setValue(currentProduct.supplierBranchId, forKey: CartKeys.supplierBranchId.rawValue)
        cartProduct.setValue(currentProduct.displayPrice, forKey: CartKeys.displayPrice.rawValue)
        cartProduct.setValue(currentProduct.measuringUnit, forKey: CartKeys.measuringUnit.rawValue)
        cartProduct.setValue(currentProduct.sku, forKey: CartKeys.sku.rawValue)
        cartProduct.setValue(currentProduct.canUrgent, forKey: CartKeys.canUrgent.rawValue)
        cartProduct.setValue(currentProduct.priceType.rawValue.toString, forKey: CartKeys.priceType.rawValue)
        cartProduct.setValue(currentProduct.fixedPrice, forKey: CartKeys.fixedPrice.rawValue)
        cartProduct.setValue(currentProduct.strHourlyPrice, forKey: CartKeys.hourlyPrice.rawValue)
        cartProduct.setValue(currentProduct.urgentValue, forKey: CartKeys.urgentValue.rawValue)
        cartProduct.setValue(currentProduct.urgentType, forKey: CartKeys.urgentType.rawValue)
        cartProduct.setValue(currentProduct.category, forKey: CartKeys.category.rawValue)
        cartProduct.setValue(currentProduct.categoryId, forKey: CartKeys.categoryId.rawValue)
        coreDataStack.saveMainContext()
    }
    
    
    func manageCart(product : Product?, quantity : Int){
        
        NSNotificationCenter.defaultCenter().postNotificationName(CartNotification, object: self,userInfo: ["badge" : quantity])
        if quantity == 0 {
            removeProductFromCart(product?.id)
        }else{
            updateProductsToCart(CoreDataEntity.Cart.rawValue, product: product, quantity: quantity)
        }
    }
    
    
    func updateProductsToCart(entityName : String, product : Product?, quantity : Int){
        
        let managedContext = coreDataStack.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: CoreDataEntity.Cart.rawValue)
        fetchRequest.returnsObjectsAsFaults = false
        let predicate = NSPredicate(format: "id = %@", product?.id ?? "")
        fetchRequest.predicate = predicate
        guard let currentProduct = product else {
            return
        }
        
        do {
            guard let cartProduct = try managedContext.executeFetchRequest(fetchRequest) as? [NSManagedObject] else { return }
            for cart in cartProduct {
                
                cart.setValue(currentProduct.id, forKey: CartKeys.id.rawValue)
                cart.setValue(currentProduct.dateModified, forKey: CartKeys.dateModified.rawValue)
                cart.setValue(currentProduct.handlingSupplier, forKey: CartKeys.handlingSupplier.rawValue)
                cart.setValue(currentProduct.name, forKey: CartKeys.name.rawValue)
                cart.setValue(quantity.toString, forKey: CartKeys.quantity.rawValue)
                cart.setValue(currentProduct.price, forKey: CartKeys.price.rawValue)
                cart.setValue(currentProduct.productType, forKey: CartKeys.productType.rawValue)
                cart.setValue(currentProduct.image, forKey: CartKeys.image.rawValue)
                cart.setValue(currentProduct.deliveryCharges, forKey: CartKeys.deliveryCharges.rawValue)
                cart.setValue(currentProduct.handlingAdmin, forKey: CartKeys.handlingAdmin.rawValue)
                cart.setValue(currentProduct.supplierBranchId, forKey: CartKeys.supplierBranchId.rawValue)
                cart.setValue(currentProduct.displayPrice, forKey: CartKeys.displayPrice.rawValue)
                cart.setValue(currentProduct.measuringUnit, forKey: CartKeys.measuringUnit.rawValue)
                cart.setValue(currentProduct.sku, forKey: CartKeys.sku.rawValue)
                cart.setValue(currentProduct.canUrgent, forKey: CartKeys.canUrgent.rawValue)
                cart.setValue(currentProduct.priceType.rawValue.toString, forKey: CartKeys.priceType.rawValue)
                cart.setValue(currentProduct.fixedPrice, forKey: CartKeys.fixedPrice.rawValue)
                cart.setValue(currentProduct.strHourlyPrice, forKey: CartKeys.hourlyPrice.rawValue)
                cart.setValue(currentProduct.urgentValue, forKey: CartKeys.urgentValue.rawValue)
                cart.setValue(currentProduct.urgentType, forKey: CartKeys.urgentType.rawValue)
                cart.setValue(currentProduct.category, forKey: CartKeys.category.rawValue)
                cart.setValue(currentProduct.categoryId, forKey: CartKeys.categoryId.rawValue)
            }
            if cartProduct.count == 0{
                saveProductsToCart(CoreDataEntity.Cart.rawValue, product: product, quantity: quantity)
            }
            coreDataStack.saveMainContext()
            
        }catch let error {
            print(error)
        }
    }
}

//MARK: - Delete
extension DBManager {
    func deleteAllData(entity: String){
         NSNotificationCenter.defaultCenter().postNotificationName(CartNotification, object: self,userInfo: ["badge" : 0])
        let managedContext = coreDataStack.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        managedContext.performBlockAndWait {
            do {
                let results = try managedContext.executeFetchRequest(fetchRequest)
                for managedObject in results
                {
                    let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                    managedContext.deleteObject(managedObjectData)
                }
                self.coreDataStack.saveMainContext()
            } catch let error as NSError {
                print("Detele all data in \(entity) error : \(error) \(error.userInfo)")
            }
        }
        
    }
    
    func removeProductFromCart(productId : String?){
        let managedContext = coreDataStack.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: CoreDataEntity.Cart.rawValue)
        fetchRequest.returnsObjectsAsFaults = false
        let predicate = NSPredicate(format: "id = %@", productId ?? "")
        fetchRequest.predicate = predicate
        
        
        do {
            guard let cartProduct = try managedContext.executeFetchRequest(fetchRequest) as? [NSManagedObject] else { return }
            for cart in cartProduct {
                
                coreDataStack.managedObjectContext.deleteObject(cart)
            }
            coreDataStack.saveMainContext()
        }catch let error {
            print(error)
        }
    }
    
    func isCartEmpty() -> Bool {
        let managedContext = coreDataStack.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: CoreDataEntity.Cart.rawValue)
        fetchRequest.returnsObjectsAsFaults = false
        do {
        let count = try managedContext.countForFetchRequest(fetchRequest)
             return count > 0 ? false : true
        }catch{
            return true
        }
       
    }
    
}
