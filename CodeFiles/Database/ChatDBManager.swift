//
//  ChatDBManager.swift
//  Clikat
//
//  Created by cblmacmini on 6/9/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import CoreData

enum CoreDataEntity : String {
    case Cart = "Cart"
    case Message = "Message"
}

typealias MessagesCompletionBlock = ([Message]?) -> ()

class ChatDBManager: DBManager {

    class var sharedChatManager: ChatDBManager {
        struct Static {
            static var instance: ChatDBManager?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = ChatDBManager()
        }
        
        return Static.instance!
    }
    
    func getMessagesFromDatabase(result : MessagesCompletionBlock){
        
        let fetchRequest = NSFetchRequest(entityName: CoreDataEntity.Message.rawValue)
        
        coreDataStack.managedObjectContext.performBlock {
            
            do {
                weak var weakSelf = self
                guard let results = try weakSelf?.coreDataStack.managedObjectContext.executeFetchRequest(fetchRequest) as? [NSManagedObject], cartArray = weakSelf?.convertManagedObjectArrayToMessageArray(results) else { return }
                
                result(cartArray)
            }catch let error {
                print(error)
            }
        }
    }
    
    func convertManagedObjectArrayToMessageArray(arrManagedObject : [NSManagedObject]) -> [Message]{
        
        var arrCart : [Message] = []
        for mc in arrManagedObject {
            let message = Message()
            message.userId = mc.valueForKey(ChatKeys.userId.rawValue) as? String
            message.message = mc.valueForKey(ChatKeys.message.rawValue) as? String
            message.adminId = mc.valueForKey(ChatKeys.adminId.rawValue) as? String
            message.type = mc.valueForKey(ChatKeys.type.rawValue) as? String
            message.status =  ChatStatus(rawValue: (mc.valueForKey(ChatKeys.type.rawValue) as? String) ?? "") ?? .None
            arrCart.append(message)
        }
        
        return arrCart
    }
    
    func saveMessges(messages : [Message]?) {
        
        guard let arrMessages = messages else { return }
        
        for message in arrMessages {
            self.saveMessageToCart(CoreDataEntity.Message.rawValue, message: message)
        }
    }
    
    func saveMessageToCart(entityName : String,message : Message?){
        
        guard let entity = NSEntityDescription.entityForName(entityName, inManagedObjectContext: (coreDataStack.managedObjectContext)),tempMessage = message else {
            fatalError("Could not find entity descriptions!")
        }
        
        coreDataStack.managedObjectContext.mergePolicy = NSErrorMergePolicy
        let currentMessage = NSManagedObject(entity: entity, insertIntoManagedObjectContext: coreDataStack.managedObjectContext)
        currentMessage.setValue(tempMessage.userId, forKey: ChatKeys.userId.rawValue)
        currentMessage.setValue(tempMessage.adminId, forKey: ChatKeys.adminId.rawValue)
        currentMessage.setValue(tempMessage.message, forKey: ChatKeys.message.rawValue)
        currentMessage.setValue(tempMessage.status.rawValue, forKey: ChatKeys.status.rawValue)
        currentMessage.setValue(tempMessage.type, forKey: ChatKeys.type.rawValue)
        coreDataStack.saveMainContext()
    }
}
