//
//  HomeProductCell.swift
//  Clikat
//
//  Created by Night Reaper on 20/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material
import AMRatingControl

class HomeProductCell: MaterialCollectionViewCell {
    
    @IBOutlet weak var labelReview : UILabel!
    @IBOutlet weak var viewRating : UIView!{
        didSet{
            rateControl.rating = 0
            rateControl.starWidthAndHeight = 12
            viewRating.addSubview(rateControl)
        }
    }
    @IBOutlet weak var labelSupplierName : UILabel!
    @IBOutlet weak var stackSupplier : UIStackView!
    
    @IBOutlet var imgProduct: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblCompany: UILabel!
    @IBOutlet var lblOfferPrice: UILabel!
    @IBOutlet var lblEmptyPrice: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var viewHolderPrice: UIStackView!
    @IBOutlet weak var labelAddress: UILabel!
    
    let rateControl = AMRatingControl(location: CGPoint(x: 0,y: 0), emptyImage: UIImage(asset : Asset.Ic_star_small_grey), solidImage: UIImage(asset : Asset.Ic_star_small_yellow), andMaxRating: 5)
    var product : Product? {
        didSet{
            updateProductUI()
        }
    }
    
    var supplier : Supplier? {
        didSet{
            updateSupplierUI()
        }
    }
    
    private func updateProductUI(){
        stackSupplier.hidden = true
        defer{
            lblTitle?.text = product?.name
            lblCompany?.text = product?.measuringUnit
            lblPrice?.text = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string , product?.getDisplayPrice(1.0)])
            let offerPrice = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string , product?.offerPrice])
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: offerPrice)
            attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
            viewHolderPrice?.hidden = false
            lblOfferPrice?.attributedText = attributeString
            labelAddress?.text = ""
        }
        

        
        guard let image = product?.image  else{
            return
        }
        imgProduct?.yy_setImageWithURL( NSURL(string: image), options: [.SetImageWithFadeAnimation])
        
    }
    
    private func updateSupplierUI(){
        stackSupplier.hidden = false
        defer{
            lblTitle?.text = supplier?.name
            rateControl?.rating = supplier?.rating?.toInt() ?? 0
            labelSupplierName?.text = supplier?.name
            labelReview.text = " ⋅ " + (supplier?.totalReviews ?? "0") + L10n.Reviews.string
            lblCompany?.text = ""
            lblPrice?.hidden = true
            viewHolderPrice?.hidden = true
            labelAddress?.text = supplier?.address
        }
        
        guard let image = supplier?.logo  else{
            return
        }
        imgProduct?.yy_setImageWithURL(NSURL(string: image), options: [ .SetImageWithFadeAnimation])
    }
}
