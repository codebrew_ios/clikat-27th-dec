//
//  ProductCollectionCell.swift
//  Clikat
//
//  Created by cblmacmini on 4/29/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material

class ProductCollectionCell: MaterialCollectionViewCell {
    
    
    @IBOutlet weak var imgFav: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var stepper: GMStepper!
    
    var product : Product?{
        didSet{
            updateUI()
            stepper.associatedProduct = product
        }
    }
    
    
    private func updateUI(){
        
        defer{
            lblTitle?.text = product?.name
            lblPrice?.text = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string ,product?.displayPrice])
             guard let value = Double(product?.quantity ?? "0") else { fatalError("Cart Quantity is nil") }
             stepper.value = value
        }
        guard let image = product?.image , url = NSURL(string: image) else{
            return
        }
        imgFav?.yy_setImageWithURL(url, options: [.SetImageWithFadeAnimation])
    }
    
}

//MARK: - ButtonActions

extension ProductCollectionCell {
    
    
    @IBAction func actionRemoveProduct(sender: UIButton) {
        
    }
    
    @IBAction func actionAddProduct(sender: UIButton) {
    
    }
}
