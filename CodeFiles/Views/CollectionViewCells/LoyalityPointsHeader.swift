//
//  LoyalityPointsHeader.swift
//  Clikat
//
//  Created by Night Reaper on 25/05/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class LoyalityPointsHeader: UICollectionReusableView {
  
    @IBOutlet weak var lblLoyalityPoints : UILabel!
}
