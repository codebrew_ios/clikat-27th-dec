//
//  ServiceTypeCollectionCell.swift
//  Clikat
//
//  Created by Night Reaper on 19/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material

class ServiceTypeCell: MaterialCollectionViewCell {
    
    var service : ServiceType?{
        didSet{
            updateUI()
        }
    }
    
    @IBOutlet var imgSerivce: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    
    
    
    private func updateUI(){
        
        defer{
            lblTitle?.text = service?.name ?? ""
        }
        guard let image = service?.icon , url = NSURL(string: image) else{
            return
        }
        imgSerivce?.yy_setImageWithURL(url, options: [.SetImageWithFadeAnimation])
    }
    
}
