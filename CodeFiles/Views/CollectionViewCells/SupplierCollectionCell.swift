//
//  SupplierCollectionCell.swift
//  Clikat
//
//  Created by cblmacmini on 5/30/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import AMRatingControl
class SupplierCollectionCell: BasePageCollectionCell {

    var supplier : Supplier?{
        didSet{
            updateUI()
        }
    }
    
    
    @IBOutlet weak var labelTitle: UILabel!{
        didSet{
            labelTitle.layer.shadowRadius = 2
            labelTitle.layer.shadowOffset = CGSize(width: 0, height: 3)
            labelTitle.layer.shadowOpacity = 0.2
        }
    }
    @IBOutlet weak var imageViewSupplier: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var viewRating: UIView!{
        didSet{
            rateControl.rating = 0
            rateControl.starWidthAndHeight = 20
            rateControl.userInteractionEnabled = false
            viewRating.addSubview(rateControl)
        }
    }
    @IBOutlet weak var imageListView: UIView!{
        didSet{
            
        }
    }
    @IBOutlet weak var labelTitleBackground: UIView!{
        didSet{
            let gradientLayer = CAGradientLayer()
            gradientLayer.frame = labelTitleBackground.bounds
            
            gradientLayer.colors = [UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.5).CGColor,UIColor.clearColor().CGColor]
            labelTitleBackground.layer.insertSublayer(gradientLayer, atIndex: 0)
        }
    }

    let rateControl = AMRatingControl(location: CGPoint(x: 0,y: 0), emptyImage: UIImage(asset : Asset.Ic_star_small_grey), solidImage: UIImage(asset : Asset.Ic_star_small_yellow), andMaxRating: 5)
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func updateUI(){
        
        defer {
            labelName.text = supplier?.name
            labelAddress.text = supplier?.address
            labelTitle.text = supplier?.name
            configureImageListView()
        }
        
        guard let rating = Int(supplier?.rating ?? "0"),image = supplier?.logo,imageUrl = NSURL(string:image) else { return }
        rateControl.rating = rating
        imageViewSupplier.yy_setImageWithURL(imageUrl, placeholder: UIImage(asset: Asset.Placeholder), options: .SetImageWithFadeAnimation) { (image, url, form, stage, error) in
            
        }
    }
    
    
    func configureImageListView(){
      
        let expandable = AZExpandableIconListView(frame: imageListView.bounds, imageUrls: supplier?.supplierImages ?? [])
        imageListView.addSubview(expandable)
    }
}
