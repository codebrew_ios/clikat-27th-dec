//
//  FavoritesCell.swift
//  Clikat
//
//  Created by Night Reaper on 26/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material

class FavoritesCell: MaterialCollectionViewCell {
    
    var supplier : Supplier?{
        didSet{
            updateUI()
        }
    }
    
    @IBOutlet var imgFav: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var viewBg: UIView!
    
    private func updateUI(){
        
        defer{
            lblTitle?.text = supplier?.name
            lblPrice?.text = supplier?.address
        }
        
        guard let image = supplier?.logo, url = NSURL(string: image) else{
            return
        }
        imgFav?.yy_setImageWithURL(url, options: [.SetImageWithFadeAnimation])
    }
    
}
