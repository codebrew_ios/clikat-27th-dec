//
//  BannerCell.swift
//  Clikat
//
//  Created by Night Reaper on 20/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material

class BannerCell: MaterialCollectionViewCell {
    
    @IBOutlet var imgBanner: UIImageView!
    
    var banner : Banner?{
        didSet{
            updateUI()
        }
    }
    
    private func updateUI(){
        
        guard let image = banner?.image else{
            return
        }
        imgBanner?.yy_setImageWithURL(NSURL(string: image), options: [.SetImageWithFadeAnimation])
        
    }
    
}
