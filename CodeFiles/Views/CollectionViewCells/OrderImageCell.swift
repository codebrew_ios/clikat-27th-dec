//
//  OrderImageCell.swift
//  Clikat
//
//  Created by cbl73 on 4/23/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material

class OrderImageCell: MaterialCollectionViewCell {

    
    var product : Product? {
        didSet{
            updateUI()
        }
    }
    
    @IBOutlet weak var imgOrder: UIImageView!

    
    private func updateUI(){
        
        guard let image = product?.image , url = NSURL(string: image) else{
            return
        }
        imgOrder?.yy_setImageWithURL(url, options: [.SetImageWithFadeAnimation])
        
    }
    

}
