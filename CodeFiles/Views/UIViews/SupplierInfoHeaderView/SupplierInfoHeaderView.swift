//
//  SupplierInfoHeaderView.swift
//  Clikat
//
//  Created by cblmacmini on 5/3/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class SupplierInfoHeaderView: UIView {
    
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var imageViewSupplier : UIImageView!
    var imageDelegate : ImageClickListenerDelegate?

    @IBOutlet weak var pageControl: UIPageControl!{
        didSet{
            pageControl.transform = CGAffineTransformMakeScale(0.8, 0.8)
        }
    }
    
    var supplier : Supplier?{
        didSet{
            configureCollectionView()
        }
    }
    
    var collectionDataSource = SupplierInfoHeaderDataSource()
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        xibSetup()
        collectionView.registerNib(UINib(nibName: CellIdentifiers.SupplierInfoHeaderCollectionCell,bundle: nil), forCellWithReuseIdentifier: CellIdentifiers.SupplierInfoHeaderCollectionCell)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    var view: UIView!
    
    func xibSetup() {
        
        do{
            try view = loadViewFromNib(withIdentifier: CellIdentifiers.SupplierInfoHeaderView)
            view.frame = bounds
            view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
            addSubview(view)
        }
        catch let exception{
            print(exception)
        }
        
        
    }
  
    func configureCollectionView(){
        imageViewSupplier.yy_setImageWithURL(NSURL(string: supplier?.logo ?? ""), options: .SetImageWithFadeAnimation)
        let pages = supplier?.supplierImages?.count ?? 0
        pageControl?.numberOfPages = 0
        weak var weakSelf = self
        
        guard let images = supplier?.supplierImages else{
            return
        }
        
        collectionDataSource = SupplierInfoHeaderDataSource(items: supplier?.supplierImages, tableView: collectionView, cellIdentifier: CellIdentifiers.SupplierInfoHeaderCollectionCell, headerIdentifier: nil, cellHeight: collectionView.frame.size.height, cellWidth: ScreenSize.SCREEN_WIDTH, configureCellBlock: { (cell, item) in
            guard let c = cell as? SupplierInfoHeaderCollectionCell,imageUrl = item as? String else{ return }
            c.imageViewCover.yy_setImageWithURL(NSURL(string: imageUrl), options: .SetImageWithFadeAnimation)
            }, aRowSelectedListener: { [weak self] (indexPath) in
                
                self?.imageDelegate?.imageCliked(atIndexPath: indexPath, cell: weakSelf?.collectionView?.cellForItemAtIndexPath(indexPath), images: weakSelf?.imagesToSKPhotoArray(withImages: images, caption: nil) ?? [])

                
            }, scrollViewListener: { (scrollView) in
                weakSelf?.configurePageControl(scrollView)
        })
        
        collectionView.delegate = collectionDataSource
        collectionView.dataSource = collectionDataSource
    }
}

extension SupplierInfoHeaderView {
    
    func configurePageControl(scrollView : UIScrollView){
        
        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
        let visiblePoint = CGPoint(x: CGRectGetMidX(visibleRect), y: CGRectGetMidY(visibleRect))
        let visibleIndexPath = collectionView.indexPathForItemAtPoint(visiblePoint)
        pageControl.currentPage = visibleIndexPath?.row ?? 0
    }
}

class SupplierInfoHeaderDataSource : CollectionViewDataSource {
    
    typealias ScrolViewDidScroll = (UIScrollView) -> ()
    
    var scrollViewListener : ScrolViewDidScroll?
    
    init(items : Array<AnyObject>?  , tableView : UICollectionView? , cellIdentifier : String? , headerIdentifier : String? , cellHeight : CGFloat , cellWidth : CGFloat  , configureCellBlock : ListCellConfigureBlock  , aRowSelectedListener : DidSelectedRow,scrollViewListener : ScrolViewDidScroll) {
        self.scrollViewListener = scrollViewListener
        super.init(items: items, tableView: tableView, cellIdentifier: cellIdentifier, headerIdentifier: headerIdentifier, cellHeight: cellHeight, cellWidth: cellWidth, configureCellBlock: configureCellBlock, aRowSelectedListener: aRowSelectedListener)
        
    }
    override init() {
        self.scrollViewListener = nil
        super.init()
    }
    
    override func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: cellWidth, height: collectionView.frame.height)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        guard let block = scrollViewListener else { return }
        block(scrollView)
    }
}
