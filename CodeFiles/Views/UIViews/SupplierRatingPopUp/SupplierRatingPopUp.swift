//
//  SupplierRatingPopUp.swift
//  Clikat
//
//  Created by cblmacmini on 5/27/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import SZTextView
import AMRatingControl
import Material

typealias SupplierRatingSuccessBlock = (rating : Int?,comment : String?) -> ()
class SupplierRatingPopUp: UIView {

    var view: UIView!
    var presentingViewController : UIViewController?
    
    @IBOutlet weak var viewRating: UIView!{
        didSet{
            rateControl.rating = 0
            rateControl.starWidthAndHeight = (DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS ) ? 28 : 40
            rateControl.starSpacing = UInt(HighPadding)
            viewRating.addSubview(rateControl)
        }
    }
    @IBOutlet weak var viewOverlay: UIView!
    @IBOutlet weak var viewContainer: MaterialView!
    @IBOutlet weak var tvComment: SZTextView!
    @IBOutlet weak var labelSupplierName: UILabel!{
        didSet{
            labelSupplierName?.text = ""
        }
    }
    
    var supplier : Supplier?{
        didSet{
            updateUI()
        }
    }
    var okClicked : SupplierRatingSuccessBlock?
    
    let rateControl = AMRatingControl(location: CGPoint(x: 0,y: 0), emptyImage: UIImage(asset : Asset.Ic_star_big_grey), solidImage: UIImage(asset : Asset.Ic_star_big_yellow), andMaxRating: 5)
    
    init(frame: CGRect,fromViewController : UIViewController?,supplier : Supplier?) {
        super.init(frame: frame)
        presentingViewController = fromViewController
        self.supplier = supplier
    }
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    
    func xibSetup() {
        
        do {
            view = try loadViewFromNib(withIdentifier: CellIdentifiers.SupplierRatingPopUp)
            view.frame = bounds
            view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
            addSubview(view)
        }
        catch let exception{
            print(exception)
        }
    }
    
    func updateUI(){
        guard let currentSupplier = supplier else { return }
        
        labelSupplierName.text = currentSupplier.name
        self.rateControl.rating = supplier?.myReview?.rating ?? 0
        self.tvComment.text = supplier?.myReview?.comment ?? ""
    }
    
}
//MARK: - Present and Dismiss
extension SupplierRatingPopUp {
    
    func presentRatingView(okClickListener : SupplierRatingSuccessBlock?){
        self.okClicked = okClickListener
        viewOverlay?.alpha = 0.0
        viewContainer?.transform = CGAffineTransformMakeTranslation(0, frame.size.height)
        UIView.animateWithDuration(0.5, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.6, options: .CurveEaseIn, animations: {
            weak var weakSelf = self
        weakSelf?.viewContainer?.transform = CGAffineTransformIdentity
            weakSelf?.viewOverlay?.alpha = 0.4
            }) { (completed) in
               
        }
    }
    func dismissRatingView(){
        UIView.animateWithDuration(0.4, animations: {
            weak var weakSelf = self
            weakSelf?.viewOverlay?.alpha = 0.4
            weakSelf?.viewContainer?.transform = CGAffineTransformMakeTranslation(0, weakSelf?.frame.size.height ?? 0)
            }) { (completed) in
                weak var weakSelf = self
                weakSelf?.removeFromSuperview()
        }
    }
}

//MARK: - Button Action
extension SupplierRatingPopUp {
    
    @IBAction func actionCancel(sender: UIButton) {
        dismissRatingView()
    }
    
    @IBAction func actionOk(sender: UIButton) {
        guard let block = okClicked else { return }
        block(rating: rateControl.rating, comment: tvComment.text)
    }
}

