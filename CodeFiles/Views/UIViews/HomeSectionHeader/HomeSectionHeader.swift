//
//  HomeSectionHeader.swift
//  Clikat
//
//  Created by cblmacmini on 4/23/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class HomeSectionHeader: UIView {

    @IBOutlet weak var labelTitle: UILabel!
    var view: UIView!

    @IBOutlet weak var btnViewAll : UIButton!
    typealias actionViewAllListener = (sender : UIButton?) -> ()
    var actionViewAll : actionViewAllListener?
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    
    func xibSetup() {
        
        do {
            view = try loadViewFromNib(withIdentifier: CellIdentifiers.HomeSectionHeader)
            view.frame = bounds
            view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
            addSubview(view)
        }
        catch let exception{
            print(exception)
        }
    }
    
   
}


extension HomeSectionHeader{
    
    
    @IBAction func actionBtnViewAll(sender: UIButton) {
        
        guard let block = actionViewAll else { return }
        block(sender: sender)
    }
    
}