//
//  CartButton.swift
//  Clikat
//
//  Created by cblmacmini on 5/15/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class CartButton: UIButton {
    
    let badgeView = M13BadgeView(frame: CGRect(x: 0, y: MidPadding, w: 20, h: 20))
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        badgeView.animateChanges = false
        badgeView.badgeBackgroundColor = Colors.MainColor.color()
        badgeView.hidesWhenZero = true
        badgeView.horizontalAlignment = M13BadgeViewHorizontalAlignmentRight
        badgeView.font = UIFont(name: Fonts.ProximaNova.Light, size: Size.Small.rawValue)
        badgeView.shadowBadge = true
        addSubview(badgeView)
        handleCartQuantity(NSNotification(name: "", object: nil))
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CartButton.handleCartQuantity(_:)), name: CartNotification, object: nil)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func handleCartQuantity(sender : NSNotification){
        
        
        DBManager.sharedManager.getCart { (products) in
            weak var weakSelf = self
            var badgeCount = 0
            
            defer {
                UIApplication.sharedApplication().cancelAllLocalNotifications()
                if products.count == 0 {
                    GDataSingleton.sharedInstance.currentSupplierId = nil
                    GDataSingleton.sharedInstance.currentCategoryId = nil
                }else {
                    
                    self.createLocalNotification()
                }
            }
            for product in products {
                guard let currentProduct = product as? Cart,quantity = currentProduct.quantity else { continue }
                let quant = quantity == "-1" ? "1" : quantity
                badgeCount += Int(quant) ?? 0
            }
            weakSelf?.badgeView.text = "0"
            weakSelf?.badgeView.text = String(badgeCount < 0 ? 0 : badgeCount)
            
        }
        //        guard let badgeValue = sender.userInfo?["badge"] as? Int else { return }
        //        badgeView.text = String(badgeValue)
    }
    
    func createLocalNotification(){
        
        let localNotification = UILocalNotification()
        //7200
        localNotification.fireDate = NSDate(timeIntervalSinceNow: 7200)
        localNotification.timeZone = NSTimeZone.localTimeZone()
        localNotification.alertBody = L10n.HaveYouForgotCompletingYourLastShoppingCart.string
        localNotification.alertAction = "View"
        localNotification.soundName = UILocalNotificationDefaultSoundName
        localNotification.userInfo = ["UUID" : ""]
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
    }
}
