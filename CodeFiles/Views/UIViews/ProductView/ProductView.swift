//
//  ProductView.swift
//  Clikat
//
//  Created by cblmacmini on 5/2/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class ProductView: UIView {

    @IBOutlet weak var imageViewProduct: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelProductmodel: UILabel!
    @IBOutlet weak var labelProductCode: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelQuantity: UILabel!
    
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    var view: UIView!
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: CellIdentifiers.ProductView , bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
    
    func configureProductView(product : Product?){
        
        defer{
            labelTitle.text = product?.name
            labelProductmodel?.text = product?.measuringUnit
            labelPrice.text = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string ,product?.price])
            labelQuantity?.text = UtilityFunctions.appendOptionalStrings(withArray: [L10n.Quantity.string,product?.quantity])
        }

        guard let image = product?.image , url = NSURL(string : image) else{
            return
        }
        imageViewProduct.yy_setImageWithURL(url, options: .SetImageWithFadeAnimation)
    }
}
