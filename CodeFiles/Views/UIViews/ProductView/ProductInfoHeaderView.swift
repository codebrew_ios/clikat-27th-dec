//
//  ProductInfoHeaderView.swift
//  Clikat
//
//  Created by cbl73 on 5/6/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import SKPhotoBrowser

protocol ImageClickListenerDelegate {
    
    func imageCliked(atIndexPath indexPath : NSIndexPath , cell : UICollectionViewCell?, images : [SKPhoto])
    
}

class ProductInfoHeaderView: UIView {

    @IBOutlet weak var collectionView: UICollectionView!
    var imageDelegate : ImageClickListenerDelegate?
    
    
    var collectionDataSource = ProductDetailHeaderDataSource()
    var view: UIView!

    var product : Product?{
        didSet{
            configureCollectionView()
        }
    }
  
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        xibSetup()
        collectionView.registerNib(UINib(nibName: CellIdentifiers.SupplierInfoHeaderCollectionCell,bundle: nil), forCellWithReuseIdentifier: CellIdentifiers.SupplierInfoHeaderCollectionCell)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    
    func xibSetup() {
       
        do{
            try view = loadViewFromNib(withIdentifier: CellIdentifiers.ProductInfoHeaderView)
            view.frame = bounds
            view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
            addSubview(view)
        }
        catch let exception{
            print(exception)
        }
        
  
    }
    
  
    func configureCollectionView(){
        weak var weakSelf : ProductInfoHeaderView? = self
        guard let images = product?.images else{
            return
        }
        collectionDataSource = ProductDetailHeaderDataSource(items: images, tableView: collectionView, cellIdentifier: CellIdentifiers.SupplierInfoHeaderCollectionCell, headerIdentifier: nil, cellHeight: CGRectGetHeight(collectionView.frame), cellWidth: ScreenSize.SCREEN_WIDTH, configureCellBlock: { (cell, item) in
            guard let c = cell as? SupplierInfoHeaderCollectionCell , images = weakSelf?.product?.images where images.count > 0  else{ return }
            c.imageViewCover.yy_setImageWithURL(NSURL(string: /(item as? String) ), options: .SetImageWithFadeAnimation)
            c.imageViewCover.contentMode = .ScaleAspectFit
            }, aRowSelectedListener: { (indexPath) in
                
                weak var weakSelf : ProductInfoHeaderView? = self
                weakSelf?.imageDelegate?.imageCliked(atIndexPath: indexPath , cell: weakSelf?.collectionView?.cellForItemAtIndexPath(indexPath) ,images: weakSelf?.imagesToSKPhotoArray(withImages: images, caption: weakSelf?.product?.name) ?? [])
                
        })

        collectionView.delegate = collectionDataSource
        collectionView.dataSource = collectionDataSource
    }
}


extension UIView {
    
    
    func imagesToSKPhotoArray (withImages images : [String] , caption : String?) -> [SKPhoto]{
        
        var skImages : [SKPhoto] = []
        for image in images {
            let photo = SKPhoto.photoWithImageURL(image)
            photo.caption = caption ?? ""
            photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
            skImages.append(photo)
        }
        return skImages
        
    }
}



class ProductDetailHeaderDataSource : CollectionViewDataSource {
    
    override func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: cellWidth, height: collectionView.frame.height)
    }
    
    override func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        
    }
}