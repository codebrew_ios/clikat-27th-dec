//
//  ProductDetailFirstCell.swift
//  Clikat
//
//  Created by cbl73 on 5/6/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class ProductDetailFirstCell: UITableViewCell {

    var product : Product?{
        didSet{
            updateUI()
            stepper.associatedProduct = product
        }
    }
    
    @IBOutlet weak var labelOffer: UILabel!
    @IBOutlet weak var labelMeasuringUnit: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblPrice: UILabel! 
    @IBOutlet weak var stepper: GMStepper!

    
    private  func updateUI(){
    
        lblTitle?.text = product?.name
        lblPrice?.text = UtilityFunctions.appendOptionalStrings(withArray: [ L10n.AED.string , product?.getDisplayPrice(product?.quantity?.toDouble())])
        labelMeasuringUnit?.text = product?.measuringUnit == "0" ? "" : product?.measuringUnit
        if let offer = product?.isOffer {
            labelOffer.hidden = !offer
        }
        labelOffer?.text = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string,product?.offerPrice])
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: labelOffer?.text ?? "")
        attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
        labelOffer?.attributedText = attributeString
        stepper.stepperValueListener = {[weak self] (value) in
            
            self?.lblPrice?.text = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string,self?.product?.getDisplayPrice(value)])
        }

        guard let value = Double(product?.quantity ?? "0") else { return }
        stepper.value = value
        
    }
    
}
