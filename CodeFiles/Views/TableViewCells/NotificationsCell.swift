//
//  NotificationsCell.swift
//  Clikat
//
//  Created by Night Reaper on 26/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material

class NotificationsCell: MaterialTableViewCell {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var imgView: UIImageView!
    
    var notification : Notification?{
        didSet{
        updateUI()
        }
    }
    func updateUI(){
        lblTitle.text = notification?.message
        guard let image = notification?.image, imageUrl = NSURL(string: image) else { return }
        imgView.yy_setImageWithURL(imageUrl, options: .SetImageWithFadeAnimation)
    }
}
