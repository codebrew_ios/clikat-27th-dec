//
//  OrderDetailCell.swift
//  Clikat
//
//  Created by cblmacmini on 5/2/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class OrderDetailCell: UITableViewCell {

    @IBOutlet var lblPlacedOn: UILabel!
    @IBOutlet var lblDeliveredOn: UILabel!
    @IBOutlet var lblOrderNo: UILabel!
    @IBOutlet var lblNetAmt: UILabel!
    @IBOutlet var lblPaymentMethod: UILabel!

    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblAddressLine1: UILabel!
    @IBOutlet var lblAddressLine2: UILabel!
    @IBOutlet weak var labelDeliveredOn: UILabel!

    var cellType : OrderCellType = .OrderUpcoming
    var orderDetails : OrderDetails? {
        didSet{
            updateUI()
        }
    }
    
    
    private func updateUI(){
        
        lblPlacedOn?.text = orderDetails?.createdOn
        
        
        switch cellType{
        case .OrderHistory,.RateOrder:
           lblDeliveredOn?.text = orderDetails?.deliveredOn
        case .OrderTracking,.OrderScheduled,.OrderUpcoming,.LoyaltyPoints:
           lblDeliveredOn?.text = orderDetails?.serviceDate
        }
        
        lblOrderNo?.text = orderDetails?.orderId
        lblNetAmt?.text = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string , orderDetails?.netAmount])
        lblPaymentMethod?.text = orderDetails?.paymentType.paymentMethodString()
        
        lblName?.text = orderDetails?.deliveryAddress?.name
        let deliveryAddress = orderDetails?.deliveryAddress
        lblAddressLine1?.text = UtilityFunctions.appendOptionalStrings(withArray: [deliveryAddress?.landMark,deliveryAddress?.address,deliveryAddress?.houseNo], separatorString: " ")
        if orderDetails?.status == .Delivered {
            labelDeliveredOn?.text = L10n.DeliveredOn.string
        }
        lblAddressLine2?.text = UtilityFunctions.appendOptionalStrings(withArray: [deliveryAddress?.buildingName,deliveryAddress?.city,deliveryAddress?.country], separatorString: " ")
    }
}
