//
//  HomeSearchCell.swift
//  Clikat
//
//  Created by cbl20 on 9/30/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material

class HomeSearchCell: UITableViewCell {

    typealias ActionBarCode = () -> ()
    typealias ActionSerach = (searchKey : String?) -> ()
    
    @IBOutlet weak var tfSearch: UITextField!{
        didSet{
            tfSearch?.attributedPlaceholder = NSAttributedString(string:L10n.SearchForProduct.string, attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
            tfSearch?.textAlignment = Localize.currentLanguage() == Languages.Arabic ? .Right : .Left
        }
    }
    @IBOutlet weak var bgView: MaterialView!{
        didSet{
            bgView?.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.25)
        }
    }
    var actionSearch : ActionSerach?
    var actionBarcode : ActionBarCode?
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

    @IBAction func actionBarcodeSearch(sender: UIButton) {
        guard let block = actionBarcode else { return }
        block()
    }
    @IBAction func actionSearch(sender: UIButton) {
        
        guard let block = actionSearch else { return }
        block(searchKey: tfSearch?.text)
    }
}
