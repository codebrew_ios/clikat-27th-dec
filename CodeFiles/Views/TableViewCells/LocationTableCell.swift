//
//  LocationTableCell.swift
//  Clikat
//
//  Created by cblmacmini on 5/16/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material

class LocationTableCell: MaterialTableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
