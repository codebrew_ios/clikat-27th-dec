//
//  ServiceTypeParentCell.swift
//  Clikat
//
//  Created by Night Reaper on 19/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit


protocol ServiceTypeDelegate {
    func categoryClicked(atIndexPath indexPath : NSIndexPath , service : ServiceType?)
}

class ServiceTypeParentCell: UITableViewCell {

    @IBOutlet var collectionView: UICollectionView!
    var delegate : ServiceTypeDelegate?
    
    let numberOfColumns = 3
    
    var collectionViewDataSource = CollectionViewDataSource(){
        didSet{
            collectionViewDataSource.isServiceTypeCollectionView = true
            collectionView.dataSource = collectionViewDataSource
            collectionView.delegate = collectionViewDataSource
        }
    }
    var serviceTypes : [ServiceType]?{
        didSet{
            updateUI()
        }
    }
    
    private func updateUI(){
            // Reload Collection View
        
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let HorizontalEmptySpace = floor(CGFloat(numberOfColumns - 1) * LowPadding + layout.sectionInset.left + layout.sectionInset.right)
        let width = floor((ScreenSize.SCREEN_WIDTH - HorizontalEmptySpace) / CGFloat(numberOfColumns))
        
        collectionViewDataSource = CollectionViewDataSource(items: serviceTypes, tableView: collectionView, cellIdentifier: CellIdentifiers.ServiceTypeCell , headerIdentifier: nil, cellHeight: width, cellWidth: width, configureCellBlock: {
            (cell, item) in
            (cell as? ServiceTypeCell)?.service = item as? ServiceType
            }, aRowSelectedListener: { (indexPath) in
                weak var weakSelf : ServiceTypeParentCell? = self
                weakSelf?.delegate?.categoryClicked(atIndexPath: indexPath , service : weakSelf?.serviceTypes?[indexPath.row])
        })
        
//        collectionView.reloadData()
    }

}



