//
//  CartBillCell.swift
//  Clikat
//
//  Created by cblmacmini on 5/11/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import SZTextView

class CartBillCell: UITableViewCell {
    
    @IBOutlet weak var labelHandlingCharges: UILabel!
    @IBOutlet weak var labelTotalPrice : UILabel!
    @IBOutlet weak var labelDeliveryCharges : UILabel!
    @IBOutlet weak var labelSubTotal: UILabel!
    @IBOutlet weak var labelNetTotal: UILabel!
    @IBOutlet weak var labelSubTotalHeading : UILabel!
    
    @IBOutlet weak var tvRemarks : SZTextView!
    
    var cart : [Cart]?{
        didSet{
            updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension CartBillCell {
    
    func updateUI(){
        labelSubTotalHeading?.text = L10n.SubTotal.string
        guard let arrCart = cart else { return }
        

        let totalPrice : Double = arrCart.reduce(0.0, combine: { (initial, cart) -> Double in
            let price = cart.getPrice(cart.quantity?.toDouble())?.toDouble() ?? 0.0
            let quantity = cart.quantity?.toDouble() ?? 0.0
            return initial + ((price) * (quantity < 0.0 ? 1 : quantity))
        })
        
        
        let deliveryCharges : Double = arrCart.reduce(0.0, combine: { (initial, cart) -> Double in
            let delivery = cart.deliveryCharges?.toDouble() ?? 0.0
            return delivery > initial ? delivery : initial
        })
        
        
        labelTotalPrice.text = UtilityFunctions.appendOptionalStrings(withArray: [ L10n.AED.string , totalPrice.toString])
        labelDeliveryCharges.text =  UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string ,deliveryCharges.toString])
        guard let maxSupplier = Double(Cart.getMaxSupplier(arrCart) ?? "0"), maxAdmin = Double(Cart.getMaxAdmin(arrCart) ?? "0") else { return }
        
        let handlingCharges = maxSupplier + maxAdmin
        
        labelNetTotal?.text = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string,(totalPrice + deliveryCharges + handlingCharges).toString])
        labelSubTotal?.text = labelNetTotal?.text
        labelHandlingCharges?.text = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string ,handlingCharges.toString])
        
    }
}


//MARK: - TextViewDelegate

extension CartBillCell : UITextViewDelegate {
}