//
//  BannerParentCell.swift
//  Clikat
//
//  Created by Night Reaper on 19/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
//import RCPageControl

typealias BannerClickListener = (banner : Banner?) -> ()

class BannerParentCell: UITableViewCell {

    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet weak var viewPgControl: UIView!
    var pageControl : UIPageControl?
    var collectionViewDataSource = CollectionViewDataSource(){
        didSet{
            collectionView.dataSource = collectionViewDataSource
            collectionView.delegate = collectionViewDataSource
        }
    }
    var banners : [Banner]?{
        didSet{
            updateUI()
        }
    }

    var bannerClickedBlock : BannerClickListener?
    var timer = NSTimer()
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func addPageControl(){
        if pageControl == nil {
            pageControl = UIPageControl(frame: viewPgControl.bounds)
        }
        pageControl?.backgroundColor = UIColor.clearColor()
        pageControl?.pageIndicatorTintColor = UIColor.lightGrayColor()
        pageControl?.currentPageIndicatorTintColor = Colors.MainColor.color()
//        pageControl?.currentPageIndexTextTintColor = Colors.MainColor.color()
        viewPgControl.addSubview(pageControl!)
    }
    
    private func updateUI(){
        // Reload Collection View
        
        pageControl?.numberOfPages = banners?.count > 5 ? 5 : banners?.count ?? 0
        pageControl?.hidden = banners?.count > 5 ? true : false
        let width = CGRectGetWidth(bounds)
        let height = floor(width * 0.6)
        collectionViewDataSource = CollectionViewDataSource(items: banners, tableView: collectionView, cellIdentifier: CellIdentifiers.BannerCell , headerIdentifier: nil, cellHeight: height , cellWidth: width , configureCellBlock: {
            (cell, item) in
            (cell as? BannerCell)?.banner = item as? Banner
            }, aRowSelectedListener: { [weak self] (indexPath) in
                guard let block = self?.bannerClickedBlock else { return }
                block(banner: self?.banners?[indexPath.row])
        })
        if banners?.count > 1 {
            removeTimer()
            addTimer()
        }
        collectionViewDataSource.willBeginDraggingListener = { [weak self] (scrollview) in
            self?.removeTimer()
        }
        collectionViewDataSource.didEndDraggingListener = { [weak self] (scrollview) in
            self?.addTimer()
        }
        collectionViewDataSource.willDisplayCellListener = { (cell) in
            
        }
        collectionViewDataSource.didEndDeceleratingBlock = { [weak self] (scrollView) in
            
            if !scrollView.dragging {
                self?.pageControl?.currentPage = self?.collectionView.indexPathsForVisibleItems().first?.row ?? 1
            }
        }

    }

}

protocol StringType { var get: String { get } }
extension String: StringType { var get: String { return self } }

extension Optional where Wrapped: StringType{
    func unwrap() -> String {
        return self?.get ?? ""
    }
}
//MARK: - Automatic Scrolling collectionView
extension BannerParentCell {
  
    func addTimer(){
        timer = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: #selector(BannerParentCell.nextPage), userInfo: nil, repeats: true)
        NSRunLoop.mainRunLoop().addTimer(timer, forMode: NSRunLoopCommonModes)
    }
    
    func resetIndexpath() -> NSIndexPath{
        let currentIndexPath = collectionView.indexPathsForVisibleItems().last
        let currentIndexpathReset = NSIndexPath(forItem: currentIndexPath?.item ?? 0, inSection: 0)
        
//        collectionView.scrollToItemAtIndexPath(currentIndexpathReset, atScrollPosition: .CenteredHorizontally, animated: true)
        return currentIndexpathReset
    }
    
    func nextPage(){
        let currentIndexPathReset = resetIndexpath()
    
        var nextItem = currentIndexPathReset.item + 1
        var nextSection = currentIndexPathReset.section
        if (nextItem == self.banners?.count) {
            nextItem = 0
            nextSection = 0
        }
        let nextIndexPath = NSIndexPath(forItem: nextItem, inSection: nextSection)
        pageControl?.currentPage = nextItem
        collectionView.scrollToItemAtIndexPath(nextIndexPath, atScrollPosition: .CenteredHorizontally, animated: true)
    }
    
    func removeTimer(){
        timer.invalidate()
    }

}
