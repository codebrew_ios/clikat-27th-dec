//
//  HomeProductParentCell.swift
//  Clikat
//
//  Created by Night Reaper on 19/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit


protocol HomeProductCollectionViewDelegate : class {
    func collectionViewItemClicked(atIndexPath indexPath : NSIndexPath , type : AnyObject?)
}

class HomeProductParentCell: UITableViewCell {

    @IBOutlet var collectionView: UICollectionView!
    weak var delegate : HomeProductCollectionViewDelegate?
    
    var collectionViewDataSource = CollectionViewDataSource(){
        didSet{
            collectionView.dataSource = collectionViewDataSource
            collectionView.delegate = collectionViewDataSource
        }
    }
    var products : [Product]?{
        didSet{
            updateUI(withContent: products)
        }
    }
    
    
    var suppliers : [Supplier]? {
        didSet{
            updateUI(withContent: suppliers)
        }
    }

    private func updateUI(withContent content : [AnyObject]?){
        // Reload Collection View
        
        let width = CGRectGetWidth(bounds) * 0.45
        let height = floor(CGRectGetWidth(bounds) * 0.45)
        collectionViewDataSource = CollectionViewDataSource(items: content, tableView: collectionView, cellIdentifier: CellIdentifiers.HomeProductCell , headerIdentifier: nil, cellHeight: height , cellWidth: width , configureCellBlock: {
            (cell, item) in
            
            if let _ = item as? Supplier {
                (cell as? HomeProductCell)?.supplier = item as? Supplier
            }else {
                (cell as? HomeProductCell)?.product = item as? Product
                
            }
            
            }, aRowSelectedListener: { (indexPath) in
                weak var weakSelf : HomeProductParentCell? = self
                weakSelf?.delegate?.collectionViewItemClicked(atIndexPath: indexPath, type: weakSelf?.products?[indexPath.row])
                weakSelf?.delegate?.collectionViewItemClicked(atIndexPath: indexPath, type: weakSelf?.suppliers?[indexPath.row])

        })
        collectionView.reloadData()
    }
    
}
