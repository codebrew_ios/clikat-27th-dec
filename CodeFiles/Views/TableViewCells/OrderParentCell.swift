//
//  OrderParentCell.swift
//  Clikat
//
//  Created by cbl73 on 4/23/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material
import AMRatingControl

enum OrderCellType : String {
    
    case OrderHistory
    case OrderTracking
    case OrderUpcoming
    case OrderScheduled
    case RateOrder
    case LoyaltyPoints
}

protocol OrderParentCellDelegate {
     func actionOrderTypeButton(cellType : OrderCellType,order : OrderDetails?)
     func actionOrderTypeButton(cell : OrderParentCell,orderId : String?)
}

class OrderParentCell: UITableViewCell {

    var orderDelegate : OrderParentCellDelegate?
    
    var cellType : OrderCellType = .OrderHistory{
        didSet{
            updateColors()
        }
    }

  @IBOutlet weak var viewRating: UIView!{
        didSet{
            viewRating.hidden = true
            rateControl.rating = 0
            rateControl.starWidthAndHeight = 32
            rateControl.userInteractionEnabled = false
            viewRating.addSubview(rateControl)
        }
    }


    @IBOutlet weak var labelPlacedOn: UILabel!
    @IBOutlet var lblPlacedDate: UILabel!
    @IBOutlet var btnOrderType: UIButton!
    @IBOutlet var lblDetails: UILabel!
    @IBOutlet var constraintBtnOrderWidth: NSLayoutConstraint!
    @IBOutlet var lblDeliveryDate: UILabel!
    @IBOutlet var lblOrderNo: UILabel!
    @IBOutlet var lblPriceItems: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet weak var labelExpectedDelivery: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    var dataSource : CollectionViewDataSource = CollectionViewDataSource(){
        didSet{
            collectionView.dataSource = dataSource
            collectionView.delegate = dataSource
            collectionView.registerNib(UINib(nibName: CellIdentifiers.OrderImageCell, bundle: nil), forCellWithReuseIdentifier: CellIdentifiers.OrderImageCell)
        }
    }
    
    let rateControl = AMRatingControl(location: CGPoint(x: 0,y: 0), emptyImage: UIImage(asset : Asset.Ic_star_big_grey), solidImage: UIImage(asset : Asset.Ic_star_big_yellow), andMaxRating: 5)

    var order : OrderDetails?{
        didSet{
            updateUI()
        }
    }
    
    private func updateUI(){
        
        lblStatus?.text = order?.status.stringValue()
        lblOrderNo?.text = order?.orderId
        lblPriceItems?.text = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string , order?.netAmount]) + "  •  " + UtilityFunctions.appendOptionalStrings(withArray: [order?.productCount , L10n.Items.string])
        
        lblPlacedDate?.text = order?.createdOn
        
        updateColors()

        let width : CGFloat = 100.0
        dataSource = CollectionViewDataSource(items: order?.product , tableView: collectionView, cellIdentifier: CellIdentifiers.OrderImageCell , headerIdentifier: nil, cellHeight: width , cellWidth: width , configureCellBlock: {
            (cell, item) in

            guard let product = item as? Product , tempCell = cell as? OrderImageCell else{
                return
            }
            tempCell.product = product
            
            }, aRowSelectedListener: { (indexPath) in
        })
    }
    
    private func updateColors(){
        lblDetails?.hidden = true        
        let color = order?.status.color()
        btnOrderType?.layer.borderWidth = 1
        btnOrderType.setTitleColor(color, forState: .Normal)
        btnOrderType?.layer.borderColor = color?.CGColor
        lblStatus?.textColor = color
        
        switch cellType {
        case .OrderHistory:
             btnOrderType?.setTitle(L10n.REORDER.string, forState: .Normal)
             
            if order?.status == .Rejected || order?.status == .None || order?.status == .CustomerCancel {
                labelExpectedDelivery?.text = "" ; return }
            labelExpectedDelivery?.text = L10n.DeliveredOn.string
            lblDetails?.hidden = false
            lblDeliveryDate?.text = order?.deliveredOn
             btnOrderType?.hidden = LocationSingleton.sharedInstance.location?.areaEN?.id == order?.areaId ? false : true
           
        case .OrderTracking:
            lblDeliveryDate?.text = order?.serviceDate
            btnOrderType?.setTitle(L10n.TRACK.string, forState: .Normal)
        case .OrderUpcoming:
            lblDeliveryDate?.text = order?.serviceDate
            constraintBtnOrderWidth?.constant = 120.0
            layoutIfNeeded()
            if let schdeuledParameter = order?.scheduleOrder where schdeuledParameter == "1"{
                btnOrderType?.setTitle(L10n.CONFIRMORDER.string, forState: .Normal)
            }
            else{
                btnOrderType?.setTitle(L10n.CANCELORDER.string, forState: .Normal)
            }
        case .RateOrder:
            lblDeliveryDate?.text = order?.deliveredOn
            labelExpectedDelivery?.text = L10n.DELIVERED.string
            viewRating?.hidden = false
        case .OrderScheduled:
            lblDeliveryDate?.text = order?.serviceDate
            btnOrderType?.setTitle(L10n.CONFIRMORDER.string, forState: .Normal)
            constraintBtnOrderWidth?.constant = 120.0
            layoutIfNeeded()
        case .LoyaltyPoints:
            updateUILoyalty()
        }

   
    }
  
    
    @IBAction func actionOrderType(sender: AnyObject) {
        
        orderDelegate?.actionOrderTypeButton(cellType, order: order)
        orderDelegate?.actionOrderTypeButton(self,orderId: order?.orderId)
    }
}

//MARK: - Update UI Loyalty Points
extension OrderParentCell {
    
    func updateUILoyalty(){
        btnOrderType?.hidden = true
        labelPlacedOn?.hidden = true
        
        lblPriceItems?.text = UtilityFunctions.appendOptionalStrings(withArray: [order?.totalPoints, L10n.Points.string]) + "  •  " + UtilityFunctions.appendOptionalStrings(withArray: [order?.product?.count.toString , L10n.Items.string])
        lblDeliveryDate?.text = order?.serviceDate
    }
}
