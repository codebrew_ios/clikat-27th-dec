//
//  SideMenuCell.swift
//  Clikat
//
//  Created by Night Reaper on 15/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {
    
    @IBOutlet var imgIcon: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    
    var menuItem : GenericMenu?{
        didSet{
            updateUI()
        }
    }
    
    
    private func updateUI(){
        defer{
            lblTitle?.text = menuItem?.getTitle() ?? ""
        }
        guard let image = menuItem?.getImageName() else{
            return
        }
        imgIcon?.image = UIImage(asset: image)
    }
    
    
}

