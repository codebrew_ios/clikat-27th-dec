//
//  DeliverySpeedCell.swift
//  Clikat
//
//  Created by cblmacmini on 5/19/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class DeliverySpeedCell: UITableViewCell {

    @IBOutlet weak var imageViewChecked: UIImageView!
    @IBOutlet weak var labelDeliveryType: UILabel!
    @IBOutlet weak var labelDeliveryCharges: UILabel!
    @IBOutlet weak var labelDeliveryDesc : UILabel!
    
    var deliverySpeed : DeliverySpeed?{
        didSet{
            updateUI()
        }
    }
    var deliveryData : Delivery?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateUI(){
        guard let speed = deliverySpeed else { return }
        
        labelDeliveryType.text = speed.name
        imageViewChecked.image =  UIImage(asset: speed.selected ? Asset.Radio_on : Asset.Radio_off)
        labelDeliveryDesc.text = speed.type == .Postpone ? L10n.DeliveryChargesApplicableAccordingly.string : L10n.DeliveryCharges.string
        guard let type = deliverySpeed?.type else { return }
        
        switch type {
        case .Postpone:
            labelDeliveryCharges.text = ""
        case .Standard:
            labelDeliveryCharges.text = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string,deliveryData?.deliveryCharges], separatorString: "")
        case .Urgent:
            
            
            let delCharges = deliveryData?.deliveryCharges?.toDouble() ?? 0
            let urgentCharges = deliveryData?.urgentPrice?.toDouble() ?? 0
            labelDeliveryCharges.text = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string,(urgentCharges).toString], separatorString: "")
        }
    }

}
