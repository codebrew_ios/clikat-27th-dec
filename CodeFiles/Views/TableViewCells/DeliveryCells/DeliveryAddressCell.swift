//
//  DeliveryAddressCell.swift
//  Clikat
//
//  Created by cblmacmini on 5/19/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import TYAlertController
import IQKeyboardManager
import EZSwiftExtensions
import CoreLocation
class DeliveryAddressCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var labelPlaceholder : UILabel!
    
    let arrTextfieldNames = [L10n.Landmark.string , L10n.AddressLineFirst.string , L10n.AddressLineSecond.string , L10n.HouseNo.string]
    
    var selectedIndexPath : NSIndexPath? = NSIndexPath(forItem: 0, inSection: 0)
    
    var collectionDataSource = CollectionViewDataSource(){
        didSet{
            collectionView.registerNib(UINib(nibName: CellIdentifiers.DeliveryAddressCollectionCell,bundle: nil), forCellWithReuseIdentifier: CellIdentifiers.DeliveryAddressCollectionCell)
            collectionView.dataSource = collectionDataSource
            collectionView.delegate = collectionDataSource
        }
    }
    
    var arrAddress : [Address]?{
        didSet{
            labelPlaceholder?.hidden = arrAddress?.count == 0 ? false : true
            configureCollectionView()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension DeliveryAddressCell {
    
    func configureCollectionView(){
        collectionView.allowsMultipleSelection = false
        collectionDataSource = CollectionViewDataSource(items: arrAddress, tableView: collectionView, cellIdentifier: CellIdentifiers.DeliveryAddressCollectionCell, headerIdentifier: nil, cellHeight: collectionView.frame.size.height - MidPadding, cellWidth: ScreenSize.SCREEN_WIDTH / 1.6, configureCellBlock: { (cell, item) in
            weak var weakSelf = self
            weakSelf?.configureCell(cell,item: item)
            }, aRowSelectedListener: { (indexPath) in
                weak var weakSelf = self
                weakSelf?.configureCellSelection(indexPath)
        })
    }
    
    func configureCell(cell : AnyObject?,item : AnyObject?){
        guard let tempCell = cell as? DeliveryAddressCollectionCell,address = item as? Address,index = arrAddress?.indexOf(address) else { return }
        tempCell.address = address
        tempCell.imageViewSelected.hidden = index == selectedIndexPath?.row ? false : true
        tempCell.viewBg.layer.borderColor = index == selectedIndexPath?.row ? Colors.MainColor.color().CGColor : UIColor.lightGrayColor().CGColor
        tempCell.viewBg.backgroundColor = index == selectedIndexPath?.row ? Colors.MainColor.color().colorWithAlphaComponent(0.1) : UIColor.whiteColor()
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(DeliveryAddressCell.handleLongPress(_:)))
        tempCell.addGestureRecognizer(longPress)
    }
    
    func configureCellSelection(indexPath : NSIndexPath){
        selectedIndexPath = indexPath
        btnEdit.enabled = true
        collectionView.reloadData()
    }
    
    
    func handleLongPress(sender : UILongPressGestureRecognizer){
        guard let indexPath = collectionView.indexPathForItemAtPoint(sender.locationInView(collectionView)),cell = collectionView.cellForItemAtIndexPath(indexPath) as? DeliveryAddressCollectionCell else { return }
        
        if sender.state == .Began {
            cell.viewBg.backgroundColor = UIColor.redColor().colorWithAlphaComponent(0.25)
            
            var context = UIGraphicsGetCurrentContext()
            UIView.beginAnimations(nil, context: &context)
            UIView.setAnimationDuration(0.3)
            UIView.setAnimationTransition(.FlipFromLeft, forView: cell, cache: true)
            UIView.commitAnimations()
            deleteContent(indexPath,cell: cell)
        }
    }
    
    func deleteContent(indexPath : NSIndexPath,cell : DeliveryAddressCollectionCell){
        
        UtilityFunctions.showAlert(nil, message: L10n.AreYouSureYouWantToDeleteThisAddress.string, success: { [weak self] in
            
            self?.deleteAddressWebservice(self?.arrAddress?[indexPath.row].id)
            self?.collectionDataSource.items?.removeAtIndex(indexPath.row)
            self?.arrAddress?.removeAtIndex(indexPath.row)
            self?.collectionView.reloadData()
            self?.btnEdit.enabled = self?.arrAddress?.count > 0 ? true : false
            }) {
                weak var weakSelf = self
                guard let selectedIndex = weakSelf?.selectedIndexPath else {
                    cell.viewBg.backgroundColor = UIColor.whiteColor()
                    return
                }
                cell.viewBg.backgroundColor = indexPath ==  selectedIndex ? Colors.MainColor.color().colorWithAlphaComponent(0.1) : UIColor.whiteColor()
        }   
    }
    
    func deleteAddressWebservice(addressId : String?){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.DeleteAddress(FormatAPIParameters.DeleteAddress(addressId: addressId).formatParameters())) { (response) in
            
        }
    }
}

//MARK: - Button Actions
extension DeliveryAddressCell {
    
    @IBAction func actionEdit(sender: UIButton) {
        if CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != .Denied {
            showAddAddressController(arrAddress?[selectedIndexPath?.row ?? 0],isEdit: true)
        }else {
            guard let window = UIApplication.sharedApplication().delegate?.window,controller = window?.rootViewController else { return }
            UtilityFunctions.alertToEncourageLocationAccess(controller)
        }
    }
    
    @IBAction func actionAddNew(sender: UIButton) {
        if CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != .Denied {
            showAddAddressController(nil,isEdit: false)
        }else {
            guard let window = UIApplication.sharedApplication().delegate?.window,controller = window?.rootViewController else { return }
         UtilityFunctions.alertToEncourageLocationAccess(controller)
        }
    }
}
//MARK: - Address Selector
extension DeliveryAddressCell {
    
    func showAddAddressController(address : Address?,isEdit : Bool){
        guard let window = UIApplication.sharedApplication().delegate?.window,controller = window?.rootViewController else { return }
        let VC = StoryboardScene.Options.instantiateAddressPickerViewController()
        controller.dim(.In, alpha: 0.5, speed: 0.5)
        controller.modalPresentationStyle = .OverCurrentContext
        VC.selectedAddress = address
        VC.isEdit = isEdit
        if isEdit {
            VC.placeLink = address?.placeLink
        }
        VC.saveAddressBlock = { [weak self] (selectedAddress) in
            self?.handleAddressBlock(selectedAddress, isEdit: isEdit)
        }
        controller.presentVC(VC)
    }
}

//MARK: - Alert

extension DeliveryAddressCell {
    
    func handleAddressBlock(address : Address,isEdit : Bool){
        
        if isEdit {
            address.id = arrAddress?[selectedIndexPath?.row ?? 0].id
            collectionDataSource.items?[selectedIndexPath?.row ?? 0] = address
            arrAddress?[selectedIndexPath?.row ?? 0] = address
        }else {
            collectionDataSource.items?.append(address)
            arrAddress?.append(address)
        }
        addAddress(address,isEdit: isEdit)
    }
}

//MARK: - Webservice Edit and Add Address

extension DeliveryAddressCell {
    
    func addAddress(address : Address?,isEdit : Bool){
        
        let params : [String : AnyObject]?
        if isEdit {
            guard let selectedAddressId = arrAddress?[selectedIndexPath?.row ?? 0].id else { return }
            params = FormatAPIParameters.EditAddress(address: address, addressId: selectedAddressId).formatParameters()
        }else {
            params = FormatAPIParameters.AddNewAddress(address: address).formatParameters()
        }
        let api = isEdit ? API.EditAddress(params) : API.AddNewAddress(params)
        
        APIManager.sharedInstance.opertationWithRequest(withApi: api) { (response) in
            weak var weakSelf = self
            switch response {
            case .Success(let address):
                weakSelf?.addAddressHandler(address,isEdit: isEdit)
            case .Failure(_):
                break
            }
        }
    }
    
    func addAddressHandler(addedAddress : AnyObject?,isEdit : Bool){
        guard let address = addedAddress as? Address,index = arrAddress?.count else { return }
        
        let saveIndex = isEdit ? (selectedIndexPath?.row ?? 0) : index - 1
        arrAddress?[saveIndex] = address
        collectionDataSource.items?[saveIndex] = address
        collectionView.reloadData()
    }
}

//MARK: - TextField Delegates 

extension DeliveryAddressCell : UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        return true
    }
}
