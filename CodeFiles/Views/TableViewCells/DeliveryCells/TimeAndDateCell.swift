//
//  TimeAndDateCell.swift
//  Clikat
//
//  Created by cblmacmini on 5/19/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import RMDateSelectionViewController

class TimeAndDateCell: UITableViewCell {

    @IBOutlet weak var btnDate: UIButton!
    @IBOutlet weak var btnTime: UIButton!
    
    var selectedDate : NSDate?{
        didSet{
            updateUI()
        }
    }
  
    @IBAction func actionPickDate(sender: UIButton) {
        
    }

    @IBAction func actionPickTime(sender: UIButton) {
        
    }
    
    func updateUI(){
        guard let date = selectedDate else { return }
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd EEEE"
        btnDate.setTitle(dateFormatter.stringFromDate(date), forState: .Normal)
        let timeFormatter = NSDateFormatter()
        timeFormatter.dateFormat = "h : mm a"
        btnTime.setTitle(timeFormatter.stringFromDate(date), forState: .Normal)
    }
}
