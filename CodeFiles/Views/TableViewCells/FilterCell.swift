//
//  FilterCell.swift
//  Clikat
//
//  Created by Night Reaper on 18/05/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class FilterCell: UITableViewCell {

    @IBOutlet var viewBg: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    var isBgSelected : Bool = false {
        didSet{
            if (isBgSelected) {
                viewBg.backgroundColor = Colors.MainColor.color()
                textLabel?.textColor = UIColor.whiteColor()
            }
            else {
                viewBg.backgroundColor = UIColor.clearColor()
                textLabel?.textColor = Colors.MainColor.color()
            }
        }
    }
   
}
