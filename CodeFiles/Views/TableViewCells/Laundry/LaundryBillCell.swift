//
//  LaundryBillCell.swift
//  Clikat
//
//  Created by cblmacmini on 6/10/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class LaundryBillCell: UITableViewCell {

    @IBOutlet weak var labelNetTotal: UILabel!
    
    var laundryOrder : LaundryProductListing?{
        didSet{
            updateUI()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateUI(){
        
    }
}
