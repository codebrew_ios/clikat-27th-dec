//
//  LaundryProductCell.swift
//  Clikat
//
//  Created by cblmacmini on 6/10/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class LaundryProductCell: UITableViewCell {

    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var labelProductName: UILabel!
    @IBOutlet weak var stepper: GMStepper!{
        didSet{
            stepper.label.textColor = UIColor.blackColor().colorWithAlphaComponent(0.25)
        }
    }
    @IBOutlet weak var labelPrice: UILabel!
    
    var product : Product?{
        didSet{
            product?.category = "2"
            labelProductName?.text = product?.name
            stepper.associatedProduct = product
            stepper.value = product?.quantity?.toDouble() ?? 0
//            guard let price = Double(product?.price ?? "0") else { return }
            labelPrice.text = UtilityFunctions.appendOptionalStrings(withArray: [product?.price])
            if product?.price?.toInt() <= 0 {
                stepper.hidden = true
                labelPrice.hidden = true
            }
            guard let image = product?.image,url = NSURL(string:image) else { return }
            imageProduct.yy_setImageWithURL(url, options: .SetImageWithFadeAnimation)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
