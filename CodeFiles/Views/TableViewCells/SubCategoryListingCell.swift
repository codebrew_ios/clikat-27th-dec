//
//  SubCategoryListingCell.swift
//  Clikat
//
//  Created by cbl73 on 4/21/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material

class SubCategoryListingCell: MaterialTableViewCell {

    @IBOutlet weak var imageSubCategory: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
 
    
    var subCategory : SubCategory?{
        didSet{
            updateUI()
        }
    }
    
    private func updateUI(){
        
        defer{
            
            lblTitle?.text = subCategory?.name
            lblDesc?.text = subCategory?.subCategoryDesc
        }
        
        guard let image = subCategory?.image else{
            return
        }
        imageSubCategory?.yy_setImageWithURL(NSURL(string: image), options: [.SetImageWithFadeAnimation ]) 
    }

}
