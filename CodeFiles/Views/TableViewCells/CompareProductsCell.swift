//
//  CompareProductsCell.swift
//  Clikat
//
//  Created by cblmacmini on 7/18/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material
class CompareProductsCell: MaterialTableViewCell {

    @IBOutlet weak var imageProduct : UIImageView!
    @IBOutlet weak var labelProductName : UILabel!
    @IBOutlet weak var labelProductDesc : UILabel!
    @IBOutlet weak var labelSKU : UILabel!
    
    var product : Product?{
        didSet{
            updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    
    
    func updateUI(){
        labelProductName?.text = product?.name
        labelSKU?.text = product?.sku
        labelProductDesc?.text = product?.measuringUnit
        guard let image = product?.images?.first else{
            return
        }
        imageProduct?.yy_setImageWithURL(NSURL(string: image), options: [.SetImageWithFadeAnimation])
    }
}
