//
//  ProductListingCell.swift
//  Clikat
//
//  Created by cbl73 on 4/21/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material

enum ProductListingCellType {
    
    case Product
    case Cart
}


class ProductListingCell: UITableViewCell {
    
    typealias ProductClicked = (product : Product?) -> ()
    
    var productClicked : ProductClicked?
    
    var category : String?
    var categoryId : String?{
        didSet{
            stepper.associatedProduct?.categoryId = categoryId
        }
    }
    var product : Product?{
        didSet{
            updateUI(.Product)
            stepper.associatedProduct = product
        }
    }
    
    var cart : Cart? {
        didSet{
            updateUI(.Cart)
            let cartProduct = Product(cart: cart)
             
            stepper.associatedProduct = cartProduct
        }
    }
    
    @IBOutlet var imgProduct: UIImageView!{
        didSet{
            imgProduct.layer.borderWidth = 1
            imgProduct.layer.borderColor = UIColor.blackColor().colorWithAlphaComponent(0.12).CGColor
            
        }
    }
    @IBOutlet var lblTitle: UILabel!{
        didSet{
        }
    }
    @IBOutlet var lblQty: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet weak var stepper: GMStepper!
    @IBOutlet weak var lblSupplierName: UILabel!
    @IBOutlet weak var labelOfferPrice: UILabel!

    
    
    private func updateUI(type : ProductListingCellType){
        let object : Cart? = type == .Cart ? cart : product
        defer{
            lblTitle?.text = object?.name
            let unit = object?.measuringUnit
            let sku = object?.sku
            let qtyText = UtilityFunctions.appendOptionalStrings(withArray: [unit], separatorString: " ")
            lblQty?.text = qtyText
            lblSupplierName.text = product?.supplierName
            guard let value = Double(object?.quantity ?? "0") else { fatalError("Cart Quantity is nil") }
            stepper.value = value
            lblPrice?.text = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string,object?.getDisplayPrice(value)])
        }
        labelOfferPrice.hidden = true
        if let isOffer = product?.isOffer where isOffer {
            let offerPrice = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string , product?.offerPrice])
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: offerPrice)
            attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
            labelOfferPrice?.attributedText = attributeString
            labelOfferPrice.hidden = false
        }
        
        
        stepper.hidden = object?.quantity == "-1" ? true : false
    
        stepper.stepperValueListener = {[weak self] (value) in
            
            self?.lblPrice?.text = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string,object?.getDisplayPrice(value)])
        }

        guard let image = type == .Cart ? cart?.image : product?.image , formatedImage = image.percentEscapedString() else{
            return
        }
        imgProduct?.yy_setImageWithURL(NSURL(string: formatedImage), options: [.SetImageWithFadeAnimation])

    }
}
//MARK: - Tap Gesture
extension ProductListingCell {
    
    @IBAction func btnClick(sender: UIButton) {
        guard let block = productClicked else { return }
        block(product: product)
    }

}
