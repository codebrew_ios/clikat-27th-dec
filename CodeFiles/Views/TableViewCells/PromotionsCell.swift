//
//  PromotionsCell.swift
//  Clikat
//
//  Created by Night Reaper on 26/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material

class PromotionsCell: MaterialTableViewCell {


    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDetail: UILabel!
    @IBOutlet var imgView: UIImageView!

    
    var promotion : Promotion? {
        didSet{
            updateUI()
        }
    }
    
    private func updateUI(){
    
        defer {
            lblTitle?.text = promotion?.promotionName
            lblDetail?.text = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string,promotion?.promotionPrice])
        }
        guard let image = promotion?.promotionImage , url = NSURL(string: image) else{
            return
        }
        imgView?.yy_setImageWithURL(url, options: [.SetImageWithFadeAnimation])
    
    }

}
