//
//  OrderStatusCell.swift
//  Clikat
//
//  Created by cblmacmini on 5/2/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class OrderStatusCell: UITableViewCell {

    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelItemsCount: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var labelDatePlaced: UILabel!
    @IBOutlet weak var labelDateShipped: UILabel!
    @IBOutlet weak var labelDateNearYou: UILabel!
    @IBOutlet weak var labelDateDelivered: UILabel!
    
    
    @IBOutlet var imgPlaced: UIImageView!
    @IBOutlet var imgShipped: UIImageView!
    @IBOutlet var imgNearYou: UIImageView!
    @IBOutlet var imgDelivered: UIImageView!
    
    @IBOutlet var viewPS: UIView!
    @IBOutlet var viewSN: UIView!
    @IBOutlet var viewND: UIView!

    var orderDetails : OrderDetails?{
        didSet{
            updateUI()
        }
    }
    
    var cellType : OrderCellType = .OrderHistory
    
    
    

    
    private func updateUI(){
        
  
        labelPrice?.text = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string , orderDetails?.netAmount])
        labelItemsCount?.text = orderDetails?.productCount
        labelStatus?.text = orderDetails?.status.stringValue()
        labelStatus?.textColor  = orderDetails?.status.color()
        
        labelDatePlaced?.text = orderDetails?.createdOnLine ?? "\n"
        labelDateShipped?.text = orderDetails?.shippedOnLine ?? "\n"
        labelDateNearYou?.text = orderDetails?.nearOnLine ?? "\n"
        switch cellType{
            
        case .OrderHistory:
            labelDateDelivered?.text =  orderDetails?.deliveredOnLine ?? "\n"
        case .OrderTracking:
            labelDateDelivered?.text =  orderDetails?.serviceDateLine ?? "\n"
        case .OrderUpcoming:
            labelDateDelivered?.text =  orderDetails?.serviceDateLine ?? "\n"
        case .OrderScheduled:
            labelDateDelivered?.text =  orderDetails?.serviceDateLine ?? "\n"
        case .RateOrder:
            labelDateDelivered?.text =  orderDetails?.deliveredOnLine ?? "\n"
        default: break
        }
        
        
        labelDatePlaced?.preferredMaxLayoutWidth = ScreenSize.SCREEN_WIDTH/4
        labelDateShipped?.preferredMaxLayoutWidth = ScreenSize.SCREEN_WIDTH/4
        labelDateNearYou?.preferredMaxLayoutWidth = ScreenSize.SCREEN_WIDTH/4
        labelDateDelivered?.preferredMaxLayoutWidth = ScreenSize.SCREEN_WIDTH/4
        
        updateStatusBar()
        
    }

    
    func updateStatusBar(){
        guard let detail = orderDetails else{
            return
        }
        
        switch detail.status   {
            
        case .Confirmed:
            imgPlaced?.image = UIImage(asset: Asset.Ic_check_on)
            imgShipped?.image = UIImage(asset: Asset.Ic_check_off)
            imgNearYou?.image = UIImage(asset: Asset.Ic_check_off)
            imgDelivered?.image = UIImage(asset: Asset.Ic_check_off)
            
            viewPS?.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.1)
            viewSN?.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.1)
            viewND?.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.1)
            
            
        case .Shipped:
            imgPlaced?.image = UIImage(asset: Asset.Ic_check_on)
            imgShipped?.image = UIImage(asset: Asset.Ic_check_on)
            imgNearYou?.image = UIImage(asset: Asset.Ic_check_off)
            imgDelivered?.image = UIImage(asset: Asset.Ic_check_off)
            viewPS?.backgroundColor = Colors.MainColor.color()
            viewSN?.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.1)
            viewND?.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.1)
            
        case .Nearby:
            imgPlaced?.image = UIImage(asset: Asset.Ic_check_on)
            imgShipped?.image = UIImage(asset: Asset.Ic_check_on)
            imgNearYou?.image = UIImage(asset: Asset.Ic_check_on)
            imgDelivered?.image = UIImage(asset: Asset.Ic_check_off)
            viewPS?.backgroundColor = Colors.MainColor.color()
            viewSN?.backgroundColor = Colors.MainColor.color()
            viewND?.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.1)
            
        case .Delivered , .FeedbackGiven:
            imgPlaced?.image = UIImage(asset: Asset.Ic_check_on)
            imgShipped?.image = UIImage(asset: Asset.Ic_check_on)
            imgNearYou?.image = UIImage(asset: Asset.Ic_check_on)
            imgDelivered?.image = UIImage(asset: Asset.Ic_check_on)
            viewPS?.backgroundColor = Colors.MainColor.color()
            viewSN?.backgroundColor = Colors.MainColor.color()
            viewND?.backgroundColor = Colors.MainColor.color()
            
        case  .Pending , .Rejected , .Tracked , .CustomerCancel , .Schedule :
            imgPlaced?.image = UIImage(asset: Asset.Ic_check_off)
            imgShipped?.image = UIImage(asset: Asset.Ic_check_off)
            imgNearYou?.image = UIImage(asset: Asset.Ic_check_off)
            imgDelivered?.image = UIImage(asset: Asset.Ic_check_off)
            viewPS?.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.1)
            viewSN?.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.1)
            viewND?.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.1)
            
        }
    }
}

