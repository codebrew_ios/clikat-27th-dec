//
//  OrderBillCell.swift
//  Clikat
//
//  Created by cblmacmini on 5/9/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class OrderBillCell: UITableViewCell {

    
    @IBOutlet weak var labelNetAmount: UILabel!
    @IBOutlet weak var labelHandlingFee: UILabel!
    @IBOutlet weak var labelDeliveryCharges: UILabel!
    @IBOutlet weak var labelPayableAMount: UILabel!
    
    
    @IBOutlet weak var tvRemarks: UITextView!
    
    var orderSummary : OrderSummary?{
        didSet{
            
            updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateUI(){
        
        labelHandlingFee.text = orderSummary?.handlingCharges
        labelDeliveryCharges.text = orderSummary?.deliveryCharges
        labelNetAmount.text = orderSummary?.displayNetTotal
        labelPayableAMount.text = orderSummary?.netPayableAmount
    }

}
