//
//  SupplierListingCell.swift
//  Clikat
//
//  Created by cbl73 on 4/21/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import Material




class SupplierListingCell: MaterialTableViewCell {
    
    @IBOutlet weak var viewContainer : MaterialView!{
        didSet{
            let gradient = CAGradientLayer()
            gradient.frame =  CGRect(origin: CGPointZero, size: viewContainer.frame.size)
            gradient.colors = [Colors.strokeStart.color(), Colors.strokeMid.color(),Colors.strokeEnd.color()]
            
            let shape = CAShapeLayer()
            shape.lineWidth = 10
            shape.path = UIBezierPath(rect: viewContainer.bounds).CGPath
            shape.strokeColor = UIColor.blackColor().CGColor
            shape.fillColor = UIColor.clearColor().CGColor
            gradient.mask = shape
            
            viewContainer.layer.addSublayer(gradient)
        }
    }
    @IBOutlet weak var labelStatus : UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTotalReviews: UILabel!
    @IBOutlet weak var lblMinOrder: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblDeliveryTime: UILabel!
    @IBOutlet weak var imgPaymentCard: UIImageView!
    @IBOutlet weak var imgPaymentCOD: UIImageView!
    @IBOutlet var imgBadge: UIImageView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet var imgStatus: UIImageView!
    
    //MARK: - Compare Product
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelOfferPrice: UILabel!
    
    @IBOutlet weak var sponsorImage: UIImageView!
    
    var supplier : Supplier?{
        didSet{
            updateUI()
        }
    }
    
    var compareProduct : Product?{
        didSet{
            labelOfferPrice.hidden = true
            labelPrice.hidden = false
            labelPrice.text = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string,compareProduct?.getDisplayPrice(1)])
            if let isOffer = compareProduct?.isOffer where isOffer {
                let offerPrice = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string , compareProduct?.offerPrice])
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: offerPrice)
                attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
                labelOfferPrice?.attributedText = attributeString
                labelOfferPrice.hidden = false
            }
        }
    }
    
    
    private func updateUI(){
        
        defer{
            lblMinOrder?.text = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string,supplier?.minOrder])
            lblTotalReviews?.text = UtilityFunctions.appendOptionalStrings(withArray: ["(",supplier?.totalReviews,L10n.Reviews.string,")"])
            let deliveryTime = [supplier?.deliveryMinTime , supplier?.deliveryMaxTime].flatMap({ $0 }).joinWithSeparator("-")
            lblDeliveryTime?.text = supplier?.displayDeliveryTime
            
            lblName?.text = supplier?.name
            lblRating?.text = supplier?.rating
            paymentOptions()
            let status = supplier?.status ?? .DoesntMatter
            imgStatus?.image = UIImage(asset: status.status())
            labelStatus.text = status.statusStringValue()
            var textColor : UIColor? = UIColor.redColor()
            switch status {
            case .Busy:
                textColor = UIColor(hexString: "0xF2C307")
            case .Closed:
                textColor = UIColor(hexString: "0xDF3737")
            case .Online:
                textColor = UIColor(hexString: "0x89D232")
            default:
                break
            }
            
            labelStatus.textColor = textColor
           
           
        }
        sponsorImage?.hidden = supplier?.isSponsor == "1" ? false : true
        sponsorImage?.transform = Localize.currentLanguage() == Languages.Arabic ? CGAffineTransformMakeRotation(CGFloat(M_PI / 2)) : CGAffineTransformIdentity
        viewContainer.backgroundColor = supplier?.isSponsor == "1" ? Colors.sponsorBackGround.color() : UIColor.whiteColor()
        viewContainer.borderWidth = supplier?.isSponsor == "1" ? 2.0 : 0.0

         let commissionPackage = supplier?.commissionPackage ?? .DoesntMatter
        imgBadge?.image = nil
        if let badge = commissionPackage.medal() {
             imgBadge?.image = UIImage(asset : badge)
        }
        
        guard let image = supplier?.logo else{
            return
        }
        imgLogo?.yy_setImageWithURL(NSURL(string: image), options: [.SetImageWithFadeAnimation])
    }
    
    
    private func paymentOptions (){
        
        
        guard let paymentOption = supplier?.paymentMethod else{
            return
        }
        paymentOption.visibilityBasedOnDelivery(withImgCOD: imgPaymentCOD, imgCard: imgPaymentCard)
       
    } 
}
