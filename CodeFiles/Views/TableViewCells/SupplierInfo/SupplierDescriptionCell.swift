//
//  SupplierDescriptionCell.swift
//  Clikat
//
//  Created by cblmacmini on 5/4/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class SupplierDescriptionCell: UITableViewCell {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var viewPlaceholder: UIView!
    @IBOutlet var lblPlaceholder: UILabel!{
        didSet{
            lblPlaceholder.kern(ButtonKernValue)
        }
    }
    
    var htmlString : String?{
        didSet{
            guard let htmlStr = htmlString,data = htmlStr.dataUsingEncoding(NSUnicodeStringEncoding) else { return }
            do {
                let attributedString = try NSAttributedString(data: data,options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType],documentAttributes: nil)
                textView?.attributedText = attributedString
            } catch {
                
            }
            viewPlaceholder?.hidden = htmlString?.trim().characters.count == 0 ? false : true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension String {
    func trim() -> String? {
        return self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
    }
}
