//
//  OtherReviewCell.swift
//  Clikat
//
//  Created by cblmacmini on 5/4/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import AMRatingControl

class OtherReviewCell: UITableViewCell {

    @IBOutlet weak var imageViewUser: UIImageView!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var viewRating: UIView!{
        didSet{
            rateControl.rating = 0
            rateControl.starWidthAndHeight = 12
            viewRating.addSubview(rateControl)
        }
    }
    @IBOutlet weak var labelComment: UILabel!
    
    let rateControl = AMRatingControl(location: CGPoint(x: 0,y: 0), emptyImage: UIImage(asset : Asset.Ic_star_small_grey), solidImage: UIImage(asset : Asset.Ic_star_small_yellow), andMaxRating: 5)
    var currentReview : Review?{
        didSet{
            updateUI()
        }
    }
    
    private func updateUI(){
        
        defer{
            labelUserName.text = currentReview?.userName
            labelComment.text = currentReview?.comment
            rateControl.rating = currentReview?.rating ?? 0
        }
        guard let image = currentReview?.userImage , url = NSURL(string: image) else { return }
        imageViewUser.yy_setImageWithURL(url, options: .SetImageWithFadeAnimation)

    }
}
