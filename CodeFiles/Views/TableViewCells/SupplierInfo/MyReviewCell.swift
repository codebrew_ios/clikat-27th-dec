//
//  MyReviewCell.swift
//  Clikat
//
//  Created by cblmacmini on 5/4/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import AMRatingControl

class MyReviewCell: UITableViewCell {
    
    @IBOutlet weak var imageViewUser: UIImageView!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelRating: UILabel!
    @IBOutlet weak var viewRating: UIView!{
        didSet{
            rateControl.rating = 0
            rateControl.starWidthAndHeight = (DeviceType.IS_IPHONE_5 || DeviceType.IS_IPHONE_4_OR_LESS ) ? 28 : 40
            rateControl.starSpacing = UInt(HighPadding)
            rateControl.userInteractionEnabled = false
            viewRating.addSubview(rateControl)
        }
    }
    
    var myReview : Review?{
        didSet{
            updateUI()
        }
    }
    
    func updateUI(){
        
        defer{
            labelRating.text = myReview?.rating?.toString  ?? L10n.NotRatedYet.string
            labelUserName.text = GDataSingleton.sharedInstance.loggedInUser?.firstName ?? L10n.Guest.string
            rateControl.rating = myReview?.rating ?? 0
        }
        
        guard let image = myReview?.userImage , url = NSURL(string: image) else{
            return
        }
        
        imageViewUser.yy_setImageWithURL(url, options: .SetImageWithFadeAnimation)
    }
    
    let rateControl = AMRatingControl(location: CGPoint(x: 0,y: 0), emptyImage: UIImage(asset : Asset.Ic_star_big_grey), solidImage: UIImage(asset : Asset.Ic_star_big_yellow), andMaxRating: 5)
    
}
