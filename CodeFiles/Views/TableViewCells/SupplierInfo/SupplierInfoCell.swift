//
//  SupplierInfoCell.swift
//  Clikat
//
//  Created by cblmacmini on 5/4/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import AMRatingControl


enum CategorySerialNo : String {
    /*
     1) Grocery ->  Min Del time , Delivery Charge
     2) House Hold -> Min Ser time , Service Charge  "order" : 8(Cleaning)  , 3 (Maintainance)
     3) Salon -> Min Ser time , Visiting Charge  "order" : 10,
     */
    
    case HouseCleaning = "8"
    case HouseMaitainance = "3"
    case BeautySalon = "10"
    
}

class SupplierInfoCell: UITableViewCell {

    var passedData : PassedData?
    var supplier : Supplier?{
        didSet{
            updateUI()
        }
    }//ic_star_small_grey.png
    let rateControl = AMRatingControl(location: CGPoint(x: 0,y: 0), emptyImage: UIImage(asset : Asset.Ic_star_small_grey), solidImage: UIImage(asset : Asset.Ic_star_small_yellow), andMaxRating: 5)

    @IBOutlet weak var ratingBgView: UIView!{
        didSet{
            rateControl.rating = 0
            rateControl.starWidthAndHeight = 12
            ratingBgView.addSubview(rateControl)
        }
    }
    
    
    @IBOutlet var gridImg0: UIImageView!
    @IBOutlet var grid0Title: UILabel!
    @IBOutlet var grid0Desc: UILabel!
    
    @IBOutlet var grid2Img: UIImageView!
    @IBOutlet var grid2Title: UILabel!
    @IBOutlet var grid2Desc: UILabel!
    
    
    @IBOutlet var lblTotalReviews: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var imgMedal: UIImageView!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var lblOpensAt: UILabel!
    @IBOutlet var imgStatus: UIImageView!
    @IBOutlet var lblMinAmt: UILabel!
    @IBOutlet var lblOrdersDone: UILabel!
    @IBOutlet var lblBusinessSince: UILabel!
    @IBOutlet var imgCard: UIImageView!
    @IBOutlet var imgCash: UIImageView!
    
    private func updateUI(){
      
        defer{
            rateControl?.rating = supplier?.rating?.toInt() ?? 0
            lblTotalReviews?.text = UtilityFunctions.appendOptionalStrings(withArray: [supplier?.totalReviews , L10n.Reviews.string])
            lblName?.text = supplier?.name
            lblAddress?.text = supplier?.address
            lblStatus?.text = supplier?.status.statusStringValue()
            lblOpensAt?.text = UtilityFunctions.appendOptionalStrings(withArray: [L10n.OpensAt.string , supplier?.openingTime , " - ",supplier?.closeTime])
            lblMinAmt?.text = UtilityFunctions.appendOptionalStrings(withArray: [L10n.AED.string,supplier?.minOrder])
            lblOrdersDone?.text = supplier?.ordersDoneSoFar
            lblBusinessSince?.text = supplier?.businessSince
            paymentOptions()
            centralGridIcons()
        }
        let commissionPackage = supplier?.commissionPackage ?? .DoesntMatter
        imgMedal?.image = nil
        if let badge = commissionPackage.bigMedal() {
            imgMedal?.image = UIImage(asset : badge)
        }
        
        guard let statusImage = supplier?.status.status() else{
            return
        }
        imgStatus?.image = UIImage(asset: statusImage)
    }
    
    
    private func centralGridIcons(){
        grid0Desc?.text = supplier?.convertMinutes(supplier?.deliveryMinTime?.toInt() ?? 0)
        grid2Desc?.text = UtilityFunctions.appendOptionalStrings(withArray: [ L10n.AED.string , supplier?.deliveryCharges])
        guard let serialNo = passedData?.categoryOrder else{
            return
        }

        switch serialNo {
        case CategorySerialNo.HouseCleaning.rawValue , CategorySerialNo.HouseMaitainance.rawValue :
            gridImg0?.image = UIImage(asset: Asset.Ic_hm_service_time)
            grid0Title?.text = L10n.MinServiceTime.string
            grid2Img?.image = UIImage(asset: Asset.Ic_hm_service_charges)
            grid2Title?.text = L10n.ServiceCharge.string
            
        case CategorySerialNo.BeautySalon.rawValue :
            gridImg0?.image = UIImage(asset: Asset.Ic_salon_service_time)
            grid0Title?.text = L10n.MinServiceTime.string
            grid2Img?.image = UIImage(asset: Asset.Ic_salon_visiting_charges)
            grid2Title?.text = L10n.VisitingCharges.string
            
        default:
            gridImg0?.image = UIImage(asset: Asset.Ic_grocery_deliverytime)
            grid0Title?.text = L10n.MinDeliveryTime.string
            grid2Img?.image = UIImage(asset: Asset.Ic_grocery_delivery_charges)
            grid2Title?.text = L10n.DeliveryCharges.string
        
        }
        
    }

    private func paymentOptions (){
        guard let paymentOption = supplier?.paymentMethod else{
            return
        }
        paymentOption.visibilityBasedOnDelivery(withImgCOD: imgCash, imgCard: imgCard)
    }
    
}
