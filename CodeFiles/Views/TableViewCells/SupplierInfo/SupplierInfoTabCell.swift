//
//  SupplierInfoTabCell.swift
//  Clikat
//
//  Created by cblmacmini on 5/4/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

protocol SupplierInfoTabCellDelegate {
    func changeSupplierInfoTab(selectedTab : SelectedTab)
}

enum SelectedTab : Int {
    case Uniqueness = 2
    case Review = 3
    case About = 1
    static func selectedTab(index : Int) -> SelectedTab {
        switch index {
        case SelectedTab.Review.rawValue:
            return .Review
        case SelectedTab.About.rawValue:
            return .About
        default:
            return .Uniqueness
        }
    }
}

class SupplierInfoTabCell: UITableViewCell {

    var delegate : SupplierInfoTabCellDelegate?
    
    @IBOutlet weak var viewHighlight: UIView!


    @IBAction func actionTabSelected(sender: UIButton) {
        
        var frame = viewHighlight.frame
        frame.origin = CGPoint(x: sender.frame.origin.x, y: frame.y)
        UIView.animateWithDuration(0.2) {
            self.viewHighlight.frame = frame
        }
        let tab = SelectedTab.selectedTab(sender.tag)
        self.delegate?.changeSupplierInfoTab(tab)
    }
    func adjustViewHighlight(tab : SelectedTab){
        var frame = viewHighlight.frame
        
        switch tab {
        case .About:
            frame.origin = CGPoint(x: 0, y: frame.y)
        case .Review:
            frame.origin = CGPoint(x: 2 * frame.width, y: frame.y)
        case .Uniqueness:
            frame.origin = CGPoint(x: frame.size.width, y: frame.y)
        }
        
        self.viewHighlight.frame = frame
        
    }

}
