//
//  PickupDateCell.swift
//  Clikat
//
//  Created by cblmacmini on 5/21/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class PickupDateCell: UITableViewCell {

    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    
    var pickupDate : NSDate?{
        didSet{
            guard let date = pickupDate else { return }
            labelDate.text = UtilityFunctions.getDateFormatted(DateFormat.DateFormatUI, date: date)
            labelTime.text = UtilityFunctions.getTimeFormatted(DateFormat.TimeFormatUI, date: date)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
