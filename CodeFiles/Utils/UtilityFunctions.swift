//
//  UtilityFunctions.swift
//  Clikat Supplier
//
//  Created by Night Reaper on 08/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation
import TYAlertController
import RMDateSelectionViewController
import EZSwiftExtensions
import SystemConfiguration
import NVActivityIndicatorView


extension UIWindow : Dimmable {
    
}
class UtilityFunctions {
    
    class func startLoader(){

//        UtilityFunctions.sharedAppDelegateInstance().window?.addSubview(activityIndicator)
    }
    
    class func stopLoader(){
//        GiFHUD.dismiss()
    }
    
    

    class func shareContentOnSocialMedia (withViewController controller : UIViewController?,message : String?){
        let  activityController = UIActivityViewController(activityItems: [message ?? ""], applicationActivities: [])
        controller?.presentViewController(activityController, animated: true, completion: nil)
    }
    
    class func appendOptionalStrings(withArray array : [String?]) -> String {

        return array.flatMap{$0}.joinWithSeparator(" ")
    }
    
    class func appendOptionalStrings(withArray array : [String?],separatorString : String) -> String {
        
        return array.flatMap{$0}.joinWithSeparator(separatorString)
    }
    
    class func blendImage(blendingImage: UIImage?, color: UIColor) -> UIImage?{
        
        guard let image = blendingImage else{return nil}
        
        let ciImage = CIImage(image: image)
        guard let filter = CIFilter(name: "CIMultiplyCompositing"), colorFilter = CIFilter(name: "CIConstantColorGenerator") else {return UIImage()}
        let ciColor = CIColor(color: color)
        colorFilter.setValue(ciColor, forKey: kCIInputColorKey)
        let colorImage = colorFilter.outputImage
        filter.setValue(colorImage, forKey: kCIInputImageKey)
        filter.setValue(ciImage, forKey: kCIInputBackgroundImageKey)
        return UIImage(CIImage: filter.outputImage!)
        
    }
    
    
    
    class func sharedAppDelegateInstance() -> AppDelegate  {
        
        let delegate = UIApplication.sharedApplication().delegate
        return delegate as! AppDelegate
    }
    
    class func showAlert(message : String?){
        
        let alert = TYAlertView(title: "", message: message)
        alert.buttonDefaultBgColor = Colors.MainColor.color()
        alert.addAction(TYAlertAction(title: "OK", style: TYAlertActionStyle.Default, handler: { (action) in
            alert.hideView()
        }))
        alert.showInWindow()
    }
    
    class func showAlert(title : String?,message : String?,success: () -> (),cancel: () -> ()){
        let alert = TYAlertView(title: title,message: message)
        alert.buttonDefaultBgColor = Colors.MainColor.color()
        alert.addAction(TYAlertAction(title: "Cancel", style: TYAlertActionStyle.Default, handler: { (action) in
            alert.hideView()
            cancel()
        }))
        alert.addAction(TYAlertAction(title: "OK", style: TYAlertActionStyle.Default, handler: { (action) in
            alert.hideView()
            success()
        }))

        alert.showInWindow()
    }
    
     static func showDatePicker(viewController : UIViewController, datePickerMode : UIDatePickerMode, initialDate : NSDate = NSDate()  , minDate : NSDate?,title : String?,message : String?, selectedDate: (NSDate) -> (),cancel: () -> ()){
        let selectAction = RMAction(title: "Select", style: .Additional) { (datePickerController) in
            guard let dpVC = datePickerController as? RMDateSelectionViewController else { return }
            selectedDate(dpVC.datePicker.date)
        }
        
        let cancelAction = RMAction(title: "Cancel", style: .Cancel) { (datePickerController) in
            cancel()
        }
        
        guard let datePicker = RMDateSelectionViewController(style: .White, title: title, message: message, selectAction: selectAction, andCancelAction: cancelAction) else { return }
        datePicker.datePicker.date = initialDate
        datePicker.datePicker.datePickerMode = datePickerMode
        datePicker.datePicker.minimumDate = minDate
        ez.runThisAfterDelay(seconds: 0.01) {
            viewController.presentVC(datePicker)
        }
        
    }
    
    class func showActionSheet(withTitle title : String? , subTitle : String? , vc : UIViewController? , senders : [AnyObject] , success : (AnyObject,Int) -> ()){
        let alertView = TYAlertView(title: title, message: subTitle)
        alertView.buttonDefaultBgColor = Colors.MainColor.color()
        let alert = TYAlertController(alertView: alertView , preferredStyle: TYAlertControllerStyle.ActionSheet , transitionAnimation: TYAlertTransitionAnimation.Fade)
        
        for (index,element) in senders.enumerate() {
            alertView.addAction(TYAlertAction(title: element as? String, style: TYAlertActionStyle.Default, handler: { (action) in
                alertView.hideView()
                success(element,index)
            }))
        }
        alertView.addAction(TYAlertAction(title: L10n.Cancel.string, style: TYAlertActionStyle.Cancel, handler: { (action) in
            alertView.hideView()
        }))
        vc?.presentVC(alert)
        
    }


    
    static func showDatePicker(viewController : UIViewController,minDate : NSDate?,title : String?,message : String?, selectedDate: (NSDate) -> (),cancel: () -> ()){
        let selectAction = RMAction(title: "Select", style: .Additional) { (datePickerController) in
            guard let dpVC = datePickerController as? RMDateSelectionViewController else { return }
            selectedDate(dpVC.datePicker.date)
        }
        
        let cancelAction = RMAction(title: "Cancel", style: .Cancel) { (datePickerController) in
            cancel()
        }
        
        guard let datePicker = RMDateSelectionViewController(style: .White, title: title, message: message, selectAction: selectAction, andCancelAction: cancelAction) else { return }
        datePicker.datePicker.minimumDate = minDate
        ez.runThisAfterDelay(seconds: 0.01) {
            viewController.presentVC(datePicker)
        }
        
    }
    
    static func getDateFormatted(format : String,date : NSDate?) -> String?{
        let dateformatter = NSDateFormatter()
        dateformatter.dateFormat = format
        return dateformatter.stringFromDate(date ?? NSDate())
    }
    
    static func getTimeFormatted(format : String,date : NSDate?) -> String{
        let timeformatter = NSDateFormatter()
        timeformatter.dateFormat = format
        return timeformatter.stringFromDate(date ?? NSDate())
    }

    
    
    static func addParallaxToView(view : UIView){
        let leftRightMin = -20.0
        let leftRightMax = 20.0
        
        let upDownMin = -20.0
        let upDownMax = 20.0
        
        
        let leftRight = UIInterpolatingMotionEffect(keyPath: "center.x",type: .TiltAlongHorizontalAxis)
        leftRight.minimumRelativeValue = leftRightMin
        leftRight.maximumRelativeValue = leftRightMax
        let upDown = UIInterpolatingMotionEffect(keyPath: "center.y",type: .TiltAlongVerticalAxis)
        upDown.minimumRelativeValue = upDownMin
        upDown.maximumRelativeValue = upDownMax
        
        //Create a motion effect group
        let meGroup = UIMotionEffectGroup()
        meGroup.motionEffects = [leftRight, upDown]
        
        view.addMotionEffect(meGroup)
    }
    static func showSweetAlert(title : String?,message : String?,success: () -> (),cancel: () -> ()){
        SweetAlert().showAlert(title ?? "", subTitle: message, style: AlertStyle.Warning, buttonTitle:L10n.Cancel.string, buttonColor:UIColor.darkGrayColor() , otherButtonTitle:  L10n.OK.string, otherButtonColor: Colors.AlertButton.color()) { (isOtherButton) -> Void in
            if isOtherButton == true {
                cancel()
            }else {
                success()
            }
        }
        
        
    }
    
    
    
    
    
    static func showSweetAlert(title : String?,message : String?,style:AlertStyle,success: () -> (),cancel: () -> ()){
        SweetAlert().showAlert(title ?? "", subTitle: message, style: style, buttonTitle:L10n.Cancel.string, buttonColor:UIColor.darkGrayColor() , otherButtonTitle:  L10n.OK.string, otherButtonColor: Colors.AlertButton.color()) { (isOtherButton) -> Void in
            if isOtherButton == true {
                cancel()
            }else {
                success()
            }
        }
        
        
    }
    
    static func showSweetAlert(title : String?,message : String?,style : AlertStyle,success: () -> ()){
        
        SweetAlert().showAlert(title ?? "", subTitle: message, style: style, buttonTitle:L10n.OK.string, buttonColor:Colors.AlertButton.color()) { (isOtherButton) -> Void in
            if isOtherButton == true {
                success()
            }
        }
    }
    static func showSweetAlert(title : String?,message : String?,style : AlertStyle){
        
        SweetAlert().showAlert(title ?? "", subTitle: message, style: AlertStyle.Success)
    }
    
    static func isCameraPermission() -> Bool{
        let cameraMediaType = AVMediaTypeVideo
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatusForMediaType(cameraMediaType)
        
        switch cameraAuthorizationStatus {
            
        case .Authorized: return true
        case .Restricted: return false
        case .Denied: return false
        case .NotDetermined:
            AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo, completionHandler: { (granted) in
                
            })
            return true
        }
    }
    
}

extension UtilityFunctions {
    //Show Camera Unavailable Alert
    
    static func alertToEncourageCameraAccessWhenApplicationStarts(viewController : UIViewController?){
        //Camera not available - Alert
        let cameraUnavailableAlertController = UIAlertController (title: L10n.CameraUnavailable.string, message: L10n.ItLooksLikeYourPrivacySettingsArePreventingUsFromAccessingYourCamera.string, preferredStyle: .Alert)
        
        let settingsAction = UIAlertAction(title: L10n.Settings.string, style: .Destructive) { (_) -> Void in
            let settingsUrl = NSURL(string:UIApplicationOpenSettingsURLString)
            if let url = settingsUrl {
                dispatch_async(dispatch_get_main_queue()) {
                    UIApplication.sharedApplication().openURL(url)
                }
                
            }
        }
        let cancelAction = UIAlertAction(title: L10n.OK.string, style: .Default, handler: nil)
        cameraUnavailableAlertController .addAction(settingsAction)
        cameraUnavailableAlertController .addAction(cancelAction)
        viewController?.presentViewController(cameraUnavailableAlertController , animated: true, completion: nil)
    }
    static func alertToEncourageLocationAccess(viewController : UIViewController?){
        //Camera not available - Alert
        let cameraUnavailableAlertController = UIAlertController (title: L10n.LocationUnavailable.string, message: L10n.PleaseCheckToSeeIfYouHaveEnabledLocationServices.string, preferredStyle: .Alert)
        
        let settingsAction = UIAlertAction(title: L10n.Settings.string, style: .Destructive) { (_) -> Void in
            let settingsUrl = NSURL(string:UIApplicationOpenSettingsURLString)
            if let url = settingsUrl {
                dispatch_async(dispatch_get_main_queue()) {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
        }
        let cancelAction = UIAlertAction(title: L10n.OK.string, style: .Default, handler: nil)
        cameraUnavailableAlertController .addAction(settingsAction)
        cameraUnavailableAlertController .addAction(cancelAction)
        viewController?.presentViewController(cameraUnavailableAlertController , animated: true, completion: nil)
    }
    
    static func convertArrayIntoJson(array: [Dictionary<String,String>]?) -> String? {
        
        do {
            
            let jsonData = try NSJSONSerialization.dataWithJSONObject(array ?? [], options: NSJSONWritingOptions.PrettyPrinted)
            
            var string = NSString(data: jsonData, encoding: NSUTF8StringEncoding) ?? ""
            print(string)
            string = string.stringByReplacingOccurrencesOfString("\n", withString: "")
            string = string.stringByReplacingOccurrencesOfString("\\\"", withString: "\"")
            string = string.stringByReplacingOccurrencesOfString("\"", withString: "")
            string = string.stringByReplacingOccurrencesOfString("\\", withString: "") // removes \
            string = string.stringByReplacingOccurrencesOfString(" ", withString: "")
            string = string.stringByReplacingOccurrencesOfString("/", withString: "")
            print(string)
            return string as String
        }catch let error as NSError {
            
            print(error.description)
            return ""
        }
    }
}


//MARK: - ImageScaling
extension UIImage {
   
    func resizeWith(width: CGFloat,height : CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: width, height:height )))
        imageView.contentMode = .ScaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.renderInContext(context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
}