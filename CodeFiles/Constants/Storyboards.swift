// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

import Foundation
import UIKit

protocol StoryboardSceneType {
  static var storyboardName: String { get }
}

extension StoryboardSceneType {
  static func storyboard() -> UIStoryboard {
    return UIStoryboard(name: self.storyboardName, bundle: nil)
  }

  static func initialViewController() -> UIViewController {
    guard let vc = storyboard().instantiateInitialViewController() else {
      fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
    }
    return vc
  }
}

extension StoryboardSceneType where Self: RawRepresentable, Self.RawValue == String {
  func viewController() -> UIViewController {
    return Self.storyboard().instantiateViewControllerWithIdentifier(self.rawValue)
  }
  static func viewController(identifier: Self) -> UIViewController {
    return identifier.viewController()
  }
}

protocol StoryboardSegueType: RawRepresentable { }

extension UIViewController {
  func performSegue<S: StoryboardSegueType where S.RawValue == String>(segue: S, sender: AnyObject? = nil) {
    performSegueWithIdentifier(segue.rawValue, sender: sender)
  }
}

// swiftlint:disable file_length
// swiftlint:disable type_body_length

struct StoryboardScene {
  enum LaunchScreen: StoryboardSceneType {
    static let storyboardName = "LaunchScreen"

    static func initialViewController() -> UISplitViewController {
      guard let vc = storyboard().instantiateInitialViewController() as? UISplitViewController else {
        fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
      }
      return vc
    }
  }
  enum Laundry: String, StoryboardSceneType {
    static let storyboardName = "Laundry"

    case OrderViewControllerScene = "OrderViewController"
    static func instantiateOrderViewController() -> OrderViewController {
      guard let vc = StoryboardScene.Laundry.OrderViewControllerScene.viewController() as? OrderViewController
      else {
        fatalError("ViewController 'OrderViewController' is not of the expected class OrderViewController.")
      }
      return vc
    }

    case PickupDetailsControllerScene = "PickupDetailsController"
    static func instantiatePickupDetailsController() -> PickupDetailsController {
      guard let vc = StoryboardScene.Laundry.PickupDetailsControllerScene.viewController() as? PickupDetailsController
      else {
        fatalError("ViewController 'PickupDetailsController' is not of the expected class PickupDetailsController.")
      }
      return vc
    }

    case ServicesViewControllerScene = "ServicesViewController"
    static func instantiateServicesViewController() -> ServicesViewController {
      guard let vc = StoryboardScene.Laundry.ServicesViewControllerScene.viewController() as? ServicesViewController
      else {
        fatalError("ViewController 'ServicesViewController' is not of the expected class ServicesViewController.")
      }
      return vc
    }
  }
  enum Main: String, StoryboardSceneType {
    static let storyboardName = "Main"

    case DrawerMenuViewControllerScene = "DrawerMenuViewController"
    static func instantiateDrawerMenuViewController() -> DrawerMenuViewController {
      guard let vc = StoryboardScene.Main.DrawerMenuViewControllerScene.viewController() as? DrawerMenuViewController
      else {
        fatalError("ViewController 'DrawerMenuViewController' is not of the expected class DrawerMenuViewController.")
      }
      return vc
    }

    case HomeViewControllerScene = "HomeViewController"
    static func instantiateHomeViewController() -> HomeViewController {
      guard let vc = StoryboardScene.Main.HomeViewControllerScene.viewController() as? HomeViewController
      else {
        fatalError("ViewController 'HomeViewController' is not of the expected class HomeViewController.")
      }
      return vc
    }

    case ItemListingViewControllerScene = "ItemListingViewController"
    static func instantiateItemListingViewController() -> ItemListingViewController {
      guard let vc = StoryboardScene.Main.ItemListingViewControllerScene.viewController() as? ItemListingViewController
      else {
        fatalError("ViewController 'ItemListingViewController' is not of the expected class ItemListingViewController.")
      }
      return vc
    }

    case LeftNavigationViewControllerScene = "LeftNavigationViewController"
    static func instantiateLeftNavigationViewController() -> LeftNavigationViewController {
      guard let vc = StoryboardScene.Main.LeftNavigationViewControllerScene.viewController() as? LeftNavigationViewController
      else {
        fatalError("ViewController 'LeftNavigationViewController' is not of the expected class LeftNavigationViewController.")
      }
      return vc
    }

    case PackageProductListingViewControllerScene = "PackageProductListingViewController"
    static func instantiatePackageProductListingViewController() -> PackageProductListingViewController {
      guard let vc = StoryboardScene.Main.PackageProductListingViewControllerScene.viewController() as? PackageProductListingViewController
      else {
        fatalError("ViewController 'PackageProductListingViewController' is not of the expected class PackageProductListingViewController.")
      }
      return vc
    }

    case PackageSupplierListingViewControllerScene = "PackageSupplierListingViewController"
    static func instantiatePackageSupplierListingViewController() -> PackageSupplierListingViewController {
      guard let vc = StoryboardScene.Main.PackageSupplierListingViewControllerScene.viewController() as? PackageSupplierListingViewController
      else {
        fatalError("ViewController 'PackageSupplierListingViewController' is not of the expected class PackageSupplierListingViewController.")
      }
      return vc
    }

    case ProductDetailViewControllerScene = "ProductDetailViewController"
    static func instantiateProductDetailViewController() -> ProductDetailViewController {
      guard let vc = StoryboardScene.Main.ProductDetailViewControllerScene.viewController() as? ProductDetailViewController
      else {
        fatalError("ViewController 'ProductDetailViewController' is not of the expected class ProductDetailViewController.")
      }
      return vc
    }

    case RightNavigationViewControllerScene = "RightNavigationViewController"
    static func instantiateRightNavigationViewController() -> RightNavigationViewController {
      guard let vc = StoryboardScene.Main.RightNavigationViewControllerScene.viewController() as? RightNavigationViewController
      else {
        fatalError("ViewController 'RightNavigationViewController' is not of the expected class RightNavigationViewController.")
      }
      return vc
    }

    case SearchViewControllerScene = "SearchViewController"
    static func instantiateSearchViewController() -> SearchViewController {
      guard let vc = StoryboardScene.Main.SearchViewControllerScene.viewController() as? SearchViewController
      else {
        fatalError("ViewController 'SearchViewController' is not of the expected class SearchViewController.")
      }
      return vc
    }

    case ServicesViewControllerScene = "ServicesViewController"
    static func instantiateServicesViewController() -> ServicesViewController {
      guard let vc = StoryboardScene.Main.ServicesViewControllerScene.viewController() as? ServicesViewController
      else {
        fatalError("ViewController 'ServicesViewController' is not of the expected class ServicesViewController.")
      }
      return vc
    }

    case SubcategoryViewControllerScene = "SubcategoryViewController"
    static func instantiateSubcategoryViewController() -> SubcategoryViewController {
      guard let vc = StoryboardScene.Main.SubcategoryViewControllerScene.viewController() as? SubcategoryViewController
      else {
        fatalError("ViewController 'SubcategoryViewController' is not of the expected class SubcategoryViewController.")
      }
      return vc
    }

    case SupplierInfoViewControllerScene = "SupplierInfoViewController"
    static func instantiateSupplierInfoViewController() -> SupplierInfoViewController {
      guard let vc = StoryboardScene.Main.SupplierInfoViewControllerScene.viewController() as? SupplierInfoViewController
      else {
        fatalError("ViewController 'SupplierInfoViewController' is not of the expected class SupplierInfoViewController.")
      }
      return vc
    }

    case SupplierListingViewControllerScene = "SupplierListingViewController"
    static func instantiateSupplierListingViewController() -> SupplierListingViewController {
      guard let vc = StoryboardScene.Main.SupplierListingViewControllerScene.viewController() as? SupplierListingViewController
      else {
        fatalError("ViewController 'SupplierListingViewController' is not of the expected class SupplierListingViewController.")
      }
      return vc
    }
  }
  enum Options: String, StoryboardSceneType {
    static let storyboardName = "Options"

    case AddressPickerViewControllerScene = "AddressPickerViewController"
    static func instantiateAddressPickerViewController() -> AddressPickerViewController {
      guard let vc = StoryboardScene.Options.AddressPickerViewControllerScene.viewController() as? AddressPickerViewController
      else {
        fatalError("ViewController 'AddressPickerViewController' is not of the expected class AddressPickerViewController.")
      }
      return vc
    }

    case BarCodeScannerViewControllerScene = "BarCodeScannerViewController"
    static func instantiateBarCodeScannerViewController() -> BarCodeScannerViewController {
      guard let vc = StoryboardScene.Options.BarCodeScannerViewControllerScene.viewController() as? BarCodeScannerViewController
      else {
        fatalError("ViewController 'BarCodeScannerViewController' is not of the expected class BarCodeScannerViewController.")
      }
      return vc
    }

    case CartViewControllerScene = "CartViewController"
    static func instantiateCartViewController() -> CartViewController {
      guard let vc = StoryboardScene.Options.CartViewControllerScene.viewController() as? CartViewController
      else {
        fatalError("ViewController 'CartViewController' is not of the expected class CartViewController.")
      }
      return vc
    }

    case CategorySelectionControllerScene = "CategorySelectionController"
    static func instantiateCategorySelectionController() -> CategorySelectionController {
      guard let vc = StoryboardScene.Options.CategorySelectionControllerScene.viewController() as? CategorySelectionController
      else {
        fatalError("ViewController 'CategorySelectionController' is not of the expected class CategorySelectionController.")
      }
      return vc
    }

    case CompareProductResultControllerScene = "CompareProductResultController"
    static func instantiateCompareProductResultController() -> CompareProductResultController {
      guard let vc = StoryboardScene.Options.CompareProductResultControllerScene.viewController() as? CompareProductResultController
      else {
        fatalError("ViewController 'CompareProductResultController' is not of the expected class CompareProductResultController.")
      }
      return vc
    }

    case CompareProductsControllerScene = "CompareProductsController"
    static func instantiateCompareProductsController() -> CompareProductsController {
      guard let vc = StoryboardScene.Options.CompareProductsControllerScene.viewController() as? CompareProductsController
      else {
        fatalError("ViewController 'CompareProductsController' is not of the expected class CompareProductsController.")
      }
      return vc
    }

    case FilterViewControllerScene = "FilterViewController"
    static func instantiateFilterViewController() -> FilterViewController {
      guard let vc = StoryboardScene.Options.FilterViewControllerScene.viewController() as? FilterViewController
      else {
        fatalError("ViewController 'FilterViewController' is not of the expected class FilterViewController.")
      }
      return vc
    }

    case LiveSupportViewControllerScene = "LiveSupportViewController"
    static func instantiateLiveSupportViewController() -> LiveSupportViewController {
      guard let vc = StoryboardScene.Options.LiveSupportViewControllerScene.viewController() as? LiveSupportViewController
      else {
        fatalError("ViewController 'LiveSupportViewController' is not of the expected class LiveSupportViewController.")
      }
      return vc
    }

    case LoyalityPointsViewControllerScene = "LoyalityPointsViewController"
    static func instantiateLoyalityPointsViewController() -> LoyalityPointsViewController {
      guard let vc = StoryboardScene.Options.LoyalityPointsViewControllerScene.viewController() as? LoyalityPointsViewController
      else {
        fatalError("ViewController 'LoyalityPointsViewController' is not of the expected class LoyalityPointsViewController.")
      }
      return vc
    }

    case LoyaltyPointOrdersControllerScene = "LoyaltyPointOrdersController"
    static func instantiateLoyaltyPointOrdersController() -> LoyaltyPointOrdersController {
      guard let vc = StoryboardScene.Options.LoyaltyPointOrdersControllerScene.viewController() as? LoyaltyPointOrdersController
      else {
        fatalError("ViewController 'LoyaltyPointOrdersController' is not of the expected class LoyaltyPointOrdersController.")
      }
      return vc
    }

    case MyFavoritesViewControllerScene = "MyFavoritesViewController"
    static func instantiateMyFavoritesViewController() -> MyFavoritesViewController {
      guard let vc = StoryboardScene.Options.MyFavoritesViewControllerScene.viewController() as? MyFavoritesViewController
      else {
        fatalError("ViewController 'MyFavoritesViewController' is not of the expected class MyFavoritesViewController.")
      }
      return vc
    }

    case NotificationsViewControllerScene = "NotificationsViewController"
    static func instantiateNotificationsViewController() -> NotificationsViewController {
      guard let vc = StoryboardScene.Options.NotificationsViewControllerScene.viewController() as? NotificationsViewController
      else {
        fatalError("ViewController 'NotificationsViewController' is not of the expected class NotificationsViewController.")
      }
      return vc
    }

    case PromotionsViewControllerScene = "PromotionsViewController"
    static func instantiatePromotionsViewController() -> PromotionsViewController {
      guard let vc = StoryboardScene.Options.PromotionsViewControllerScene.viewController() as? PromotionsViewController
      else {
        fatalError("ViewController 'PromotionsViewController' is not of the expected class PromotionsViewController.")
      }
      return vc
    }

    case SettingsViewControllerScene = "SettingsViewController"
    static func instantiateSettingsViewController() -> SettingsViewController {
      guard let vc = StoryboardScene.Options.SettingsViewControllerScene.viewController() as? SettingsViewController
      else {
        fatalError("ViewController 'SettingsViewController' is not of the expected class SettingsViewController.")
      }
      return vc
    }

    case TermsAndConditionsControllerScene = "TermsAndConditionsController"
    static func instantiateTermsAndConditionsController() -> TermsAndConditionsController {
      guard let vc = StoryboardScene.Options.TermsAndConditionsControllerScene.viewController() as? TermsAndConditionsController
      else {
        fatalError("ViewController 'TermsAndConditionsController' is not of the expected class TermsAndConditionsController.")
      }
      return vc
    }
  }
  enum Order: String, StoryboardSceneType {
    static let storyboardName = "Order"

    case DeliveryViewControllerScene = "DeliveryViewController"
    static func instantiateDeliveryViewController() -> DeliveryViewController {
      guard let vc = StoryboardScene.Order.DeliveryViewControllerScene.viewController() as? DeliveryViewController
      else {
        fatalError("ViewController 'DeliveryViewController' is not of the expected class DeliveryViewController.")
      }
      return vc
    }

    case LoyaltyPointsSummaryControllerScene = "LoyaltyPointsSummaryController"
    static func instantiateLoyaltyPointsSummaryController() -> LoyaltyPointsSummaryController {
      guard let vc = StoryboardScene.Order.LoyaltyPointsSummaryControllerScene.viewController() as? LoyaltyPointsSummaryController
      else {
        fatalError("ViewController 'LoyaltyPointsSummaryController' is not of the expected class LoyaltyPointsSummaryController.")
      }
      return vc
    }

    case OrderDetailControllerScene = "OrderDetailController"
    static func instantiateOrderDetailController() -> OrderDetailController {
      guard let vc = StoryboardScene.Order.OrderDetailControllerScene.viewController() as? OrderDetailController
      else {
        fatalError("ViewController 'OrderDetailController' is not of the expected class OrderDetailController.")
      }
      return vc
    }

    case OrderHistoryViewControllerScene = "OrderHistoryViewController"
    static func instantiateOrderHistoryViewController() -> OrderHistoryViewController {
      guard let vc = StoryboardScene.Order.OrderHistoryViewControllerScene.viewController() as? OrderHistoryViewController
      else {
        fatalError("ViewController 'OrderHistoryViewController' is not of the expected class OrderHistoryViewController.")
      }
      return vc
    }

    case OrderSchedularViewControllerScene = "OrderSchedularViewController"
    static func instantiateOrderSchedularViewController() -> OrderSchedularViewController {
      guard let vc = StoryboardScene.Order.OrderSchedularViewControllerScene.viewController() as? OrderSchedularViewController
      else {
        fatalError("ViewController 'OrderSchedularViewController' is not of the expected class OrderSchedularViewController.")
      }
      return vc
    }

    case OrderSummaryControllerScene = "OrderSummaryController"
    static func instantiateOrderSummaryController() -> OrderSummaryController {
      guard let vc = StoryboardScene.Order.OrderSummaryControllerScene.viewController() as? OrderSummaryController
      else {
        fatalError("ViewController 'OrderSummaryController' is not of the expected class OrderSummaryController.")
      }
      return vc
    }

    case PaymentMethodControllerScene = "PaymentMethodController"
    static func instantiatePaymentMethodController() -> PaymentMethodController {
      guard let vc = StoryboardScene.Order.PaymentMethodControllerScene.viewController() as? PaymentMethodController
      else {
        fatalError("ViewController 'PaymentMethodController' is not of the expected class PaymentMethodController.")
      }
      return vc
    }

    case RateMyOrderControllerScene = "RateMyOrderController"
    static func instantiateRateMyOrderController() -> RateMyOrderController {
      guard let vc = StoryboardScene.Order.RateMyOrderControllerScene.viewController() as? RateMyOrderController
      else {
        fatalError("ViewController 'RateMyOrderController' is not of the expected class RateMyOrderController.")
      }
      return vc
    }

    case ScheduledOrderControllerScene = "ScheduledOrderController"
    static func instantiateScheduledOrderController() -> ScheduledOrderController {
      guard let vc = StoryboardScene.Order.ScheduledOrderControllerScene.viewController() as? ScheduledOrderController
      else {
        fatalError("ViewController 'ScheduledOrderController' is not of the expected class ScheduledOrderController.")
      }
      return vc
    }

    case TrackMyOrderViewControllerScene = "TrackMyOrderViewController"
    static func instantiateTrackMyOrderViewController() -> TrackMyOrderViewController {
      guard let vc = StoryboardScene.Order.TrackMyOrderViewControllerScene.viewController() as? TrackMyOrderViewController
      else {
        fatalError("ViewController 'TrackMyOrderViewController' is not of the expected class TrackMyOrderViewController.")
      }
      return vc
    }

    case UpcomingOrdersViewControllerScene = "UpcomingOrdersViewController"
    static func instantiateUpcomingOrdersViewController() -> UpcomingOrdersViewController {
      guard let vc = StoryboardScene.Order.UpcomingOrdersViewControllerScene.viewController() as? UpcomingOrdersViewController
      else {
        fatalError("ViewController 'UpcomingOrdersViewController' is not of the expected class UpcomingOrdersViewController.")
      }
      return vc
    }
  }
  enum Register: String, StoryboardSceneType {
    static let storyboardName = "Register"

    case LocationViewControllerScene = "LocationViewController"
    static func instantiateLocationViewController() -> LocationViewController {
      guard let vc = StoryboardScene.Register.LocationViewControllerScene.viewController() as? LocationViewController
      else {
        fatalError("ViewController 'LocationViewController' is not of the expected class LocationViewController.")
      }
      return vc
    }

    case LoginViewControllerScene = "LoginViewController"
    static func instantiateLoginViewController() -> LoginViewController {
      guard let vc = StoryboardScene.Register.LoginViewControllerScene.viewController() as? LoginViewController
      else {
        fatalError("ViewController 'LoginViewController' is not of the expected class LoginViewController.")
      }
      return vc
    }

    case OTPViewControllerScene = "OTPViewController"
    static func instantiateOTPViewController() -> OTPViewController {
      guard let vc = StoryboardScene.Register.OTPViewControllerScene.viewController() as? OTPViewController
      else {
        fatalError("ViewController 'OTPViewController' is not of the expected class OTPViewController.")
      }
      return vc
    }

    case PhoneNoViewControllerScene = "PhoneNoViewController"
    static func instantiatePhoneNoViewController() -> PhoneNoViewController {
      guard let vc = StoryboardScene.Register.PhoneNoViewControllerScene.viewController() as? PhoneNoViewController
      else {
        fatalError("ViewController 'PhoneNoViewController' is not of the expected class PhoneNoViewController.")
      }
      return vc
    }

    case RegisterFirstStepControllerScene = "RegisterFirstStepController"
    static func instantiateRegisterFirstStepController() -> RegisterFirstStepController {
      guard let vc = StoryboardScene.Register.RegisterFirstStepControllerScene.viewController() as? RegisterFirstStepController
      else {
        fatalError("ViewController 'RegisterFirstStepController' is not of the expected class RegisterFirstStepController.")
      }
      return vc
    }

    case RegisterViewControllerScene = "RegisterViewController"
    static func instantiateRegisterViewController() -> RegisterViewController {
      guard let vc = StoryboardScene.Register.RegisterViewControllerScene.viewController() as? RegisterViewController
      else {
        fatalError("ViewController 'RegisterViewController' is not of the expected class RegisterViewController.")
      }
      return vc
    }

    case SplashViewControllerScene = "SplashViewController"
    static func instantiateSplashViewController() -> SplashViewController {
      guard let vc = StoryboardScene.Register.SplashViewControllerScene.viewController() as? SplashViewController
      else {
        fatalError("ViewController 'SplashViewController' is not of the expected class SplashViewController.")
      }
      return vc
    }
  }
}

struct StoryboardSegue {
}
