//
//  WebConstants-Swift.h
//  NUSIT
//
//  Created by Gagan on 26/02/15.
//  Copyright (c) 2015 Gagan. All rights reserved.
//

#ifndef NUSIT_WebConstants_Swift_h
#define NUSIT_WebConstants_Swift_h


#define GSBaseUrl "http://api.whatashaadi.com/api/"



#define KFacebookApi "https://graph.facebook.com/me/?fields=id,cover,picture,name,gender,bio,location,email,birthday&access_token=%@"
#define KFacebookProfilePic "https://graph.facebook.com/%@/picture?width=%.0f&height=%.0f"

#endif
