// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

import Foundation

// swiftlint:disable file_length
// swiftlint:disable type_body_length
enum L10n {
  /// Service Charge
  case ServiceCharge
  /// Min. Service Time
  case MinServiceTime
  /// Visiting Charges
  case VisitingCharges
  /// Min. Delivery Time
  case MinDeliveryTime
  /// Opens at 
  case OpensAt
  /// Please select an Address
  case PleaseSelectAnAddress
  /// Credit/Debit card
  case CreditDebitCard
  /// Your Cart has no items.Please add items to cart to Proceed.
  case YourCartHasNoItemsPleaseAddItemsToCartToProceed
  /// Landmark
  case Landmark
  /// Address Line First
  case AddressLineFirst
  /// Address Line Second
  case AddressLineSecond
  /// Pincode
  case Pincode
  /// Delivery Address
  case DeliveryAddress
  /// Enter your details.
  case EnterYourDetails
  /// Delivery charges applicable accordingly
  case DeliveryChargesApplicableAccordingly
  /// Delivery charges
  case DeliveryCharges
  /// Select Time and Date
  case SelectTimeAndDate
  /// Please select a booking schedule and time
  case PleaseSelectABookingScheduleAndTime
  /// Select booking time
  case SelectBookingTime
  /// Reordering will clear you cart. Press OK to continue.
  case ReOrderingWillClearYouCart
  /// Pending
  case PENDING
  /// Delivered
  case DELIVERED
  /// Confirmed
  case CONFIRMED
  /// Feedback Given
  case FEEDBACKGIVEN
  /// Rejected
  case REJECTED
  /// Shipped
  case SHIPPED
  /// Near By
  case NEARBY
  /// Tracked
  case TRACKED
  /// Cancelled
  case CUSTOMERCANCELLED
  /// Scheduled
  case SCHEDULED
  /// Order Details
  case OrderDetails
  /// items
  case Items
  /// REORDER
  case REORDER
  /// TRACK
  case TRACK
  /// BOOKED
  case BOOKED
  /// CANCEL ORDER
  case CANCELORDER
  /// CONFIRM ORDER
  case CONFIRMORDER
  /// Monthly
  case Monthly
  /// Weekly
  case Weekly
  /// Pickup
  case Pickup
  /// Please enter your email address
  case PleaseEnterYourEmailAddress
  /// Please enter a valid email address
  case PleaseEnterAValidEmailAddress
  /// Please enter your password
  case PleaseEnterYourPassword
  /// Password should be minimum 6 characters.
  case PasswordShouldBeMinimum6Characters
  /// Current session expired \n Please Login to continue.
  case SessionExpiredLoginToContinue
  /// OTP Sent.
  case OTPSent
  /// Select picture
  case SelectPicture
  /// Please select your profile picture.
  case PleaseSelectYourProfilePicture
  /// Please enter your name.
  case PleaseEnterYourName
  /// Camera
  case Camera
  /// Photo Library
  case PhotoLibrary
  /// Home
  case Home
  /// Live support
  case LiveSupport
  /// Cart
  case Cart
  /// Promotions
  case Promotions
  /// Notifications
  case Notifications
  /// My Account
  case MyAccount
  /// My favorites
  case MyFavorites
  /// Order history
  case OrderHistory
  /// Track my order
  case TrackMyOrder
  /// Rate my order
  case RateMyOrder
  /// Upcoming orders
  case UpcomingOrders
  /// Loyality points
  case LoyalityPoints
  /// Share app
  case ShareApp
  /// Settings
  case Settings
  /// Guest
  case Guest
  /// Welcome
  case Welcome
  /// Points
  case Points
  /// Grocery
  case Grocery
  /// Laundry
  case Laundry
  /// Household
  case Household
  /// Flowers
  case Flowers
  /// Fitness
  case Fitness
  /// Photography
  case Photography
  /// Baby sitter
  case BabySitter
  /// Cleaning
  case Cleaning
  /// Party
  case Party
  /// Beauty salon
  case BeautySalon
  /// Medicines
  case Medicines
  /// Water delivery
  case WaterDelivery
  /// Packages
  case Packages
  /// OK
  case OK
  /// Save
  case Save
  /// Cancel
  case Cancel
  /// Choose booking cycle
  case ChooseBookingCycle
  ///  Reviews
  case Reviews
  /// AED 
  case AED
  ///  Mins
  case Mins
  /// Results for 
  case ResultsFor
  /// Days
  case Days
  /// Hours
  case Hours
  /// Discoverability
  case Discoverability
  /// Delivery
  case Delivery
  /// Supplier Type
  case SupplierType
  /// Rating
  case Rating
  /// 1 Star
  case _1Star
  /// 2 Star
  case _2Star
  /// 3 Star
  case _3Star
  /// 4 Star and above
  case _4StarAndAbove
  /// Gold
  case Gold
  /// Silver
  case Silver
  /// Platinum
  case Platinum
  /// Cash on delivery
  case CashOnDelivery
  /// Card
  case Card
  /// Both
  case Both
  /// Online
  case Online
  /// Busy
  case Busy
  /// Closed
  case Closed
  /// Cancelled
  case Cancelled
  /// Change Password
  case ChangePassword
  /// Notifications Language
  case NotificationsLanguage
  /// Manage Address
  case ManageAddress
  /// Logout
  case Logout
  /// Select notification language
  case SelectNotificationLanguage
  /// English
  case English
  /// Arabic
  case Arabic
  /// You haven't earned any loyalty points yet.
  case YouHavenTEarnedAnyLoyaltyPointsYet
  /// Please select produts from same supplier
  case SelectProdutsFromSameSupplier
  /// No Remarks
  case NoRemarks
  /// When do you want the service?
  case WhenDoYouWantTheService
  /// Pickup location
  case PickupLocation
  /// Select pickup date and time
  case SelectPickupDateAndTime
  /// Are you sure you want to delete this address.
  case AreYouSureYouWantToDeleteThisAddress
  /// Select Country
  case SelectCountry
  /// Select City
  case SelectCity
  /// Select Zone
  case SelectZone
  /// Select Area
  case SelectArea
  /// Please select all fields above.
  case PleaseSelectAllFieldsAbove
  /// Please select a country
  case PleaseSelectACountry
  /// Please select a city
  case PleaseSelectACity
  /// Please fill all details
  case PleaseFillAllDetails
  /// Please enter valid Pincode
  case PleaseEnterValidPincode
  /// Old Password
  case OldPassword
  /// New Password
  case NewPassword
  /// Confirm Password
  case ConfirmPassword
  /// New Password must not be same as old password
  case NewPasswordMustNotBeSameAsOldPassword
  /// Passwords do not match
  case PasswordsDoNotMatch
  /// Forgot Password
  case ForgotPassword
  /// Password recovery has been sent to your email id
  case PasswordRecoveryHasBeenSentToYourEmailId
  /// The Leading Online Home Services In UAE.. https://itunes.apple.com/us/app/clikat/id1147970115?ls=1&mt=8
  case TheLeadingOnlineHomeServicesInUAE
  /// Please check your internet connection.
  case PleaseCheckYourInternetConnection
  /// Cancel Order
  case CancelOrder
  /// Success
  case Success
  /// Do you really want to cancel this order?
  case DoYouReallyWantToCancelThisOrder
  /// You have cancelled your order successfully
  case YouHaveCancelledYourOrderSuccessfully
  /// Log out!
  case LogOut
  /// Are you sure you want to logout?
  case AreYouSureYouWantToLogout
  /// You are successfully logged out
  case YouAreSuccessfullyLoggedOut
  /// Please enter valid country code follwoed by phone number
  case PleaseEnterValidCountryCodeFollwoedByPhoneNumber
  /// Delivery Speed
  case DeliverySpeed
  /// Sorry… Your Order is Below Minimum Order Price.
  case SorryYourOrderIsBelowMinimumOrderPrice
  /// Recommended
  case Recommended
  /// Offers
  case Offers
  /// Your order has been placed successfully.
  case YourOrderHaveBeenPlacedSuccessfully
  /// Order placed successfully
  case OrderPlacedSuccessfully
  /// Your order has been scheduled successfully
  case YourOrderHaveBeenSheduledSuccessfully
  /// Are you sure?
  case AreYouSure
  /// Changing the language will clear your cart. Are you sure you want to proceed?
  case ChangingTheLanguageWillClearYourCart
  /// Typing
  case Typing
  /// Offline
  case Offline
  /// Email
  case Email
  /// Not rated yet
  case NotRatedYet
  /// Search
  case Search
  /// Other
  case Other
  /// Status
  case Status
  /// Loyalty Points Type
  case LoyaltyPointsType
  /// Payment Method
  case PaymentMethod
  /// Terms & Conditions
  case TermsAndConditions
  /// About Us
  case AboutUs
  /// Sort
  case Sort
  /// Open
  case Open
  /// Adding products from different suppliers will clear your cart.
  case AddingProductsFromDiffrentSuppliersWillClearYourCart
  /// Min. Order Amount
  case MinOrderAmount
  /// Min. Delviery Time
  case MinDelvieryTime
  /// Home Service
  case HOMESERVICE
  /// At Place Service
  case ATPLACESERVICE
  /// Select service
  case SelectService
  /// Ladies Beauty Salon
  case LadiesBeautySalon
  /// City
  case City
  /// Area
  case Area
  /// House No.
  case HouseNo
  /// Compare Products
  case CompareProducts
  /// Somewhere, Somehow, Something Went Wrong
  case SomewhereSomehowSomethingWentWrong
  /// Adding products from promotions will clear your cart.
  case AddingProductsFromPromotionsWillClearYourCart
  /// Please enter your house no.
  case PleaseEnterYourHouseNo
  /// Please enter your building name
  case PleaseEnterYourBuildingName
  /// Please select your location
  case PleaseSelectYourLocation
  /// Please enter a landmark name
  case PleaseEnterALandmarkName
  /// Please enter your city
  case PleaseEnterYourCity
  /// Please enter your counrty
  case PleaseEnterYourCounrty
  /// My Orders
  case MyOrders
  ///  not available 
  case NotAvailable
  /// Your Order will be confirmed during next supplier working hours/day.
  case YourOrderWillBeConfirmedDuringNextSupplierWorkingHoursDay
  /// Changing the current area will clear you cart.
  case ChangingTheCurrentAreaWillClearYouCart
  /// Change pick up time no suppliers are available for this pickup timing
  case ChangePickUpTimeNoSuppliersAreAvailableForThisPickupTiming
  /// No supplier found!
  case NoSupplierFound
  /// My Addresses
  case MyAddresses
  /// Item Detail
  case ItemDetail
  /// Scheduled Orders
  case ScheduledOrders
  /// Sorry, not enough points to redeem.
  case SorryNotEnoughPointsToRedeem
  /// Loading
  case Loading
  /// Looks like your order has been delivered. Would you like to rate your order?
  case LooksLikeYourOrderHasBeenDeliveredWouldYouLikeToRateYourOrder
  /// Rate Order
  case RateOrder
  /// Quantity : 
  case Quantity
  /// Delivered on
  case DeliveredOn
  /// Pending Orders
  case PendingOrders
  /// End
  case IosZDCChatEnd
  /// Email address
  case IosZDCChatEmailPlaceholder
  /// Message
  case IosZDCChatPreChatFormMessagePlaceholder
  /// File type not permitted
  case IosZDCChatUploadErrorType
  /// Could not connect
  case IosZDCChatCantConnectTitle
  /// %@ needs access to your photos
  case IosZDCChatAccessGallery(String)
  /// No agents available
  case IosZDCChatNoAgentsTitle
  /// Back
  case IosZDCChatBackButton
  /// Fields marked with * are required
  case IosZDCChatPreChatFormRequired
  /// Failed to download. Tap to retry.
  case IosZDCChatDownloadFailedMessage
  /// Please enter a valid email address
  case IosZDCChatPreChatFormInvalidEmail
  /// No connection
  case IosZDCChatNoConnectionTitle
  /// Could not send message
  case IosZDCChatSendOfflineMessageErrorTitle
  /// Leave a comment
  case IosZDCChatRatingCommentTitle
  /// Save image
  case IosZDCChatImageViewerSaveButton
  /// Reconnecting...
  case IosZDCChatReconnecting
  /// email@address.com
  case IosZDCChatTranscriptEmailAlertEmailPlaceholder
  /// Cancel
  case IosZDCChatCancelButton
  /// Enable this from the home screen, Settings > %@
  case IosZDCChatAccessHowto(String)
  /// OK
  case IosZDCChatOk
  /// No
  case IosZDCChatNo
  /// Connection lost
  case IosZDCChatChatConnectionLostTitle
  /// %@ *
  case IosZDCChatPreChatFormRequiredTemplate(String)
  /// Done
  case IosZDCChatDone
  /// Before you end this chat, would you like to email a transcript?
  case IosZDCChatTranscriptEmailAlertMessage
  /// Error accessing file
  case IosZDCChatUploadErrorAccess
  /// Please wait for an agent. There are currently %@ visitor(s) waiting to be served.
  case IosZDCChatVisitorQueue(String)
  /// No Internet connection. Please try again when connected
  case IosZDCChatNoConnectionMessage
  /// Name
  case IosZDCChatPreChatFormNamePlaceholder
  /// %@ needs access to your camera
  case IosZDCChatAccessCamera(String)
  /// Send
  case IosZDCChatChatTextEntrySendButton
  /// Retry
  case IosZDCChatRetry
  /// Starting chat...
  case IosZDCChatChatStartingChatMessage
  /// Message
  case IosZDCChatChatTextEntryPlaceholderText
  /// There are no agents currently online.
  case IosZDCChatAgentsOfflineMessage
  /// What can we help you with?
  case IosZDCChatPreChatFormDepartmentPlaceholder
  /// %@ left the chat
  case IosZDCChatAgentLeft(String)
  /// Would you like to retry?
  case IosZDCChatSendOfflineMessageErrorMessage
  /// Failed to send. Tap to retry.
  case IosZDCChatUnsentMessage
  /// Phone number
  case IosZDCChatPreChatFormPhonePlaceholder
  /// Sorry, there are no agents available to chat. Please try again later or leave us a message.
  case IosZDCChatNoAgentsMessage
  /// We've not heard from you for a while so this chat session has been closed. Please start a new chat if you still have questions.
  case IosZDCChatTimeoutMessage
  /// Send
  case IosZDCChatTranscriptEmailAlertSendButton
  /// Take photo
  case IosZDCChatUploadSourceCamera
  /// Leave a comment...
  case IosZDCChatRatingCommentButton
  /// No connection
  case IosZDCChatNetworkConnectionError
  /// Sorry, we can't connect you right now. Please try again later.
  case IosZDCChatCantConnectMessage
  /// End chat
  case IosZDCChatChatEndedTitle
  /// File size too large
  case IosZDCChatUploadErrorSize
  /// Please enter a valid phone number
  case IosZDCChatPreChatFormInvalidPhone
  /// Send
  case IosZDCChatSendOfflineMessageErrorSendButton
  /// Email a transcript
  case IosZDCChatTranscriptEmailAlertTitle
  /// End chat
  case IosZDCChatEndButton
  /// Are you sure you would like to end this chat?
  case IosZDCChatChatEndedMessage
  /// Unable to send message.
  case IosZDCChatOfflineMessageFailedMessage
  /// Leave a comment
  case IosZDCChatRatingCommentPlaceholder
  /// %@ joined the chat
  case IosZDCChatAgentJoined(String)
  /// Leave a message
  case IosZDCChatNoAgentsButton
  /// Message
  case IosZDCChatMessageButton
  /// Chat
  case IosZDCChatTitle
  /// Next
  case IosZDCChatNextButton
  /// Cancel
  case IosZDCChatCancel
  /// There are currently no agents online. Would you like to send a message?
  case IosZDCChatAccountOfflineMessage
  /// Photo library
  case IosZDCChatUploadSourceGallery
  /// We have been unable to reconnect. Do you wish to continue trying?
  case IosZDCChatChatConnectionLostMessage
  /// Rate this chat
  case IosZDCChatRatingTitle
  /// Edit comment...
  case IosZDCChatRatingEditButton
  /// Yes
  case IosZDCChatYes
  /// Don't send
  case IosZDCChatTranscriptEmailAlertDontSendButton
  /// no connection, please check the internet connection
  case InternetConnectionMessage
  /// Sorry, something went wrong please try again!
  case GeneralError
  /// Cancel this payment?
  case TitleMessage
  /// Invalid card number
  case InvaildCard
  /// NO
  case NoBtn
  /// Done
  case DoneBtn
  /// Ok
  case OkBtn
  /// Credit Card number must consist of 16 digits.
  case InvaildCardNumber
  /// Pay
  case PayBtn
  /// Init a secure connection...
  case InitConn
  /// Your Receipt
  case YourReceiptLbl
  /// Required field,cannot be left empty
  case PfCancelRequiredField
  /// Credit Card
  case TitleviewLbl
  /// Alert
  case AlertTitle
  /// EXPIRY DATE
  case ExpDateLbl
  /// The entered credit card type does not match the selected payment option.
  case PfErrorsCardNumberMismatchPo
  /// CARDHOLDER NAME
  case CardNamePl
  /// the date in the past
  case PASTDATEMSG
  /// CARD NUMBER
  case CardNumberPl
  /// Month & Year
  case MonthyearLbl
  /// CVV
  case CVCtxt
  /// SAVE THIS CARD
  case SaveCarLbl
  /// technical problem
  case TechnicalIssue
  /// YES
  case YesBtn
  /// Great
  case PfRespPageGreat
  /// Failed
  case PfRespPageFailed
  /// Invalid Expiry Date
  case InvalidExp
  /// Invalid CVV
  case InvalidCVV
  /// Expected Delivery On
  case ExpectedDeliveryOn
  /// By signing up you agree to the 
  case BySigningUpYouAgreeToThe
  /// Privacy Policy
  case PrivacyPolicy
  /// Terms and conditions.
  case TermsAndConditionsSignUp
  ///  and 
  case And
  /// I would like to recommend using
  case IWouldLikeToRecommendUsing
  /// via Clikat
  case ViaClikat
  /// Warning
  case Warning
  /// Please select dates to schedule.
  case PleaseSelectDatesToSchedule
  /// Bronze
  case Bronze
  /// Notification Language Changed Successfully
  case NotificationLanguageChangedSuccessfully
  /// Order Confirmed Successfully
  case OrderConfirmedSuccessfully
  /// Delivery on
  case DeliveryOn
  /// No Product Found!
  case NoProductFound
  /// Have you Forgot Completing Your Last Shopping Cart?
  case HaveYouForgotCompletingYourLastShoppingCart
  /// Supplier Rated Successfully
  case SupplierRatedSuccessfully
  /// Search for product
  case SearchForProduct
  /// Send
  case Send
  /// Sub Total
  case SubTotal
  /// Camera Unavailable
  case CameraUnavailable
  /// It looks like your privacy settings are preventing us from accessing your camera.
  case ItLooksLikeYourPrivacySettingsArePreventingUsFromAccessingYourCamera
  /// Location Unavailable
  case LocationUnavailable
  /// Please check to see if you have enabled location services.
  case PleaseCheckToSeeIfYouHaveEnabledLocationServices
}
// swiftlint:enable type_body_length

extension L10n: CustomStringConvertible {
  var description: String { return self.string }

  var string: String {
    switch self {
      case .ServiceCharge:
        return L10n.tr("Service Charge")
      case .MinServiceTime:
        return L10n.tr("Min. Service Time")
      case .VisitingCharges:
        return L10n.tr("Visiting Charges")
      case .MinDeliveryTime:
        return L10n.tr("Min. Delivery Time")
      case .OpensAt:
        return L10n.tr("Opens at ")
      case .PleaseSelectAnAddress:
        return L10n.tr("Please select an Address")
      case .CreditDebitCard:
        return L10n.tr("Credit/Debit card")
      case .YourCartHasNoItemsPleaseAddItemsToCartToProceed:
        return L10n.tr("Your Cart has no items.Please add items to cart to Proceed.")
      case .Landmark:
        return L10n.tr("Landmark")
      case .AddressLineFirst:
        return L10n.tr("Address Line First")
      case .AddressLineSecond:
        return L10n.tr("Address Line Second")
      case .Pincode:
        return L10n.tr("Pincode")
      case .DeliveryAddress:
        return L10n.tr("Delivery Address")
      case .EnterYourDetails:
        return L10n.tr("Enter your details.")
      case .DeliveryChargesApplicableAccordingly:
        return L10n.tr("Delivery charges applicable accordingly")
      case .DeliveryCharges:
        return L10n.tr("Delivery charges")
      case .SelectTimeAndDate:
        return L10n.tr("Select Time and Date")
      case .PleaseSelectABookingScheduleAndTime:
        return L10n.tr("Please select a booking schedule and time")
      case .SelectBookingTime:
        return L10n.tr("Select booking time")
      case .ReOrderingWillClearYouCart:
        return L10n.tr("ReOrdering will clear you cart")
      case .PENDING:
        return L10n.tr("PENDING")
      case .DELIVERED:
        return L10n.tr("DELIVERED")
      case .CONFIRMED:
        return L10n.tr("CONFIRMED")
      case .FEEDBACKGIVEN:
        return L10n.tr("FEEDBACKGIVEN")
      case .REJECTED:
        return L10n.tr("REJECTED")
      case .SHIPPED:
        return L10n.tr("SHIPPED")
      case .NEARBY:
        return L10n.tr("NEARBY")
      case .TRACKED:
        return L10n.tr("TRACKED")
      case .CUSTOMERCANCELLED:
        return L10n.tr("CUSTOMER CANCELLED")
      case .SCHEDULED:
        return L10n.tr("SCHEDULED")
      case .OrderDetails:
        return L10n.tr("Order Details")
      case .Items:
        return L10n.tr("items")
      case .REORDER:
        return L10n.tr("REORDER")
      case .TRACK:
        return L10n.tr("TRACK")
      case .BOOKED:
        return L10n.tr("BOOKED")
      case .CANCELORDER:
        return L10n.tr("CANCEL ORDER")
      case .CONFIRMORDER:
        return L10n.tr("CONFIRM ORDER")
      case .Monthly:
        return L10n.tr("Monthly")
      case .Weekly:
        return L10n.tr("Weekly")
      case .Pickup:
        return L10n.tr("Pickup")
      case .PleaseEnterYourEmailAddress:
        return L10n.tr("Please enter your email address")
      case .PleaseEnterAValidEmailAddress:
        return L10n.tr("Please enter a valid email address")
      case .PleaseEnterYourPassword:
        return L10n.tr("Please enter your password")
      case .PasswordShouldBeMinimum6Characters:
        return L10n.tr("Password should be minimum 6 characters.")
      case .SessionExpiredLoginToContinue:
        return L10n.tr("Session expired login to continue")
      case .OTPSent:
        return L10n.tr("OTP Sent")
      case .SelectPicture:
        return L10n.tr("Select picture")
      case .PleaseSelectYourProfilePicture:
        return L10n.tr("Please select your profile picture")
      case .PleaseEnterYourName:
        return L10n.tr("Please enter your name")
      case .Camera:
        return L10n.tr("Camera")
      case .PhotoLibrary:
        return L10n.tr("Photo Library")
      case .Home:
        return L10n.tr("Home")
      case .LiveSupport:
        return L10n.tr("Live support")
      case .Cart:
        return L10n.tr("Cart")
      case .Promotions:
        return L10n.tr("Promotions")
      case .Notifications:
        return L10n.tr("Notifications")
      case .MyAccount:
        return L10n.tr("My Account")
      case .MyFavorites:
        return L10n.tr("My favorites")
      case .OrderHistory:
        return L10n.tr("Order history")
      case .TrackMyOrder:
        return L10n.tr("Track my order")
      case .RateMyOrder:
        return L10n.tr("Rate my order")
      case .UpcomingOrders:
        return L10n.tr("Upcoming orders")
      case .LoyalityPoints:
        return L10n.tr("Loyality points")
      case .ShareApp:
        return L10n.tr("Share app")
      case .Settings:
        return L10n.tr("Settings")
      case .Guest:
        return L10n.tr("Guest")
      case .Welcome:
        return L10n.tr("Welcome")
      case .Points:
        return L10n.tr("Points")
      case .Grocery:
        return L10n.tr("Grocery")
      case .Laundry:
        return L10n.tr("Laundry")
      case .Household:
        return L10n.tr("Household")
      case .Flowers:
        return L10n.tr("Flowers")
      case .Fitness:
        return L10n.tr("Fitness")
      case .Photography:
        return L10n.tr("Photography")
      case .BabySitter:
        return L10n.tr("Baby sitter")
      case .Cleaning:
        return L10n.tr("Cleaning")
      case .Party:
        return L10n.tr("Party")
      case .BeautySalon:
        return L10n.tr("Beauty salon")
      case .Medicines:
        return L10n.tr("Medicines")
      case .WaterDelivery:
        return L10n.tr("Water delivery")
      case .Packages:
        return L10n.tr("Packages")
      case .OK:
        return L10n.tr("OK")
      case .Save:
        return L10n.tr("Save")
      case .Cancel:
        return L10n.tr("Cancel")
      case .ChooseBookingCycle:
        return L10n.tr("Choose booking cycle")
      case .Reviews:
        return L10n.tr("reviews")
      case .AED:
        return L10n.tr("AED")
      case .Mins:
        return L10n.tr("mins")
      case .ResultsFor:
        return L10n.tr("Results for ")
      case .Days:
        return L10n.tr("days")
      case .Hours:
        return L10n.tr("Hours")
      case .Discoverability:
        return L10n.tr("Discoverability")
      case .Delivery:
        return L10n.tr("Delivery")
      case .SupplierType:
        return L10n.tr("Supplier Type")
      case .Rating:
        return L10n.tr("Rating")
      case ._1Star:
        return L10n.tr("1 Star")
      case ._2Star:
        return L10n.tr("2 Star")
      case ._3Star:
        return L10n.tr("3 Star")
      case ._4StarAndAbove:
        return L10n.tr("4 Star and above")
      case .Gold:
        return L10n.tr("Gold")
      case .Silver:
        return L10n.tr("Silver")
      case .Platinum:
        return L10n.tr("Platinum")
      case .CashOnDelivery:
        return L10n.tr("Cash on delivery")
      case .Card:
        return L10n.tr("Card")
      case .Both:
        return L10n.tr("Both")
      case .Online:
        return L10n.tr("Online")
      case .Busy:
        return L10n.tr("Busy")
      case .Closed:
        return L10n.tr("Closed")
      case .Cancelled:
        return L10n.tr("Cancelled")
      case .ChangePassword:
        return L10n.tr("Change Password")
      case .NotificationsLanguage:
        return L10n.tr("Notifications Language")
      case .ManageAddress:
        return L10n.tr("Manage Address")
      case .Logout:
        return L10n.tr("Logout")
      case .SelectNotificationLanguage:
        return L10n.tr("Select notification language")
      case .English:
        return L10n.tr("English")
      case .Arabic:
        return L10n.tr("Arabic")
      case .YouHavenTEarnedAnyLoyaltyPointsYet:
        return L10n.tr("You haven't earned any loyalty points yet")
      case .SelectProdutsFromSameSupplier:
        return L10n.tr("Select produts from same supplier")
      case .NoRemarks:
        return L10n.tr("No Remarks")
      case .WhenDoYouWantTheService:
        return L10n.tr("When do you want the service")
      case .PickupLocation:
        return L10n.tr("Pickup location")
      case .SelectPickupDateAndTime:
        return L10n.tr("Select pickup date and time")
      case .AreYouSureYouWantToDeleteThisAddress:
        return L10n.tr("Are you sure you want to delete this address")
      case .SelectCountry:
        return L10n.tr("Select Country")
      case .SelectCity:
        return L10n.tr("Select City")
      case .SelectZone:
        return L10n.tr("Select Zone")
      case .SelectArea:
        return L10n.tr("Select Area")
      case .PleaseSelectAllFieldsAbove:
        return L10n.tr("Please select all fields above")
      case .PleaseSelectACountry:
        return L10n.tr("Please select a country")
      case .PleaseSelectACity:
        return L10n.tr("Please select a city")
      case .PleaseFillAllDetails:
        return L10n.tr("Please fill all details")
      case .PleaseEnterValidPincode:
        return L10n.tr("Please enter valid Pincode")
      case .OldPassword:
        return L10n.tr("Old Password")
      case .NewPassword:
        return L10n.tr("New Password")
      case .ConfirmPassword:
        return L10n.tr("Confirm Password")
      case .NewPasswordMustNotBeSameAsOldPassword:
        return L10n.tr("New Password must not be same as old password")
      case .PasswordsDoNotMatch:
        return L10n.tr("Passwords do not match")
      case .ForgotPassword:
        return L10n.tr("Forgot Password")
      case .PasswordRecoveryHasBeenSentToYourEmailId:
        return L10n.tr("Password recovery has been sent to your email id")
      case .TheLeadingOnlineHomeServicesInUAE:
        return L10n.tr("The Leading Online Home Services In UAE")
      case .PleaseCheckYourInternetConnection:
        return L10n.tr("Please check your internet connection")
      case .CancelOrder:
        return L10n.tr("Cancel Order")
      case .Success:
        return L10n.tr("Success")
      case .DoYouReallyWantToCancelThisOrder:
        return L10n.tr("Do you really want to cancel this order")
      case .YouHaveCancelledYourOrderSuccessfully:
        return L10n.tr("You have cancelled your order successfully")
      case .LogOut:
        return L10n.tr("LogOut")
      case .AreYouSureYouWantToLogout:
        return L10n.tr("Are you sure you want to logout")
      case .YouAreSuccessfullyLoggedOut:
        return L10n.tr("You are successfully logged out")
      case .PleaseEnterValidCountryCodeFollwoedByPhoneNumber:
        return L10n.tr("Please enter valid country code follwoed by phone number")
      case .DeliverySpeed:
        return L10n.tr("Delivery Speed")
      case .SorryYourOrderIsBelowMinimumOrderPrice:
        return L10n.tr("Sorry… Your Order is Below Minimum Order Price.")
      case .Recommended:
        return L10n.tr("Recommended")
      case .Offers:
        return L10n.tr("Offers")
      case .YourOrderHaveBeenPlacedSuccessfully:
        return L10n.tr("Your order have been placed successfully")
      case .OrderPlacedSuccessfully:
        return L10n.tr("Order placed successfully")
      case .YourOrderHaveBeenSheduledSuccessfully:
        return L10n.tr("Your order have been sheduled successfully")
      case .AreYouSure:
        return L10n.tr("Are you sure")
      case .ChangingTheLanguageWillClearYourCart:
        return L10n.tr("Changing the language will clear your cart.")
      case .Typing:
        return L10n.tr("Typing")
      case .Offline:
        return L10n.tr("Offline")
      case .Email:
        return L10n.tr("Email")
      case .NotRatedYet:
        return L10n.tr("Not rated yet")
      case .Search:
        return L10n.tr("Search")
      case .Other:
        return L10n.tr("Other")
      case .Status:
        return L10n.tr("Status")
      case .LoyaltyPointsType:
        return L10n.tr("Loyalty Points Type")
      case .PaymentMethod:
        return L10n.tr("Payment Method")
      case .TermsAndConditions:
        return L10n.tr("Terms and Conditions")
      case .AboutUs:
        return L10n.tr("About Us")
      case .Sort:
        return L10n.tr("Sort")
      case .Open:
        return L10n.tr("Open")
      case .AddingProductsFromDiffrentSuppliersWillClearYourCart:
        return L10n.tr("Adding products from diffrent suppliers will clear your cart")
      case .MinOrderAmount:
        return L10n.tr("Min. Order Amount")
      case .MinDelvieryTime:
        return L10n.tr("Min. Delviery Time")
      case .HOMESERVICE:
        return L10n.tr("HOME SERVICE")
      case .ATPLACESERVICE:
        return L10n.tr("AT PLACE SERVICE")
      case .SelectService:
        return L10n.tr("Select service")
      case .LadiesBeautySalon:
        return L10n.tr("Ladies Beauty Salon")
      case .City:
        return L10n.tr("City")
      case .Area:
        return L10n.tr("Area")
      case .HouseNo:
        return L10n.tr("House No")
      case .CompareProducts:
        return L10n.tr("Compare Products")
      case .SomewhereSomehowSomethingWentWrong:
        return L10n.tr("Somewhere Somehow Something Went Wrong")
      case .AddingProductsFromPromotionsWillClearYourCart:
        return L10n.tr("Adding products from promotions will clear your cart")
      case .PleaseEnterYourHouseNo:
        return L10n.tr("Please enter your house no.")
      case .PleaseEnterYourBuildingName:
        return L10n.tr("Please enter your building name")
      case .PleaseSelectYourLocation:
        return L10n.tr("Please select your location")
      case .PleaseEnterALandmarkName:
        return L10n.tr("Please enter a landmark name")
      case .PleaseEnterYourCity:
        return L10n.tr("Please enter your city")
      case .PleaseEnterYourCounrty:
        return L10n.tr("Please enter your counrty")
      case .MyOrders:
        return L10n.tr("My Orders")
      case .NotAvailable:
        return L10n.tr("Not Available")
      case .YourOrderWillBeConfirmedDuringNextSupplierWorkingHoursDay:
        return L10n.tr("Your Order will be confirmed during next supplier working hours/day.")
      case .ChangingTheCurrentAreaWillClearYouCart:
        return L10n.tr("Changing the current area will clear you cart")
      case .ChangePickUpTimeNoSuppliersAreAvailableForThisPickupTiming:
        return L10n.tr("Change pick up time no suppliers are available for this pickup timing")
      case .NoSupplierFound:
        return L10n.tr("No supplier found")
      case .MyAddresses:
        return L10n.tr("My Addresses")
      case .ItemDetail:
        return L10n.tr("Item Detail")
      case .ScheduledOrders:
        return L10n.tr("Scheduled Orders")
      case .SorryNotEnoughPointsToRedeem:
        return L10n.tr("Sorry not enough points to redeem")
      case .Loading:
        return L10n.tr("Loading")
      case .LooksLikeYourOrderHasBeenDeliveredWouldYouLikeToRateYourOrder:
        return L10n.tr("Looks like your order has been delivered Would you like to rate your order")
      case .RateOrder:
        return L10n.tr("Rate Order")
      case .Quantity:
        return L10n.tr("Quantity")
      case .DeliveredOn:
        return L10n.tr("Delivered on")
      case .PendingOrders:
        return L10n.tr("Pending Orders")
      case .IosZDCChatEnd:
        return L10n.tr("ios.ZDCChat.end")
      case .IosZDCChatEmailPlaceholder:
        return L10n.tr("ios.ZDCChat.emailPlaceholder")
      case .IosZDCChatPreChatFormMessagePlaceholder:
        return L10n.tr("ios.ZDCChat.preChatForm.messagePlaceholder")
      case .IosZDCChatUploadErrorType:
        return L10n.tr("ios.ZDCChat.upload.error.type")
      case .IosZDCChatCantConnectTitle:
        return L10n.tr("ios.ZDCChat.cantConnectTitle")
      case .IosZDCChatAccessGallery(let p0):
        return L10n.tr("ios.ZDCChat.access.gallery", p0)
      case .IosZDCChatNoAgentsTitle:
        return L10n.tr("ios.ZDCChat.noAgentsTitle")
      case .IosZDCChatBackButton:
        return L10n.tr("ios.ZDCChat.backButton")
      case .IosZDCChatPreChatFormRequired:
        return L10n.tr("ios.ZDCChat.preChatForm.required")
      case .IosZDCChatDownloadFailedMessage:
        return L10n.tr("ios.ZDCChat.download.failedMessage")
      case .IosZDCChatPreChatFormInvalidEmail:
        return L10n.tr("ios.ZDCChat.preChatForm.invalidEmail")
      case .IosZDCChatNoConnectionTitle:
        return L10n.tr("ios.ZDCChat.noConnectionTitle")
      case .IosZDCChatSendOfflineMessageErrorTitle:
        return L10n.tr("ios.ZDCChat.sendOfflineMessageError.title")
      case .IosZDCChatRatingCommentTitle:
        return L10n.tr("ios.ZDCChat.rating.comment.title")
      case .IosZDCChatImageViewerSaveButton:
        return L10n.tr("ios.ZDCChat.imageViewer.saveButton")
      case .IosZDCChatReconnecting:
        return L10n.tr("ios.ZDCChat.reconnecting")
      case .IosZDCChatTranscriptEmailAlertEmailPlaceholder:
        return L10n.tr("ios.ZDCChat.transcriptEmailAlert.emailPlaceholder")
      case .IosZDCChatCancelButton:
        return L10n.tr("ios.ZDCChat.cancelButton")
      case .IosZDCChatAccessHowto(let p0):
        return L10n.tr("ios.ZDCChat.access.howto", p0)
      case .IosZDCChatOk:
        return L10n.tr("ios.ZDCChat.ok")
      case .IosZDCChatNo:
        return L10n.tr("ios.ZDCChat.no")
      case .IosZDCChatChatConnectionLostTitle:
        return L10n.tr("ios.ZDCChat.chatConnectionLost.title")
      case .IosZDCChatPreChatFormRequiredTemplate(let p0):
        return L10n.tr("ios.ZDCChat.preChatForm.requiredTemplate", p0)
      case .IosZDCChatDone:
        return L10n.tr("ios.ZDCChat.done")
      case .IosZDCChatTranscriptEmailAlertMessage:
        return L10n.tr("ios.ZDCChat.transcriptEmailAlert.message")
      case .IosZDCChatUploadErrorAccess:
        return L10n.tr("ios.ZDCChat.upload.error.access")
      case .IosZDCChatVisitorQueue(let p0):
        return L10n.tr("ios.ZDCChat.visitorQueue", p0)
      case .IosZDCChatNoConnectionMessage:
        return L10n.tr("ios.ZDCChat.noConnectionMessage")
      case .IosZDCChatPreChatFormNamePlaceholder:
        return L10n.tr("ios.ZDCChat.preChatForm.namePlaceholder")
      case .IosZDCChatAccessCamera(let p0):
        return L10n.tr("ios.ZDCChat.access.camera", p0)
      case .IosZDCChatChatTextEntrySendButton:
        return L10n.tr("ios.ZDCChat.chatTextEntry.sendButton")
      case .IosZDCChatRetry:
        return L10n.tr("ios.ZDCChat.retry")
      case .IosZDCChatChatStartingChatMessage:
        return L10n.tr("ios.ZDCChat.chat.startingChatMessage")
      case .IosZDCChatChatTextEntryPlaceholderText:
        return L10n.tr("ios.ZDCChat.chatTextEntry.placeholderText")
      case .IosZDCChatAgentsOfflineMessage:
        return L10n.tr("ios.ZDCChat.agentsOffline.message")
      case .IosZDCChatPreChatFormDepartmentPlaceholder:
        return L10n.tr("ios.ZDCChat.preChatForm.departmentPlaceholder")
      case .IosZDCChatAgentLeft(let p0):
        return L10n.tr("ios.ZDCChat.agentLeft", p0)
      case .IosZDCChatSendOfflineMessageErrorMessage:
        return L10n.tr("ios.ZDCChat.sendOfflineMessageError.message")
      case .IosZDCChatUnsentMessage:
        return L10n.tr("ios.ZDCChat.unsentMessage")
      case .IosZDCChatPreChatFormPhonePlaceholder:
        return L10n.tr("ios.ZDCChat.preChatForm.phonePlaceholder")
      case .IosZDCChatNoAgentsMessage:
        return L10n.tr("ios.ZDCChat.noAgentsMessage")
      case .IosZDCChatTimeoutMessage:
        return L10n.tr("ios.ZDCChat.timeoutMessage")
      case .IosZDCChatTranscriptEmailAlertSendButton:
        return L10n.tr("ios.ZDCChat.transcriptEmailAlert.sendButton")
      case .IosZDCChatUploadSourceCamera:
        return L10n.tr("ios.ZDCChat.upload.source.camera")
      case .IosZDCChatRatingCommentButton:
        return L10n.tr("ios.ZDCChat.rating.commentButton")
      case .IosZDCChatNetworkConnectionError:
        return L10n.tr("ios.ZDCChat.network.connectionError")
      case .IosZDCChatCantConnectMessage:
        return L10n.tr("ios.ZDCChat.cantConnectMessage")
      case .IosZDCChatChatEndedTitle:
        return L10n.tr("ios.ZDCChat.chatEndedTitle")
      case .IosZDCChatUploadErrorSize:
        return L10n.tr("ios.ZDCChat.upload.error.size")
      case .IosZDCChatPreChatFormInvalidPhone:
        return L10n.tr("ios.ZDCChat.preChatForm.invalidPhone")
      case .IosZDCChatSendOfflineMessageErrorSendButton:
        return L10n.tr("ios.ZDCChat.sendOfflineMessageError.sendButton")
      case .IosZDCChatTranscriptEmailAlertTitle:
        return L10n.tr("ios.ZDCChat.transcriptEmailAlert.title")
      case .IosZDCChatEndButton:
        return L10n.tr("ios.ZDCChat.endButton")
      case .IosZDCChatChatEndedMessage:
        return L10n.tr("ios.ZDCChat.chatEndedMessage")
      case .IosZDCChatOfflineMessageFailedMessage:
        return L10n.tr("ios.ZDCChat.offlineMessageFailed.message")
      case .IosZDCChatRatingCommentPlaceholder:
        return L10n.tr("ios.ZDCChat.rating.comment.placeholder")
      case .IosZDCChatAgentJoined(let p0):
        return L10n.tr("ios.ZDCChat.agentJoined", p0)
      case .IosZDCChatNoAgentsButton:
        return L10n.tr("ios.ZDCChat.noAgentsButton")
      case .IosZDCChatMessageButton:
        return L10n.tr("ios.ZDCChat.messageButton")
      case .IosZDCChatTitle:
        return L10n.tr("ios.ZDCChat.title")
      case .IosZDCChatNextButton:
        return L10n.tr("ios.ZDCChat.nextButton")
      case .IosZDCChatCancel:
        return L10n.tr("ios.ZDCChat.cancel")
      case .IosZDCChatAccountOfflineMessage:
        return L10n.tr("ios.ZDCChat.accountOffline.message")
      case .IosZDCChatUploadSourceGallery:
        return L10n.tr("ios.ZDCChat.upload.source.gallery")
      case .IosZDCChatChatConnectionLostMessage:
        return L10n.tr("ios.ZDCChat.chatConnectionLost.message")
      case .IosZDCChatRatingTitle:
        return L10n.tr("ios.ZDCChat.rating.title")
      case .IosZDCChatRatingEditButton:
        return L10n.tr("ios.ZDCChat.rating.editButton")
      case .IosZDCChatYes:
        return L10n.tr("ios.ZDCChat.yes")
      case .IosZDCChatTranscriptEmailAlertDontSendButton:
        return L10n.tr("ios.ZDCChat.transcriptEmailAlert.dontSendButton")
      case .InternetConnectionMessage:
        return L10n.tr("internetConnectionMessage")
      case .GeneralError:
        return L10n.tr("general_error")
      case .TitleMessage:
        return L10n.tr("titleMessage")
      case .InvaildCard:
        return L10n.tr("InvaildCard")
      case .NoBtn:
        return L10n.tr("noBtn")
      case .DoneBtn:
        return L10n.tr("doneBtn")
      case .OkBtn:
        return L10n.tr("okBtn")
      case .InvaildCardNumber:
        return L10n.tr("InvaildCardNumber")
      case .PayBtn:
        return L10n.tr("PayBtn")
      case .InitConn:
        return L10n.tr("Init_conn")
      case .YourReceiptLbl:
        return L10n.tr("YourReceiptLbl")
      case .PfCancelRequiredField:
        return L10n.tr("pf_cancel_required_field")
      case .TitleviewLbl:
        return L10n.tr("titleviewLbl")
      case .AlertTitle:
        return L10n.tr("alertTitle")
      case .ExpDateLbl:
        return L10n.tr("ExpDateLbl")
      case .PfErrorsCardNumberMismatchPo:
        return L10n.tr("pf_errors_card_number_mismatch_po")
      case .CardNamePl:
        return L10n.tr("CardNamePl")
      case .PASTDATEMSG:
        return L10n.tr("PAST_DATE_MSG")
      case .CardNumberPl:
        return L10n.tr("CardNumberPl")
      case .MonthyearLbl:
        return L10n.tr("monthyearLbl")
      case .CVCtxt:
        return L10n.tr("CVCtxt")
      case .SaveCarLbl:
        return L10n.tr("saveCarLbl")
      case .TechnicalIssue:
        return L10n.tr("TechnicalIssue")
      case .YesBtn:
        return L10n.tr("yesBtn")
      case .PfRespPageGreat:
        return L10n.tr("pf_resp_page_great")
      case .PfRespPageFailed:
        return L10n.tr("pf_resp_page_failed")
      case .InvalidExp:
        return L10n.tr("InvalidExp")
      case .InvalidCVV:
        return L10n.tr("InvalidCVV")
      case .ExpectedDeliveryOn:
        return L10n.tr("Expected Delivery On")
      case .BySigningUpYouAgreeToThe:
        return L10n.tr("By signing up you agree to the ")
      case .PrivacyPolicy:
        return L10n.tr("Privacy Policy")
      case .TermsAndConditionsSignUp:
        return L10n.tr("Terms and conditionsSignUp")
      case .And:
        return L10n.tr("and")
      case .IWouldLikeToRecommendUsing:
        return L10n.tr("I would like to recommend using")
      case .ViaClikat:
        return L10n.tr("via Clikat")
      case .Warning:
        return L10n.tr("Warning")
      case .PleaseSelectDatesToSchedule:
        return L10n.tr("Please select dates to schedule.")
      case .Bronze:
        return L10n.tr("Bronze")
      case .NotificationLanguageChangedSuccessfully:
        return L10n.tr("Notification Language Changed Successfully")
      case .OrderConfirmedSuccessfully:
        return L10n.tr("Order Confirmed Successfully")
      case .DeliveryOn:
        return L10n.tr("Delivery on")
      case .NoProductFound:
        return L10n.tr("No Product Found!")
      case .HaveYouForgotCompletingYourLastShoppingCart:
        return L10n.tr("Have you Forgot Completing Your Last Shopping Cart?")
      case .SupplierRatedSuccessfully:
        return L10n.tr("Supplier Rated Successfully")
      case .SearchForProduct:
        return L10n.tr("Search for product")
      case .Send:
        return L10n.tr("Send")
      case .SubTotal:
        return L10n.tr("Sub Total")
      case .CameraUnavailable:
        return L10n.tr("Camera Unavailable")
      case .ItLooksLikeYourPrivacySettingsArePreventingUsFromAccessingYourCamera:
        return L10n.tr("It looks like your privacy settings are preventing us from accessing your camera.")
      case .LocationUnavailable:
        return L10n.tr("Location Unavailable")
      case .PleaseCheckToSeeIfYouHaveEnabledLocationServices:
        return L10n.tr("Please check to see if you have enabled location services.")
    }
  }

  private static func tr(key: String, _ args: CVarArgType...) -> String {
    let format = NSLocalizedString(key, comment: "")
    return String(format: format, locale: NSLocale.currentLocale(), arguments: args)
  }
}

func tr(key: L10n) -> String {
  return key.string
}
