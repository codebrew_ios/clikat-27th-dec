//
//  AppDelegate.swift
//  Clikat
//
//  Created by Night Reaper on 14/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManager
import GooglePlaces
import GoogleMaps
import GooglePlacePicker
import ZDCChat
import Fabric
import EZSwiftExtensions
import SwiftyJSON
//import Crashlytics
import YYWebImage
import AVFoundation
import Flurry_iOS_SDK
import Adjust

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var mainStoryboard: UIStoryboard?
    lazy var coreDataStack = CoreDataStack()
    
    let adjust = Adjust()
    //Sound
    var soundURL: NSURL?
    var soundID: SystemSoundID = 0
    var pushSound : AVAudioPlayer?
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        window?.backgroundColor = UIColor.whiteColor()
        switchViewControllers(isArabic: false)
        IQKeyboardManager.sharedManager().enable = true
        window?.makeKeyAndVisible()
        UIView.appearance().semanticContentAttribute = .ForceLeftToRight
        
        LocationManager.sharedInstance.startTrackingUser()
        GMSPlacesClient.provideAPIKey(GoogleApiKey)
        GMSServices.provideAPIKey(GoogleApiKey)
        
        Flurry.startSession("8XK9KF9J73KM3VSFDQNK")
//      Fabric.sharedSDK().debug = true
//      Fabric.with([Crashlytics.self])

        ChatStyling.applyStyling()
        ZDCChat.initializeWithAccountKey("48yGhCMMohSibuqyfDSSiEcx8VSsCT54")
        ZDCLog.enable(true)
//      ZDCLog.setLogLevel(.Warn)
        
        //Adjust
        let config = ADJConfig(appToken: "33g56qjx73k0", environment: ADJEnvironmentProduction)
        adjust.appDidLaunch(config)
        
        AdjustEvent.AppLaunch.sendEvent()
        //Push Notifications
        let type: UIUserNotificationType = [UIUserNotificationType.Badge, UIUserNotificationType.Alert, UIUserNotificationType.Sound]
        let setting = UIUserNotificationSettings(forTypes: type, categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(setting)
        UIApplication.sharedApplication().registerForRemoteNotifications()
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        //      DBManager.sharedManager.deleteAllData("Cart")
        
        
        QGSdk.getSharedInstance().onStart("1db3f71ee0b9c89bd37c", setDevProfile: false)
        
        QGSdk.getSharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        guard let pushNotification = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey] else { return true }
        
        GDataSingleton.sharedInstance.pushDict = pushNotification
        handlePushNavigation(JSON(pushNotification))
        //MARK: - Reachability
        if GDataSingleton.sharedInstance.tokenData != nil {
            QGSdk.getSharedInstance().setToken(GDataSingleton.sharedInstance.tokenData)
        }
        return true
    }
    
    
    func switchViewControllers(isArabic arabic : Bool){
        
        
        if arabic {
            UIView.appearance().semanticContentAttribute = .ForceRightToLeft
            UITextField.appearance()
            let navigationVc = StoryboardScene.Main.instantiateRightNavigationViewController()
            window?.rootViewController = navigationVc
            BundleLocalization.sharedInstance().language = Languages.Arabic
            Localize.setCurrentLanguage(Languages.Arabic)
            return
        }
        UIView.appearance().semanticContentAttribute = .ForceLeftToRight
        let navigationVc = StoryboardScene.Main.instantiateLeftNavigationViewController()
        window?.rootViewController = navigationVc
        BundleLocalization.sharedInstance().language = Languages.English
        Localize.setCurrentLanguage(Languages.English)
    }

    func applicationWillTerminate(application: UIApplication) {
        GDataSingleton.sharedInstance.showRatingPopUp = "1"
        coreDataStack.saveMainContext()
    }

    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        
        if url.scheme != FBUrlScheme {
            ez.runThisAfterDelay(seconds: 1, after: {
                NSNotificationCenter.defaultCenter().postNotificationName(UrlSchemeNotification, object: nil, userInfo:url.fragments)
            })
        }
       

        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    
    
   
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let notificationSettings : UIUserNotificationSettings = UIApplication.sharedApplication().currentUserNotificationSettings()!
        var _ = notificationSettings.types
        GDataSingleton.sharedInstance.tokenData = deviceToken
        QGSdk.getSharedInstance().setToken(deviceToken)
        let deviceToken = deviceToken.description.stringByReplacingOccurrencesOfString("<", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil).stringByReplacingOccurrencesOfString(">", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil).stringByReplacingOccurrencesOfString(" ", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        GDataSingleton.sharedInstance.deviceToken = deviceToken
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print(error.localizedDescription)
    }
}




extension NSURL {
    var fragments: [String: String] {
        var results = [String: String]()
        if let pairs : [String]? = self.query?.componentsSeparatedByString("&") where (pairs ?? []).count > 0  {
            for pair: String in pairs ?? [] {
                if let keyValue = pair.componentsSeparatedByString("=") as [String]?  where keyValue.count > 1{
                    results.updateValue(keyValue[1], forKey: keyValue[0])
                }
            }
            
        }
        return results
    }
}

enum AdjustEvent : String{
    
    case CategoryDetail = "apml81"
    case Reorder = "cnidc2"
    case Cart = "12tj4g"
    case DeepLink = "hnph1c"
    case ScheduleOrder = "k45hcz"
    case ProductDetail = "qf73yl"
    case AppLaunch = "6152w6"
    case Home = "5c4vvl"
    case LiveSupport = "snq5oe"
    case Promotions = "5q3kb0"
    case CompareProducts = "ccaf0v"
    case Favourites = "wnou78"
    case PendingOrders = "wfffh8"
    case ScheduledOrders = "gwo6r2"
    case TrackOrder = "3aag52"
    case RateOrder = "splkev"
    case LoyaltyPoints = "z27cqv"
    case OrderDetail = "uih164"
    case LoyaltyPointsOrder = "efsxes"
    
    case Login = "5ulx30"
    case Delivery = "ltfjve"
    case Order = "i390ad"
    case Purchase = "rcjjeu"
    case SignUp = "lqdpiu"
    
    func sendEvent(){
        let event = ADJEvent(eventToken: self.rawValue)
        (UIApplication.sharedApplication().delegate as? AppDelegate)?.adjust.trackEvent(event)
    }
    func sendEvent(revenue : Double?){
        let event = ADJEvent(eventToken: self.rawValue)
        event.setRevenue(/revenue, currency: "AED")
        (UIApplication.sharedApplication().delegate as? AppDelegate)?.adjust.trackEvent(event)
    }
}

