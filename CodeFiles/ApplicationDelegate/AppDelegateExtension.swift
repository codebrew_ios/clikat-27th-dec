//
//  AppDelegateExtension.swift
//  Clikat
//
//  Created by cblmacmini on 8/28/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit
import SwiftyJSON
import EZSwiftExtensions
//import LNNotificationsUI

extension AppDelegate{
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        QGSdk.getSharedInstance().application(application, didReceiveRemoteNotification: userInfo)
        handlePush(userInfo)
    }
    func handlePush(userInfo : [NSObject : AnyObject]?){
        guard let dict = userInfo else { return }
        let json = JSON(dict)
        if UIApplication.sharedApplication().applicationState == .Active {
            HDNotificationView.showNotificationViewWithImage(UIImage(asset: Asset.Ic_notification), title: "Clikat", message: userInfo?["message"] as? String, isAutoHide: true) { [weak self] in
                self?.pushSound?.stop()
                HDNotificationView.hideNotificationView()
                self?.handlePushNavigation(json)
            }
            
            if let filePath = NSBundle.mainBundle().pathForResource("push", ofType: "mp3"){
                soundURL = NSURL(fileURLWithPath: filePath)
                do {
                    let sound = try AVAudioPlayer(contentsOfURL: soundURL!)
                    pushSound = sound
                    sound.play()
                } catch {
                    // couldn't load file :(
                }
            }
        }else {
            handlePushNavigation(json)
        }
    }
    
    func handlePushNavigation(pushDict : JSON?){
        if pushDict?["orderId"].stringValue == "" { return }
        GDataSingleton.sharedInstance.pushDict = nil
        let VC = StoryboardScene.Order.instantiateOrderDetailController()
        let order = OrderDetails(orderId: pushDict?["orderId"].stringValue)
        VC.isPush = true
        VC.type = .OrderUpcoming
        VC.orderDetails = order
        
        if Localize.currentLanguage() == Languages.Arabic {
            let navigationVc = StoryboardScene.Main.instantiateRightNavigationViewController()
            navigationVc.viewControllers = [VC]
            window?.rootViewController = navigationVc
            
            return
        }
        let navigationVc = StoryboardScene.Main.instantiateLeftNavigationViewController()
        
        navigationVc.viewControllers = [VC]
        window?.rootViewController = navigationVc
    }
}
