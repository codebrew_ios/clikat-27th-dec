//
//  LocationDataSource.swift
//  Clikat
//
//  Created by cblmacmini on 8/11/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class LocationDataSource: TableViewDataSource {

    var myLocations : Array<AnyObject>?
    
    override init(){
        super.init()
    }
    
    init (items : Array<AnyObject>?,myLocations : Array<AnyObject>? , height : CGFloat , tableView : UITableView? , cellIdentifier : String?  , configureCellBlock : ListCellConfigureBlock? , aRowSelectedListener : DidSelectedRow) {
        self.myLocations = myLocations
        super.init(items: items, height: UITableViewAutomaticDimension, tableView: tableView, cellIdentifier: cellIdentifier, configureCellBlock: configureCellBlock, aRowSelectedListener: aRowSelectedListener)
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        guard let location = myLocations where location.count > 0 else { return 1 }
        return 2
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let location = myLocations where location.count > 0 else { return items?.count ?? 0 }
        
        return section == 0 ? location.count ?? 0 : items?.count ?? 0
    }

    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let location = myLocations where location.count > 0 else { return nil }
        
        return section == 0 ? L10n.MyAddresses.string : L10n.Area.string
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let location = myLocations where location.count > 0 else { return 0 }
        return 40
    }
    
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.contentView.backgroundColor = UIColor.whiteColor()
        header.textLabel?.textColor = UIColor.lightGrayColor()
        header.textLabel?.font = UIFont(name: Fonts.ProximaNova.SemiBold , size: 14)
    }
    
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let identifier = cellIdentifier else{
            fatalError("Cell identifier not provided")
        }
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(identifier , forIndexPath: indexPath) as UITableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        if let block = self.configureCellBlock {
            
            block(cell: cell , item: indexPath)
        }
        return cell
    }
}
