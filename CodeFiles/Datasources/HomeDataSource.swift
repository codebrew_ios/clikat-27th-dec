//
//  HomeDataSource.swift
//  Clikat
//
//  Created by Night Reaper on 19/04/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation


enum HomeScreenSection : Int {
    
    case Banners = 0
    case ServiceTypes
    case Recommended
    case Offers
    case None
    case Search
    
    
    static let allValues = [HomeScreenSection.Banners,HomeScreenSection.Search, HomeScreenSection.ServiceTypes, HomeScreenSection.Offers,HomeScreenSection.Recommended , HomeScreenSection.None]
    
    
    func title(witHome home : Home?) -> String? {
        switch self {
        case .Recommended:
            guard let count = home?.arrayRecommendedEN?.count where count > 0 else{return nil}
            return L10n.Recommended.string
            
        case .Offers:
            guard let count = home?.arrayOffersEN?.count where count > 0 else{return nil}
            return L10n.Offers.string
            
//        case .History:
//            guard let count = home?.arrayPurchaseHistory?.count where count > 0 else{return nil}
//            return NSLocalizedString("Based on you purchase history", comment: "")

        default : return nil
        }
    }

    func identifier() -> String {
        switch self {
        case .Banners :
            return CellIdentifiers.BannerParentCell
        case ServiceTypes :
            return CellIdentifiers.ServiceTypeParentCell
        case .Search:
            return CellIdentifiers.HomeSearchCell
        default:
            break
        }
        return CellIdentifiers.HomeProductParentCell

    }
    
    func rowHeight(witHome home : Home?) -> CGFloat {
        
        let horizontalEmptySpace : CGFloat = 48.0 // Section Inset + (numberOfColumns - 1) * 8.0
        let veritcalEmptySpace : CGFloat = 64.0 // Section Inset + (numberOfRows - 1) * 8.0
        let numberOfColumnsCollectionView = 3
        var numberOfRowsCollectionView : Int{
            get{
                let totalItems = (home?.arrayServiceTypesEN?.count ?? 0) / 3
                let remainder = (home?.arrayServiceTypesEN?.count ?? 0) % 3
                return remainder > 0 ? totalItems + 1 : totalItems
            }
        }
        
        switch self {
        case .ServiceTypes:
            return (((ScreenSize.SCREEN_WIDTH - horizontalEmptySpace) / CGFloat(numberOfColumnsCollectionView)) * CGFloat(numberOfRowsCollectionView)) + veritcalEmptySpace
        case .Offers,.Recommended:
            return ScreenSize.SCREEN_WIDTH * 0.5
        case .Banners:
            return home?.arrayBanners?.count > 0 ? ScreenSize.SCREEN_WIDTH * 0.6 : 64
        case .Search:
            return 64
        default:
            return ScreenSize.SCREEN_WIDTH * 0.6
        }
    }
    
    func numberOfRowsInSection(witHome home : Home?) -> Int {
        switch self {
        case HomeScreenSection.Offers :
            guard let count = home?.arrayOffersEN?.count where count > 0 else{return 0}
            return 1
        case HomeScreenSection.Recommended:
            guard let count = home?.arrayRecommendedEN?.count where count > 0 else{return 0}
            return 1
        case HomeScreenSection.ServiceTypes :
            guard let count = home?.arrayServiceTypesEN?.count where count > 0 else{return 0}
            return 1
        case HomeScreenSection.Banners  :
            return 1
        case .Search:
            return 1
        default :
            break
        }
        return 0
    }
}


class HomeDataSource : NSObject {
    
    typealias  HomeListCellConfigureBlock = (cell : AnyObject , item : AnyObject , type : HomeScreenSection) -> ()
    typealias  HomeDidSelectedRow = (indexPath : NSIndexPath , type : HomeScreenSection) -> ()

    var home : Home?
    var tableView  : UITableView?
    
    var configureCellBlock : HomeListCellConfigureBlock?
    var aRowSelectedListener : HomeDidSelectedRow?
    
    var sectionHeader : HomeSectionHeader?
    
     init (home : Home? , tableView : UITableView? , configureCellBlock : HomeListCellConfigureBlock , aRowSelectedListener : HomeDidSelectedRow) {
        
        self.tableView = tableView
        self.home = home
        self.configureCellBlock = configureCellBlock
        self.aRowSelectedListener = aRowSelectedListener
    }

    override init(){
        super.init()
    }
    
}




extension HomeDataSource : UITableViewDelegate , UITableViewDataSource{
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let sectionCase = HomeScreenSection.allValues[indexPath.section]
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier( sectionCase.identifier(), forIndexPath: indexPath) as UITableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.backgroundColor = UIColor.clearColor()
        
        if let block = configureCellBlock , item: AnyObject = home {
            block(cell: cell, item: item, type: sectionCase)
        }
        return cell
    }
    

    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionCase = HomeScreenSection.allValues[section]
        
        /**
            Insert Array count check here
         */
        let sectionHeader = HomeSectionHeader(frame: CGRect(x: 0, y: 0, w: ScreenSize.SCREEN_WIDTH, h: 48))
        
        guard let title = sectionCase.title(witHome : home) else { return UIView(frame:CGRectZero) }
        sectionHeader.labelTitle.text = title
        sectionHeader.btnViewAll.hidden = title == L10n.Offers.string ? false : true
        if title == L10n.Offers.string { self.sectionHeader = sectionHeader }
        return sectionHeader
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let block = aRowSelectedListener{
            let sectionCase = HomeScreenSection.allValues[indexPath.section]
            block(indexPath: indexPath,type: sectionCase)
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionCase = HomeScreenSection.allValues[section]
        return sectionCase.numberOfRowsInSection(witHome : home)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 5
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let sectionCase = HomeScreenSection.allValues[section]
        guard let _ = sectionCase.title(witHome : home) else { return 0.0 }
        return 48
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        let sectionCase = HomeScreenSection.allValues[indexPath.section]
        return sectionCase.rowHeight(witHome : home)
    }
    
}
