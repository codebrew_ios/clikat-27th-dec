//
//  BarButtonDataSource.swift
//  Clikat
//
//  Created by cblmacmini on 4/24/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class BarButtonDataSource: NSObject {
    
    var currentIndex = 0
    var currentSection = 0
    private var shouldUpdateButtonBarView = true
    var changeCurrentIndexProgressive: ((oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void)?
    var changeCurrentIndex: ((oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, animated: Bool) -> Void)?
    
    var buttonBarView : ButtonBarView?
    var tableView : UITableView?
    
    var items : Array<AnyObject>?
    
    lazy private var cachedCellWidths: [CGFloat]? = { [unowned self] in
        return self.calculateWidths()
        }()
    
    var selected : [Bool] = []
    
    init(items : Array<AnyObject> , barButtonView : ButtonBarView,tableView : UITableView?) {
        
        self.buttonBarView = barButtonView
        self.items = items
        self.tableView = tableView
        self.selected = Array(count: items.count, repeatedValue: false)
        if selected.count > 0 {
            self.selected[0] = true
        }
    }
    
    
    override init() {
        
        super.init()
    }

    private func calculateWidths() -> [CGFloat] {
        guard let arr = items else { return [] }
        var minimumCellWidths = [CGFloat]()
        let label = UILabel(frame: CGRectZero)
        
        for item in arr {
            label.text = (item as? DetailedSubCategories)?.strTitle ?? ""
            minimumCellWidths.append(label.intrinsicContentSize().width + LowPadding * 2)
        }
        return minimumCellWidths
    }
}

extension BarButtonDataSource : UICollectionViewDataSource,UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
       
        guard indexPath.item != currentIndex else { return }
        
        buttonBarView?.moveToIndex(indexPath.item, animated: true, swipeDirection: .None, pagerScroll: .Yes)
        shouldUpdateButtonBarView = false
        
        let oldCell = buttonBarView?.cellForItemAtIndexPath(NSIndexPath(forItem: currentIndex, inSection: 0)) as? ButtonBarViewCell
        let newCell = buttonBarView?.cellForItemAtIndexPath(NSIndexPath(forItem: indexPath.item, inSection: 0)) as? ButtonBarViewCell
        
        if let changeCurrentIndexProgressive = changeCurrentIndexProgressive {
            changeCurrentIndexProgressive(oldCell: oldCell, newCell: newCell, progressPercentage: 1, changeCurrentIndex: true, animated: true)
        }
            
        else {
            if let changeCurrentIndex = changeCurrentIndex {
                changeCurrentIndex(oldCell: oldCell, newCell: newCell, animated: true)
            }
        }
        
//        UtilityFunctions.delay(0.2) {
            weak var weakSelf : BarButtonDataSource? = self
            weakSelf?.tableView?.scrollToRowAtIndexPath(NSIndexPath(forRow: 0,inSection: indexPath.item), atScrollPosition: .Top, animated: true)
//        }
        
        
        self.selected = Array(count: items?.count ?? 0, repeatedValue: false)
        selected[indexPath.row] = true
        buttonBarView?.reloadData()
        currentIndex = indexPath.item
    }
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as? ButtonBarViewCell else {
            fatalError("UICollectionViewCell should be or extend from ButtonBarViewCell")
        }
        
        cell.label.textColor = selected[indexPath.row] ? Colors.MainColor.color() : UIColor.blackColor().colorWithAlphaComponent(0.25)
       
        cell.label.font = selected[indexPath.row] ?  UIFont(name: Fonts.ProximaNova.SemiBold, size: 16) : UIFont(name: Fonts.ProximaNova.Regular, size: 14)
        cell.label.text = (items?[indexPath.item] as? DetailedSubCategories)?.strTitle

        if let changeCurrentIndexProgressive = changeCurrentIndexProgressive {
            changeCurrentIndexProgressive(oldCell: currentIndex == indexPath.item ? nil : cell, newCell: currentIndex == indexPath.item ? cell : nil, progressPercentage: 1, changeCurrentIndex: true, animated: false)
        }
            
        else {
            if let changeCurrentIndex = changeCurrentIndex {
                changeCurrentIndex(oldCell: currentIndex == indexPath.item ? nil : cell, newCell: currentIndex == indexPath.item ? cell : nil, animated: false)
            }
        }
        return cell
    } 
}


extension BarButtonDataSource {
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        guard let cellWidthValue = cachedCellWidths?[indexPath.row] else {
            fatalError("cachedCellWidths for \(indexPath.row) must not be nil")
        }
        return CGSizeMake(cellWidthValue, collectionView.frame.size.height)
    }
}
