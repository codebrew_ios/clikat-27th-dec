//
//  OrderDetailDataSource.swift
//  Clikat
//
//  Created by cblmacmini on 5/2/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class OrderDetailDataSource: TableViewDataSource {

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(getCellIdentifier(indexPath) ?? "", forIndexPath: indexPath) as UITableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        if let block = self.configureCellBlock , item: AnyObject = self.items{
            block(cell: cell , item: item)
        }
        return cell
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {        
        return tableViewRowHeight
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return tableViewRowHeight
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? nil : L10n.OrderDetails.string
    }
    
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        // Text Color
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.contentView.backgroundColor = Colors.lightGrayBackgroundColor.color()
        header.textLabel?.textColor = UIColor.lightGrayColor()
        header.textLabel?.font = UIFont(name: Fonts.ProximaNova.Regular , size: Size.Medium.rawValue)
    }
    
    func getCellIdentifier(indexPath : NSIndexPath) -> String?{
        return indexPath.section == 0 ? CellIdentifiers.OrderStatusCell : CellIdentifiers.OrderDetailCell
    }
}
