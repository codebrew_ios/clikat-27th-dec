//
//  ItemListingDataSource.swift
//  Clikat
//
//  Created by cblmacmini on 4/24/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class ItemListingDataSource: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    typealias ChangeSectionBlock = (section : Int) -> ()
    var items : Array<AnyObject>?
    var tableView : UITableView?
    var buttonBarView : ButtonBarView?
    var aRowSelectedListener : DidSelectedRow?
    var currentIndex = 0
    var currentSection = 0
    var changeSectionListener : ChangeSectionBlock?
    var configureCellBlock : ListCellConfigureBlock?
    
    init(items : Array<AnyObject>?, tableView : UITableView,buttonBarView : ButtonBarView? , aRowSelectedListener : DidSelectedRow?) {
        super.init()
        self.items = items
        self.tableView = tableView
        self.buttonBarView = buttonBarView
        self.aRowSelectedListener = aRowSelectedListener
        tableView.registerNib(UINib(nibName: CellIdentifiers.ProductListingCell,bundle: nil), forCellReuseIdentifier: CellIdentifiers.ProductListingCell)
        
    }
    override init() {
        super.init()
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return items?.count ?? 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (items?[section] as? DetailedSubCategories)?.arrProducts?.count ?? 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifiers.ProductListingCell) as? ProductListingCell else { fatalError() }
        cell.product = (items?[indexPath.section] as? DetailedSubCategories)?.arrProducts?[indexPath.row]
        cell.selectionStyle = .None
        cell.productClicked = { [weak self] product in
            guard let block = self?.aRowSelectedListener else{
                return
            }
            block(indexPath: indexPath)
        }
        
        if let block = configureCellBlock  {
            block(cell: cell, item: indexPath)
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return (items?[section] as? DetailedSubCategories)?.strTitle
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        guard let cell = tableView?.visibleCells.first else { return }
        
        if let section = tableView?.indexPathForCell(cell)?.section , isDraging = tableView?.dragging where isDraging{
            if section != currentSection {
                buttonBarView?.moveToIndex(section, animated: true, swipeDirection: SwipeDirection.Left, pagerScroll: PagerScroll.ScrollOnlyIfOutOfScreen)
                currentSection = section
                
                guard let block = changeSectionListener else { return }
                block(section: section)
            }
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 136
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
}
