//
//  SettingsDataSource.swift
//  Clikat
//
//  Created by cblmacmini on 6/1/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

enum SettingsSection : Int {
    
    case ManageAddress = 0
    case Notifications
    case Other
    
    static let allValues = [SettingsSection.ManageAddress,SettingsSection.Notifications,SettingsSection.Other]
    
    static func numberofSections() -> Int{
        return SettingsSection.allValues.count
    }
    
    func numberOfRowsInSection() -> Int{
        switch self {
        case .ManageAddress:
            return 1
        case .Notifications:
            return 2
        case .Other:
            return GDataSingleton.sharedInstance.loggedInUser?.fbId == nil ? 2 : 1
        }
    }
    func titleForHEaderInSection() -> String{
        switch self {
        case .ManageAddress:
            return L10n.ManageAddress.string
        case .Notifications:
            return L10n.Notifications.string
        case .Other:
            return L10n.Other.string
        }
    }
    
    func identifier() -> String {
        switch self {
        case .ManageAddress:
            return CellIdentifiers.DeliveryAddressCell
        default:
            return CellIdentifiers.SettingsCell
        }
    }
    func heightForRow() -> CGFloat{
        switch self {
        case .ManageAddress:
            return UITableViewAutomaticDimension
        default:
            return 48
        }
    }
}

typealias SettingsCellConfigureBlock = (cell : AnyObject,section : SettingsSection,indexPath : NSIndexPath) -> ()
class SettingsDataSource: TableViewDataSource {

    var configureSettingsCellBlock : SettingsCellConfigureBlock?
    var scrollViewListener : ScrollViewScrolled?
    
    init(tableView : UITableView,configurecell : SettingsCellConfigureBlock,rowSelectedBlock : DidSelectedRow,scrollListener : ScrollViewScrolled) {
        super.init(items: nil, height: UITableViewAutomaticDimension, tableView: tableView, cellIdentifier: nil, configureCellBlock: nil, aRowSelectedListener: rowSelectedBlock)
        self.configureSettingsCellBlock = configurecell
        self.scrollViewListener = scrollListener
    }
    
    override init() {
         super.init()
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return SettingsSection.numberofSections()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let row = SettingsSection.allValues[section]
        return row.numberOfRowsInSection()
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let row = SettingsSection.allValues[indexPath.section]
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(row.identifier() , forIndexPath: indexPath) as UITableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        if let block = self.configureSettingsCellBlock {
            block(cell: cell, section: row, indexPath: indexPath)
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let row = SettingsSection.allValues[indexPath.section]
        return row.heightForRow()
    }

    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.contentView.backgroundColor = UIColor.whiteColor()
        header.textLabel?.textColor = UIColor.lightGrayColor()
        header.textLabel?.font = UIFont(name: Fonts.ProximaNova.SemiBold , size: 12)
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let row = SettingsSection.allValues[section]
        return row.titleForHEaderInSection()
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        guard let block = scrollViewListener else { return }
        block(scrollView)
    }
}
