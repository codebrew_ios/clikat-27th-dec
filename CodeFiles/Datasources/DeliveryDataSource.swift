//
//  DeliveryDataSource.swift
//  Clikat
//
//  Created by cblmacmini on 5/19/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

enum DeliverySpeedType : Int {
    case Standard = 0
    case Urgent
    case Postpone
    static let allValues = [DeliverySpeedType.Standard,DeliverySpeedType.Urgent,DeliverySpeedType.Postpone]
    
}

enum DeliveryRow : Int {
    
    case DeliveryAddressCell = 0
    case DeliverySpeedCell
    case TimeAndDateCell
    
    
    static let allValues = [DeliveryRow.DeliveryAddressCell, DeliveryRow.DeliverySpeedCell,DeliveryRow.TimeAndDateCell]
    
    
    
    func identifier(selectedDeliverySpeed : DeliverySpeedType,indexPath : NSIndexPath,delivery : Delivery?) -> String {
        switch self {
        case .DeliveryAddressCell :
            return CellIdentifiers.DeliveryAddressCell
        case .DeliverySpeedCell :
            if (selectedDeliverySpeed == .Standard){ return CellIdentifiers.DeliverySpeedCell }else {
                return indexPath.row == (delivery?.deliverySpeeds?.count ?? 0) ? CellIdentifiers.TimeAndDateCell : CellIdentifiers.DeliverySpeedCell
            }
        default: return ""
        }
        
    }
    
    func rowHeight() -> CGFloat {
        switch self {
        case .DeliverySpeedCell :
            return 72
        default:
            break
        }
        return UITableViewAutomaticDimension
    }
    
    static func numberOfSection() -> Int {
        return 2
    }
    
    func numberOfRowsInSection(selectedDeliverySpeed : DeliverySpeedType,delivery : Delivery?) -> Int{
        
        
        switch self {
        case .DeliveryAddressCell:
            return 1
        case .DeliverySpeedCell :
            return (selectedDeliverySpeed == .Standard || selectedDeliverySpeed == .Urgent) ? delivery?.deliverySpeeds?.count ?? 0 : (delivery?.deliverySpeeds?.count ?? 0) + 1
        default:
            break
        }
        return 0
    }
    
    func titleForHeaderInSection() -> String {
        switch self {
        case .DeliveryAddressCell:
            return L10n.DeliveryAddress.string
        case .DeliverySpeedCell :
            return L10n.DeliverySpeed.string
        default:
            break
        }
        return ""
    }
}

typealias DeliveryCellConfigureBlock = (cell : AnyObject , NSIndexPath : NSIndexPath?,selectedDeliverySpeed : DeliverySpeedType?) -> ()

class DeliveryDataSource: TableViewDataSource {
    
    var configureDeliveryCellBlock : DeliveryCellConfigureBlock?
    
    var deliveryDetails : Delivery?
    var selectedDeliverySpeed : DeliverySpeedType = .Standard
    
    var loyaltyPointsSummary : LoyaltyPointsSummary?
    
    init(items: Delivery?, height: CGFloat, tableView: UITableView?, cellIdentifier: String?, configureCellBlock: DeliveryCellConfigureBlock?, aRowSelectedListener: DidSelectedRow) {
        super.init(items: items?.deliverySpeeds, height: height, tableView: tableView, cellIdentifier: cellIdentifier, configureCellBlock: nil, aRowSelectedListener: aRowSelectedListener)
        self.deliveryDetails = items
        self.configureDeliveryCellBlock = configureCellBlock
    }
    
    override init() {
        super.init()
    }
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return loyaltyPointsSummary == nil ? DeliveryRow.numberOfSection() : 1
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let rows = DeliveryRow.allValues[section]
        return rows.numberOfRowsInSection(selectedDeliverySpeed,delivery: deliveryDetails)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let row = DeliveryRow.allValues[indexPath.section]
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(row.identifier(selectedDeliverySpeed,indexPath: indexPath,delivery: deliveryDetails) , forIndexPath: indexPath) as UITableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        if let block = self.configureDeliveryCellBlock{
            block(cell: cell, NSIndexPath: indexPath, selectedDeliverySpeed: selectedDeliverySpeed)
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let rows = DeliveryRow.allValues[indexPath.section]
        return rows.rowHeight()
    }
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let row = DeliveryRow.allValues[section]
        return row.titleForHeaderInSection()
    }
    
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.contentView.backgroundColor = UIColor.whiteColor()
        header.textLabel?.textColor = UIColor.lightGrayColor()
        header.textLabel?.font = UIFont(name: Fonts.ProximaNova.SemiBold , size: 12)
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
}
