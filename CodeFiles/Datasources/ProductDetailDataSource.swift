//
//  ProductDetailDataSource.swift
//  Clikat
//
//  Created by cbl73 on 5/6/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import Foundation

enum ProductDetailRow : Int {
    
    case FirstInfoCell = 0
    case NutritionalContentCell
    
    
    static let allValues = [ProductDetailRow.FirstInfoCell, ProductDetailRow.NutritionalContentCell]
    
    
    
    func identifier() -> String {
        switch self {
        case .FirstInfoCell :
            return CellIdentifiers.ProductDetailFirstCell
        case NutritionalContentCell :
            return CellIdentifiers.SupplierDescriptionCell
        }
        
    }
    
    func rowHeight(witHome home : Home?) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func numberOfRowsInSection() -> Int {
        return 2
    }
}

typealias ScrollViewScrolled = (UIScrollView) -> ()

class ProductDetailDataSource : TableViewDataSource {
    
    typealias ConfigureProductCellBlock = (cell : AnyObject,indexPath : NSIndexPath) -> ()
    var scrollViewListener : ScrollViewScrolled?
    var configureSupplierCellBlock: ConfigureProductCellBlock?
    
    
    var product : Product?
    
    init(product: Product?, height: CGFloat, tableView: UITableView?, cellIdentifier: String?, configureCellBlock: ConfigureProductCellBlock, aRowSelectedListener: DidSelectedRow,scrollViewListener : ScrollViewScrolled) {
        super.init(items: [], height: height, tableView: tableView, cellIdentifier: cellIdentifier, configureCellBlock : nil, aRowSelectedListener: aRowSelectedListener)
        self.product = product
        self.configureSupplierCellBlock = configureCellBlock
        self.scrollViewListener = scrollViewListener
    }
    
    override init() {
        super.init()
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        if let block = scrollViewListener {
            block(scrollView)
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ProductDetailRow.allValues.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let row =  ProductDetailRow.allValues[indexPath.row]
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(row.identifier() , forIndexPath: indexPath) as UITableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        if let block = self.configureSupplierCellBlock{
            block(cell: cell ,indexPath: indexPath)
        }
        return cell
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 200
    }

    
}