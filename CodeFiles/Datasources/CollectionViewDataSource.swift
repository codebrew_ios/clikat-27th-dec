//
//  CollectionViewDataSource.swift
//  Whatashadi
//
//  Created by Night Reaper on 29/10/15.
//  Copyright © 2015 Gagan. All rights reserved.
//


import UIKit

typealias WillBeginDragginBlock = (scrollView : UIScrollView) -> ()
typealias DidEndDraggingBlock = (scrollView : UIScrollView) -> ()
typealias WillDisplayCellBlock = (cell : UICollectionViewCell) -> ()
typealias DidEndDeceleratingBlock = (scrollView : UIScrollView) -> ()

class CollectionViewDataSource: NSObject  {
    
    var items : Array<AnyObject>?
    var cellIdentifier : String?
    var headerIdentifier : String?
    var tableView  : UICollectionView?
    var cellHeight : CGFloat = 0.0
    var cellWidth : CGFloat = 0.0
    var isServiceTypeCollectionView : Bool = false
    
    
    var configureCellBlock : ListCellConfigureBlock?
    var aRowSelectedListener : DidSelectedRow?
    
    var willBeginDraggingListener : WillBeginDragginBlock?
    var didEndDraggingListener : DidEndDraggingBlock?
    var willDisplayCellListener : WillDisplayCellBlock?
    var didEndDeceleratingBlock : DidEndDeceleratingBlock?

    init (items : Array<AnyObject>?  , tableView : UICollectionView? , cellIdentifier : String? , headerIdentifier : String? , cellHeight : CGFloat , cellWidth : CGFloat  , configureCellBlock : ListCellConfigureBlock  , aRowSelectedListener : DidSelectedRow)  {
        
        self.tableView = tableView
        self.items = items
        self.cellIdentifier = cellIdentifier
        self.headerIdentifier = headerIdentifier
        self.cellWidth = cellWidth
        self.cellHeight = cellHeight
        self.configureCellBlock = configureCellBlock
        self.aRowSelectedListener = aRowSelectedListener
        
    }
    
    override init() {
        super.init()
    }
    
}

extension CollectionViewDataSource : UICollectionViewDelegate , UICollectionViewDataSource{
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        guard let identifier = cellIdentifier else{
            fatalError("Cell identifier not provided")
        }
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(identifier ,
                                                                         forIndexPath: indexPath) as UICollectionViewCell
        if let block = self.configureCellBlock , item: AnyObject = self.items?[indexPath.row]{
            block(cell: cell , item: item)
        }
        return cell
        
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items?.count ?? 0
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if let block = self.aRowSelectedListener{
            block(indexPath: indexPath)
        }
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        guard let block = willDisplayCellListener else { return }
        block(cell: cell)
    }

}


extension CollectionViewDataSource : UICollectionViewDelegateFlowLayout{
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if isServiceTypeCollectionView && indexPath.row == (items?.count ?? 0) - 1  {
            let layout = collectionViewLayout as! UICollectionViewFlowLayout
            switch (items?.count ?? 0) % 3 {
            case 1 :
                return CGSizeMake(CGRectGetWidth(collectionView.bounds) - (layout.sectionInset.left + layout.sectionInset.right ) , cellHeight)
            case 2 :
                return CGSizeMake((cellWidth * 2) + LowPadding , cellHeight)
            default:
                return CGSizeMake(cellWidth, cellHeight)

            }
        }
        return CGSizeMake(cellWidth, cellHeight)
    }
}

extension CollectionViewDataSource : UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        guard let block = willBeginDraggingListener else { return }
        block(scrollView: scrollView)
    }
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        guard let block = didEndDraggingListener else { return }
        block(scrollView: scrollView)
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        guard let block = didEndDeceleratingBlock else { return }
        block(scrollView: scrollView)
    }
}

/*
extension CollectionViewDataSource : UIScrollViewDelegate{
    
    func scrollViewWillEndDragging(scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        guard let layout = self.tableView?.collectionViewLayout as? UICollectionViewFlowLayout where scrollView.contentOffset.y < 0 else{return}
        let cellWithIncludingSpace = tableViewRowWidth + layout.minimumLineSpacing
        var offset = targetContentOffset.memory
        let index = round((scrollView.contentInset.left + offset.x)/cellWithIncludingSpace)
        offset = CGPointMake(index * cellWithIncludingSpace  - scrollView.contentInset.left - (2 * layout.minimumLineSpacing) , -scrollView.contentInset.top)
        targetContentOffset.memory = offset        
    }
    
}
 */
