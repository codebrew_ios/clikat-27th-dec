//
//  CartDataSource.swift
//  Clikat
//
//  Created by cblmacmini on 5/11/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class CartDataSource: TableViewDataSource {

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (items?.count ?? 0) + 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let identifier = getCellIdentifier(indexPath) else{
            fatalError("Cell identifier not provided")
        }
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(identifier , forIndexPath: indexPath) as UITableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        if let block = self.configureCellBlock , item: AnyObject = indexPath{
            block(cell: cell , item: item)
        }
        return cell
    }
    
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return indexPath.row == items?.count ? UITableViewAutomaticDimension : 135
    }
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 200
    }
    
    func getCellIdentifier(indexpath : NSIndexPath) -> String?{
        return indexpath.row == items?.count ? CellIdentifiers.CartBillCell : CellIdentifiers.ProductListingCell
    }
}
