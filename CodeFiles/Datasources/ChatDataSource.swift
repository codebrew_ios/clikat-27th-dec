//
//  ChatDataSource.swift
//  Clikat
//
//  Created by cblmacmini on 6/9/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class ChatDataSource: TableViewDataSource {

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(getIdentifier(items?[indexPath.row]) ?? "" , forIndexPath: indexPath) as UITableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        if let block = self.configureCellBlock , item: AnyObject = self.items?[indexPath.row]{
            block(cell: cell , item: item)
        }
        return cell
    }
    
    func getIdentifier(item : AnyObject?) -> String?{
        guard let message = item as? Message,myMessage = message.myMessage else { return "" }
        
        return myMessage ? CellIdentifiers.LiveSupportMyCell : CellIdentifiers.LiveSupportOtherCell
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
