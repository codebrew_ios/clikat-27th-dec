//
//  PickupDetailsDataSource.swift
//  Clikat
//
//  Created by cblmacmini on 5/21/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

typealias PickupDetailCellConfigureBlock = (cell : AnyObject , indexPath : NSIndexPath?) -> ()

class PickupDetailsDataSource: TableViewDataSource {
    
    var pickupDetails : PickupDetails?
    
    var configurepickupCellBlock : PickupDetailCellConfigureBlock?
    
    init(pickUpDetails: PickupDetails?, height: CGFloat, tableView: UITableView?, cellIdentifier: String?, configureCellBlock: PickupDetailCellConfigureBlock?, aRowSelectedListener: DidSelectedRow) {
        super.init(items: nil, height: height, tableView: tableView, cellIdentifier: cellIdentifier, configureCellBlock: nil, aRowSelectedListener: aRowSelectedListener)
        self.pickupDetails = pickUpDetails
        self.configurepickupCellBlock = configureCellBlock
    }
    
    override init() {
        super.init()
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(indexPath.section == 0 ? CellIdentifiers.PickupDateCell : CellIdentifiers.DeliveryAddressCell , forIndexPath: indexPath) as UITableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        if let block = self.configurepickupCellBlock {
            block(cell: cell ,indexPath: indexPath)
        }
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        print(view.frame.height)
        header.contentView.backgroundColor = UIColor.whiteColor()
        header.textLabel?.textColor = UIColor.lightGrayColor()
        header.textLabel?.font = UIFont(name: Fonts.ProximaNova.SemiBold , size: 12)
        header.textLabel?.text = section == 0 ? L10n.WhenDoYouWantTheService.string : L10n.PickupLocation.string
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return indexPath.section == 0 ? 72 : ScreenSize.SCREEN_WIDTH * 0.5
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    func tableView(tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
}
