//
//  SearchDataSource.swift
//  Clikat
//
//  Created by cblmacmini on 4/29/16.
//  Copyright © 2016 Gagan. All rights reserved.
//

import UIKit

class SearchDataSource: TableViewDataSource {

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return items?.count ?? 0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let identifier = cellIdentifier else{
            fatalError("Cell identifier not provided")
        }
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(identifier , forIndexPath: indexPath) as UITableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        if let block = self.configureCellBlock , item: AnyObject = self.items?[indexPath.section]{
            block(cell: cell , item: item)
        }
        return cell
        
    }
    
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        // Text Color
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.textLabel?.textColor = Colors.MainColor.color()
        header.textLabel?.text = [L10n.ResultsFor.string , "\"" , (items?[section] as? Search)?.title ,"\""].flatMap{ $0 }.joinWithSeparator("")
        header.textLabel?.font = UIFont(name: Fonts.ProximaNova.SemiBold , size: Size.Medium.rawValue)
        header.contentView.backgroundColor = Colors.lightGrayBackgroundColor.color()
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
}