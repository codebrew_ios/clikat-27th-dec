#ifdef __OBJC__
#import <UIKit/UIKit.h>
#endif

#import "NSObject+RMArchivable.h"
#import "NSObject+RMCopyable.h"
#import "NSUserDefaults+RMSaveCustomObject.h"
#import "RMMapper.h"

FOUNDATION_EXPORT double RMMapperVersionNumber;
FOUNDATION_EXPORT const unsigned char RMMapperVersionString[];

